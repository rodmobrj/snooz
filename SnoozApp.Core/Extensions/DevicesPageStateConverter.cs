﻿using System;
using System.Globalization;
using SnoozApp.Enums;
using Xamarin.Forms;

namespace SnoozApp.Extensions
{
    public class DevicesPageStateConverter : IValueConverter
    {
        public object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var state = (ConnectionState)value;
            switch (state)
            {
                case ConnectionState.Scanning:
                    return "Searching for SNOOZ"; 
                case ConnectionState.Connecting:
                    return "Connecting to SNOOZ";
                case ConnectionState.Disconnected:
                    return "";
            }
            return "";
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException("DeviceStateConverter is one-way");
        }
    }
}    