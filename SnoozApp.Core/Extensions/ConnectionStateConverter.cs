﻿using System;
using System.Globalization;
using SnoozApp.Enums;
using Xamarin.Forms;

namespace SnoozApp.Extensions
{
    public class ConnectionStateConverter : IValueConverter
    {
        public object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var state = (ConnectionState)value;
            switch (state)
            {
                case ConnectionState.Scanning:
                    return "Searching...";
                case ConnectionState.Connecting:
                    return "Connecting...";
                case ConnectionState.Connected:
                    return "Connected";
                case ConnectionState.Disconnected:
                    return "No SNOOZ found";
            }
            return "No SNOOZ found";
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException("DeviceStateConverter is one-way");
        }
    }
}