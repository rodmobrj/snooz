﻿using System;
using System.Globalization;
using SnoozApp.Enums;
using Xamarin.Forms;

namespace SnoozApp.Extensions
{
	public class FirmwareVersionToV2TimerVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var firmwareVersion = (FirmwareVersions)value;
			return firmwareVersion >= FirmwareVersions.V2;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException("FirmwareVersionToV2TimerVisibilityConverter is one-way");
		}
	}
}