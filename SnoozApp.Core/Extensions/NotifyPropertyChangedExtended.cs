﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms.Internals;

namespace SnoozApp.Extensions
{
    public abstract class NotifyPropertyChangedExtended : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T reference, T value, string[] notifyOtherPropertyNames = null, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(reference, value))
            {
                return false;
            }
            reference = value;
            NotifyPropertyChanged(propertyName);
            if (notifyOtherPropertyNames != null && notifyOtherPropertyNames.Length > 0)
            {
                notifyOtherPropertyNames.ForEach(NotifyPropertyChanged);
            }
            return true;
        }
    }
}
