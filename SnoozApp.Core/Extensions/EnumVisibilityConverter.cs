﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace SnoozApp.Extensions
{
    public class EnumVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var intValue = (int)value;
            var intParameter = (int)parameter;

            return intValue == intParameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("EnumVisibilityConverter is one-way");
        }
    }
}