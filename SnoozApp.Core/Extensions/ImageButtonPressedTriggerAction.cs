﻿using System;
using Xamarin.Forms;

namespace SnoozApp.Extensions
{
    public class ImageButtonPressedTriggerAction : TriggerAction<Button>
    {
        //public FileImageSource PressedImage;
        protected override void Invoke (Button button)
        {
            //button.Image = this.PressedImage;
            button.TextColor = Color.Blue;
        }
    }
}