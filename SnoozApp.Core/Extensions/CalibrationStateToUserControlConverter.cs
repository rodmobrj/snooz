﻿using System;
using System.Globalization;
using System.Linq;
using SnoozApp.Enums;
using SnoozApp.Modules;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Diagnostics;

namespace SnoozApp.Extensions
{
	public class CalibrationStateToUserControlConverter : IValueConverter
	{
		public CalibrationStateToUserControlConverter()
		{
		}
		private readonly static List<ContentView> calibrationStateUserControls = new List<ContentView>
		{
			new CalibrationStart(),
			new CalibrationStep1(),
			new CalibrationStep2(),
			new CalibrationCalibrating(),
			new CalibrationFinished(),
			new CalibrationDisabled()
		};

		public object Convert(
			object value,
			Type targetType,
			object parameter,
			CultureInfo culture)
		{
			var state = (CalibrationWorkflowState)value;
			switch (state)
			{
				case CalibrationWorkflowState.Setup:
					return new ContentView(); //Return an empty ContentView. The setup screen is implemented in the CalibrationPage
				case CalibrationWorkflowState.Start:
					return calibrationStateUserControls.OfType<CalibrationStart>().FirstOrDefault();
				case CalibrationWorkflowState.Step1:
					return calibrationStateUserControls.OfType<CalibrationStep1>().FirstOrDefault();
				case CalibrationWorkflowState.Step2:
					return calibrationStateUserControls.OfType<CalibrationStep2>().FirstOrDefault();
				case CalibrationWorkflowState.Calibrating:
					return calibrationStateUserControls.OfType<CalibrationCalibrating>().FirstOrDefault();
				case CalibrationWorkflowState.Finished:
					return calibrationStateUserControls.OfType<CalibrationFinished>().FirstOrDefault();
				case CalibrationWorkflowState.Disabled:
					return calibrationStateUserControls.OfType<CalibrationDisabled>().FirstOrDefault();
			}
			return "Incorrect Enum";
		}

		public object ConvertBack(
			object value,
			Type targetType,
			object parameter,
			CultureInfo culture)
		{
			throw new NotImplementedException("DeviceStateConverter is one-way");
		}
	}
}