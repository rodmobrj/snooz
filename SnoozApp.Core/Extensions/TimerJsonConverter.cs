﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SnoozApp.Modules;
using System;

namespace SnoozApp.Extensions
{
    public class TimerJsonConverter : JsonConverter
    {
        const int DAYS_A_WEEK = 7;
        readonly Action<TimerModel> _verifyTimerDeltaAction;

        public override bool CanRead => true;
        public override bool CanWrite => false;

        public TimerJsonConverter(Action<TimerModel> verifyTimerDeltaAction = null)
            => _verifyTimerDeltaAction = verifyTimerDeltaAction;

        public override bool CanConvert(Type objectType)
            => objectType == typeof(TimerModel);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var loadedTimer = JToken.Load(reader).ToObject<TimerModel>();
            var newTimer = new TimerModel(loadedTimer, updateWeekDays: false);

            if (_verifyTimerDeltaAction != null)
                newTimer.SetVerifyFadeDelta(_verifyTimerDeltaAction);

            for (var index = 0; index < DAYS_A_WEEK; index++)
            {
                newTimer.WeekDays[index].IsOn = loadedTimer.WeekDays[DAYS_A_WEEK].IsOn;
                loadedTimer.WeekDays.RemoveAt(DAYS_A_WEEK);
            }

            return newTimer;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => throw new NotImplementedException();
    }
}
