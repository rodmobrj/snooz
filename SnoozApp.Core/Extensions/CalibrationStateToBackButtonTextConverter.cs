﻿using System;
using System.Globalization;
using System.Linq;
using SnoozApp.Enums;
using SnoozApp.Modules;
using Xamarin.Forms;
using System.Collections.Generic;

namespace SnoozApp.Extensions
{
	public class CalibrationStateToBackButtonTextConverter : IValueConverter
	{
		public object Convert(
			object value,
			Type targetType,
			object parameter,
			CultureInfo culture)
		{
			var state = (CalibrationWorkflowState)value;
			switch (state)
			{
				case CalibrationWorkflowState.Disabled:
				case CalibrationWorkflowState.Setup:
				case CalibrationWorkflowState.Start:
				case CalibrationWorkflowState.Step1:
				case CalibrationWorkflowState.Step2:
					return "Back";
					
				case CalibrationWorkflowState.Calibrating:
				case CalibrationWorkflowState.Finished:
					return "";
			}
			return "";
		}

		public object ConvertBack(
			object value,
			Type targetType,
			object parameter,
			CultureInfo culture)
		{
			throw new NotImplementedException("DeviceStateConverter is one-way");
		}
	}
}