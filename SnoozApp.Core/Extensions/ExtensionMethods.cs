﻿using System;

namespace SnoozApp
{
    public static class ExtensionMethods
    {
        public static uint SecondsSinceSunday(this DateTime dateTime)
		{
            return Convert.ToUInt32((dateTime - DateTime.Today.AddDays(-(double)((int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday))).TotalSeconds);
		}

		public static ushort MinutesSinceSunday(this DateTime dateTime)
		{
            return Convert.ToUInt16((dateTime - DateTime.Today.AddDays(-(double)((int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday))).TotalMinutes);
		}
    }
}
