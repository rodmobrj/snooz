﻿using System;
using SnoozApp.Core.Helpers;
using Xamarin.Forms;

namespace SnoozApp.Core.Extensions
{
    public static class RssiImageConverter
    {


        public static ImageSource GetImageSourceFromRssiValue(double rssi)
        {
            if(rssi == 0)
            {
                return DefaultImageSource();
            }
            else if(rssi >= -70)
            {
                return PrepareImageSource(IconFonts.SignalCellular3, Color.Green);
            }
            else if(rssi < -71 && rssi >= -82)
            {
                return PrepareImageSource(IconFonts.SignalCellular2, Color.Yellow);
            }
            else
            {
                return PrepareImageSource(IconFonts.SignalCellular1, Color.Red);
            }
        }

        static ImageSource PrepareImageSource(string signalStrengthImage,Color imageColor)
        {
            return new FontImageSource
            {
                FontFamily = IconFonts.FontAlias,
                Glyph = signalStrengthImage,
                Color = imageColor,
            };
        }

        public static ImageSource DefaultImageSource()
        {
            return PrepareImageSource(IconFonts.SignalCellularOutline, Color.Gray);
        }
    }
}
