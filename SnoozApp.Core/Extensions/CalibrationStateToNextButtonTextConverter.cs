﻿using System;
using System.Globalization;
using System.Linq;
using SnoozApp.Enums;
using SnoozApp.Modules;
using Xamarin.Forms;
using System.Collections.Generic;

namespace SnoozApp.Extensions
{
	public class CalibrationStateToNextButtonTextConverter : IValueConverter
	{
		public object Convert(
			object value,
			Type targetType,
			object parameter,
			CultureInfo culture)
		{
			var state = (CalibrationWorkflowState)value;
			switch (state)
			{
				case CalibrationWorkflowState.Start:
				case CalibrationWorkflowState.Step1:
					return "Next";
				case CalibrationWorkflowState.Step2:
					return "Start";
				case CalibrationWorkflowState.Calibrating:
					return "---";
				case CalibrationWorkflowState.Finished:
					return "";
				case CalibrationWorkflowState.Setup:
					return "Calibrate";
				case CalibrationWorkflowState.Disabled:
					return "";
			}
			return "";
		}

		public object ConvertBack(
			object value,
			Type targetType,
			object parameter,
			CultureInfo culture)
		{
			throw new NotImplementedException("DeviceStateConverter is one-way");
		}
	}
}