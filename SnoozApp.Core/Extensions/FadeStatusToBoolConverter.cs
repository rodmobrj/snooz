﻿using System;
using System.Globalization;
using SnoozApp.Controls;
using Xamarin.Forms;

namespace SnoozApp.Core.Extensions
{
    public class FadeStatusToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //View control = null;

            string textButton = string.Empty;
            var control = parameter as Binding;

            if (Device.RuntimePlatform == Device.iOS)
            {
                var iosButton = control.Source as ColorToggleButtoniOS;
                textButton = iosButton.Text;
            }

            if (Device.RuntimePlatform == Device.Android)
            {
                var androidButton = control.Source as ColorToggleButtonAndroid;
                textButton = androidButton.Text;
            }

            if (value is string fade)
            {
                return fade == textButton;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => null;
    }
}
