﻿using System;
using System.Globalization;
using SnoozApp.Enums;
using Xamarin.Forms;

namespace SnoozApp.Extensions
{
    public class ConfigurationStateConverter : IValueConverter
    {
        public object Convert(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            var state = (ConnectionState)value;
            switch (state)
            {
                case ConnectionState.Scanning:
                    return "Press and hold the power button on your SNOOZ for 5 seconds";
                case ConnectionState.Connecting:
                    return "Connecting";
                case ConnectionState.Connected:
                    return "Complete!";
                case ConnectionState.Disconnected:
                    return "Press and hold the power button on your SNOOZ for 5 seconds";
            }
            return "Press and hold the power button on your SNOOZ for 5 seconds";
        }

        public object ConvertBack(
            object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException("DeviceStateConverter is one-way");
        }
    }
}