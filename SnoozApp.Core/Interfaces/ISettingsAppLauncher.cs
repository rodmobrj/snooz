﻿namespace SnoozApp.Core.Interfaces
{
    public interface ISettingsAppLauncher
    {
        void LaunchSettingsApp(string appBundleId);
    }
}