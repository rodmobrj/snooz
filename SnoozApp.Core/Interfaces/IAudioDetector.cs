﻿using System;

namespace SnoozApp
{
    public interface IAudioDetector
    {
        event EventHandler<EventArgs> AudioLevelsUpdated;
		event EventHandler<EventArgs> InitializationFailed;
        void Initialize();
		void StartRecording();
        float AudioLevel { get;}
    }
}