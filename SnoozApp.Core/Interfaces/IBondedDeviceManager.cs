﻿using System;

namespace SnoozApp
{
    public interface IBondedDeviceManager
    {
        void Initialize();
    }
}