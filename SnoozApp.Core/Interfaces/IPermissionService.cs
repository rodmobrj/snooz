﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using static Xamarin.Essentials.Permissions;

namespace SnoozApp.Core.Interfaces
{
    public interface IPermissionService
    {
        Task<PermissionStatus> CheckAndRequestLocationPermission(BasePermission permission);
    }
}
