﻿using System;

namespace SnoozApp
{
	public interface ICustomTimePickerController 
	{
		void TimePickerPressed();
		void TimePickerExited();
	}
}
