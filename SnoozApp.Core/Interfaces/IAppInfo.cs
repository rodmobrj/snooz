﻿namespace SnoozApp.Interfaces
{
    public interface IAppInfo
    {
        string PackageNameOrBundleId { get; }
        string Version { get; }
    }
}