﻿using SnoozApp.Core.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using SnoozApp.Modules;

namespace SnoozApp.Core.Interfaces
{
    public interface IAlarmSetter
    {
        void CancelAllAlarms();
        int GetSavedMinutesBeforeValue();
        string GetSavedAlarmName();
        Task PlayAlarmSoundAsync(string alarmName);
        void SetAlarmsFromTimers(List<TimerModel> timers, bool isNextWeek = false);
        void SetAlarmsFromWeeklySchedules(List<WeeklySchedule> weeklySchedules, bool isNextWeek = false);
        void SetBedTimeReminderAlarm(WeeklySchedule schedule, int minutesBefore, bool setNextWeekAlarms = false, bool isNextWeek = false);
        void SetWakeUpAlarm(WeeklySchedule schedule, string alarmName, bool setNextWeekAlarms = false, bool isNextWeek = false);
        void SetDaylightSavingAlarm();
        void CancelDaylightSaverAlarms();
    }
}
