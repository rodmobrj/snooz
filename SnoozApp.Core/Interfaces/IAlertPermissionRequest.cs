﻿using System;
using System.Threading.Tasks;

namespace SnoozApp.Core.Interfaces
{
    public interface IAlertPermissionRequest
    {
        Task<bool> RequestPermission();
    }
}
