﻿namespace SnoozApp.Enums
{
    public enum CalibrationState
    {
        Disabled,
        Idle,
        Starting,

        WaitingForOff,
        DelayAtOff,
        SampleAtOff,

        WaitingForOn,
        WaitingForSpeed1,
        DelayAtSpeed1,
        SampleAtSpeed1,

        WaitingForSpeed2,
        DelayAtSpeed2,
        SampleAtSpeed2,

        Finishing,
		Finished
    }
}