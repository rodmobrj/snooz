﻿using System.Collections.Generic;

namespace SnoozApp.Extensions
{
    public class ScheduleNotification
    {
        public enum MinutesBefore
        {
            Zero = 0,
            Five = 5,
            Ten = 10 ,
            Fifteen = 15,
            Thirty = 30,
            FortyFive = 45
        }

        public const string ZERO_MINUTES_BEFORE = "0 Minutes Before";
        public const string FIVE_MINUTES_BEFORE = "5 Minutes Before";
        public const string TEN_MINUTES_BEFORE = "10 Minutes Before";
        public const string FIFTEEN_MINUTES_BEFORE = "15 Minutes Before";
        public const string THIRTY_MINUTES_BEFORE = "30 Minutes Before";
        public const string FORTY_FIVE_MINUTES_BEFORE = "45 Minutes Before";

        public static readonly List<ScheduleNotification> Options = new List<ScheduleNotification>
        {
            new ScheduleNotification {IntMinute = (int) MinutesBefore.Zero, StrMinute = ZERO_MINUTES_BEFORE},
            new ScheduleNotification {IntMinute = (int) MinutesBefore.Five, StrMinute = FIVE_MINUTES_BEFORE},
            new ScheduleNotification {IntMinute = (int) MinutesBefore.Ten, StrMinute = TEN_MINUTES_BEFORE},
            new ScheduleNotification {IntMinute = (int) MinutesBefore.Fifteen, StrMinute = FIFTEEN_MINUTES_BEFORE},
            new ScheduleNotification {IntMinute = (int) MinutesBefore.Thirty, StrMinute = THIRTY_MINUTES_BEFORE},
            new ScheduleNotification {IntMinute = (int) MinutesBefore.FortyFive, StrMinute = FORTY_FIVE_MINUTES_BEFORE},
        };

        public string StrMinute { get; set; }
        public int IntMinute { get; set; }
    }
}
