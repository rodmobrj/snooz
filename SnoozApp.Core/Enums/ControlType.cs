﻿namespace SnoozApp.Enums
{
    public enum ControlType
    {
        Nightlight = 0,
        Timer = 1,
        BabyMonitor = 2,
		None = 3,
        Support = 4
    }
}