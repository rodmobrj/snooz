﻿namespace SnoozApp.Enums
{
    public enum CalibrationWorkflowState
    {
		Setup,
		Start,
		Step1,
		Step2,
        Calibrating,
		Finished,
		Disabled
    }
}