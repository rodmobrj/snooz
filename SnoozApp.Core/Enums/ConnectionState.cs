﻿namespace SnoozApp.Enums
{ 
    public enum ConnectionState
    {
        Disconnected,
        Scanning,
        Connecting,
        Connected
    }
}