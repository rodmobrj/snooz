﻿using System.Collections.Generic;

namespace SnoozApp.Extensions
{
    public class ScheduleAlarm
    {
        public const string ACOUSTIC_LOOP = "Acoustic Loop";
        public const string PHONE_RING = "Phone Ring";

        public static readonly List<ScheduleAlarm> Options = new List<ScheduleAlarm>
        {
            new ScheduleAlarm{AlarmName = ScheduleAlarm.ACOUSTIC_LOOP},
            new ScheduleAlarm{AlarmName = ScheduleAlarm.PHONE_RING}
        };

        public string AlarmName { get; set; }
        public int CdAlarm { get; set; }
    }
}
