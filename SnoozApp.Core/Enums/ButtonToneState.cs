﻿using System.ComponentModel;

namespace SnoozApp.Core.Enums
{
    public enum ButtonToneState
    {
        [Description("White Noise (Low)")]
        WhiteNoiseLow = 0,
        [Description("White Noise (Med)")]
        WhiteNoiseMed = 1,
        [Description("White Noise (High)")]
        WhiteNoiseHigh = 2,
        [Description("Fan (Low)")]
        FanLow = 3,
        [Description("Fan (Med)")]
        FanMed = 4,
        [Description("Fan (High)")]
        FanHigh = 5
    }
}