﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SnoozApp.Core.Modules;

namespace SnoozApp.Core.Enums
{
    public enum MessageType
    {
        [Description("Random")]
        Random = 0,
        [Description("Default")]
        Default = 1,
        [Description("Bible Verses")]
        BibleVerses = 2,
        [Description("Sleep Science")]
        SleepScience = 3,
        [Description("Historical Wisdom")]
        HistoricalWisdom = 4,
        [Description("Dream Interpretation")]
        DreamInterpretation = 5
    }

    public class MessageTypeWithDescription
    {
        public MessageType Enum { get; set; }
        public string Description { get; set; }
    }

    public static class MessageTypeWithDescriptionEnum
    {
        public static List<MessageTypeWithDescription> GetTypeWithDescription(List<MessageType> categories)
        {

            List<MessageTypeWithDescription> _types = new List<MessageTypeWithDescription>();
            foreach (var item in categories)
                _types.Add(new MessageTypeWithDescription
                {
                    Description = EnumTools.GetDescription(item),
                    Enum = item
                });

            return _types;
        }
        

    }
}
