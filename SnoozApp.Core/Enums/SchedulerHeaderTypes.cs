﻿using System;
namespace SnoozApp.Core.Enums
{
    public enum SchedulerHeaderTypes
    {
        Timer = 0,
        Fade = 1,
        BedTimeReminder = 2,
    }
}
