﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace SnoozApp.Core.Enums
{
    public class EnumTools
    {
        public static string GetDescription(Enum en = null)
        {
            if (en == null) return null;
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }
    }
}
