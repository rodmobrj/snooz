﻿using System;

namespace SnoozApp.Core.Enums
{
    public class WeeklySchedule
    {
        public int Id { get; set; }
        public int Week { get; set; }
        public bool StartEnabled { get; set; }
        public bool StopEnabled { get; set; }
        public int HourStart { get; set; }
        public int MinuteStart { get; set; }
        public int HourStop { get; set; }
        public int MinuteStop { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
