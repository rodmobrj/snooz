﻿namespace SnoozApp.Enums
{
    public enum TimerType
    {
        Start = 0,
        Stop = 1,
    }
}
