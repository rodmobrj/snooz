﻿namespace SnoozApp.Enums
{
	public enum LightState
	{
		Off = 0,
		Low = 1,
		Medium = 2,
		High = 3,
		DarkMode = 4
	}
}
