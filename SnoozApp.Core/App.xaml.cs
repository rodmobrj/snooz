﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DryIoc;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using Prism;
using Prism.DryIoc;
using Prism.Ioc;
using Prism.Navigation;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using SnoozApp.Core.Interfaces;
//using SnoozApp.Core.Pages;
using SnoozApp.Pages.DeviceConfiguration;
using SnoozApp.Core.Services.Permission;
using SnoozApp.Core.ViewModels;
using SnoozApp.Core.ViewModels.DeviceConfiguration;
using SnoozApp.Enums;
using SnoozApp.Modules;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Device = Xamarin.Forms.Device;

[assembly: ExportFont("materialdesignicons-webfont.ttf", Alias = "MyAwesomeCustomFont")]
namespace SnoozApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class App : PrismApplication
    {
        public const string CONFIRM_DELETE_MESSAGE = "ConfirmDeleteMessage";
		public const string CONFIRM_FORGET_MESSAGE = "ConfirmForgetMessage";
        public const string FADE_HELP_MESSAGE = "FadeHelpMessage";

        public static int AutoFadeAdjustment;

        public static bool ClosingTime;
        public static bool TryConnecting;
        public static bool CalibrationPrompted;

        public static bool CanCloseBluetoothPopup { get; private set; } = true;
        public static bool IsBluetoothUnauthorized { get; private set; }
        public static IContainerProvider IoCContainer { get; private set; }
        public new static INavigationService NavigationService => IoCContainer?.Resolve<INavigationService>();

        public App(IPlatformInitializer initializer) : base(initializer)
        {
            SetLocale();
        }

        protected override async void OnInitialized()
        {
            Debug.WriteLine($"OnInitialized Called  - {DateTime.Now}");

            InitializeComponent();

            if (BluetoothModule.Instance.CoupledSnoozes.Any())
                await NavigationService.NavigateAsync($"/{nameof(ConnectionPage)}");
            else
                await NavigationService.NavigateAsync($"/{nameof(OriginalConfigurationPage)}");

            Debug.WriteLine($"OnInitialized Finished  - {DateTime.Now}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            IoCContainer = Container;
            
            containerRegistry.RegisterForNavigation<ConnectionPage, ConnectionViewModel>();
            containerRegistry.RegisterForNavigation<OriginalConfigurationPage, OriginalConfigurationViewModel>();
            containerRegistry.RegisterForNavigation<CalibrationPage, CalibrationViewModel>();
            containerRegistry.RegisterForNavigation<DevicesPage, DevicesViewModel>();
            containerRegistry.RegisterForNavigation<MainPageV1, MainViewModelV1>();
            containerRegistry.RegisterForNavigation<MainPageV2V3, MainViewModelV2V3>();
            containerRegistry.RegisterForNavigation<AppSupport, AppSupportViewModel>();
            containerRegistry.RegisterForNavigation<ConnectionSupport, ConnectionSupportViewModel>();

            containerRegistry.RegisterForNavigation<DeviceSupportPage, DeviceSupportViewModel>();
            containerRegistry.RegisterForNavigation<SupportTicketPage, SupportTicketViewModel>();
            containerRegistry.RegisterForNavigation<ContactUsPage, ContactUsViewModel>();
            containerRegistry.RegisterForNavigation<HelpPage, HelpViewModel>();
            containerRegistry.RegisterForNavigation<About, AboutViewModel>();
            containerRegistry.RegisterForNavigation<BedTimerReminderPage, BedTimerReminderViewModel>();
            containerRegistry.RegisterForNavigation<SchedulerPage, SchedulerPageViewModel>();
            containerRegistry.RegisterForNavigation<QuickSupportPage, QuickSupportViewModel>();
            containerRegistry.RegisterForNavigation<BLESignalSupportPage, BLESignalSupportViewModel>();
            containerRegistry.RegisterForNavigation<AdvancedSettings, AdvancedSettingsViewModel>();

            containerRegistry.RegisterForNavigation<DeviceConfigurationPage, DeviceConfigurationViewModel>();
            containerRegistry.RegisterForNavigation<ButtonConfigurationPage, ButtonConfigurationViewModel>();
            containerRegistry.RegisterForNavigation<GoConfigurationPage, GoConfigurationViewModel>();

            containerRegistry.RegisterSingleton<IPermissionService, PermissionService>();
        }

        protected override async void OnStart()
        {
            Debug.WriteLine($"OnStart Called - {DateTime.Now}");

            AppCenter.Start("ios=a63dd9c2-df3e-476b-988a-16b118196ddf;" +
                  "android=e6f5f000-2214-46d5-9e69-db84a85bc6cf;",
                  typeof(Analytics), typeof(Crashes));

            if (Device.RuntimePlatform.Equals(Device.Android) &&
                !CrossBluetoothLE.Current.IsOn)
                await ShowBluetoothOffPopUpAsync();

            CrossBluetoothLE.Current.StateChanged += OnBluetoothStateChanged;

            ClosingTime = false;
			BluetoothModule.Instance.StartWriteCommandProcessor();

            Debug.WriteLine($"OnStart Finished - {DateTime.Now}");
        }

        protected async override void OnSleep()
        {
            Debug.WriteLine("OnSleep Called");
            ClosingTime = true;
            TryConnecting = false;

            // The Microphone permissions prompt forces us back through OnSleep.
            // Don't disconnect devices in this case. ClosingTime will be set to True in the OnResume and TryConnecting can remain false
            if (!CalibrationPrompted)
            {
                Debug.WriteLine("OnSleep Executed");
                await BluetoothModule.Instance.StopScanningAsync();
                await BluetoothModule.Instance.DisconnectDevicesAsync();
            }

            Debug.WriteLine("OnSleep Connection State: " + BluetoothModule.Instance.ConnectionState);
        }

        protected override async void OnResume()
        {

            if (BluetoothModule.Instance.ConnectionState != ConnectionState.Disconnected)
            {
                await BluetoothModule.Instance.StopScanningAsync();
                await BluetoothModule.Instance.DisconnectDevicesAsync();
            }
            Debug.WriteLine("OnResume Called: " + BluetoothModule.Instance.ConnectionState);

            ClosingTime = false;
			BluetoothModule.Instance.StartWriteCommandProcessor();

			// The Microphone permissions prompt forces us back through OnResume
			// Don't switch to the connection page in this case.
			if (!CalibrationPrompted)
            {
                if (BluetoothModule.Instance.CoupledSnoozes.Any())
                {
                    await NavigationService.NavigateAsync($"/{nameof(ConnectionPage)}");
                }
                else
                {
                    await NavigationService.NavigateAsync($"/{nameof(OriginalConfigurationPage)}");
                }
            }
            else
                CalibrationPrompted = false;
        }

        public static string GetMainPageName()
        {
            Debug.WriteLine("App.cs: " + BluetoothModule.Instance.FirmwareVersion);
            switch (BluetoothModule.Instance.FirmwareVersion)
            {
                case FirmwareVersions.V1:
                    return nameof(MainPageV1);
                case FirmwareVersions.V2:
                    return nameof(MainPageV2V3);
                case FirmwareVersions.V3:
                    return nameof(MainPageV2V3);
                case FirmwareVersions.V4:
                    return nameof(MainPageV2V3);
                default:
                    return nameof(MainPageV2V3);
            }
        }

        public static async Task ShowBluetoothOffPopUpAsync()
        {
            if (PopupNavigation.Instance.PopupStack.Any())
                if (Device.RuntimePlatform.Equals(Device.iOS))
                    await CloseBluetoothOffPopUpAsync();
                else
                    return;

            // On Android it would trigger DeadObjectException
            // preventing BluetoothOffPopupPage to show
            // This call is not needed for iOS, because you can't switch off BLE without pushing the App
            // to the background, and when you do OnSleep call DisconnectDevicesAsync().
            // if (Device.RuntimePlatform.Equals(Device.iOS))
            //  await BluetoothModule.Instance.DisconnectDevicesAsync();

            if (Current.MainPage == null)
            {
                await Task.Delay(1500);
            }
            await Current.MainPage
                    .Navigation
                    .PushPopupAsync(new BluetoothOffPopupPage());
            Debug.WriteLine("******BLE Popup Appearing");


        }

        static void SetLocale()
        {
            var enUs = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = enUs;
            Thread.CurrentThread.CurrentUICulture = enUs;
        }
        
        static async Task CloseBluetoothOffPopUpAsync()
        {
            if (!PopupNavigation.Instance.PopupStack.Any()
                || !CanCloseBluetoothPopup)
                return;

            await Current.MainPage
                         .Navigation
                         .PopAllPopupAsync();

            Debug.WriteLine("******BLE Popup Disappearing");
        }

        static async void OnBluetoothStateChanged(object sender, BluetoothStateChangedArgs e)
        {
            switch (e.NewState)
            {
                case BluetoothState.Unauthorized:
                    if (Device.RuntimePlatform.Equals(Device.Android)) 
                        return;

                    IsBluetoothUnauthorized = CrossBluetoothLE.Current.State 
                                              == BluetoothState.Unauthorized;
                    if (IsBluetoothUnauthorized) 
                        await ShowBluetoothOffPopUpAsync();
                    return;
                case BluetoothState.Off:
                    // Android ONLY: prevents closing the bluetooth off screen
                    // due to DeadObjectException being triggered on disconnection
                    if (Device.RuntimePlatform.Equals(Device.Android))
                        CanCloseBluetoothPopup = false;
                    await ShowBluetoothOffPopUpAsync();
                    return;
                case BluetoothState.On:
                    await CloseBluetoothOffPopUpAsync();
                    return;
            }
        }
    }
}