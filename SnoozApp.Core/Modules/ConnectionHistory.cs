﻿using System;
namespace SnoozApp.Core.Modules
{
    public class ConnectionHistory
    {
        public readonly int LastMotorSpeed;
        public readonly int LastFirmware;
        public readonly int SchedulerInUse;
        public readonly int LastLightState;
        public readonly int EverCalibrated;
        public readonly int ConnectionCount;


        public ConnectionHistory(int lastmotorspeed, int lastfirmware, int SchedulerInUse, int lastlightstate, int evercalibrated, int connectioncount)
        {
            LastMotorSpeed = lastmotorspeed;
            LastFirmware = lastfirmware;
            this.SchedulerInUse = SchedulerInUse;
            LastLightState = lastlightstate;
            EverCalibrated = evercalibrated;
            ConnectionCount = connectioncount;
        }
    }
}
