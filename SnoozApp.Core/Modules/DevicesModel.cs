﻿using System;
namespace SnoozApp.Core.Modules
{
    public class DevicesModel
    {

        public string DeviceName { get; set; }
        public string DeviceImage { get; set; }
    }
}
