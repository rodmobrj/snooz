﻿using System;
using Prism.Mvvm;
using SnoozApp.Enums;

namespace SnoozApp.Modules
{
    public class CoupledSnooz : BindableBase
    {
		string _name;
		public string Name 
        {
			get =>_name;
			set => SetProperty(ref this._name, value);
		}

        public Guid Id { get; }
        public ulong Password { get; }

        FirmwareVersions _version;
        public FirmwareVersions Version
        {
            get => _version;
            set => SetProperty(ref this._version, value);
        }

        public CoupledSnooz(string name, Guid id, ulong password, FirmwareVersions version)
        {
            Name = name;
            Id = id;
            Password = password;
            Version = version;
        }
    }
}