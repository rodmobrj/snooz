﻿using Newtonsoft.Json;
using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using Plugin.BLE.Abstractions.Exceptions;
using SnoozApp.Core.Modules;
using SnoozApp.Enums;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;


namespace SnoozApp.Modules
{
    public enum BluegigaCommand
    {
        // V1
        MotorSpeed = 1,
        MotorEnabled = 2,
        TopLed = 3,
        BottomLed = 4,
        Timer = 5,
        Password = 6,
        UpdateStatus = 7,
        TurnOnTimer = 8, // NOT USED

        // V2
        SetOnTimers = 9,
        SetOffTimers = 10,
        SyncTime = 11,
        ScheduleRequest = 12,

        // V3
        Nightlight = 13,
        SetFadeOnTime = 14,
        SetFadeOffTime = 15,
        FadeRequest = 16
    }

    public enum BluegigaResponseCommand
    {
        SendOnSchedule = 1,
        SendOffSchedule = 2,
        SendFadeSettings = 3
    }

    public static class BluegigaStatusPointer
    {
        // V1
        public const int MotorSpeed = 0;
        public const int MotorEnabled = 1;
        public const int TopLed = 2;
        public const int BottomLed = 3;
        public const int Timer = 4;
        public const int TimerHour = 8;
        public const int TimerMinute = 9;
        public const int PasswordReceived = 10;

        // V2
        public const int TimeSyncStatus = 11;
        public const int CurrentTime = 12;

        // V3
        public const int NightlightEnabledSnooz = 16;
        public const int NightlightLevel1 = 17;
        public const int NightlightLevel2 = 18;
        public const int NightlightEnabledApp = 19;
    }

    public static class BluegigaResponsePointer
    {
        public const int Command = 0;
    }

    public static class BluegigaScheduleRequestReponsePointer
    {
        public const int Command = 0;
        public const int EnableMask = 1;
        public const int Timer0 = 2;
        public const int Timer1 = 4;
        public const int Timer2 = 6;
        public const int Timer3 = 8;
        public const int Timer4 = 10;
        public const int Timer5 = 12;
        public const int Timer6 = 14;
    }

    public static class BluegigaFadeRequestResponsePointer
    {
        public const int Command = 0;           // 1 byte
        public const int FadeOnEnabled = 1;     // 1 byte
        public const int FadeOnTime = 2;        // 4 bytes
        public const int FadeOffEnabled = 6;    // 1 byte
        public const int FadeOffTime = 7;       // 4 bytes
    }

    public class BluetoothModule : BaseModule<BluetoothModule>
    {
        private ConcurrentQueue<Tuple<BluegigaCommand, byte[]>> writeCommandsQueue = new ConcurrentQueue<Tuple<BluegigaCommand, byte[]>>();

        public event EventHandler<DeviceEventArgs> SnoozDiscovered;
        public event EventHandler<EventArgs> SnoozConnected;
        public event EventHandler<DeviceEventArgs> SnoozDisconnected;
        public event EventHandler<DeviceEventArgs> SnoozConnectionLost;
        public event EventHandler<EventArgs> ScanTimeoutElapsed;
        public event EventHandler<SnoozStatusUpdatedEventArgs> SnoozStatusUpdated;
        public event EventHandler<ScheduleResponseFrameObject> SnoozTimerOffScheduleUpdate;
        public event EventHandler<ScheduleResponseFrameObject> SnoozTimerOnScheduleUpdate;
        public event EventHandler<FadeResponseFrameObject> SnoozFadeUpdate;

        private IService ControlService;
        private ICharacteristic CommandCharacteristic;
        private ICharacteristic StatusCharacteristic;
        private ICharacteristic ResponseCharacteristic;

        private readonly Guid ControlServiceGuid = new Guid("729f0608-496a-47fe-a124-3a62aaa3fbc0");
        private readonly Guid CommandCharacteristicGuid = new Guid("90759319-1668-44da-9ef3-492d593bd1e5");
        private readonly Guid StatusCharacteristicGuid = new Guid("80c37f00-cc16-11e4-8830-0800200c9a66");
        private readonly Guid ResponseCharacteristicGuid = new Guid("f0499b1b-33ab-4df8-a6f2-2484a2ad1451");

        private CancellationTokenSource _cancellationTokenSource;

        public const byte SnoozAdvertisingDataFlagsByte = 0x02;
        public const byte SnoozAdvertisingDataCouplingBit = 0x01;
        public const byte SnoozAdvertisingDataNightLightBit = 0x02;
        public const byte SnoozAdvertisingDataFirmwareVersionBit = 0x03;
        public const int SnoozAdvertisingDataFirmwareVersionLength = 6;

        public const string SNOOZ_DEVICE_IDENTIFIER = "Snooz";

        private readonly IAdapter adapter;

        private IDevice _CurrentlyConnectedSnooz;
        public IDevice CurrentlyConnectedSnooz
        {
            get => _CurrentlyConnectedSnooz;
            set => SetProperty(ref this._CurrentlyConnectedSnooz, value);
        }

        public String CurrentlyConnectedSnoozName => this.CoupledSnoozes.FirstOrDefault(s => s.Id == CurrentlyConnectedSnooz?.Id)?.Name;

        private UInt64 connectedSnoozPassword;

        public class StatusFrameObject
        {
            public byte MotorSpeed { get; set; }
            public bool MotorEnabled { get; set; }
            public byte TopLedBrightness { get; set; }
            public byte BottomLedBrightness { get; set; }
            public uint TimeRemaining { get; set; }
            public byte TimerHour { get; set; }
            public byte TimerMinute { get; set; }
            public bool PasswordReceived { get; set; }
            public bool TimeSyncStatus { get; set; }
            public uint CurrentTime { get; set; }
            public bool NightlightEnabledSnooz { get; set; }
            public byte NightlightLevel1 { get; set; }
            public byte NightlightLevel2 { get; set; }
            public bool NightlightEnabledApp { get; set; }
        }

        public class ScheduleResponseFrameObject
        {
            public BluegigaResponseCommand Command { get; set; }
            public byte TimerEnableMask { get; set; }
            public ushort[] Timers { get; set; }
        }

        public class FadeResponseFrameObject
        {
            public BluegigaResponseCommand Command { get; } = BluegigaResponseCommand.SendFadeSettings;
            public bool FadeOnEnabled { get; set; }
            public uint FadeOnTime { get; set; }
            public bool FadeOffEnabled { get; set; }
            public uint FadeOffTime { get; set; }
        }

        public bool ConnectedSnoozHasNightLight { get; set; }
        public StatusFrameObject StatusFrame { get; set; }
        public ScheduleResponseFrameObject TimerOnScheduleFrame { get; set; }
        public ScheduleResponseFrameObject TimerOffScheduleFrame { get; set; }
        public FadeResponseFrameObject FadeResponseFrame { get; set; }

        public FirmwareVersions FirmwareVersion { get; set; }
        public List<CoupledSnooz> CoupledSnoozes { get; set; }

        // List of discovered devices. Repopulated on every scan.
        List<IDevice> _discoveredSnoozes;
        public List<IDevice> DiscoveredSnoozes
        {
            get => _discoveredSnoozes;
            set => SetProperty(ref _discoveredSnoozes, value);
        }

        // The last Snooz that the app connected to. Stored persistently in the settings data
        CoupledSnooz _lastConnectedSnooz;
        public CoupledSnooz LastConnectedSnooz
        {
            get => _lastConnectedSnooz;
            set
            {
                if (value.Password != 0)
                    SettingsModule.LastConnectedDevice = JsonConvert.SerializeObject(value);
                _lastConnectedSnooz = value;
            }
        }

        ConnectionState _connectionState;
        public ConnectionState ConnectionState
        {
            get =>_connectionState;
            set => SetProperty(ref _connectionState, value);
        }

        public BluetoothModule()
        {
            // Initialize members
            adapter = CrossBluetoothLE.Current.Adapter;
            adapter.ScanMode = ScanMode.LowLatency;

            DiscoveredSnoozes = new List<IDevice>();
            CoupledSnoozes = new List<CoupledSnooz>();
            StatusFrame = new StatusFrameObject();
            TimerOffScheduleFrame = new ScheduleResponseFrameObject();
            TimerOnScheduleFrame = new ScheduleResponseFrameObject();
            FadeResponseFrame = new FadeResponseFrameObject();
            UpdateStatusFrame(new byte[20]);
            ConnectionState = ConnectionState.Disconnected;

            // Retrieve last connected snooz from settings data
            try
            {
                var lastConnectedSnooz = JsonConvert.DeserializeObject<CoupledSnooz>
                    (SettingsModule.LastConnectedDevice);
                if (lastConnectedSnooz != null)
                    LastConnectedSnooz = lastConnectedSnooz;
            }
            catch
            {
                LastConnectedSnooz = new CoupledSnooz(string.Empty, Guid.Empty, 0, FirmwareVersions.V0);
            }

            // Retrieve list of previously coupled snoozes from settings data
            try
            {
                var snoozes = JsonConvert.DeserializeObject<List<CoupledSnooz>>(SettingsModule.CoupledDevices);
                if (snoozes != null)
                {
                    foreach (var snooz in snoozes)
                        CoupledSnoozes.Add(snooz);
                }
            }
            catch
            {
            }

            // Subscribe to adapter events
            adapter.DeviceDiscovered += Adapter_DeviceDiscovered;
            adapter.DeviceConnected += Adapter_DeviceConnected;
            adapter.DeviceDisconnected += Adapter_DeviceDisconnected;
            adapter.DeviceConnectionLost += Adapter_DeviceConnectionLost;
            adapter.ScanTimeoutElapsed += Adapter_ScanTimeoutElapsed;
        }

        public void StartWriteCommandProcessor()
            => Task.Run(ProcessWriteCommandsQueueWorker);

        public async Task StartScanningAsync()
        {
            if (!this.adapter.IsScanning && !App.ClosingTime)
            {
                Debug.WriteLine("==================================Scanning Device");
                this.ConnectionState = ConnectionState.Scanning;

                this.DiscoveredSnoozes.Clear();
                var connectedDevice = this.adapter.ConnectedDevices.FirstOrDefault(); //SNOOZ device that is connected

                if (connectedDevice != null) 
                {
                    this.DiscoveredSnoozes.Add(connectedDevice);
                }
                await this.adapter.StartScanningForDevicesAsync();
            }
        }

        public async Task StopScanningAsync()
        {
            if (this.adapter.IsScanning)
            {
                Debug.WriteLine("==================================Stopping the scan");
                await this.adapter.StopScanningForDevicesAsync();
            }
        }

        private void Adapter_DeviceDiscovered(object sender, DeviceEventArgs e)
        {
            if ((e.Device?.Name != null) && (e.Device.Name.Contains(SNOOZ_DEVICE_IDENTIFIER)))
            {
                this.SnoozDiscovered?.Invoke(this, e);
                this.DiscoveredSnoozes.Add(e.Device);
            }
        }

        public async Task<bool> ConnectToKnownDeviceAsync()
        {
            IDevice device;
            CancellationTokenSource tokenSource = new CancellationTokenSource();

            this.ConnectionState = ConnectionState.Connecting;

            Debug.WriteLine("Known Connection: " + LastConnectedSnooz.Version);

            if (LastConnectedSnooz != null && LastConnectedSnooz.Version != FirmwareVersions.V0)
            {
                this.connectedSnoozPassword = this.LastConnectedSnooz.Password;
                this.FirmwareVersion = LastConnectedSnooz.Version;
                UpdateFirmwareVersionCoupledSnooz(LastConnectedSnooz.Id, FirmwareVersions.V0);
                try
                {
                    tokenSource.CancelAfter(5000);
                    device = await this.adapter.ConnectToKnownDeviceAsync(this.LastConnectedSnooz.Id, new ConnectParameters(false, false), tokenSource.Token);
                }
                catch
                {
                    return false;
                }
                finally
                {
                    tokenSource.Dispose();
                    tokenSource = null;
                }
            }

            else
                {
                    return false;
                }

            Debug.WriteLine("Connect to Known Device Successful");
            Debug.WriteLine("RSSI: " + await device.UpdateRssiAsync());
            Debug.WriteLine("RSSI: " + device.Rssi);
            Debug.WriteLine("Known Connection2: " + ConnectionState);

            device.UpdateConnectionInterval(ConnectionInterval.High);
            Task.Run(this.ConnectionWatchdog);
            return true;
        }

       
        public async Task<bool> ConnectToDeviceAsync(IDevice device, ulong password)
        {
            Debug.WriteLine("ConnectToDeviceAsync =============");
            await this.StopScanningAsync();

            if (device == null || App.ClosingTime)
                return false;

            var advertisingData = device?.AdvertisementRecords?.FirstOrDefault(s => s.Type == AdvertisementRecordType.ManufacturerSpecificData);
            if (advertisingData?.Data == null)
                return false;

            var dataFlagsByte = advertisingData.Data[BluetoothModule.SnoozAdvertisingDataFlagsByte];
            this.ConnectedSnoozHasNightLight = ((dataFlagsByte & BluetoothModule.SnoozAdvertisingDataNightLightBit) == BluetoothModule.SnoozAdvertisingDataNightLightBit);
            var firmwareVersionPartialByte = dataFlagsByte >> (BluetoothModule.SnoozAdvertisingDataFirmwareVersionBit - 1);
            this.FirmwareVersion = (FirmwareVersions)Convert.ToInt32(1 + firmwareVersionPartialByte);

            Debug.WriteLine(BitConverter.ToString(advertisingData.Data));
            Debug.WriteLine(Convert.ToString(dataFlagsByte, 2));
            Debug.WriteLine(Convert.ToInt32(Convert.ToString(dataFlagsByte, 2), 2));
            Debug.WriteLine(firmwareVersionPartialByte);
            Debug.WriteLine(FirmwareVersion);

            this.connectedSnoozPassword = password;
            this.ConnectionState = ConnectionState.Connecting;

            CancellationTokenSource tokenSource = new CancellationTokenSource();

            try
            {
                await this.adapter.ConnectToDeviceAsync(device, new ConnectParameters(autoConnect: false, forceBleTransport: true), tokenSource.Token);

            }
            catch (DeviceConnectionException ex)
            {
                return false;
            }
            finally
            {
                device.UpdateConnectionInterval(ConnectionInterval.High);
                tokenSource.Dispose();
                tokenSource = null;
            }

            Task.Run(this.ConnectionWatchdog);

            return true;
        }


        public async Task<bool> RetryConnectToDevice(IDevice device, ulong password)
        {
            var ConnectionResult = false;
            var Attempts = 0;
            var MaxAttempts = 5;

            await DisconnectDevicesAsync();

            while (!ConnectionResult && Attempts < MaxAttempts && !App.ClosingTime && App.TryConnecting)
            {
                Debug.WriteLine("==================================Retry Connection: " + Attempts);
                await Task.Delay(500);
                ConnectionResult = await ConnectToDeviceAsync(device, password);
                Attempts++;
            }

            return ConnectionResult;
        }

        private async void Adapter_DeviceConnected(object sender, DeviceEventArgs e)
        {

            if (App.ClosingTime)
            {
                await DisconnectDevicesAsync();
                return;
            }

            //Adapter is connected here, but we keep it as connecting so the Task Watcdog runs until the password is received.
            try
            {
                if (e.Device != null)
                {
                    Debug.WriteLine("==================================Adapter_DeviceConnected");

                    this.CurrentlyConnectedSnooz = e.Device;
                    await this.LoadServicesAsync(this.CurrentlyConnectedSnooz);
                    this.WriteCommandCharacteristic(BluegigaCommand.Password, BitConverter.GetBytes(this.connectedSnoozPassword));
                }
            }
            catch
            {
                Debug.WriteLine("exception: Adapter_DeviceConnected");

                if(e.Device !=null && !App.ClosingTime && App.TryConnecting)
                {
                    RepairConnection(e.Device, this.connectedSnoozPassword);
                }

            }
            Debug.WriteLine("Adapter_DeviceConnected " + ConnectionState);

        }

        private async void RepairConnection(IDevice device, ulong password)
        {
            Debug.WriteLine("Repairing Connection");
            await this.DisconnectDevicesAsync();
            await this.RetryConnectToDevice(device, password);
        }

        private async Task ConnectionWatchdog()
        {
            Debug.WriteLine("Task Watchdog");
            while (this.ConnectionState == ConnectionState.Connecting)
            {
                Debug.WriteLine("Task Watchdog Running");
                    await Task.Delay(1000);
                    this.WriteCommandCharacteristic(BluegigaCommand.UpdateStatus);
            }
        }

        public async Task DisconnectDevicesAsync()
        {
            Debug.WriteLine("Bluetooth Module: DisconnectDevicesAsync()");

            ConnectionState = ConnectionState.Disconnected;

            if (adapter == null || adapter.ConnectedDevices == null)
                return;

            // Loop through the list of connected devices stored in the adapter 
            // and disconnect from each one. This should only be one Snooz.

            var connectedDevicesList = adapter.ConnectedDevices.ToList();
            try
            {
                foreach (var device in connectedDevicesList)
                {
                    await adapter.DisconnectDeviceAsync(device);
                }
            }
            catch
            {
                Debug.WriteLine("DisconnectDevicesAsync Catch");
            }
        }

        public void WriteTimerCommand(UInt32 timeRemaining, int hours, int minutes)
        {
            var timeBytes = new byte[6];
            Buffer.BlockCopy(BitConverter.GetBytes(timeRemaining), 0, timeBytes, 0, 4);
            timeBytes[4] = (byte)hours;
            timeBytes[5] = (byte)minutes;
            this.WriteCommandCharacteristic(BluegigaCommand.Timer, timeBytes);
        }

        private byte ConvertBoolsToByte(IEnumerable<bool> input)
        {
            var boolArray = input.Take(8).ToArray();
            byte result = 0;
            foreach (var bitIndex in Enumerable.Range(0, boolArray.Length))
            {
                result |= (byte)((boolArray[bitIndex] ? 1 : 0) << bitIndex);
            }
            return result;
        }

        public void WriteSchedule(IEnumerable<Night> week)
        {
            if (week.Count() != 7)
            {
                return;
            }

            var sunday = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday);

            // pretty sure this could just be weekNight.Start.Time.TotalMinutes
            var startMinutes = week.Select(weekNight => (sunday + weekNight.Start.Time).MinutesSinceSunday()).ToList();
            var stopMinutes = week.Select(weekNight => (sunday + weekNight.Stop.Time).MinutesSinceSunday()).ToList();

            WriteOnOffTimersCommand(BluegigaCommand.SetOnTimers,
                                    ConvertBoolsToByte(week.Select(day => day.Start.Enabled)),
                                    startMinutes);
            WriteOnOffTimersCommand(BluegigaCommand.SetOffTimers,
                                    ConvertBoolsToByte(week.Select(day => day.Stop.Enabled)),
                                    stopMinutes);

            this.WriteCommandCharacteristic(BluegigaCommand.ScheduleRequest);
        }

        private void WriteOnOffTimersCommand(BluegigaCommand command, byte enableMask, List<ushort> sundayOffsets)
        {
            var timerData = new byte[15];
            timerData[0] = enableMask;

            foreach (var weekdayIndex in Enumerable.Range(0, 7))
            {
                Buffer.BlockCopy(BitConverter.GetBytes(sundayOffsets[weekdayIndex]), 0, timerData, (1 + 2 * weekdayIndex), 2);
            }

            this.WriteCommandCharacteristic(command, timerData);
            //this.WriteSyncTimeCommand(); // Sync Time command should always be sent after a SetOnTimers or SetOffTimers command
            //Code above was moved to Scheduler page so it writes out only once
        }

        public void WriteFadeOnCommand(bool enable, uint fadeTime)
        {
            this.WriteFadeOnOffCommand(BluegigaCommand.SetFadeOnTime, enable, fadeTime);
        }

        public void WriteFadeOffCommand(bool enable, uint fadeTime)
        {
            this.WriteFadeOnOffCommand(BluegigaCommand.SetFadeOffTime, enable, fadeTime);
        }

        private void WriteFadeOnOffCommand(BluegigaCommand command, bool enable, uint fadeTime)
        {
            var fadeData = new byte[5];
            fadeData[0] = Convert.ToByte(enable);
            Buffer.BlockCopy(BitConverter.GetBytes(fadeTime), 0, fadeData, 1, 4);

            this.WriteCommandCharacteristic(command, fadeData);
            this.WriteCommandCharacteristic(BluegigaCommand.FadeRequest);
        }

        public void WriteSyncTimeCommand()
        {
            Debug.WriteLine("Bluetooth Module : Write SyncTime");
            this.WriteCommandCharacteristic(BluegigaCommand.SyncTime, BitConverter.GetBytes(DateTime.Now.SecondsSinceSunday()));
        }

        public void WriteNightlightCommand(bool nightlightEnabled, byte nightlightLevel1, byte nightlightLevel2)
        {
            var nightlightBytes = new byte[3];
            nightlightBytes[0] = Convert.ToByte(nightlightEnabled);
            nightlightBytes[1] = nightlightLevel1;
            nightlightBytes[2] = nightlightLevel2;

            this.WriteCommandCharacteristic(BluegigaCommand.Nightlight, nightlightBytes);
        }

        public void WriteCommandCharacteristic(BluegigaCommand command)
        {
            this.WriteCommandCharacteristic(command, dataBytes: new byte[] { });
        }

        public void WriteCommandCharacteristic(BluegigaCommand command, byte dataByte)
        {
            this.WriteCommandCharacteristic(command, dataBytes: new byte[] { dataByte });
        }

        public void WriteCommandCharacteristic(BluegigaCommand command, byte[] dataBytes)
        {
            if (this.CommandCharacteristic != null)
            {
                if (dataBytes == null)
                {
                    dataBytes = new byte[] { };
                }

                byte[] commandArray = new byte[dataBytes.Length + 1];
                commandArray[0] = (byte)command;
                if (dataBytes.Length > 0)
                {
                    Buffer.BlockCopy(dataBytes, 0, commandArray, 1, dataBytes.Length);
                }

                this.writeCommandsQueue.Enqueue(new Tuple<BluegigaCommand, byte[]>(command, commandArray));
            }
        }

        private async Task ProcessWriteCommandsQueueWorker()
        {
            while (!App.ClosingTime)
            {
                if (this.CommandCharacteristic != null && this.writeCommandsQueue.Count > 0)
                {
                    var writeSuccessful = false;

                    Tuple<BluegigaCommand, byte[]> command;
                    if (this.writeCommandsQueue.TryDequeue(out command))
                    {
                        try
                        {
                            writeSuccessful = await this.CommandCharacteristic.WriteAsync(command.Item2);
                        }
                        catch (CharacteristicReadException)
                        {
                            writeSuccessful = false;
                        }
                    }
                }

                await Task.Delay(50);
            }
        }

        private async Task LoadServicesAsync(IDevice device)
        {
            Debug.WriteLine("===================== Bluetooth Module: Load Services");
            await this.HandleDiscoveredServicesAsync(await device.GetServicesAsync());
        }


        private async Task HandleDiscoveredServicesAsync(IEnumerable<IService> discoveredServices)
        {
            Debug.WriteLine("===================== Bluetooth Module: HandleDiscoveredServicesAsync");
            foreach (var service in discoveredServices)
            {
                if (service.Id == this.ControlServiceGuid)
                {
                    this.ControlService = service;
                    await this.HandleControlCharacteristicsAsync(await this.ControlService.GetCharacteristicsAsync());
                }
            }
        }

        private async Task HandleControlCharacteristicsAsync(IEnumerable<ICharacteristic> discoveredCharacteristics)
        {
            Debug.WriteLine("===================== Bluetooth Module: HandleControlCharacteristicsAsync");
            var characteristics = discoveredCharacteristics.ToList();

            this.StatusCharacteristic = characteristics.FirstOrDefault(c => c.Id == this.StatusCharacteristicGuid);
            Debug.WriteLine("Status Characteristics ID: " + this.StatusCharacteristic.Id);

            if (this.StatusCharacteristic != null)
            {
                this.StatusCharacteristic.ValueUpdated += this.StatusCharacteristic_ValueUpdate;
                await this.StatusCharacteristic.StartUpdatesAsync();
            }

            Debug.WriteLine("Status Characteristics ID: 1");


            this.ResponseCharacteristic = characteristics.FirstOrDefault(c => c.Id == this.ResponseCharacteristicGuid);
            if (this.ResponseCharacteristic != null)
            {
                this.ResponseCharacteristic.ValueUpdated += this.ResponseCharacteristic_ValueUpdate;
                await this.ResponseCharacteristic.StartUpdatesAsync();
            }

            Debug.WriteLine("Status Characteristics ID: 2");

            this.CommandCharacteristic = characteristics.FirstOrDefault(c => c.Id == this.CommandCharacteristicGuid);

            Debug.WriteLine("Status Characteristics ID: 3");

        }

        private async void StatusCharacteristic_ValueUpdate(object sender, CharacteristicUpdatedEventArgs e)
        {
            Debug.WriteLine("====================== StatusCharacteristic_ValueUpdate" + e?.Characteristic?.Value?.Count());

            if (e?.Characteristic?.Value?.Count() < BluegigaStatusPointer.PasswordReceived)
                return;

            var valueReceived = e?.Characteristic?.Value.ElementAt(BluegigaStatusPointer.PasswordReceived);

            // Because the password information is stored in the status characteristic the connection logic is triggered
            // by the status characteristic value update event.
            if ((valueReceived & 0x01) == 0x00) //The 0 bit is 0 if Snooz has not received the correct password
            {
                byte[] passwordBytes = new byte[8];
                passwordBytes = BitConverter.GetBytes(this.connectedSnoozPassword);
                Debug.WriteLine("==================================Password Not Received - Sending Password: " + this.connectedSnoozPassword);
                this.WriteCommandCharacteristic(BluegigaCommand.Password, passwordBytes);
            }
            else if (this.ConnectionState == ConnectionState.Connecting) // Correct password has been received
            {
                // Add Snooz to coupled list if it is not already there
                if (this.CoupledSnoozes.All(d => d.Id != this.CurrentlyConnectedSnooz.Id))
                {
                    // Because iOS Bluetooth doesn't pull in the name correctly we are rebuilding it from the GUID here.
                    string snoozName = "SNOOZ-" + this.CurrentlyConnectedSnooz.Id.ToString().Substring(32).ToUpper();
                    this.CoupledSnoozes.Add(new CoupledSnooz(snoozName, this.CurrentlyConnectedSnooz.Id, this.connectedSnoozPassword, this.FirmwareVersion));

                    Debug.WriteLine("Add Snooz to CoupledSnoozes: " + this.CurrentlyConnectedSnooz.Name + " " + this.CurrentlyConnectedSnooz.Id + " " + this.connectedSnoozPassword);
                    var json = JsonConvert.SerializeObject(CoupledSnoozes);
                    SettingsModule.CoupledDevices = json;
                }

                //SettingsModule.LoadCurrentlyConnectedSnoozSettings();
                // Save this snooz as the last connected Snooz

                this.LastConnectedSnooz = this.CoupledSnoozes.First(s => s.Id == this.CurrentlyConnectedSnooz.Id);

                if(this.LastConnectedSnooz.Version == FirmwareVersions.V0)
                    UpdateFirmwareVersionCoupledSnooz(this.LastConnectedSnooz.Id, this.FirmwareVersion);

                this.SnoozConnected?.Invoke(sender, e);
                this.ConnectionState = ConnectionState.Connected;

            }

            this.UpdateStatusFrame(e.Characteristic.Value);
            this.SnoozStatusUpdated?.Invoke(sender, new SnoozStatusUpdatedEventArgs(this.StatusFrame));

            Debug.WriteLine("====================== StatusCharacteristic_ValueUpdate: " + ConnectionState);

        }

        private void ResponseCharacteristic_ValueUpdate(object sender, CharacteristicUpdatedEventArgs e)
        {
            var command = e.Characteristic.Value[BluegigaResponsePointer.Command];
            switch (command)
            {
                case (byte)BluegigaResponseCommand.SendOffSchedule:
                case (byte)BluegigaResponseCommand.SendOnSchedule:
                    var scheduleResponseFrame = new ScheduleResponseFrameObject
                    {
                        Command = (BluegigaResponseCommand)e.Characteristic.Value[BluegigaScheduleRequestReponsePointer.Command],
                        TimerEnableMask = e.Characteristic.Value[BluegigaScheduleRequestReponsePointer.EnableMask],
                        Timers = new ushort[]
                        {
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer0),
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer1),
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer2),
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer3),
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer4),
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer5),
                            BitConverter.ToUInt16(e.Characteristic.Value, BluegigaScheduleRequestReponsePointer.Timer6)
                        }
                    };

                    if (scheduleResponseFrame.Command == BluegigaResponseCommand.SendOffSchedule)
                    {
                        this.TimerOffScheduleFrame = scheduleResponseFrame;
                        this.SnoozTimerOffScheduleUpdate?.Invoke(sender, this.TimerOffScheduleFrame);
                    }
                    else
                    {
                        this.TimerOnScheduleFrame = scheduleResponseFrame;
                        this.SnoozTimerOnScheduleUpdate?.Invoke(sender, this.TimerOnScheduleFrame);
                    }
                    break;
                case (byte)BluegigaResponseCommand.SendFadeSettings:
                    this.FadeResponseFrame = new FadeResponseFrameObject
                    {
                        FadeOnEnabled = BitConverter.ToBoolean(e.Characteristic.Value, BluegigaFadeRequestResponsePointer.FadeOnEnabled),
                        FadeOnTime = BitConverter.ToUInt32(e.Characteristic.Value, BluegigaFadeRequestResponsePointer.FadeOnTime),
                        FadeOffEnabled = BitConverter.ToBoolean(e.Characteristic.Value, BluegigaFadeRequestResponsePointer.FadeOffEnabled),
                        FadeOffTime = BitConverter.ToUInt32(e.Characteristic.Value, BluegigaFadeRequestResponsePointer.FadeOffTime),
                    };
                    this.SnoozFadeUpdate?.Invoke(sender, this.FadeResponseFrame);
                    break;
            }

        }

        public void UpdateStatusFrame(byte[] rawStatusFrame)
        {
            this.StatusFrame.MotorSpeed = rawStatusFrame[BluegigaStatusPointer.MotorSpeed];
            this.StatusFrame.MotorEnabled = (rawStatusFrame[BluegigaStatusPointer.MotorEnabled] & 0x01) == 0x01;
            this.StatusFrame.TopLedBrightness = rawStatusFrame[BluegigaStatusPointer.TopLed];
            this.StatusFrame.BottomLedBrightness = rawStatusFrame[BluegigaStatusPointer.BottomLed];
            this.StatusFrame.TimerHour = rawStatusFrame[BluegigaStatusPointer.TimerHour];
            this.StatusFrame.TimerMinute = rawStatusFrame[BluegigaStatusPointer.TimerMinute];
            this.StatusFrame.PasswordReceived = (rawStatusFrame[BluegigaStatusPointer.PasswordReceived] & 0x01) == 0x01;

            byte[] timeBytes = new byte[4];
            Array.Copy(rawStatusFrame, BluegigaStatusPointer.Timer, timeBytes, 0, 4);
            this.StatusFrame.TimeRemaining = BitConverter.ToUInt32(timeBytes, 0);


            if (this.FirmwareVersion >= FirmwareVersions.V2)
            {
                this.StatusFrame.TimeSyncStatus = (rawStatusFrame[BluegigaStatusPointer.TimeSyncStatus] & 0x01) == 0x01;
                Array.Copy(rawStatusFrame, BluegigaStatusPointer.CurrentTime, timeBytes, 0, 4);
                this.StatusFrame.CurrentTime = BitConverter.ToUInt32(timeBytes, 0);
            }

            if (this.FirmwareVersion >= FirmwareVersions.V3)
            {
                this.StatusFrame.NightlightEnabledSnooz = (rawStatusFrame[BluegigaStatusPointer.NightlightEnabledSnooz] & 0x01) == 0x01;
                this.StatusFrame.NightlightLevel1 = rawStatusFrame[BluegigaStatusPointer.NightlightLevel1];
                this.StatusFrame.NightlightLevel2 = rawStatusFrame[BluegigaStatusPointer.NightlightLevel2];
                this.StatusFrame.NightlightEnabledApp = (rawStatusFrame[BluegigaStatusPointer.NightlightEnabledApp] & 0x01) == 0x01;
            }
        }


        private void Adapter_DeviceDisconnected(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("==================================Device Disconnected: Start");
            this.ControlService = null;
            this.CommandCharacteristic = null;
            if (this.StatusCharacteristic != null)
            {
                this.StatusCharacteristic.ValueUpdated -= this.StatusCharacteristic_ValueUpdate;
                // NOTE: We don't need to call StopUpdatesAsync on the characteristic because the updates
                //       will automatically stop after the device is disconnected
                this.StatusCharacteristic = null;
            }
            if (this.ResponseCharacteristic != null)
            {
                this.ResponseCharacteristic.ValueUpdated -= this.ResponseCharacteristic_ValueUpdate;
                // NOTE: We don't need to call StopUpdatesAsync on the characteristic because the updates
                //       will automatically stop after the device is disconnected
                this.ResponseCharacteristic = null;
            }
            this.CurrentlyConnectedSnooz = null;
            this.SnoozDisconnected?.Invoke(sender, e);
            this.ConnectionState = ConnectionState.Disconnected;
            Debug.WriteLine("==================================Device Disconnected: Finished");

        }


        public async void Adapter_DeviceConnectionLost(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("==================================Device Connection Lost: Start");
            this.ControlService = null;
            this.CommandCharacteristic = null;
            if (this.StatusCharacteristic != null)
            {
                this.StatusCharacteristic.ValueUpdated -= this.StatusCharacteristic_ValueUpdate;
                // NOTE: We don't need to call StopUpdatesAsync on the characteristic because the updates
                //       will automatically stop after the device is disconnected
                this.StatusCharacteristic = null;
            }
            if (this.ResponseCharacteristic != null)
            {
                this.ResponseCharacteristic.ValueUpdated -= this.ResponseCharacteristic_ValueUpdate;
                // NOTE: We don't need to call StopUpdatesAsync on the characteristic because the updates
                //       will automatically stop after the device is disconnected
                this.ResponseCharacteristic = null;
            }
            this.CurrentlyConnectedSnooz = null;
            this.SnoozConnectionLost?.Invoke(sender, e);
            this.ConnectionState = ConnectionState.Disconnected;
            Debug.WriteLine("==================================Device Connection Lost: Finished");

        }


        private void Adapter_ScanTimeoutElapsed(object sender, System.EventArgs e)
        {
            Debug.WriteLine("==================================Scan Timeout Elapsed");
            this.ConnectionState = ConnectionState.Disconnected;
            this.ScanTimeoutElapsed?.Invoke(sender, e);
        }

        public void RenameCoupledSnooz(Guid id, string newName)
        {
            var coupledSnooz = CoupledSnoozes.FirstOrDefault(s => s.Id == id);
            if (coupledSnooz == null || string.IsNullOrWhiteSpace(newName))  
                return;

            coupledSnooz.Name = newName;
            RaisePropertyChanged(nameof(CurrentlyConnectedSnooz));
            RaisePropertyChanged(nameof(CurrentlyConnectedSnoozName));
            SettingsModule.CoupledDevices = JsonConvert.SerializeObject(CoupledSnoozes);
        }

        public void UpdateFirmwareVersionCoupledSnooz(Guid id, FirmwareVersions newVersion)
        {
            Debug.WriteLine("======================== UpdateFirmwareVersionCoupledSnooz");
            var coupledSnooz = CoupledSnoozes.FirstOrDefault(s => s.Id == id);
            if (coupledSnooz == null || newVersion == FirmwareVersions.V0)
                return;

            coupledSnooz.Version = newVersion;
            SettingsModule.CoupledDevices = JsonConvert.SerializeObject(CoupledSnoozes);
        }

        public void ForgetCoupledSnooz(Guid id)
        {
            var coupledSnooz = CoupledSnoozes.FirstOrDefault(s => s.Id == id);
            if (coupledSnooz == null) 
                return;

            CoupledSnoozes.Remove(coupledSnooz);
            SettingsModule.CoupledDevices = JsonConvert.SerializeObject(CoupledSnoozes);
        }
    }
}