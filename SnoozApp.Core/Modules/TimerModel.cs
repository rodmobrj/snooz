﻿using Newtonsoft.Json;
using SnoozApp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;

namespace SnoozApp.Modules
{
    public class WeekDayModel : NotifyPropertyChangedExtended, IEquatable<WeekDayModel>
    {
        public string Id { get; private set; }
        public readonly int WeekDayIndex;

        bool _isChecked;
        public bool IsChecked { get => _isChecked; set => SetProperty(ref _isChecked, value); }
        bool _isOn;
        public bool IsOn { get => _isOn; set => SetProperty(ref _isOn, value); }

        public WeekDayModel(int weekDayIndex)
        {
            WeekDayIndex = weekDayIndex;
            Id = Guid.NewGuid().ToString();
        }

        public void SetId(string id) => Id = id;

        public bool Equals(WeekDayModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Id, other.Id) 
                   && _isOn == other._isOn;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() 
                   && Equals((WeekDayModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _isOn.GetHashCode();
                return hashCode;
            }
        }
    }

    public class TimerModel : NotifyPropertyChangedExtended, IEquatable<TimerModel>
    {
        const string STOP = "Stop";
        const string STOP_NEXT_DAY = "Stop (Next Day)";

        Action<TimerModel> _verifyFadeDelta;

        public bool Enabled => StartEnabled || StopEnabled;
        public string StopDisplay => StartEnabled && StopEnabled 
                                     ? (StartTime >= StopTime ? STOP_NEXT_DAY : STOP) 
                                     : STOP;

        public string Id { get; private set; }
        public List<WeekDayModel> WeekDays { get; protected set; }

        string _name;
        public string Name
        {
            get => _name; 
            set => SetProperty(ref _name, value);
        }

        bool _startEnabled;
        public bool StartEnabled
        {
            get => _startEnabled; 
            set => SetProperty(ref _startEnabled, value, new [] { nameof(Enabled), nameof(StopDisplay) });
        }

        bool _stopEnabled;
        public bool StopEnabled
        {
            get => _stopEnabled; 
            set => SetProperty(ref _stopEnabled, value, new [] { nameof(Enabled), nameof(StopDisplay) });
        }

        TimeSpan _startTime;
        public TimeSpan StartTime
        {
            get => _startTime;
            set
            {
                SetProperty(ref _startTime, value);
                _verifyFadeDelta?.Invoke(this);
            }
        }

        TimeSpan _stopTime;
        public TimeSpan StopTime
        {
            get => _stopTime;
            set
            {
                SetProperty(ref _stopTime, value, new[] {nameof(StopDisplay)});
                _verifyFadeDelta?.Invoke(this);
            }
        }

        [JsonConstructor]
        public TimerModel(Night night = null) => Init(night);
        
        public TimerModel(Action<TimerModel> verifyFadeDelta, Night night = null)
        {
            _verifyFadeDelta = verifyFadeDelta;
            Init(night);
        }

        public TimerModel(TimerModel timer, bool updateWeekDays = true, IReadOnlyList<string> weekDayIds = null)
            => Init(timer, updateWeekDays, weekDayIds);
        

        public void SetVerifyFadeDelta(Action<TimerModel> action)
            => _verifyFadeDelta = action;

        void InitWeekDays()
            => WeekDays = Enumerable.Range(0, 7)
                                    .Select(weekDayIndex => new WeekDayModel(weekDayIndex))
                                    .ToList();

        void Init(Night night = null)
        {
            _startEnabled = night != null && night.Start.Enabled;
            _stopEnabled = night != null && night.Stop.Enabled;
            _startTime = night?.Start.Time ?? new TimeSpan(22, 0, 0);
            _stopTime = night?.Stop.Time ?? new TimeSpan(6, 0, 0);
            Id = Guid.NewGuid().ToString();
            InitWeekDays();
        }

        void Init(TimerModel timer, bool updateWeekDays, IReadOnlyList<string> weekDayIds)
        {
            _startEnabled = timer.StartEnabled;
            _stopEnabled = timer.StopEnabled;
            _startTime = timer.StartTime;
            _stopTime = timer.StopTime;
            Id = Guid.NewGuid().ToString();
            InitWeekDays();
            
            if (updateWeekDays)
                WeekDays.ForEach(day =>
                {
                    var index = WeekDays.IndexOf(day);
                    if (weekDayIds != null)
                        day.SetId(weekDayIds[index]);
                    day.IsOn = timer.WeekDays[index].IsOn;
                });
        }

        public void SetId(string id)
            => Id = id;

        public bool Equals(TimerModel other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _startEnabled == other._startEnabled 
                   && _stopEnabled == other._stopEnabled 
                   && _startTime.Equals(other._startTime) 
                   && _stopTime.Equals(other._stopTime) 
                   && WeekDays.SequenceEqual(other.WeekDays);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() 
                   && Equals((TimerModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = _startEnabled.GetHashCode();
                hashCode = (hashCode * 397) ^ _stopEnabled.GetHashCode();
                hashCode = (hashCode * 397) ^ _startTime.GetHashCode();
                hashCode = (hashCode * 397) ^ _stopTime.GetHashCode();
                hashCode = (hashCode * 397) ^ (WeekDays != null ? WeekDays.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    public class ScheduleBackup
    {
        public static readonly TimerJsonConverter TimerJsonConverter = new TimerJsonConverter();
        public static readonly JsonSerializerSettings TimerJsonSettings = new JsonSerializerSettings
        {
            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            Formatting = Formatting.Indented
        }; 

        public TimerModel[] Timers { get; set; }
        public int FadeInValue { get; set; }
        public int FadeOutValue { get; set; }

        public static void SaveToLocalStorage(TimerModel[] timers, int fadeInValue, int fadeOutValue)
        {
            var saveJson = JsonConvert.SerializeObject(new ScheduleBackup
            {
                Timers = timers,
                FadeInValue = fadeInValue,
                FadeOutValue = fadeOutValue
            }, TimerJsonSettings);

            SettingsModule.SavedAdvancedSchedule = saveJson;
        }

        public static ScheduleBackup LoadFromLocalStorage(bool getLastConnectedSnoozSchedule = false)
        {
            // Get last or currently connected SNOOZ schedule
            var json = getLastConnectedSnoozSchedule 
                ? Preferences.Get($"{BluetoothModule.Instance.LastConnectedSnooz?.Id.ToString()}" +
                                  $"{SettingsModule.BASE_SAVED_ADVANCED_SCHEDULE_KEY}", null) 
                : SettingsModule.SavedAdvancedSchedule;

            if (string.IsNullOrWhiteSpace(json) || json == "null")
                return new ScheduleBackup();

            return JsonConvert.DeserializeObject<ScheduleBackup>(json, TimerJsonConverter);
        }
    }
}
