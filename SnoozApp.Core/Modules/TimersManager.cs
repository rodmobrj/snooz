﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Prism.Mvvm;
using SnoozApp.Modules;
using Xamarin.Forms.Internals;
using static SnoozApp.Modules.BluetoothModule;

namespace SnoozApp.Core.Modules
{
    public class TimersManager : BindableBase
    {
        static List<EndPoint> StartTimes;
        static List<EndPoint> StopTimes;

        List<string> NightNames = new List<string>
        {
            "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
        };

        public bool CanAddTimer => Timers.Count < 7;

        ObservableCollection<TimerModel> _timers = new ObservableCollection<TimerModel>();
        public ObservableCollection<TimerModel> Timers
        {
            get => _timers;
            set => SetProperty(ref _timers, value);
        }

        public int NumberOfActiveTimers => Timers.Where(time =>
        {
            return time.StartEnabled || time.StopEnabled;

        }).Count();

        public string CurrentNightDisplay => CurrentActiveNight.Display;

        private Night CurrentActiveNight
        {
            get
            {
                // the current timer is either one occuring right now
                // or the next scheduled one, within a day.
                var currentNight = new Night { Start = new EndPoint { Enabled = false }, Stop = new EndPoint { Enabled = false } };
                var today = NightNames.IndexOf(DateTime.Today.DayOfWeek.ToString());
                var nextDay = today == 6 ? 0 : today + 1;
                var timer = Timers.FirstOrDefault(t => t.Enabled &&
                                                  t.WeekDays[today].IsOn &&
                                                  ((t.StartEnabled &&
                                                    DateTime.Today + t.StartTime > DateTime.Now &&
                                                    DateTime.Today + t.StartTime <= DateTime.Now.AddDays(1)) ||
                                                   (t.StopEnabled &&
                                                    DateTime.Today + t.StopTime > DateTime.Now &&
                                                    DateTime.Today + t.StopTime <= DateTime.Now.AddDays(1))));

                if (timer != null)
                {
                    currentNight.Start = new EndPoint { Enabled = timer.StartEnabled, Time = timer.StartTime };
                    currentNight.Stop = new EndPoint { Enabled = timer.StopEnabled, Time = timer.StopTime };
                }
                return currentNight;
            }
        }

        ScheduleResponseFrameObject _scheduleResponseFrame;

        public TimersManager(ScheduleResponseFrameObject scheduleResponseFrame)
        {
            _scheduleResponseFrame = scheduleResponseFrame;

            GetTimesFromSnooz(_scheduleResponseFrame);

            if (StartTimes == null || StopTimes == null) return;

            MakeTimers();
        }

        void GetTimesFromSnooz(ScheduleResponseFrameObject scheduleResponseFrame)
        {
            try
            {
                var timersEnabled = Enumerable.Range(0, 7)
                                              .Select(i => (scheduleResponseFrame.TimerEnableMask
                                                            >> i & 0x01) == 0x01)
                                              .ToList();
                var timers = scheduleResponseFrame.Timers
                                                  .Select(t => TimeSpan.FromMinutes(t))
                                                  .ToList();
                var newTimes = timers.Zip(timersEnabled,
                                         (timer, enabled) => new EndPoint
                                         {
                                             Time = timer,
                                             Enabled = enabled
                                         }).ToList();

                switch (scheduleResponseFrame.Command)
                {
                    case BluegigaResponseCommand.SendOffSchedule:
                        StopTimes = newTimes; // Usada no MakeNights() - Que retorna a lista de noites de acordo com essa propriedade
                        break;
                    case BluegigaResponseCommand.SendOnSchedule:
                        StartTimes = newTimes; // Usada no MakeNights() - Que retorna a lista de noites de acordo com essa propriedade
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"MainViewModelV2V3.GetTimes exception: {ex}");
            }
        }

        void MakeTimers()
        {
            
            try
            {
                ClearTimers();
                var snoozNights = MakeNights();
                var localScheduleBackup = ScheduleBackup.LoadFromLocalStorage();
                var localNights = localScheduleBackup.Timers.ToNights();

                if (localNights.SequenceEqual(snoozNights))
                    localScheduleBackup.Timers?.ForEach(Timers.Add);
                else
                    snoozNights.ToTimers().ForEach(Timers.Add);


                if (!Timers.Any())
                    Timers?.Add(new TimerModel());
                

                RaisePropertyChanged(nameof(Timers));
                RaisePropertyChanged(nameof(CurrentActiveNight));
                RaisePropertyChanged(nameof(CurrentNightDisplay));
                

                
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"MainViewModelV2V3.MakeTimers exception: {ex}");
            }
        }

        Night[] MakeNights()
        {
            var nights = new List<Night>();
            for (var i = 0; i < 7; i++)
            {
                nights.Add(new Night
                {
                    Start = new EndPoint { Enabled = StartTimes[i].Enabled, Time = new TimeSpan(StartTimes[i].Time.Hours, StartTimes[i].Time.Minutes, 0) },
                    Stop = new EndPoint { Enabled = StopTimes[i].Enabled, Time = new TimeSpan(StopTimes[i].Time.Hours, StopTimes[i].Time.Minutes, 0) }
                });
            }

            return nights.ToArray();
        }

        public void ClearTimers()
        {
            var timers = new ObservableCollection<TimerModel>();
            timers.CollectionChanged += TimersCollectionChanged;
            Timers = timers;
        }

        void TimersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Timers.ForEach(timer => timer.Name = $"Timer {Timers.Select(t => t.Id).ToList().IndexOf(timer.Id) + 1}");
            RaisePropertyChanged(nameof(CanAddTimer));
        }

        public string ReturnBackupJsonTimers()
        {
            string _backupJsonTimers;

            if (!Timers.Any())
            {
                _backupJsonTimers = null;
                Timers?.Add(new TimerModel());
            }
            else
                _backupJsonTimers = JsonConvert.SerializeObject(Timers?.ToArray(), ScheduleBackup.TimerJsonSettings);

            return _backupJsonTimers;
        }

        
    }
}
