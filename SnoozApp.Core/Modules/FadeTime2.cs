﻿using System;
namespace SnoozApp.Core.Modules
{
    public static class FadeTime2
    {
        public const string OffText = "Off";
        public const string AutoText = "Auto";
        public const string ThirtySecText = "30s";
        public const string OneMinText = "1m";
        public const string FiveMinText = "5m";

        public const int Off = 0;
        public const int Auto = 1; //Conversion to Weber's law is done in MainViewModel, since it depends on motor speed
        public const int ThirtySec = 30;
        public const int OneMin = 60;
        public const int FiveMin = 300;

        public static readonly string[] List = { OffText, AutoText, ThirtySecText, OneMinText, FiveMinText };
    }
}
