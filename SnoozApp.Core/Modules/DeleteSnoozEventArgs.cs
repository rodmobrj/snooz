﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnoozApp.Modules;

namespace SnoozApp.Modules
{
    public class DeleteSnoozEventArgs
    {
		public Guid Id;
		public string Name;

		public DeleteSnoozEventArgs(Guid id, string name)
        {
            this.Id = id;
			this.Name = name;
        }
    }
}