﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SnoozApp.Core.Enums;

namespace SnoozApp.Core.Modules
{
    public class MessageManager
    {
        readonly IList<MessageModel> Messages;
        static MessageModel lastMessage;
        Random random = new Random();

        public MessageManager()
        {
            Messages = new List<MessageModel>();
            CreateMessageList();
        }

        public MessageModel GetMessage(int messageValue)
        {
            var type = (MessageType)messageValue;

            if (type != MessageType.Random)
                return GetMessageByCategory(type);
            else
                return GetRandomMessage();
        }

        private MessageModel GetMessageByCategory(MessageType type)
        {
            var messagesFilteredByType = Messages.Where(x => x.Type == type).ToList();

            var count = messagesFilteredByType.Count;
            var index = random.Next(count);

            return messagesFilteredByType[index];
        }

        private MessageModel GetRandomMessage()
        {
            MessageModel message = new MessageModel();

            bool canExitLoop = false;

            while (!canExitLoop)
            {
                var count = Messages.Count;
                var index = random.Next(count);

                Debug.WriteLine(index);

                message = Messages[index];

                if (lastMessage is null || lastMessage.Type != message.Type)
                {
                    lastMessage = message;
                    canExitLoop = true;
                }
            }

            return message;
        }

        public List<MessageTypeWithDescription> GetMessageCategories()
        {
            var existingCategories = new List<MessageType>();

            foreach (var message in Messages)
            {
                if (!existingCategories.Contains(message.Type))
                    existingCategories.Add(message.Type);
            }

            if (existingCategories.Count > 0)
                existingCategories.Add(MessageType.Random);

            var categories = MessageTypeWithDescriptionEnum.GetTypeWithDescription(existingCategories);

            return categories;
        }

        

        private void CreateMessageList()
        {
            #region Default
            Messages.Add(new MessageModel { Name = "Time to start heading to bed.", Type = MessageType.Default });
            #endregion

            #region Bible Verses
            Messages.Add(new MessageModel { Name = "Strive for peace with everyone, and for the holiness without which no one will see the Lord. -Hebrews 12:14", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "And the peace of God, which surpasses all understanding, will guard your hearts and your minds in Christ Jesus. -Philippians 4:7", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "I can do all things through him who strengthens me. -Philippians 4:13", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Greater love has no one than this, that someone lay down his life for his friends. -John 15:13", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The Lord is at hand; do not be anxious about anything, but in everything by prayer and supplication with thanksgiving let your requests be made known to God. -Philippians 4:6", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "There is no fear in love, but perfect love casts out fear. -1 John 4:18", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "For fear has to do with punishment, and whoever fears has not been perfected in love. -1 John 4:18", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "We love because he first loved us. -1 John 4:19", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "For all things are possible with God. -Mark 10:27", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Love bears all things, believes all things, hopes all things, endures all things. -1 Corinthians 13:7", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "If God is for us, who can bei against us? -Romans 8:31", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Rejoice in hope, be patient in tribulation, be constant in prayer. -Romans 12:12", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Bless those who persecute you; bless and do not curse them. -Romans 12:14", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Rejoice with those who rejoice, weep with those who weep. -Romans 12:15", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Repay no one evil for evil, but give thought to do what is honorable in the sight of all. -Romans 12:17", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "If possible, so far as it depends on you, live peaceably with all. -Romans 12:18", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Do not be overcome by evil, but overcome evil with good. -Romans 12:21", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Fear not, for I am with you; be not dismayed, for I am your God; I will strengthen you, I will help you, I will uphold you with my righteous right hand. -Isaiah 41:14", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Peace I leave with you; my peace I give to you. -John 14:27", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Let not your hearts be troubled, neither let them be afraid.  John 14:27", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "I lay down and slept; I woke again, for the LORD sustained me. -Pslam 3:5", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "In peace I will both lie down and sleep; for you alone, O LORD, make me dwell in safety. -Pslam 4:8", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Blessed is the man who takes refuge in him! -Pslam 34:8", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The LORD is near to the brokenhearted and saves the crushed in spirit. -Psalm 34:18", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Many are the afflictions of the righteous, but the LORD delivers him out of them all. -Pslam 34:19", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "God is our refuge and strength, a very present help in trouble. -Psalm 46:1", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "When I am afraid, I put my trust in you. -Pslam 56:3", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Even though I walk through the valley of the shadow of death, I will fear no evil, for you are with me; your rod and your staff, they comfort me. -Psalm 23:4", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The LORD is my light and my salvation; whom shall I fear? -Pslam 27:1", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The LORD is the strongholda of my life; of whom shall I be afraid? -Psalm 27:1", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Cast your burden on the LORD, and he will sustain you; he will never permit the righteous to be moved. -Psalm 55:27", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Oh give thanks to the LORD, for he is good; for his steadfast love endures forever! -Pslam 118:1", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Out of my distress I called on the LORD; the LORD answered me and set me free. -Psalm 118:5", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The LORD is on my side; I will not fear. -Psalm 118:6", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "It is better to take refuge in the LORD than to trust in man. -Psalm 118:8", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "It is better to take refuge in the LORD than to trust in princes. -Psalm 118:9", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The LORD is my strength and my song; he has become my salvation. -Psalm 118:!4", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The LORD is God, and he has made his light to shine upon us. Psalm 118:27", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "For God gave us a spirit not of fear but of power and love and self-control. 2 Timothy 1:7", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The angel of the LORD encamps around those who fear him, and delivers them. -Psalm 34:7", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Anxiety in a man’s heart weighs him down, but a good word makes him glad. -Proverbs 12:25", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Therefore do not be anxious about tomorrow, for tomorrow will be anxious for itself. -Matthew 6:34", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Humble yourselves, therefore, under the mighty hand of God so that at the proper time he may exalt you, casting all your anxieties on him, because he cares for you. -1 Peter 5:6", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "But immediately he spoke to them and said, “Take heart; it is I. Do not be afraid.” -Mark 6:50", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Be strong and courageous. -Deuteronomy 31:6", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "For I, the LORD your God, hold your right hand; it is I who say to you, “Fear not, I am the one who helps you.” -Isaiah 41:13", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "The fear of man lays a snare, but whoever trusts in the LORD is safe. -Proverbs 29:25", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Many seek the face of a ruler, but it is from the LORD that a man gets justice. -Proverbs 29:26", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "And he awoke and rebuked the wind and said to the sea, “Peace! Be still!” And the wind ceased, and there was a great calm. -Mark 4:39", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "I sought the LORD, and he answered me and delivered me from all my fears. -Psalm 34:4", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "Oh, taste and see that the LORD is good! Blessed is the man who takes refuge in him! Psalm 34:8", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "You shall not fear them, for it is the LORD your God who fights for you.’ -Deuteronomy 3:22", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "But he laid his right hand on me, saying, “Fear not, I am the first and the last, 18and the living one. -Revelation 1:17", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "But overhearinge what they said, Jesus said to the ruler of the synagogue, “Do not fear, only believe.” Mark 5:36", Type = MessageType.BibleVerses });
            Messages.Add(new MessageModel { Name = "If God is for us, who can bei against us? -Romans 8:31", Type = MessageType.BibleVerses });
            #endregion

            #region Sleep Science
            Messages.Add(new MessageModel { Name = "75% of us dream in color. Before color television, just 15% did.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "It’s not uncommon for deaf people to use sign language in their sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "It’s thought that up to 15% of the population are sleepwalkers", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Humans are the only mammals that willingly delay sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "The record for the longest period without sleep is 11 days.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Research shows you'll sleep better during a new moon and worse during a full moon.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Sea otters hold hands when they sleep so they don't drift away from each other.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Tiredness peaks twice a day, at 2AM and 2PM.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "One of our biggest sleep distractions is 24-hour internet access.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "If it takes you less than 5 minutes to fall asleep at night, you're probably sleep-deprived.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Some parts of the brain use more oxygen and glucose while asleep than when awake.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "The first three hours of sleep have the deepest stages of sleep (Slow Wave Sleep).", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Scientists call the time between 3am and 5am the ‘dead zone’. It’s when our body clock makes us ‘dead’ tired.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Two thirds of a cat's life is spent asleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "1 in 4 married couples sleep in separate beds.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "People born blind experience dreams involving things such as emotion, sound and smell rather than sight.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Within 5 minutes of waking up, 50% of your dream is forgotten.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Pair tolerance is reduced by sleep deprivation.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "41% of the British population sleep in the foetal position.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Scientists beleive there is a link between sleep position and personality.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Whales and dolphins literally fall half asleep. Each side of their brain takes turns so they can come up for air. ", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Only 21% of Americans get the recommended seven to eight hours sleep each night.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Kids will spend 40% of their childhood asleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "1 in 5 Americans sleep with a pet", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "An experiment in 1998 at Cornel University found that a bright light shone on the backs of human knees can reset the brain's sleep-wake clock.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Sleeping less than 7 hours each night reduces your life expectancy.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Only one-half of a dolphin's brain goes to sleep at a time.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Lack of sleep can cause weight gain of 2 pounds in under a week.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Before alarm clocks were invented, there were profession of people who went tapping on client's windows with long sticks until they were awake.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "A snail can sleep for 3 years.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "It's impossible to sneeze while sleeping.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Most people can survive for up to 2 months without eating, but people can only live up to 11 days without sleeping.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Sleeping on the job is acceptable in Japan, as it's viewed as exhaustion from working hard.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "The ability of the brain to tell what's important from what's not is compromised by lack of sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Studies suggest people experience better sleep during the new moon and worse sleep during a full moon.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Many Tibetan monks sleep upright.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Half of the pilots surveyed in the UK admitted to having fallen asleep while flying a passenger plane.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Cats sleep for 70% of their lives.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Believing You've Slept Well, Even If You Haven't, Improves Performance.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Memories take hold better during sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Horses can sleep standing.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Rabbits often sleep with their eyes open.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "John Lennon sometimes liked to sleep in an old coffin.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Studies suggest later school start times improve sleep and daytime functioning in adolescents.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "In 1849, David Atchison became President of the United States for just one day, and he spent most of the day sleeping.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "You burn more calories sleeping than you do watching television.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "The occurrence of nightmares could be due to heart conditions, migraine, sleep deprivation and beta blockers.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Sleep deprivation affects the brain in multiple ways that can impair judgment and slow reaction.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Neuroscientists believe babies don't dream for the first few years of their life.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Studies suggest, women experience significantly more nightmares than men and have more emotional dreams. ", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "A baby's brain can use up to 50% of the total glucose supply, which may help explain why babies need so much sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "During WW1, a Hungarian man was shot in the frontal lobe, making it impossible for him to fall asleep. He continued to live a full, sleepless life.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Giraffes only need 5 to 30 minutes of sleep in a 24-hour period.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Morphine derives its name from Morpheus, the god of sleep and dreaming in Greek Mythology, for its tendency to cause sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Koalas sleep up to 20 hours a day.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "In darkness, most people eventually adjust to a 48-hour cycle: 36 hours of activity followed by 12 hours of sleep. The reasons are still unclear.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "The chances of us eating even one spider in our sleep throughout our lifetime is close to 0%.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "The average person in France sleeps 8.83 hours per day, the most in the developed world.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "40% of adult Americans have always slept on the same side of the bed.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Drinking caffeine in the evening delays our brain's release of melatonin and interrupts our circadian rhythm by as much as 40 minutes.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "While you sleep, your brain filters out noises that might wake you up if it doesn't think you're in danger.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "We lose over a pound of weight during sleep by exhaling.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Studies suggest people who procrastinate may be more likely to have insomnia.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "It's illegal to lie down and fall asleep with your shoes on in North Dakota.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Trees sleep at night, relaxing their branches after dawn and perking them up before sunrise.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "3AM is when most Americans are sleeping (95.1%). Conversely, 6PM is when most Americans are awake (97.5%).", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "People in Britain who wake in the middle of the night are most likely to do it at 3:44 a.m.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Studies show that napping at work can boost your productivity.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "People who play video games are more likely to be lucid dreamers, having control over their actions in dreams, than those who don't.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Research has found that the more visually creative a person is, the lower the quality of their sleep.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Studies suggest night owls are more likely to suffer from nightmares.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Wealthy ancient Egyptians slept with neck supports rather than pillows to preserve their hairstyles.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "A UK study found 75% of couples go to bed at different times at least four times a week.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "If you deprive a fruit fly or a fish of sleep, it will try to catch up the next day.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Researchers from NASA say the perfect nap lasts for 26 minutes.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "1% to 3% of the world population are short sleepers, people who live happily on just a few hours of sleep per night.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Sleeping under a weighted blanket can help reduce insomnia and anxiety.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Blind people are twice as likely to smell things in their dreams as sighted people.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Winston Churchill took a two-hour nap every day.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Leonardo da Vinci's sleep schedule included 20-minute naps every four hours.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Charles Dickens slept facing north to improve his creativity.", Type = MessageType.SleepScience });
            Messages.Add(new MessageModel { Name = "Inventor Nikola Tesla never slept for more than two hours a day.", Type = MessageType.SleepScience });
            #endregion

            #region Historical Wisdom
            Messages.Add(new MessageModel { Name = "The only man who never makes a mistake is the man who never does anything. -Theodore Roosevelt", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Believe you can and you're halfway there. -Theodore Roosevelt", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Courage is not having the strength to go on; it is going on when you don't have the strength. -Theodore Roosevelt", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "No one cares how much you know, until they know how much you care. -Theodore Roosevelt", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "You know what I consider the worst disabiliyt of all? Procrastination and laziness. Give me blindness over that any day of the week. -Richard Tuner", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "If we defeat adversity with honor and integrity we can turn adversity into an asset and that can lead to prosperity. -Richard Turner", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Take possible out of impossible. -Richard Turner", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Every great dream begins with a dreamer. -Harriet Tubman", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Always remember, you have within you the strength, the patience, and the passion to reach for the stars to change the world. -Harriet Tubman", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Darkness cannot drive out darkness: only light can do that. Hate cannot drive out hate: only love can do that. -Martin Luther King Jr.", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Our lives begin to end the day we become silent about things that matter. -Martin Luther King Jr.", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Faith is taking the first step even when you can't see the whole staircase. -Martin Luther King Jr.", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Forgiveness is not an occasional act; it is a constant attitude. -Martin Luther King Jr.", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Let no man pull you so low as to hate him. -Martin Luther King Jr.", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Folks are usually about as happy as they make their minds up to be. -Abraham Lincoln", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Do I not destroy my enemies when I make them my friends? -Abraham Lincoln", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Character is like a tree and reputation its shadow. The shadow is what we think it is and the tree is the real thing. -Abraham Lincoln", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The best way to predict your future is to create it. -Abraham Lincoln", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Whatever you are, be a good one. -Abraham Lincoln", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Do you want to know who you are? Don't ask. Act! Action will delineate and define you. -Thomas Jefferson", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "I'm a greater believer in luck, and I find the harder I work the more I have of it. -Thomas Jefferson", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The most valuable of all talents is that of never using two words when one will do. -Thomas Jefferson", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "What more valuable than Gold? Diamonds. Than Diamonds? Virtue. -Benjamin Franklin", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "All mankind is divided into three classes: those that are immovable, those that are movable, and those that move. -Benjamin Franklin", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Dost thou love life? Then do not squander time, for that is the stuff life is made of. -Benjamin Franklin", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Words may show a man's wit but actions his meaning. -Benjamin Franklin", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The Constitution only gives people the right to pursue happiness. You have to catch it yourself. -Benjamin Franklin", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Your worth consists in what you are and not in what you have. -Thomas Edison", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Show me a thoroughly satisfied man and I will show you a failure. -Thomas Edison", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "If we did all the things we are capable of, we would literally astound ourselves. -Thomas Edison", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "I never did anything by accident, nor did any of my inventions come by accident; they came by work. -Thomas Edison", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Be the change that you wish to see in the world. -Mahatma Gandhi", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Live as if you were to die tomorrow. Learn as if you were to live forever. -Mahatma Gandhi", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Happiness is when what you think, what you say, and what you do are in harmony. -Mahatma Gandhi", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The weak can never forgive. Forgiveness is the attribute of the strong. -Mahatma Gandhi", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The best way to find yourself is to lose yourself in the service of others. -Mahatma Gandhi", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Strength does not come from physical capacity. It comes from indomitable will. -Mahatma Gandhi", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Yesterday is gone. Tomorrow has not yet come. We have only today. Let us begin. -Mother Teresa", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "It's not how much we give but how much love we put into giving. -Mother Teresa", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "If you are kind, people may accuse you of selfish, ulterior motives. Be kind anyway. -Mother Teresa", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The good you do today, will often be forgotten. Do good anyway. -Mother Teresa", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "I am but a small pencil in the hand of a writing God. -Mother Teresa", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "God grant me the serenity to accept the things I cannot change, courage to change the things I can, and the wisdom to know the difference. -Reinhold Niebuhr", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Whether you think you can, or think you can't, you're probably right. -Henry Ford", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "In the middle of difficulty lies opportunity. -Albert Einstein", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The greatest glory in living lies not in never falling, but in rising every time we fall. -Nelson Mandela", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "You have power over your mind — not outside events. Realize this and you will find strength. -Marcus Aurelius", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "We are what we repeatedly do. Excellence, then, is not an act, but a habit. -Aristotle", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Knowing yourself is the beginning of all wisdom. -Aristotle", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Man only likes to count his troubles; he doesn't calculate his happiness. -Fyodor Dostoyevsky", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "A man who dares to waste one hour of time has not discovered the value of life. -Charles Darwin", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Be kind, for everyone you meet is fighting a harder battle. -Plato", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "Everyone thinks of changing the world, but no one thinks of changing himself. -Leo Tolstoy", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "If you want to be happy, be. -Leo Tolstoy", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "To err is human; to forgive, divine. -Alexander Pope", Type = MessageType.HistoricalWisdom });
            Messages.Add(new MessageModel { Name = "The mind is its own place, and in itself can make a Heaven of Hell, a Hell of Heaven. -John Milton", Type = MessageType.HistoricalWisdom });
            #endregion

            #region Dream Interpretation
            Messages.Add(new MessageModel { Name = "Teeth Falling Down: Something in your life may be challenging your self-confidence.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Unable to find a Toilet: There is someting negative in your life you are sturggling to let go of. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Being Chased: There is a problem in your life that you are not dealing with. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Naked in Public: There is a situation in your life that makes you feel vulnerable and exposed.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Unprepared for an Exam: You may be critically evaluating your ability to hold to a stardard you have set for yourself.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Flying: There is a new opportunity or you have been released from a circumstance that was weighing you down. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Out of Control Vehicle: You may be evaluating the path of life you are on. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Secret Door: You have the chance for a new exciting opportunity", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Being Late: Time is running out to acomplish a goal, or that you feel you may lose an opportunity.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Openning Door: You are thinking about a past skill you were passionate about, but had to let go of. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Looking for Something: You are looking for deeper fulfillment in your life. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Climbing up a Hill/Mountain: You are stuggling to acomplish a goal.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Haunted by Ghosts: There is a habit or memory from the past is that haunting you.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Past Ended Relationships: A warning not to repeat the past relationship patterns with current relationships.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Traveling ot the Future: You are thinking about how to improve yourself or achieve certain goals.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Taking a Shower: You are seeking to clarify things for an emotional situation you are dealing with. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Being Invisible: You feel your efforts are not being noticed.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Losing a Shoe: You are concerned about for ability to stand up for what you believe. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Empty Workplace: You feel your professional abilities aren't being fully recognized.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Trapped or Imprisoned: You feel that you have lost some of your independence.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Restricted Tunnel: You feel something is holding you back. ", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Small Doors/Holes: There are opportunities, but not big enough to get you anywhere.", Type = MessageType.DreamInterpretation });
            Messages.Add(new MessageModel { Name = "Surrounded by Snakes: You feel you are surrounded by enemies.", Type = MessageType.DreamInterpretation });
            #endregion
        }
    }

    public class MessageModel
    {
        public string Name { get; set; }
        public MessageType Type { get; set; }

    }
}
