﻿using Prism.Mvvm;

namespace SnoozApp.Core.Modules
{
    public abstract class BaseModule<T> : BindableBase where T : BaseModule<T>, new()
    {
        static readonly object InstanceLock = new object();
        static T _instance;

        public static T Instance
        {
            get
            {
                lock (InstanceLock)
                {
                    return _instance ?? (_instance = new T());
                }
            }
        }
    }
}
