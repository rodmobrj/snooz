﻿using SnoozApp.Core.Enums;
using SnoozApp.Core.Interfaces;
using SnoozApp.Extensions;
using SnoozApp.Modules;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SnoozApp.Core.Modules
{
    public enum DayOfWeekValue
    {
        Sunday = 1,
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6,
        Saturday = 7
    }

    public abstract class BaseAlarmSetterModule : IAlarmSetter
    {
        public int GetSavedMinutesBeforeValue()
            => ScheduleNotification.Options[SettingsModule.SelectedMinutesBeforeIndex]
                                   .IntMinute;

        public string GetSavedAlarmName()
            => ScheduleAlarm.Options[SettingsModule.SelectedAlarmNameIndex]
                            .AlarmName;

        public abstract void CancelAllAlarms();
        public abstract void CancelDaylightSaverAlarms();

        public void SetAlarmsFromTimers(List<TimerModel> timers, bool isNextWeek = false)
        {
            if (timers == null || !timers.Any())
            {
                //SettingsModule.IsNotificationToggled = false;
                CancelAllAlarms();
                return;
            }

            var weeklySchedule = (from time in timers
                                  from week in time.WeekDays
                                  where week.IsOn
                                  select new WeeklySchedule
                                  {
                                      Id = ConvertToCalendarDayOfWeek(week.WeekDayIndex),
                                      Week = ConvertToCalendarDayOfWeek(week.WeekDayIndex),
                                      Title = "Bedtime Reminder",
                                      Message = GetMessage(),
                                      StartEnabled = time.StartEnabled,
                                      StopEnabled = time.StopEnabled,
                                      HourStart = time.StartTime.Hours,
                                      MinuteStart = time.StartTime.Minutes,
                                      HourStop = time.StopTime.Hours,
                                      MinuteStop = time.StopTime.Minutes
                                  }).ToList();

            SetAlarmsFromWeeklySchedules(weeklySchedule);
        }

        private string GetMessage()
        {
            var messageManager = new MessageManager();
            var message = messageManager.GetMessage(SettingsModule.MessageTypeValue);

            return message.Name;
        }

        public void SetAlarmsFromWeeklySchedules(List<WeeklySchedule> weeklySchedules, bool isNextWeek = false)
        {
            if (weeklySchedules == null || !weeklySchedules.Any())
                return;

            var isWakeUpAlarmToggled = SettingsModule.IsAlarmToggled;
            var isBedTimeReminderToggled = SettingsModule.IsNotificationToggled;

            if (!isBedTimeReminderToggled && !isWakeUpAlarmToggled // Both BedTime Reminder and WakeUp Alarm disabled
                || !isWakeUpAlarmToggled && weeklySchedules.All(w => !w.StartEnabled) // Only BedTime Reminder enabled but Start disabled
                || !isBedTimeReminderToggled && weeklySchedules.All(w => !w.StopEnabled)) // Only WakeUp Alarm enabled but Stop disabled
                return;

            CancelAllAlarms();

            var minutesBefore = GetSavedMinutesBeforeValue();
            var alarmName = GetSavedAlarmName();
            WeeklySchedule lastSchedule = null;

            for (var index = 0; index < weeklySchedules.Count; index++)
            {
                var item = weeklySchedules[index];

                if (index == weeklySchedules.Count - 1)
                    lastSchedule = item;

                if (isBedTimeReminderToggled)
                    SetBedTimeReminderAlarm(item, minutesBefore, isNextWeek);

                if (isWakeUpAlarmToggled)
                    SetWakeUpAlarm(item, alarmName, isNextWeek);
            }

            #region Next Week Alarm Scheduling

            if (!isWakeUpAlarmToggled)
                SetBedTimeReminderAlarm(lastSchedule, minutesBefore, true);
            else
                SetWakeUpAlarm(lastSchedule, alarmName, true);

            #endregion
        }

        public abstract void SetBedTimeReminderAlarm(WeeklySchedule schedule, int minutesBefore, bool setNextWeekAlarms = false, bool isNextWeek = false);

        public abstract void SetWakeUpAlarm(WeeklySchedule schedule, string alarmName, bool setNextWeekAlarms = false, bool isNextWeek = false);

        public abstract Task PlayAlarmSoundAsync(string alarmName);

        static int ConvertToCalendarDayOfWeek(int dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case 0: return (int)DayOfWeekValue.Monday;
                case 1: return (int)DayOfWeekValue.Tuesday;
                case 2: return (int)DayOfWeekValue.Wednesday;
                case 3: return (int)DayOfWeekValue.Thursday;
                case 4: return (int)DayOfWeekValue.Friday;
                case 5: return (int)DayOfWeekValue.Saturday;
                case 6: return (int)DayOfWeekValue.Sunday;
                default: return 0;
            }
        }

        public void SetDaylightSavingAlarm()
        {
            var alarmName = GetSavedAlarmName();

            TimeZoneInfo timeZoneInfo = TimeZoneInfo.Local;
            TimeZoneInfo.AdjustmentRule[] adjustmentRules = timeZoneInfo.GetAdjustmentRules();

            CancelDaylightSaverAlarms();

            var year = DateTime.Now.Year;

            for (int i = 1; i <= 9; i++)
            {
                var rangeDates = GetDaylightSavingsRange(adjustmentRules, year);
                if (rangeDates is null)
                    return;


                if (rangeDates["StartDate"] > DateTime.Today)
                    CreateDaylightSavingAlarm(rangeDates["StartDate"], i * 100, alarmName, true);

                if (rangeDates["EndDate"] > DateTime.Today)
                    CreateDaylightSavingAlarm(rangeDates["EndDate"], i * 1000, alarmName, false);

                year++;
            }
        }

        public abstract void CreateDaylightSavingAlarm(DateTime dateAlarm, int id, string alarmName, bool isStartDate);

        public static Dictionary<string, DateTime> GetDaylightSavingsRange(TimeZoneInfo.AdjustmentRule[] adjustmentRules, int year)
        {
            DateTime firstOfYear = new DateTime(year, 1, 1);

            foreach (TimeZoneInfo.AdjustmentRule adjustmentRule in adjustmentRules)
            {
                if ((adjustmentRule.DateStart <= firstOfYear) && (firstOfYear <= adjustmentRule.DateEnd))
                {
                    return new Dictionary<string, DateTime>()
                    {
                        {"StartDate", GetTransitionDate(adjustmentRule.DaylightTransitionStart, year).AddDays(-1)},
                        {"EndDate", GetTransitionDate(adjustmentRule.DaylightTransitionEnd, year).AddDays(-1)},
                    };
                }
            }

            return null;
        }

        public static DateTime GetTransitionDate(TimeZoneInfo.TransitionTime transitionTime, int year)
        {
            if (transitionTime.IsFixedDateRule)
            {
                return new DateTime(year, transitionTime.Month, transitionTime.Day);
            }
            else
            {
                if (transitionTime.Week == 5)
                {
                    // Special value meaning the last DayOfWeek (e.g., Sunday) in the month.
                    DateTime transitionDate = new DateTime(year, transitionTime.Month, 1);
                    transitionDate = transitionDate.AddMonths(1);

                    transitionDate = transitionDate.AddDays(-1);
                    while (transitionDate.DayOfWeek != transitionTime.DayOfWeek)
                    {
                        transitionDate = transitionDate.AddDays(-1);
                    }

                    return transitionDate;
                }
                else
                {
                    DateTime transitionDate = new DateTime(year, transitionTime.Month, 1);
                    transitionDate = transitionDate.AddDays(-1);

                    for (int howManyWeeks = 0; howManyWeeks < transitionTime.Week; howManyWeeks++)
                    {
                        transitionDate = transitionDate.AddDays(1);
                        while (transitionDate.DayOfWeek != transitionTime.DayOfWeek)
                        {
                            transitionDate = transitionDate.AddDays(1);
                        }
                    }

                    return transitionDate;
                }
            }
        }

    }
}