﻿using System.Globalization;
using SnoozApp.Core.Enums;
using Xamarin.Essentials;

namespace SnoozApp.Modules
{
    public static class SettingsModule
    {
        static string CurrentlyConnectedSnoozId 
            => BluetoothModule.Instance.CurrentlyConnectedSnooz?.Id.ToString();

        const string MessageTypeValueIndexKey = "MessageTypeNameIndexKey";
        static int _messageTypeValue = Preferences.Get(MessageTypeValueIndexKey, 1);
        public static int MessageTypeValue
        {
            get => _messageTypeValue;
            set
            {
                if (value.Equals(_messageTypeValue)) return;

                _messageTypeValue = value;
                Preferences.Set(MessageTypeValueIndexKey, value);
            }
        }

        const string SelectedMinutesBeforeIndexKey = "SelectedMinutesBeforeIndex";
        static int _selectedMinutesBeforeIndex = Preferences.Get(SelectedMinutesBeforeIndexKey, 0);
        public static int SelectedMinutesBeforeIndex
        {
            get => _selectedMinutesBeforeIndex;
            set
            {
                if (value.Equals(_selectedMinutesBeforeIndex))
                    return;

                _selectedMinutesBeforeIndex = value;
                Preferences.Set(SelectedMinutesBeforeIndexKey, value);
            }
        }

        const string SelectedAlarmNameIndexKey = "SelectedAlarmNameIndex";
        static int _selectedAlarmNameIndex = Preferences.Get(SelectedAlarmNameIndexKey, 0);
        public static int SelectedAlarmNameIndex
        {
            get => _selectedAlarmNameIndex;
            set
            {
                if (value.Equals(_selectedAlarmNameIndex))
                    return;

                _selectedAlarmNameIndex = value;
                Preferences.Set(SelectedAlarmNameIndexKey, value);
            }
        }

        const string IsNotificationToggledKey = "IsToggledNotification";
        static bool _isNotificationToggled = Preferences.Get(IsNotificationToggledKey, false);
        public static bool IsNotificationToggled
        {
            get => _isNotificationToggled;
            set
            {
                if (value.Equals(_isNotificationToggled))
                    return;

                _isNotificationToggled = value;
                Preferences.Set(IsNotificationToggledKey, value);
            }
        }

        const string IsDaylightSavingsToggledKey = "IsDaylightSavingsToggled";
        static bool _isDaylightSavingsToggled = Preferences.Get(IsDaylightSavingsToggledKey, false);
        public static bool IsDaylightSavingsToggled
        {
            get => _isDaylightSavingsToggled;
            set
            {
                if (value.Equals(_isDaylightSavingsToggled))
                    return;

                _isDaylightSavingsToggled = value;
                Preferences.Set(IsDaylightSavingsToggledKey, value);
            }
        }

        const string IsAlarmToggledKey = "IsToggledAlarm";
        static bool _isAlarmToggled = Preferences.Get(IsAlarmToggledKey, false);
        public static bool IsAlarmToggled
        {
            get => _isAlarmToggled;
            set
            {
                if (value.Equals(_isAlarmToggled))
                    return;

                _isAlarmToggled = value;
                Preferences.Set(IsAlarmToggledKey, value);
            }
        }

        const string IsFirstTimeHasDeviceKey = "IsFirstTimeHasDevice";
        static bool _isFirstTimeHasDevice = GetBoolean(IsFirstTimeHasDeviceKey, true);
        public static bool IsFirstTimeHasDevice
        {
            get => _isFirstTimeHasDevice;
            set
            {
                if (value == _isFirstTimeHasDevice)
                    return;

                _isFirstTimeHasDevice = value;
                Preferences.Set(IsFirstTimeHasDeviceKey, value);
            }
        }

        const string EverReviewedAppKey = "EverReviewedAppKey";
        static bool _everReviewedAppKey = GetBoolean(EverReviewedAppKey, false);
        public static bool EverReviewedApp
        {
            get => _everReviewedAppKey;
            set
            {
                if (value == _everReviewedAppKey)
                    return;

                _everReviewedAppKey = value;
                Preferences.Set(EverReviewedAppKey, value);
            }
        }


        const string SignalMonitorEnabledKey = "SignalMonitorEnabledKey";
        static bool _signalMonitorEnabledKey = GetBoolean(SignalMonitorEnabledKey, false);
        public static bool SignalMonitorEnabled
        {
            get => _signalMonitorEnabledKey;
            set
            {
                if (value == _signalMonitorEnabledKey)
                    return;

                _signalMonitorEnabledKey = value;
                Preferences.Set(SignalMonitorEnabledKey, value);
            }
        }



        public const string BASE_SAVED_ADVANCED_SCHEDULE_KEY = "_SavedSchedule";
        static string SavedAdvancedScheduleKey => $"{CurrentlyConnectedSnoozId}{BASE_SAVED_ADVANCED_SCHEDULE_KEY}";
        static string _savedAdvancedSchedule = string.Empty;
        public static string SavedAdvancedSchedule
        {
            get => _savedAdvancedSchedule;
            set
            {
                if (value.Equals(_savedAdvancedSchedule))
                    return;

                _savedAdvancedSchedule = value;
                Preferences.Set(SavedAdvancedScheduleKey, value);
            }
        }

        public const string BASE_SAVED_BASIC_SCHEDULE_KEY = "_BasicSavedSchedule";
        static string SavedBasicScheduleKey => $"{CurrentlyConnectedSnoozId}{BASE_SAVED_BASIC_SCHEDULE_KEY}";
        static string _savedBasicSchedule = string.Empty;
        public static string SavedBasicSchedule
        {
            get => _savedBasicSchedule;
            set
            {
                if (value.Equals(_savedBasicSchedule))
                    return;

                _savedBasicSchedule = value;
                Preferences.Set(SavedBasicScheduleKey, value);
            }
        }
        
        const string CoupledDevicesKey = "coupled_devices_list";
        static string _coupledDevices = Preferences.Get(CoupledDevicesKey, string.Empty);
        public static string CoupledDevices
        {
            get => _coupledDevices;
            set
            {
                if (value.Equals(_coupledDevices))
                    return;

                _coupledDevices = value;
                Preferences.Set(CoupledDevicesKey, value);
            }
        }

        const string CalibrationDataKey = "calibration_data";
        static string _calibrationData = Preferences.Get(CalibrationDataKey, string.Empty);
        public static string CalibrationData
        {
            get => _calibrationData;
            set
            {
                if (value.Equals(_calibrationData))
                    return;

                _calibrationData = value;
                Preferences.Set(CalibrationDataKey, value);
            }
        }

        const string LastConnectedDeviceKey = "last_connected_device";
        static string _lastConnectedDevice = Preferences.Get(LastConnectedDeviceKey, string.Empty);
        public static string LastConnectedDevice
        {
            get => _lastConnectedDevice;
            set
            {
                if (value.Equals(_lastConnectedDevice))
                    return;

                _lastConnectedDevice = value;
                Preferences.Set(LastConnectedDeviceKey, value);
            }
        }

        const string ConnectionCountKey = "ConnectionCount";
        static int _connectionCount = GetInt(ConnectionCountKey, 0);
        public static int ConnectionCount
        {
            get => _connectionCount;
            set
            {
                if (value == _connectionCount)
                    return;

                _connectionCount = value;
                Preferences.Set(ConnectionCountKey, value);
            }
        }

        const string DeviceScheduleMaskKey = "device_schedule_mask";
        static string _deviceScheduleMask = Preferences.Get(DeviceScheduleMaskKey, string.Empty);
        public static string DeviceScheduleMask
        {
            get => _deviceScheduleMask;
            set
            {
                if (value.Equals(_deviceScheduleMask))
                    return;

                _deviceScheduleMask = value;
                Preferences.Set(DeviceScheduleMaskKey, value);
            }
        }

        const string ConnectionHistoryKey = "connection_history";
        static string _connectionHistory = Preferences.Get(ConnectionHistoryKey, string.Empty);
        public static string ConnectionHistory
        {
            get => _connectionHistory;
            set
            {
                if (value.Equals(_connectionHistory))
                    return;

                _connectionHistory = value;
                Preferences.Set(ConnectionHistoryKey, value);
            }
        }

        // Loads settings that rely on a unique device id
        public static void LoadCurrentlyConnectedSnoozSettings()
        {
            _savedAdvancedSchedule = Preferences.Get(SavedAdvancedScheduleKey, string.Empty);
            _savedBasicSchedule = Preferences.Get(SavedBasicScheduleKey, string.Empty);
        }

        // In the App previous versions all settings were saved as string
        static bool GetBoolean(string key, bool defaultValue)
        {
            try
            {
                return Preferences.Get(key, defaultValue);
            }
            catch
            {
                var valueAsString = Preferences.Get(key, string.Empty);
                var valueAsBool = valueAsString.ToLower().Equals("true");

                Preferences.Set(key, valueAsBool);
                return valueAsBool;
            }
        }

        static int GetInt(string key, int defaultValue)
        {
            try
            {
                return Preferences.Get(key, defaultValue);
            }
            catch
            {
                var valueAsString = Preferences.Get(key, string.Empty);
                int.TryParse(valueAsString, NumberStyles.Integer, CultureInfo.CurrentCulture, out var valueAsInt);

                Preferences.Set(key, valueAsInt);
                return valueAsInt;
            }
        }

        
    }
}