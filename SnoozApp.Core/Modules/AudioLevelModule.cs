﻿using Newtonsoft.Json;
using SnoozApp.Core.Modules;
using SnoozApp.Enums;
using System;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace SnoozApp.Modules
{
    public class AudioLevelModule : BaseModule<AudioLevelModule>
    {
        private const float CALIBRATION_BASELINE_40_PCT = 68.5f;
        private const int NUMBER_OF_CALIBRATION_SAMPLES = 10;

		public event EventHandler<EventArgs> CalibrationFinished;

        public class CalibrationData
        {
            public bool Calibrated;
            public float Measurement1;
            public float Measurement2;
            public float AmbientMeasurement;

            public CalibrationData(bool calibrated, float measurement1, float measurement2, float ambientMeasurement)
            {
                this.Calibrated = calibrated;
                this.Measurement1 = measurement1;
                this.Measurement2 = measurement2;
                this.AmbientMeasurement = ambientMeasurement;
            }
        }

        private CalibrationState _CalibrationState;
        public CalibrationState CalibrationState
        {
            get
            {
                return this._CalibrationState;
            }
            set
            {
                this.SetProperty(ref this._CalibrationState, value);
            }
        }

        public event EventHandler<EventArgs> AudioLevelsUpdated;

        private BluetoothModule BluetoothModule;

        public IAudioDetector AudioDetector;

        private float[] ambientSamples;
        private float[] measurement1Samples;
        private float[] measurement2Samples;
        private int ambientSamplesCounter;
        private int measurement1SamplesCounter;
        private int measurement2SamplesCounter;
		private bool aborting;

        private float _CalibratedDecibelLevel;
        public float CalibratedDecibelLevel
        {
            get
            {
                return this._CalibratedDecibelLevel;
            }
            set
            {
                this.SetProperty(ref this._CalibratedDecibelLevel, value);
            }
        }

        private bool _Calibrated;
        public bool Calibrated
        {
            get
            {
                return this._Calibrated;
            }
            set
            {
                this.SetProperty(ref this._Calibrated, value);
                this.SetCalibrationData();
            }
        }

        private float _Measurement1;
        public float Measurement1
        {
            get
            {
                return this._Measurement1;
            }
            set
            {
                SetProperty(ref _Measurement1, value);
                SetCalibrationData();
                RaisePropertyChanged(nameof(Difference));
            }
        }

        private float _Measurement2;
        public float Measurement2
        {
            get
            {
                return this._Measurement2;
            }
            set
            {
                this.SetProperty(ref this._Measurement2, value);
                this.SetCalibrationData();
            }
        }

        public float Difference => this.Measurement1 - CALIBRATION_BASELINE_40_PCT;

        private float _AmbientMeasurement;
        public float AmbientMeasurement
        {
            get
            {
                return this._AmbientMeasurement;
            }
            set
            {
                this.SetProperty(ref this._AmbientMeasurement, value);
                this.SetCalibrationData();
            }
        }

        public AudioLevelModule()
        {
            this.GetCalibrationData();
            this.CalibrationState = CalibrationState.Idle;
            this.BluetoothModule = BluetoothModule.Instance;
            this.BluetoothModule.SnoozStatusUpdated += this.BluetoothModule_SnoozStatusUpdated;

            this.ambientSamples = new float[NUMBER_OF_CALIBRATION_SAMPLES];
            this.measurement1Samples = new float[NUMBER_OF_CALIBRATION_SAMPLES];
            this.measurement2Samples = new float[NUMBER_OF_CALIBRATION_SAMPLES];

            this.AudioDetector = DependencyService.Get<IAudioDetector>();

            // Don't initialize the microphone if the phone hasn't been calibrated yet.
            if (this.Calibrated)
            {
                try
                {
                    this.AudioDetector.Initialize();
                    this.AudioDetector.StartRecording();
                }
                catch
                {
                    this.Calibrated = false;
                    AbortCalibration();
                }

            }

            this.AudioDetector.AudioLevelsUpdated += this.AudioDetector_AudioLevelsUpdated;
			this.AudioDetector.InitializationFailed += this.AudioDetector_InitializationFailed;
        }


        private void AudioDetector_AudioLevelsUpdated(object sender, EventArgs e)
        {
            switch (this.CalibrationState)
            {
                case CalibrationState.SampleAtOff:
                    if (this.ambientSamplesCounter < 10)
                    {
                        this.ambientSamples[this.ambientSamplesCounter] = this.AudioDetector.AudioLevel;
                        this.ambientSamplesCounter++;
                    }
                    else
                    {
                        this.AmbientMeasurement = this.ambientSamples.Average();
                        this.CalibrationState = CalibrationState.WaitingForOn;
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
                    }
                    break;

                case CalibrationState.SampleAtSpeed1:
                    if (this.measurement1SamplesCounter < 10)
                    {
                        this.measurement1Samples[this.measurement1SamplesCounter] = this.AudioDetector.AudioLevel;
                        this.measurement1SamplesCounter++;
                    }
                    else
                    {
                        this.Measurement1 = this.measurement1Samples.Average();
                        this.CalibrationState = CalibrationState.WaitingForSpeed2;
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 70);
                    }
                    break;

                case CalibrationState.SampleAtSpeed2:
                    if (this.measurement2SamplesCounter < 10)
                    {
                        this.measurement2Samples[this.measurement2SamplesCounter] = this.AudioDetector.AudioLevel;
                        this.measurement2SamplesCounter++;
                    }
                    else
                    {
                        this.Measurement2 = this.measurement2Samples.Average();
                        this.CalibrationState = CalibrationState.Finishing;
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 0);
                    }
                    break;
            }

            if (this.Calibrated && !App.ClosingTime)
            {
                this.CalibratedDecibelLevel = 0.0F; //To Do: This line is added so the sound bar resets after you minimize. Need a better solution. 
                this.CalibratedDecibelLevel = this.AudioDetector.AudioLevel - this.Difference;
                this.AudioLevelsUpdated?.Invoke(this, new EventArgs());
            }
            else
            {
                this.CalibratedDecibelLevel = 0.0F;
            }

        }


        private void BluetoothModule_SnoozStatusUpdated(object sender, SnoozStatusUpdatedEventArgs e)
        {
            switch (this.CalibrationState)
            {
                case CalibrationState.WaitingForOff:
                    if (!e.StatusUpdate.MotorEnabled)
                    {
                        this.CalibrationState = CalibrationState.DelayAtOff;
                        Device.StartTimer(TimeSpan.FromSeconds(2), this.AdvanceCalibrationState);
                    }
                    else
                    {
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 0);
                    }
                    break;
                case CalibrationState.WaitingForOn:
                    if (e.StatusUpdate.MotorEnabled)
                    {
                        this.CalibrationState = CalibrationState.WaitingForSpeed1;
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 40);
                    }
                    else
                    {
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
                    }
                    break;
                case CalibrationState.WaitingForSpeed1:
                    if (e.StatusUpdate.MotorSpeed == 40)
                    {
                        this.CalibrationState = CalibrationState.DelayAtSpeed1;
                        Device.StartTimer(TimeSpan.FromSeconds(2), this.AdvanceCalibrationState);
                    }
                    else
                    {
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 40);
                    }
                    break;
                case CalibrationState.WaitingForSpeed2:
                    if (e.StatusUpdate.MotorSpeed == 70)
                    {
                        this.CalibrationState = CalibrationState.DelayAtSpeed2;
                        Device.StartTimer(TimeSpan.FromSeconds(2), this.AdvanceCalibrationState);
                    }
                    else
                    {
                        this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 70);
                    }
                    break;
                case CalibrationState.Finishing:
                    if (!e.StatusUpdate.MotorEnabled)
                    {
                        this.Calibrated = true;
                        this.SetCalibrationData();

						this.CalibrationFinished?.Invoke(this, new EventArgs());
                        this.CalibrationState = CalibrationState.Idle;
                    }
                    break;
            }
        }


        private bool AdvanceCalibrationState()
        {
            Debug.WriteLine("Calibration timer");
            switch (this.CalibrationState)
            {
                case CalibrationState.DelayAtOff:
                    this.ambientSamplesCounter = 0;
                    this.CalibrationState = CalibrationState.SampleAtOff;
                    break;
                case CalibrationState.DelayAtSpeed1:
                    this.measurement1SamplesCounter = 0;
                    this.CalibrationState = CalibrationState.SampleAtSpeed1;
                    break;
                case CalibrationState.DelayAtSpeed2:
                    this.measurement2SamplesCounter = 0;
                    this.CalibrationState = CalibrationState.SampleAtSpeed2;
                    break;
            }
            return false;
        }


        private bool CalibrationWatchdog()
        {
			// Stop the timer task if the calibration procedure is aborted.
			if (this.aborting)
			{
				return false;
			}

            Debug.WriteLine("Calibration timer");
            switch (this.CalibrationState)
            {
				// All of these cases fall through to Write Command and return true
                case CalibrationState.WaitingForOff:
                case CalibrationState.WaitingForOn:
                case CalibrationState.WaitingForSpeed1:
                case CalibrationState.WaitingForSpeed2:
                case CalibrationState.Finishing:
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.UpdateStatus);
                    return true;
				
				// These cases fall through to return false
                case CalibrationState.Idle:
				case CalibrationState.Disabled:
					return false;
					
                default:
                    return true;
            }
        }


        public void StartCalibration()
        {
			this.aborting = false;
			this.AudioDetector.StartRecording();
            this.CalibrationState = CalibrationState.WaitingForOff;
            Device.StartTimer(TimeSpan.FromSeconds(0.25), this.CalibrationWatchdog);
            this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 0);
        }

		public void AbortCalibration()
		{
			this.aborting = true;
			this.CalibrationState = CalibrationState.Idle;
		}

        void SetCalibrationData()
            => SettingsModule.CalibrationData = 
                JsonConvert.SerializeObject(new CalibrationData(Calibrated, 
                                                                Measurement1, 
                                                                Measurement2, 
                                                                AmbientMeasurement));

        void GetCalibrationData()
        {
            try
            {
                var convertedCalibrationData = 
                    JsonConvert.DeserializeObject<CalibrationData>(SettingsModule.CalibrationData);
                if (convertedCalibrationData != null)
                {
                    Calibrated = convertedCalibrationData.Calibrated;
                    Measurement1 = convertedCalibrationData.Measurement1;
                    Measurement2 = convertedCalibrationData.Measurement2;
                    AmbientMeasurement = convertedCalibrationData.AmbientMeasurement;
                }
                else
                {
                    SetDefaultCalibrationData();
                }
            }
            catch
            {
               SetDefaultCalibrationData();
            }
        }

        void SetDefaultCalibrationData()
        {
            Calibrated = false;
            Measurement1 = 0;
            Measurement2 = 0;
            AmbientMeasurement = 0;
        }

		void AudioDetector_InitializationFailed(object sender, EventArgs e)
		    => CalibrationState = CalibrationState.Disabled;
    }
}