﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SnoozApp.Modules;

namespace SnoozApp.Modules
{
    public class SnoozStatusUpdatedEventArgs
    {
        public BluetoothModule.StatusFrameObject StatusUpdate;

        public SnoozStatusUpdatedEventArgs(BluetoothModule.StatusFrameObject s)
        {
            this.StatusUpdate = s;
        }
    }
}