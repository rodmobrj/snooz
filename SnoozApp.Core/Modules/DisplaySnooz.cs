﻿using Prism.Mvvm;
using System;

namespace SnoozApp
{
    public class DisplaySnooz : BindableBase
	{
		string _name;
		public string Name
		{
			get =>_name;
			set => SetProperty(ref _name, value);
		}

		public Guid Id { get; }

		public bool IsConnectable => DiscoveredThisScan || DiscoveredLastScan;

		bool _discoveredThisScan;
		public bool DiscoveredThisScan
		{
			get => _discoveredThisScan;
			set
			{
				SetProperty(ref _discoveredThisScan, value);
				RaisePropertyChanged(nameof(IsConnectable));
			}
		}

		bool _discoveredLastScan;
		public bool DiscoveredLastScan
		{
			get => _discoveredLastScan;
			set
			{
				SetProperty(ref _discoveredLastScan, value);
				RaisePropertyChanged(nameof(IsConnectable));
			}
		}

		bool _isConnecting;
		public bool IsConnecting
		{
			get => _isConnecting;
			set => SetProperty(ref _isConnecting, value);
		}

		int _rssi;
		public int Rssi
		{
			get => _rssi;
			set
			{
				SetProperty(ref _rssi, value);
				RaisePropertyChanged(nameof(Rssi));
			}
		}

		public DisplaySnooz(string name, Guid id)
		{
			Name = name;
			Id = id;
          	DiscoveredLastScan = false;
			DiscoveredThisScan = false;
			IsConnecting = false;
		}
	}
}
