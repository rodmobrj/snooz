﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SnoozApp.Modules
{
    public static class NightExtensions
    {
        public static IEnumerable<Night> ToNights(this IEnumerable<TimerModel> timers, bool toWriteOnDevice = false)
        {

			List<Night> nights = null;
			try
			{
				
                if (toWriteOnDevice)
                    nights = Enumerable.Range(0, 7)
                                       .Select(x => new Night(false, new TimeSpan(22, 0, 0), false, new TimeSpan(6, 0, 0), (x + 1) % 7))
                                       .ToList();
                else
                    nights = Enumerable.Range(0, 7)
                                       .Select(x => new Night
                                       {
                                           Start = new EndPoint { Enabled = false, Time = new TimeSpan(22, 0, 0) },
                                           Stop = new EndPoint { Enabled = false, Time = new TimeSpan(6, 0, 0) }
                                       }).ToList();
                for (var i = 0; i < 7; i++)
                {
                    if (timers?.Where(t => t.Enabled)?.FirstOrDefault(t => (bool)t.WeekDays[i]?.IsOn) is TimerModel timer)
                    {
                        if (toWriteOnDevice)
                            nights[i] = new Night(timer.StartEnabled, timer.StartTime, timer.StopEnabled, timer.StopTime, (i + 1) % 7);
                        else
                            nights[i] = new Night
                            {
                                Start = new EndPoint { Enabled = timer.StartEnabled, Time = timer.StartTime },
                                Stop = new EndPoint { Enabled = timer.StopEnabled, Time = timer.StopTime }
                            };
                    }
                }
                


			}
			catch (Exception ex)
			{

			}
			return nights;

        }

        public static IEnumerable<TimerModel> ToTimers(this IEnumerable<Night> _nights, Action<TimerModel> verifyFadeDelta = null)
        {
            var nights = _nights.ToList();
            var timers = new List<TimerModel>();
            for (var i = 0; i < 7; i++)
            {
                var night = new Night
                {
                    Start = new EndPoint { Enabled = nights[i].Start.Enabled, Time = new TimeSpan(nights[i].Start.Time.Hours, nights[i].Start.Time.Minutes, 0) },
                    Stop = new EndPoint { Enabled = nights[i].Stop.Enabled, Time = new TimeSpan(nights[i].Stop.Time.Hours, nights[i].Stop.Time.Minutes, 0) }
                };

                var timer = timers.FirstOrDefault(t => (t.StartEnabled == night.Start.Enabled &&
                                                        t.StartTime == night.Start.Time  &&
                                                       t.StopEnabled == night.Stop.Enabled &&
                                                        t.StopTime == night.Stop.Time));
                if (timer != null)
                {
                    timer.WeekDays[i].IsOn = nights[i].Enabled;
                }
                else if (nights[i].Enabled)
                {
                    timer = new TimerModel(verifyFadeDelta, nights[i]);
                    timer.WeekDays[i].IsOn = nights[i].Enabled;
                    timers.Add(timer);
                }
            }
            return timers;
        }
    }

    public class Night : IEquatable<Night>
	{
		public Night()
		{

		}

		public Night(bool startEnabled, TimeSpan startTime, bool stopEnabled, TimeSpan stopTime)
			: this(startEnabled, startTime, stopEnabled, stopTime, 0)
		{
		}

		public Night(bool startEnabled, TimeSpan startTime, bool stopEnabled, TimeSpan stopTime, int daysFromSunday)
		{
			this.Start = new EndPoint
			{
				Enabled = startEnabled,
				Time = startTime.Add(TimeSpan.FromDays(daysFromSunday))
			};

			// If the start time is enabled and the stop time takes place before the start time (aka the stop time is
			// on the next day) then add a day to the stop time
			var stopTimeDayOffset = (startEnabled && stopTime < startTime) ? 1 : 0;
			this.Stop = new EndPoint
			{
				Enabled = stopEnabled,
				Time = stopTime.Add(TimeSpan.FromDays((daysFromSunday + stopTimeDayOffset) % 7))
			};
		}

		public EndPoint Start { get; set; }
		public EndPoint Stop { get; set; }
		public bool Enabled => this.Start.Enabled || this.Stop.Enabled;
		public string Display
		{
			get
			{
				return this.Enabled
					? this.Start.Enabled && this.Stop.Enabled
						? $"{this.Start.Display} - {this.Stop.Display}"
						: this.Start.Enabled
							? $"{this.Start.Display} (Bedtime)"
							: $"{this.Stop.Display} (Wake)"
					: null;
			}
		}
		public bool SpansMidnight => (new TimeSpan(this.Start.Time.Hours, this.Start.Time.Minutes, 0)
																>= new TimeSpan(this.Stop.Time.Hours, this.Stop.Time.Minutes, 0));

		public bool IsDefault => !this.Enabled
								&& this.Start.Time.Hours == 0
								&& this.Start.Time.Minutes == 0
								&& this.Stop.Time.Hours == 0
								&& this.Stop.Time.Minutes == 0;

        public bool Equals(Night other)
        {
            if (other == null || other.Start == null || other.Stop == null)
                return false;
            return Start?.Enabled == other.Start.Enabled &&
                   Start?.Time == other.Start.Time &&
                   Stop?.Enabled == other.Stop.Enabled &&
                   Stop?.Time == other.Stop.Time;
        }
    }

	public class EndPoint
	{
		public bool Enabled { get; set; }
		public TimeSpan Time { get; set; }
		public string Display
		{
			get
			{
				return this.Enabled
					? (new DateTime(this.Time.Ticks)).ToString("h:mm tt")
					: null;
			}
		}
	}

	public class NightDisplay
	{
		public string NightName { get; set; }
		public string NightTimesDisplay { get; set; }
	}

	public class NightEnabled
	{
		public bool StartEnabled { get; set; }
		public bool StopEnabled { get; set; }
	}
}
