﻿using System;
using System.Threading.Tasks;
using Prism.Services;
using SnoozApp.Core.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;
using static Xamarin.Essentials.Permissions;

namespace SnoozApp.Core.Services.Permission
{
    public class PermissionService : IPermissionService
    {
        const string MESSAGE_RECORD_TITLE = "Record Audio Permission Denied";
        const string MESSAGE_RECORD = "Record audio permission is needed to calibrate volume.";
        const string MESSAGE_LOCATION_TITLE = "Location Permission Denied";
        const string MESSAGE_LOCATION = "Location permission is needed to use Bluetooth LE.";
        const string CONFIRM_BUTTON = "Ok";

        string MessageTitle = string.Empty;
        string Message = string.Empty;


        IPageDialogService _pageDialogService;

        public PermissionService(IPageDialogService pageDialogService)
        {
            _pageDialogService = pageDialogService;
        }

        public async Task<PermissionStatus> CheckAndRequestLocationPermission(BasePermission permission)
        {
            ConfigureAlertMessage(permission);

            var status = await permission.CheckStatusAsync();

            if (status == PermissionStatus.Granted)
                return status;


            if (status == PermissionStatus.Denied && DeviceInfo.Platform == DevicePlatform.iOS)
            {
                await PromptUserMessage();
                return status;
            }
            
            if (permission.ShouldShowRationale())
            {
                await PromptUserMessage();
            }

            status = await permission.RequestAsync();

            return status;
        }

        void ConfigureAlertMessage(BasePermission permission)
        {
            var permissionName = permission.GetType().Name;

            switch (permissionName)
            {
                case "Microphone":
                    MessageTitle = MESSAGE_RECORD_TITLE;
                    Message = MESSAGE_RECORD;
                    break;
            }
        }

        async Task PromptUserMessage() =>
             await _pageDialogService.DisplayAlertAsync(MessageTitle, Message, CONFIRM_BUTTON);

    }
}
