﻿using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HelpPage : BasePage
    {
        public HelpPage()
        {
            InitializeComponent();
        }
    }
}