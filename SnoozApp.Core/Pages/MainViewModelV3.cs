﻿using Newtonsoft.Json;
using Plugin.BLE.Abstractions.EventArgs;
using Prism.Navigation;
using SnoozApp.Enums;
using SnoozApp.Extensions;
using SnoozApp.Modules;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using static SnoozApp.Modules.BluetoothModule;

namespace SnoozApp.Pages
{
    public static class FadeTime
    {
        public const double MOTOR_SPEED_RATIO = 0.06;

        public const string OFF_TEXT = "Off";
        public const string AUTO_TEXT = "Auto";
        public const string THIRTY_SEC_TEXT = "30sec";
        public const string ONE_MIN_TEXT = "1min";
        public const string FIVE_MIN_TEXT = "5mins";
        public const string BASIC_TEXT = "Basic";

        public const int OFF = 0;
        public const int AUTO = 1;
        public const int THIRTY_SEC = 30;
        public const int ONE_MIN = 60;
        public const int FIVE_MIN = 300;
        public const int BASIC = 29;

        public static readonly string[] List = { OFF_TEXT, AUTO_TEXT, THIRTY_SEC_TEXT, ONE_MIN_TEXT, FIVE_MIN_TEXT };
    }

    public class MainViewModelV3 : BaseViewModel
    {
        const int OffLightLevel = 0;
        const int LowLightLevel = 20;
        const int MediumLightLevel = 50;
        const int HighLightLevel = 90;
        const int LightLevelIncrement = 10;
        const int NumMotorLevelGradations = 10;
        const int MinMotorLevel = 1;
        const int MaxMotorLevel = 10;

        TimerModel[] _backupTimers;
        List<EndPoint> _startTimes;
        List<EndPoint> _stopTimes;

        #region App Usage History Generator

        void CurrentStateData()
        {
            GetConnectionHistory();

            //Get current motor speed
            LastMotorSpeed = CurrentMotorLevel;

            //Determine firmware version
            if (FirmwareVersion == FirmwareVersions.V3)
                LastFirmware = 3;

            //Count number of scheduled days
            SchedulerInUse = _backupTimers == null ? 0 : 1;

            //Determine Nightlight state
            if (MotorEnabled && NightLightState == LightState.DarkMode)
                LastLightState = 1;
            else if (MotorEnabled && NightLightState == LightState.Off)
                LastLightState = 2;
            else if (MotorEnabled && NightLightState == LightState.Low)
                LastLightState = 3;
            else if (MotorEnabled && NightLightState == LightState.Medium)
                LastLightState = 4;
            else if (MotorEnabled && NightLightState == LightState.High)
                LastLightState = 5;

            //Determine if nursery monitor calibrated
            EverCalibrated = AudioLevelModule.Calibrated ? 1 : 0;

            //Calculate how many times app has been connected
            ConnectionCount++;

            SetConnectionHistory();
        }

        public class ConnectionHistory
        {
            public int LastMotorSpeed;
            public int LastFirmware;
            public int SchedulerInUse;
            public int LastLightState;
            public int EverCalibrated;
            public int ConnectionCount;


            public ConnectionHistory(int lastmotorspeed, int lastfirmware, int schedulerInUse, int lastlightstate, int evercalibrated, int connectioncount)
            {
                LastMotorSpeed = lastmotorspeed;
                LastFirmware = lastfirmware;
                SchedulerInUse = schedulerInUse;
                LastLightState = lastlightstate;
                EverCalibrated = evercalibrated;
                ConnectionCount = connectioncount;
            }
        }

        void SetConnectionHistory()
            => SettingsModule.ConnectionHistory = JsonConvert.SerializeObject
                (new ConnectionHistory(LastMotorSpeed, 
                                       LastFirmware, 
                                       SchedulerInUse, 
                                       LastLightState, 
                                       EverCalibrated, 
                                       ConnectionCount));

        void GetConnectionHistory()
        {
            try
            {
                var convertedConnectionHistory = JsonConvert.DeserializeObject<ConnectionHistory>(SettingsModule.ConnectionHistory);
                if (convertedConnectionHistory != null)
                    ConnectionCount = convertedConnectionHistory.ConnectionCount;
                else
                    ConnectionCount = 0;
            }
            catch
            {
                ConnectionCount = 0;
                Debug.WriteLine("Connection Count catch");

            }
        }

        private int _LastMotorSpeed;
        public int LastMotorSpeed
        {
            get
            {
                return this._LastMotorSpeed;
            }
            set
            {
                this.SetProperty(ref this._LastMotorSpeed, value);
            }
        }

        private int _LastFirmware;
        public int LastFirmware
        {
            get
            {
                return this._LastFirmware;
            }
            set
            {
                this.SetProperty(ref this._LastFirmware, value);
            }
        }

        private int _SchedulerInUse;
        public int SchedulerInUse
        {
            get
            {
                return this._SchedulerInUse;
            }
            set
            {
                this.SetProperty(ref this._SchedulerInUse, value);
            }
        }


        private int _LastLightState;
        public int LastLightState
        {
            get
            {
                return this._LastLightState;
            }
            set
            {
                this.SetProperty(ref this._LastLightState, value);
            }
        }

        private int _EverCalibrated;
        public int EverCalibrated
        {
            get
            {
                return this._EverCalibrated;
            }
            set
            {
                this.SetProperty(ref this._EverCalibrated, value);
            }
        }

        private int _ConnectionCount;
        public int ConnectionCount
        {
            get
            {
                return this._ConnectionCount;
            }
            set
            {
                this.SetProperty(ref this._ConnectionCount, value);
            }
        }

        #endregion

        bool _isUsingBasicScheduling;
        public bool IsUsingBasicScheduling
        {
            get => _isUsingBasicScheduling;
            set
            {
                _isUsingBasicScheduling = value;
                RaisePropertyChanged(nameof(IsUsingBasicScheduling));
                RaisePropertyChanged(nameof(IsNightLightEnabled));
                RaisePropertyChanged(nameof(ShowMainControl));
                RaisePropertyChanged(nameof(ControlSelectionRowHeight));
            }
        }
        
        AudioLevelModule _audioLevelModule;
        public AudioLevelModule AudioLevelModule
        {
            get => _audioLevelModule;
            set => SetProperty(ref _audioLevelModule, value);
        }

        ushort _motorSpeed;
        public ushort MotorSpeed
        {
            get =>_motorSpeed;
            set
            {
                SetProperty(ref _motorSpeed, value);
                RaisePropertyChanged(nameof(CurrentMotorLevel));
                RaisePropertyChanged(nameof(BackgroundOffset));
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }

        bool _motorEnabled;
        public bool MotorEnabled
        {
            get => _motorEnabled;
            set
            {
                SetProperty(ref _motorEnabled, value);
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }

        bool _fadeInNotification;
        public bool FadeInNotification
        {
            get => _fadeInNotification;
            set => SetProperty(ref _fadeInNotification, value);
        }

        public int CurrentMotorLevel => (MotorSpeed / NumMotorLevelGradations) <= 10
                                      ? (MotorSpeed / NumMotorLevelGradations)
                                      : NumMotorLevelGradations;

        public double BackgroundOffset => -50.0 + CurrentMotorLevel * 5.0;

        public bool SlowerEnabled => MotorEnabled && CurrentMotorLevel > MinMotorLevel;
        public bool FasterEnabled => MotorEnabled && CurrentMotorLevel < MaxMotorLevel;

        public int BabyMonitorColumn => IsNightLightEnabled ? 2 : 1;
        public int SupportColumn => IsNightLightEnabled ? 3 : 2;
        public int TimerColumn => IsNightLightEnabled ? 1 : 0;
        public bool IsNightLightEnabled => (ActiveControl != ControlType.Timer || IsUsingBasicScheduling) &&
                                           (BluetoothModule.ConnectedSnoozHasNightLight || FirmwareVersion == FirmwareVersions.V3);
        public FirmwareVersions FirmwareVersion => BluetoothModule.FirmwareVersion;

        LightState _nightLightState;
        public LightState NightLightState
        {
            get => _nightLightState;
            set
            {
                _nightLightState = value;
                RaisePropertyChanged(nameof(NightLightState));
                RaisePropertyChanged(nameof(DarkModeOn));
            }
        }

        public bool DarkModeOn
        {
            get => NightLightState == LightState.DarkMode;
            set
            {
                if (value && NightLightState != LightState.DarkMode)
                {
                    SetNightLightLevel(LightState.DarkMode);
                }
                else if (!value && NightLightState != LightState.Off)
                {
                    SetNightLightLevel(LightState.Off);
                }
            }
        }

        public int NumberOfActiveTimers => (IsStartTimerActive ? 1 : 0) +
                                           (IsStopTimerActive ? 1 : 0);

        public Night CurrentActiveNight
        {
            get
            {
                // the current timer is either one occuring right now
                // or the next scheduled one, within a day.
                var currentNight = new Night { Start = new EndPoint { Enabled = false }, Stop = new EndPoint { Enabled = false } };
                var today = NightNames.IndexOf(DateTime.Today.DayOfWeek.ToString());
                var nextDay = today == 6 ? 0 : today + 1;
                var timer = Timers.FirstOrDefault(t => t.Enabled &&
                                                  t.WeekDays[today].IsOn &&
                                                  ((t.StartEnabled &&
                                                    DateTime.Today + t.StartTime > DateTime.Now &&
                                                    DateTime.Today + t.StartTime <= DateTime.Now.AddDays(1)) ||
                                                   (t.StopEnabled &&
                                                    DateTime.Today + t.StopTime > DateTime.Now &&
                                                    DateTime.Today + t.StopTime <= DateTime.Now.AddDays(1))));
                if (timer == null)
                {
                    //   Debug.WriteLine($"today = {today.ToString()}");
                    timer = Timers.FirstOrDefault(t => t.Enabled &&
                                                  t.WeekDays[nextDay].IsOn &&
                                                  ((t.StartEnabled &&
                                                    DateTime.Today + t.StartTime > DateTime.Now &&
                                                    DateTime.Today + t.StartTime <= DateTime.Now.AddDays(1)) ||
                                                   (t.StopEnabled &&
                                                    DateTime.Today + t.StopTime > DateTime.Now &&
                                                    DateTime.Today + t.StopTime <= DateTime.Now.AddDays(1))));
                }

                if (timer != null)
                {
                    currentNight.Start = new EndPoint { Enabled = timer.StartEnabled, Time = timer.StartTime };
                    currentNight.Stop = new EndPoint { Enabled = timer.StopEnabled, Time = timer.StopTime };
                }

                return currentNight;
            }
        }

        public string CurrentNightDisplay => CurrentActiveNight.Display;

        public static readonly List<string> NightNames = new List<string>
        {
            "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
        };

        public bool IsStartTimerActive => CurrentActiveNight.Start.Enabled;

        public bool IsStopTimerActive => CurrentActiveNight.Stop.Enabled;

        float _decibelLevel;
        public float DecibelLevel
        {
            get => _decibelLevel;
            set => SetProperty(ref _decibelLevel, value);
        }

        bool UserEnabledTimer { get; set; }
        public bool TimerUIDisabled => !UserEnabledTimer &&
                                       !(Timers?.Any(t => t.StartEnabled || t.StopEnabled) ?? false) &&
                                       ActiveControl == ControlType.Timer;

        public bool ShowMainControl => ActiveControl != ControlType.Timer || IsUsingBasicScheduling;

        int _newFadeInValue;
        int _fadeIn;
        public string FadeIn
        {
            get => GetFadeText(_fadeIn);
            set => _newFadeInValue = GetFadeValue(value);
        }

        int _newFadeOutValue;
        int _fadeOut;
        public string FadeOut
        {
            get => GetFadeText(_fadeOut);
            set => _newFadeOutValue = GetFadeValue(value);
        }

        ControlType _activeControl;
        public ControlType ActiveControl
        {
            get => _activeControl;
            set
            {
                SetProperty(ref _activeControl, value);
                RaisePropertyChanged(nameof(IsNightLightEnabled));
                RaisePropertyChanged(nameof(ControlSelectionRowHeight));
            }
        }

        bool _scheduleChanged;
        public bool ScheduleChanged
        {
            get => _scheduleChanged;
            set => SetProperty(ref _scheduleChanged, value);
        }

        public GridLength ControlSelectionRowHeight => ActiveControl == ControlType.Timer && !IsUsingBasicScheduling
                                                       ? new GridLength(0) : new GridLength(100, GridUnitType.Star);

        public GridLength FadeRowHeight => FirmwareVersion == FirmwareVersions.V3
                                           ? new GridLength(15, GridUnitType.Star) : new GridLength(0);

        #region Commands

        public ICommand ToggleSchedulingCommand => new Command(ToggleSchedulingMode);
        public ICommand ShowHelpCommand => new Command(ShowHelp);
        public ICommand AddNewTimerCommand => new Command(AddNewTimer);
        public ICommand RemoveTimerCommand => new Command<TimerModel>(RemoveTimer);
        public ICommand ToggleStartTimerCommand => new Command<TimerModel>(item => ToggleTimer(TimerType.Start, item));
        public ICommand ToggleStopTimerCommand => new Command<TimerModel>(item => ToggleTimer(TimerType.Stop, item));
        public ICommand ToggleWeekDayCommand => new Command<WeekDayModel>(ToggleWeekDay);
        public ICommand ToggleBasicStartTimerCommand => new Command(() => ToggleBasicTimerEnabled(ref _startEnabled, nameof(StartEnabled)));
        public ICommand ToggleBasicStopTimerCommand => new Command(() => ToggleBasicTimerEnabled(ref _stopEnabled, nameof(StopEnabled)));
        public ICommand PowerButtonPress => new Command(MotorPower);
        public ICommand FasterButtonPress => new Command(MotorFaster);
        public ICommand SlowerButtonPress => new Command(MotorSlower);
        public ICommand ControlSelectorButtonPress => new Command(SetActiveControl);
        public ICommand SetNightLightLevelCommand => new Command(SetNightLightLevel);
        public ICommand CalibrateButtonPress => CalibrateCommand;
        public ICommand DevicesPageButtonPress => DevicesCommand;
        public ICommand ToggleDarkModeCommand => new Command(() => DarkModeOn = !DarkModeOn);

        #endregion

        public MainViewModelV3(INavigationService navigationService) : base(navigationService)
        {
            Debug.WriteLine("MainViewModelV3 Ctor");
            AudioLevelModule = App.AudioLevelModule;
        }

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            ActiveControl = ControlType.None;

            Debug.WriteLine("MainViewModelV3 OnNavigatedTo");
            BluetoothModule.SnoozDisconnected += BluetoothModule_SnoozDisconnected;

            BluetoothModule.SnoozStatusUpdated += BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.UpdateStatus);

            BluetoothModule.SnoozTimerOffScheduleUpdate += BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.SnoozTimerOnScheduleUpdate += BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.WriteSyncTimeCommand();
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.ScheduleRequest);

            FadeInNotification = true;
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.FadeRequest);
            BluetoothModule.SnoozFadeUpdate += BluetoothModule_SnoozFadeTimersUpdate;

            RaisePropertyChanged(nameof(FadeInNotification));
            RaisePropertyChanged(nameof(IsNightLightEnabled));
            RaisePropertyChanged(nameof(TimerColumn));
            RaisePropertyChanged(nameof(BabyMonitorColumn));
            RaisePropertyChanged(nameof(SupportColumn));
            RaisePropertyChanged(nameof(FirmwareVersion));
            RaisePropertyChanged(nameof(ActiveControl));
            RaisePropertyChanged(nameof(ShowMainControl));
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("MainViewModelV3 OnNavigatedFrom");
            BluetoothModule.SnoozStatusUpdated -= BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected -= BluetoothModule_SnoozDisconnected;
            BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;

            FadeInNotification = true;
            BluetoothModule.SnoozFadeUpdate -= BluetoothModule_SnoozFadeTimersUpdate;
            ActiveControl = ControlType.None;
            CurrentStateData();
        }

        #endregion

        void FadeTimersUpdated()
        {
            RaisePropertyChanged(nameof(FadeIn));
            RaisePropertyChanged(nameof(FadeOut));
        }

        void MotorPower()
            => BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled,
                                                          MotorEnabled ? (byte) 0 : (byte) 1);

        void MotorFaster()
        {
            if (MotorEnabled)
            {
                ushort motorSpeed;
                if (MotorSpeed <= 90)
                {
                    motorSpeed = (ushort) (MotorSpeed + 10);
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }
                else
                {
                    motorSpeed = 100;
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }

                _motorSpeed = motorSpeed;
                UpdateFadeValues();
            }
            else
                BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
        }

        void MotorSlower()
        {
            if (MotorEnabled)
            {
                ushort motorSpeed;
                if (MotorSpeed >= 20)
                {
                    motorSpeed = (ushort) (MotorSpeed - 10);
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }
                else
                {
                    motorSpeed = 10;
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }

                _motorSpeed = motorSpeed;
                UpdateFadeValues();
            }
            else
                BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
        }

        void UpdateFadeValues()
        {
            Debug.WriteLine("UpdateFadeValues()");
            if (_fadeIn == FadeTime.AUTO)
            {
                _newFadeInValue = GetFadeValue(FadeTime.AUTO_TEXT);
                BluetoothModule.WriteFadeOnCommand(true, (uint)_newFadeInValue);
            }

            if (_fadeOut == FadeTime.AUTO)
            {
                _newFadeOutValue = GetFadeValue(FadeTime.AUTO_TEXT);
                BluetoothModule.WriteFadeOffCommand(true, (uint)_newFadeOutValue);
            }
        }

        void SetNightLightLevel(object obj)
        {
            if (obj is LightState state)
            {
                var lightLevel1 = OffLightLevel;
                var enabled = true;

                switch (state)
                {
                    case LightState.Low:
                        lightLevel1 = LowLightLevel;
                        break;
                    case LightState.Medium:
                        lightLevel1 = MediumLightLevel;
                        break;
                    case LightState.High:
                        lightLevel1 = HighLightLevel;
                        break;
                    case LightState.DarkMode:
                        break;
                    default:
                        enabled = false;
                        break;
                }

                if (FirmwareVersion >= FirmwareVersions.V3)
                    BluetoothModule.WriteNightlightCommand(enabled, (byte) lightLevel1,
                        (byte) (lightLevel1 == OffLightLevel
                            ? lightLevel1
                            : lightLevel1 + LightLevelIncrement));
                else
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.BottomLed, (byte) lightLevel1);
            }
        }

        void SetActiveControl(object obj)
        {
            if (obj is ControlType newControlType)
            {
                if (ActiveControl == newControlType)
                    ActiveControl = ControlType.None;
                else
                {
                    switch (newControlType)
                    {
                        case ControlType.BabyMonitor when !AudioLevelModule.Calibrated:
                            CalibrateCommand.Execute(null);
                            break;
                        case ControlType.Timer:
                            UserEnabledTimer = false;
                            break;
                        case ControlType.Support:
                            HelpIndexCommand.Execute(null);
                            break;
                    }

                    ActiveControl = newControlType;
                }

                RaisePropertyChanged(nameof(ActiveControl));
                RaisePropertyChanged(nameof(ShowMainControl));
                RaisePropertyChanged(nameof(TimerUIDisabled));
            }
        }

        void SetNightLightUi(int lightLevel1, bool enabled = false, bool enabledApp = false)
            => NightLightState = enabled
                ? lightLevel1 == 0 
                    ? LightState.DarkMode
                    : lightLevel1 <= LowLightLevel
                        ? LightState.Low
                        : lightLevel1 <= MediumLightLevel
                            ? LightState.Medium
                            : LightState.High
                : enabledApp && lightLevel1 == 0
                    ? LightState.DarkMode
                    : LightState.Off;

        void BluetoothModule_SnoozStatusUpdated(object sender, SnoozStatusUpdatedEventArgs e)
        {
            Debug.WriteLine("MainViewModelV3 SnoozStatusUpdated");
            MotorSpeed = e.StatusUpdate.MotorSpeed;
            MotorEnabled = e.StatusUpdate.MotorEnabled;
            SetNightLightUi(e.StatusUpdate.NightlightLevel1, 
                            e.StatusUpdate.NightlightEnabledSnooz, 
                            e.StatusUpdate.NightlightEnabledApp);

            RaisePropertyChanged(nameof(CurrentActiveNight));
            RaisePropertyChanged(nameof(CurrentNightDisplay));
            RaisePropertyChanged(nameof(NumberOfActiveTimers));
            RaisePropertyChanged(nameof(IsStartTimerActive));
            RaisePropertyChanged(nameof(IsStopTimerActive));
        }

        void BluetoothModule_SnoozDisconnected(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("MainViewModelV3 SnoozDisconnected");

            // If the app is closing don't go to the connection page yet.
            // Wait for the OnResume to open the connection page
            if (!App.ClosingTime)
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await NavigationService.GoBackAsync();
                    await NavigationService.NavigateAsync(nameof(ConnectionPage));
                });
        }

        void GetTimes(ScheduleResponseFrameObject scheduleResponseFrame)
        {
            try
            {
                var timersEnabled = Enumerable.Range(0, 7)
                                              .Select(i => (scheduleResponseFrame.TimerEnableMask
                                                            >> i & 0x01) == 0x01)
                                              .ToList();
                var timers = scheduleResponseFrame.Timers
                                                  .Select(t => TimeSpan.FromMinutes(t))
                                                  .ToList();
                var newTimes = timers.Zip(timersEnabled,
                                         (timer, enabled) => new EndPoint
                                         {
                                             Time = timer,
                                             Enabled = enabled
                                         }).ToList();

                switch (scheduleResponseFrame.Command)
                {
                    case BluegigaResponseCommand.SendOffSchedule:
                        _stopTimes = newTimes;
                        break;
                    case BluegigaResponseCommand.SendOnSchedule:
                        _startTimes = newTimes;
                        break;
                }
                
                if (_startTimes == null || _stopTimes == null) 
                    return;
            
                BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
                BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
                MakeTimers();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"MainViewModelV3.GetTimes exception: {ex}");
            }
        }
        
        void MakeTimers(bool wasToggled = false)
        {
            Debug.WriteLine("MakeTimers()");
            try
            {
                var nights = MakeNights();
                var timers = nights.ToTimers(VerifyTimerDelta).ToList();
                
                Debug.WriteLine($"{nameof(_fadeIn)} = {_fadeIn} {nameof(_fadeOut)} = {_fadeOut} {nameof(FadeIn)} = {FadeIn} {nameof(FadeOut)} = {FadeOut}");    
                
                if (_fadeIn == FadeTime.OFF
                    && _fadeOut == FadeTime.OFF
                    && !nights.Any(n => n.Enabled))
                {
                    Debug.WriteLine("New Device, basic scheduling.");
                    SetBasicScheduling();
                    Timers = new ObservableCollection<TimerModel>();
                }
                else
                {
                    if (_fadeIn == FadeTime.BASIC
                        && _fadeOut == FadeTime.BASIC)
                    {
                        Debug.WriteLine("Old Device, basic scheduling.");
                        SetBasicScheduling();
                        Timers = new ObservableCollection<TimerModel>(timers);

                        if (!wasToggled)
                        {
                            if (timers.FirstOrDefault(t => t.StartEnabled) is TimerModel startTimer)
                                _startTime = startTimer.StartTime;
                            else
                                _startTime = new TimeSpan(22, 0, 0);

                            if (timers.FirstOrDefault(t => t.StopEnabled) is TimerModel stopTimer)
                                _stopTime = stopTimer.StopTime;
                            else
                                _stopTime = new TimeSpan(6, 0, 0);

                            _startEnabled = timers.Any(t => t.StartEnabled);
                            _stopEnabled = timers.Any(t => t.StopEnabled);
                        }
                        else if (LoadBasicScheduleFromLocalStorage() is BasicScheduleBackup backup
                                 && backup.Timers != null)
                        {
                            _startEnabled = backup.StartEnabled;
                            _stopEnabled = backup.StopEnabled;
                            _startTime = backup.StartTime;
                            _stopTime = backup.StopTime;
                            Timers = new ObservableCollection<TimerModel>(backup.Timers);
                        }

                        RaisePropertyChanged(nameof(StartEnabled));
                        RaisePropertyChanged(nameof(StopEnabled));
                        RaisePropertyChanged(nameof(StartTime));
                        RaisePropertyChanged(nameof(StopTime));

                        SaveBasicScheduleToLocalStorage();
                    }
                    else
                    {
                        Debug.WriteLine("Old Device, advanced scheduling.");
                        _isUsingBasicScheduling = false;

                        var allLocalTimers = LoadScheduleFromLocalStorage();
                        Debug.WriteLine($"Snooz Timers = Local Timers ? {allLocalTimers?.Timers?.ToNights().SequenceEqual(nights)}");
                        var localSameAsSnooz = allLocalTimers?.Timers != null && allLocalTimers.Timers.ToNights().SequenceEqual(nights);
                        var wasToggledHasLocal = wasToggled && allLocalTimers?.Timers != null && allLocalTimers.Timers.Any();
                        var wasToggledNoLocal = wasToggled && (allLocalTimers?.Timers == null || !allLocalTimers.Timers.Any());

                        if (localSameAsSnooz || wasToggledHasLocal)
                            Timers = new ObservableCollection<TimerModel>(allLocalTimers.Timers);
                        else if (wasToggledNoLocal)
                            Timers = new ObservableCollection<TimerModel>();
                        else
                            Timers = new ObservableCollection<TimerModel>(timers);

                        UpdateTimersNumbers();
                        SaveScheduleToLocalStorage();
                    }
                }

                if (!Timers.Any())
                {
                    _backupTimers = null;
                    AddNewTimer();
                }

                ScheduleChanged = false;

                RaisePropertyChanged(nameof(Timers));
                RaisePropertyChanged(nameof(TimerUIDisabled));
                RaisePropertyChanged(nameof(CurrentActiveNight));
                RaisePropertyChanged(nameof(CurrentNightDisplay));
                RaisePropertyChanged(nameof(IsStartTimerActive));
                RaisePropertyChanged(nameof(IsStopTimerActive));
                RaisePropertyChanged(nameof(NumberOfActiveTimers));
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"MainViewModelV3.MakeTimers exception: {ex}");
            }
        }

        void BluetoothModule_SnoozTimerScheduleUpdate(object sender, ScheduleResponseFrameObject scheduleResponseFrame)
        {
            Debug.WriteLine("MainViewModelV3 BluetoothModule_SnoozTimerScheduleUpdate");
            GetTimes(scheduleResponseFrame);
        }

        Night[] MakeNights()
        {
            var nights = new List<Night>();
            for (var i = 0; i < 7; i++)
            {
                nights.Add(new Night
                {
                    Start = new EndPoint { Enabled = _startTimes[i].Enabled, Time = new TimeSpan(_startTimes[i].Time.Hours, _startTimes[i].Time.Minutes, 0) },
                    Stop = new EndPoint { Enabled = _stopTimes[i].Enabled, Time = new TimeSpan(_stopTimes[i].Time.Hours, _stopTimes[i].Time.Minutes, 0) }
                });
            }

            return nights.ToArray();
        }

        int GetFadeValue(string fadeTimeText)
        {
            switch (fadeTimeText)
            {
                case FadeTime.OFF_TEXT:
                    return FadeTime.OFF;
                case FadeTime.BASIC_TEXT:
                    return FadeTime.BASIC;
                case FadeTime.THIRTY_SEC_TEXT:
                    return FadeTime.THIRTY_SEC;
                case FadeTime.ONE_MIN_TEXT:
                    return FadeTime.ONE_MIN;
                case FadeTime.FIVE_MIN_TEXT:
                    return FadeTime.FIVE_MIN;
                default:
                    if (CurrentMotorLevel == 0) //To Do: Check if this is needed
                        return (int)Math.Ceiling(5 / FadeTime.MOTOR_SPEED_RATIO);
                    return (int)Math.Ceiling(CurrentMotorLevel / FadeTime.MOTOR_SPEED_RATIO);
            }
        }

        static string GetFadeText(int fadeTimeValue)
        {
            switch (fadeTimeValue)
            {
                case FadeTime.OFF:
                    return FadeTime.OFF_TEXT;
                case FadeTime.BASIC:
                    return FadeTime.BASIC_TEXT;
                case FadeTime.THIRTY_SEC:
                    return FadeTime.THIRTY_SEC_TEXT;
                case FadeTime.ONE_MIN:
                    return FadeTime.ONE_MIN_TEXT;
                case FadeTime.FIVE_MIN:
                    return FadeTime.FIVE_MIN_TEXT;
                default:
                    return FadeTime.AUTO_TEXT;
            }
        }

        static int GetFadeNumber(uint fadeTimeValue)
        {
            if (fadeTimeValue != FadeTime.OFF &&
                fadeTimeValue != FadeTime.BASIC &&
                fadeTimeValue != FadeTime.THIRTY_SEC &&
                fadeTimeValue != FadeTime.ONE_MIN &&
                fadeTimeValue != FadeTime.FIVE_MIN)
                return FadeTime.AUTO;

            return (int)fadeTimeValue;
        }

        void BluetoothModule_SnoozFadeTimersUpdate(object sender, FadeResponseFrameObject responseFrame)
        {
            Debug.WriteLine("MainViewModelV3 BluetoothModule_SnoozFadeTimersUpdate");
            BluetoothModule.SnoozFadeUpdate -= BluetoothModule_SnoozFadeTimersUpdate;

            _fadeIn = _newFadeInValue = GetFadeValue(GetFadeText(GetFadeNumber(responseFrame.FadeOnTime)));
            _fadeOut = _newFadeOutValue = GetFadeValue(GetFadeText(GetFadeNumber(responseFrame.FadeOffTime)));
            FadeTimersUpdated();
            Debug.WriteLine($"{nameof(_fadeIn)} = {_fadeIn} {nameof(_fadeOut)} = {_fadeOut} {nameof(FadeIn)} = {FadeIn} {nameof(FadeOut)} = {FadeOut}");
        }

        static void ShowHelp()
            => Application.Current.MainPage.DisplayAlert("Volume Fade Control",
                                                         "Fade works with the scheduler to turn SNOOZ on/off slowly. Select a fade time or choose auto, which adjusts the fade between 20s-3min, depending on the set volume level.",
                                                         "OK");

        void WriteSchedule(bool shouldCloseScheduler = true)
        {
            Debug.WriteLine("WriteSchedule()");

            try
            {
                ScheduleChanged = false;
                BluetoothModule.WriteSchedule(Timers.ToNights(toWriteOnDevice: true));

                if (!IsUsingBasicScheduling)
                {
                    foreach (var timerModel in Timers)
                        if (timerModel.StartEnabled
                            && timerModel.StopEnabled
                            && ShouldDisableFade(timerModel.StartTime, timerModel.StopTime))
                        {
                            _newFadeOutValue = _newFadeInValue = FadeTime.OFF;
                            break;
                        }
                }
                else
                {
                    _newFadeOutValue = _newFadeInValue = FadeTime.BASIC;
                }

                Debug.WriteLine(
                    $"{nameof(_fadeIn)} = {_fadeIn} {nameof(_newFadeInValue)} = {_newFadeInValue} {nameof(FadeIn)} = {FadeIn}");
                // Set FadeIn
                BluetoothModule.WriteFadeOnCommand(true, (uint) _newFadeInValue);

                Debug.WriteLine(
                    $"{nameof(_fadeOut)} = {_fadeOut} {nameof(_newFadeOutValue)} = {_newFadeOutValue} {nameof(FadeOut)} = {FadeOut}");
                // Set FadeOut
                BluetoothModule.WriteFadeOffCommand(true, (uint) _newFadeOutValue);

                if (!IsUsingBasicScheduling)
                    SaveScheduleToLocalStorage();
                else
                    SaveBasicScheduleToLocalStorage();

                // Goes back to main control if advanced scheduling
                if (!IsUsingBasicScheduling && shouldCloseScheduler)
                    SetActiveControl(ControlType.Timer);

                RaisePropertyChanged(nameof(Timers));
                RaisePropertyChanged(nameof(TimerUIDisabled));
                RaisePropertyChanged(nameof(CurrentActiveNight));
                RaisePropertyChanged(nameof(CurrentNightDisplay));
                RaisePropertyChanged(nameof(NumberOfActiveTimers));
                RaisePropertyChanged(nameof(IsStartTimerActive));
                RaisePropertyChanged(nameof(IsStopTimerActive));
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"WriteSchedule exception: {ex}");
            }
        }

        static bool ShouldDisableFade(TimeSpan start, TimeSpan stop)
        {
            if (start == stop)
                return false;

            var difference = new TimeSpan(0, 30, 0);
            var max = start > stop ? start : stop;
            var min = max == start ? stop : start;

            if (max - min >= difference)
            {
                Debug.WriteLine("Enable Fade");
                return false;
            }

            Debug.WriteLine("Disable Fade");
            return true;
        }

        void ToggleSchedulingMode()
        {
            if (IsUsingBasicScheduling)
            {
                SaveBasicScheduleToLocalStorage();
                SetAdvancedScheduling();
            }
            else
            {
                SaveScheduleToLocalStorage();
                SetBasicScheduling();
            }

            Debug.WriteLine($"Toggled Scheduling To {(IsUsingBasicScheduling ? "Basic" : "Advanced")}");
            RaisePropertyChanged(nameof(ControlSelectionRowHeight));

            Task.Run(() =>
            {
                MakeTimers(wasToggled: true);
                ScheduleChanged = true;
                if (IsUsingBasicScheduling)
                    WriteBasicSchedule();
                else
                    WriteSchedule(shouldCloseScheduler: false);
            });
        }

        #region Basic Scheduling

        const string Stop = "Stop";
        const string StopNextDay = "Stop (Next Day)";

        public string StopDisplay => StartEnabled && StopEnabled 
                                     ? (StartTime >= StopTime ? StopNextDay : Stop) 
                                     : Stop;

        bool _startEnabled;
        public bool StartEnabled
        {
            get => _startEnabled;
            set
            {
                Debug.WriteLine("StartEnabled.Set");
                SetProperty(ref _startEnabled, value);
                RaisePropertyChanged(nameof(StopDisplay));
            }
        }

        bool _stopEnabled;
        public bool StopEnabled
        {
            get => _stopEnabled;
            set
            {
                Debug.WriteLine("StopEnabled.Set");
                SetProperty(ref _stopEnabled, value);
                RaisePropertyChanged(nameof(StopDisplay));
            }
        }

        TimeSpan _startTime = new TimeSpan(22, 0, 0);
        public TimeSpan StartTime
        {
            get => _startTime;
            set
            {
                Debug.WriteLine("StartTime.Set");
                SetProperty(ref _startTime, value);
                Task.Run(() => WriteBasicSchedule());
            }
        }

        TimeSpan _stopTime = new TimeSpan(6, 0, 0);
        public TimeSpan StopTime
        {
            get => _stopTime;
            set
            {
                Debug.WriteLine("StopTime.Set");
                SetProperty(ref _stopTime, value);
                RaisePropertyChanged(nameof(StopDisplay));
                Task.Run(() => WriteBasicSchedule());
            }
        }

        void ToggleBasicTimerEnabled(ref bool toggleProperty, string propertyName)
        {
            toggleProperty = !toggleProperty;
            RaisePropertyChanged(propertyName);

            ScheduleChanged = true;
            WriteBasicSchedule();
        }

        void SetBasicScheduling()
        {
            IsUsingBasicScheduling = true;
            _fadeIn = _newFadeInValue = FadeTime.BASIC;
            _fadeOut = _newFadeOutValue = FadeTime.BASIC;
        }

        void WriteBasicSchedule()
        {
            if (!ScheduleChanged) return;

            Debug.WriteLine("WriteBasicSchedule()");

            foreach (var timer in Timers)
            {
                timer.StartEnabled = StartEnabled;
                timer.StopEnabled = StopEnabled;
                timer.StartTime = StartTime;
                timer.StopTime = StopTime;

                if (StartEnabled || StopEnabled) timer.WeekDays.ForEach(d => d.IsOn = true);
            }

            WriteSchedule();
        }

        void SaveBasicScheduleToLocalStorage()
        {
            var saveJson = JsonConvert.SerializeObject(new BasicScheduleBackup
            {
                Timers = Timers.ToArray(),
                StartEnabled = StartEnabled,
                StopEnabled = StopEnabled,
                StartTime = StartTime,
                StopTime = StopTime
            }, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });
            
            SettingsModule.SavedBasicSchedule = saveJson;
        }

        static BasicScheduleBackup LoadBasicScheduleFromLocalStorage()
        {
            var json = SettingsModule.SavedBasicSchedule;
            if (string.IsNullOrWhiteSpace(json) || json == "null")
                return null;
            
            return JsonConvert.DeserializeObject<BasicScheduleBackup>(json);
        }

        #endregion

        #region Advanced Scheduling

        ScheduleBackup LoadScheduleFromLocalStorage()
        {
            var json = SettingsModule.SavedAdvancedSchedule;
            if (string.IsNullOrWhiteSpace(json) || json == "null")
                return null;
            
            return JsonConvert.DeserializeObject<ScheduleBackup>(
                json, new TimerJsonConverter(VerifyTimerDelta));
        }

        void SaveScheduleToLocalStorage()
        {
            var saveJson = JsonConvert.SerializeObject(new ScheduleBackup
            {
                Timers = Timers.ToArray(),
                FadeInValue = _newFadeInValue,
                FadeOutValue = _newFadeOutValue
            }, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });
            
            SettingsModule.SavedAdvancedSchedule = saveJson;
        }

        void SetAdvancedScheduling()
        {
            IsUsingBasicScheduling = false;
            _fadeIn = _newFadeInValue = FadeTime.AUTO;
            _fadeOut = _newFadeOutValue = FadeTime.AUTO;
        }

        public bool CanAddTimer => Timers.Count < 7;

        ObservableCollection<TimerModel> _timers = new ObservableCollection<TimerModel>();
        public ObservableCollection<TimerModel> Timers
        {
            get => _timers;
            set
            {
                if (_timers != null)
                    _timers.CollectionChanged -= TimersCollectionChanged;

                SetProperty(ref _timers, value);
                _timers.CollectionChanged += TimersCollectionChanged;
            }
        }

        public ICommand WriteScheduleCommand => new Command(() => WriteSchedule());

        bool _isAddingTimer;
        void AddNewTimer()
        {
            if (_isAddingTimer) 
                return;

            _isAddingTimer = true;
            if (Timers.Count < 7)
                Device.BeginInvokeOnMainThread(() =>
                {
                    Timers.Add(new TimerModel(VerifyTimerDelta));
                    _isAddingTimer = false;
                });
        }

        void RemoveTimer(TimerModel item)
        {
            Timers.Remove(item);
            if (!Timers.Any())
                AddNewTimer();
        }

        void ToggleWeekDay(WeekDayModel item)
        {
            item.IsOn = item.IsChecked = !item.IsOn;

            if (!(Timers.FirstOrDefault(t => t.WeekDays.Any(d => d.Id.Equals(item.Id))) is TimerModel parent)
                || !parent.Enabled)
                return;

            foreach (var timer in Timers.Where(t => !t.Id.Equals(parent.Id) && t.Enabled))
            {
                timer.WeekDays[item.WeekDayIndex].IsOn = false;
                timer.WeekDays[item.WeekDayIndex].IsChecked = false;
            }
        }

        void TimersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
            => UpdateTimersNumbers();

        void UpdateTimersNumbers()
        {
            Device.BeginInvokeOnMainThread(() => 
                Timers.ForEach(timer => timer.Name = $"Timer {Timers.Select(t => t.Id).ToList().IndexOf(timer.Id) + 1}"));
            RaisePropertyChanged(nameof(CanAddTimer));
        }

        void ToggleTimer(TimerType type, TimerModel item)
        {
            var hasDaysOn = false;
            switch (type)
            {
                case TimerType.Start:
                    item.StartEnabled = !item.StartEnabled;
                    hasDaysOn = item.StartEnabled && item.WeekDays.Any(d => d.IsOn);
                    break;
                case TimerType.Stop:
                    item.StopEnabled = !item.StopEnabled;
                    hasDaysOn = item.StopEnabled && item.WeekDays.Any(d => d.IsOn);
                    break;
            }

            VerifyTimerDelta(item);

            if (!hasDaysOn)
                return;

            for (var index = 0; index < 7; index++)
            {
                if (!item.WeekDays[index].IsOn)
                    continue;

                foreach (var timer in Timers.Where(t => !t.Id.Equals(item.Id) && t.Enabled))
                    timer.WeekDays[index].IsChecked = timer.WeekDays[index].IsOn = false;
            }
        }

        // Set fade to off if delta between start and stop timer is less than 30 minutes
        void VerifyTimerDelta(TimerModel item)
        {
            const double delta = 30;

            if (IsUsingBasicScheduling) return;
            if (!item.StartEnabled) return;
            if (!item.StopEnabled) return;
            if (item.StopTime > item.StartTime
                && (item.StopTime - item.StartTime).TotalMinutes >= delta) return;
            if (item.StartTime > item.StopTime
                && (item.StartTime - item.StopTime).TotalMinutes >= delta) return;

            _fadeIn = _newFadeInValue = _fadeOut = _newFadeOutValue = GetFadeValue(FadeTime.OFF_TEXT);
            FadeTimersUpdated();
        }

        #endregion
    }
}