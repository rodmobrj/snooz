﻿using System;
using SnoozApp.ViewModels;
using Xamarin.Forms;

namespace SnoozApp.Pages
{
    public partial class MainPageV1 : BasePage
    {
        public MainPageV1()
        {
            InitializeComponent();
			MessagingCenter.Subscribe<MainViewFadeModel>(this, App.FADE_HELP_MESSAGE, this.InfoButtonClicked);
		}

		private void OnV2StopTimeTapped(object sender, EventArgs e)
		{
			this.V2StopTimePicker.RequestEntryStart();
		}

		private void OnV2StartTimeTapped(object sender, EventArgs e)
		{
			this.V2StartTimePicker.RequestEntryStart();
		}

		private void InfoButtonClicked (object sender)
		{
			DisplayAlert("Fade Control", "Fade controls allow you to schedule your snooz to turn on and off slowly, over a selectable number of seconds", "OK");
		}

        protected override bool OnBackButtonPressed() => true;
	}
}
