﻿using SnoozApp.Modules;
using System;
using System.Linq;
using Xamarin.Forms;

namespace SnoozApp.Pages
{
    public partial class MainPageV3 : BasePage
    {
        public MainPageV3()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<MainViewFadeModel>(this, App.FADE_HELP_MESSAGE, InfoButtonClicked);
        }

        void InfoButtonClicked(object sender)
            => DisplayAlert("Volume Fade Control", 
                            "Fade allows you to schedule your snooz to turn on/off slowly. " +
                            "Select a fade time or choose auto, which adjusts the fade between 20 seconds to 3 minutes, " +
                            "depending on the volume level.", 
                            "OK");

        void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
            => TimersListView.SelectedItem = null;

        void OnAddNewTimerTapped(object sender, EventArgs e)
        {
            var cell = TimersListView.ItemsSource.Cast<TimerModel>().LastOrDefault();
            TimersListView.ScrollTo(cell, ScrollToPosition.MakeVisible, true);
        }

        void OnDarkModeInfoButtonTapped(object sender, EventArgs e)
            => DisplayAlert("Touchpad LED Control", 
                            "\r\nOff: (Factory Default) Touchpad lights turn on for 7 seconds after touched." +
                            "\r\n\nDark Mode: Keeps the touchpad lights off, always." +
                            "\r\n\n1, 2, 3: Brightness settings to use touchpad lights as a nightlight.", 
                            "OK");

        void SetScheduleChanged(bool value)
        {
            if (BindingContext is MainViewModelV3 viewModel)
                viewModel.ScheduleChanged = value;
        }

        void TimePicker_Unfocused_Basic(object sender, FocusEventArgs e)
            => SetScheduleChanged(false);

        void TimePicker_Focused_Basic(object sender, FocusEventArgs e)
            => SetScheduleChanged(true);

        protected override bool OnBackButtonPressed() => true;
    }
}
