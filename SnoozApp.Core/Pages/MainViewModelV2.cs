﻿using Newtonsoft.Json;
using Plugin.BLE.Abstractions.EventArgs;
using Prism.Navigation;
using SnoozApp.Enums;
using SnoozApp.Extensions;
using SnoozApp.Modules;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Pages
{
    public class MainViewModelV2 : BaseViewModel
    {
        const int OffLightLevel = 0;
        const int LowLightLevel = 20;
        const int MediumLightLevel = 50;
        const int HighLightLevel = 90;
        const int LightLevelIncrement = 10;
        const int NumMotorLevelGradations = 10;
        const int MinMotorLevel = 1;
        const int MaxMotorLevel = 10;

        TimerModel[] _backupTimers;
        List<EndPoint> _startTimes;
        List<EndPoint> _stopTimes;

        #region App Usage History Generator

        private void CurrentStateData()
        {
            GetConnectionHistory();

            //Get current motor speed
            this.LastMotorSpeed = CurrentMotorLevel;

            //Determine firmware version
            if (FirmwareVersion == FirmwareVersions.V2)
            {
                this.LastFirmware = 2;
            }
            else if (FirmwareVersion == FirmwareVersions.V3)
            {
                this.LastFirmware = 3;
            }

            //Count number of scheduled days

            if (_backupTimers == null)
            {
                this.SchedulerInUse = 0;
            }
            else
            {
                this.SchedulerInUse = 1;
            }

            //Determine Nightlight state
            if (MotorEnabled && NightLightState == LightState.DarkMode)
            {
                this.LastLightState = 1;
            }
            else if (MotorEnabled && NightLightState == LightState.Off)
            {
                this.LastLightState = 2;
            }
            else if (MotorEnabled && NightLightState == LightState.Low)
            {
                this.LastLightState = 3;
            }
            else if (MotorEnabled && NightLightState == LightState.Medium)
            {
                this.LastLightState = 4;
            }
            else if (MotorEnabled && NightLightState == LightState.High)
            {
                this.LastLightState = 5;
            }

            //Determine if nursery monitor calibrated
            if (AudioLevelModule.Calibrated == true)
            {
                this.EverCalibrated = 1;
            }
            else
            {
                this.EverCalibrated = 0;
            }

            //Calculate how many times app has been connected
            this.ConnectionCount++;

            SetConnectionHistory();
        }

        public class ConnectionHistory
        {
            public int LastMotorSpeed;
            public int LastFirmware;
            public int SchedulerInUse;
            public int LastLightState;
            public int EverCalibrated;
            public int ConnectionCount;


            public ConnectionHistory(int lastmotorspeed, int lastfirmware, int schedulerInUse, int lastlightstate, int evercalibrated, int connectioncount)
            {
                LastMotorSpeed = lastmotorspeed;
                LastFirmware = lastfirmware;
                SchedulerInUse = schedulerInUse;
                LastLightState = lastlightstate;
                EverCalibrated = evercalibrated;
                ConnectionCount = connectioncount;
            }
        }

        void SetConnectionHistory()
            => SettingsModule.ConnectionHistory = JsonConvert.SerializeObject
                (new ConnectionHistory(LastMotorSpeed, 
                                       LastFirmware, 
                                       SchedulerInUse, 
                                       LastLightState, 
                                       EverCalibrated, 
                                       ConnectionCount));

        void GetConnectionHistory()
        {
            try
            {
                var convertedConnectionHistory = JsonConvert.DeserializeObject<ConnectionHistory>(SettingsModule.ConnectionHistory);
                if (convertedConnectionHistory != null)
                    ConnectionCount = convertedConnectionHistory.ConnectionCount;
                else
                    ConnectionCount = 0;
            }
            catch
            {
                ConnectionCount = 0;
                Debug.WriteLine("Connection Count catch");

            }
        }

        private int _LastMotorSpeed;
        public int LastMotorSpeed
        {
            get
            {
                return this._LastMotorSpeed;
            }
            set
            {
                this.SetProperty(ref this._LastMotorSpeed, value);
            }
        }

        private int _LastFirmware;
        public int LastFirmware
        {
            get
            {
                return this._LastFirmware;
            }
            set
            {
                this.SetProperty(ref this._LastFirmware, value);
            }
        }

        private int _SchedulerInUse;
        public int SchedulerInUse
        {
            get
            {
                return this._SchedulerInUse;
            }
            set
            {
                this.SetProperty(ref this._SchedulerInUse, value);
            }
        }


        private int _LastLightState;
        public int LastLightState
        {
            get
            {
                return this._LastLightState;
            }
            set
            {
                this.SetProperty(ref this._LastLightState, value);
            }
        }

        private int _EverCalibrated;
        public int EverCalibrated
        {
            get
            {
                return this._EverCalibrated;
            }
            set
            {
                this.SetProperty(ref this._EverCalibrated, value);
            }
        }

        private int _ConnectionCount;
        public int ConnectionCount
        {
            get
            {
                return this._ConnectionCount;
            }
            set
            {
                this.SetProperty(ref this._ConnectionCount, value);
            }
        }

        #endregion
        
        AudioLevelModule _audioLevelModule;
        public AudioLevelModule AudioLevelModule
        {
            get => _audioLevelModule;
            set => SetProperty(ref _audioLevelModule, value);
        }

        ushort _motorSpeed;
        public ushort MotorSpeed
        {
            get =>_motorSpeed;
            set
            {
                SetProperty(ref _motorSpeed, value);
                RaisePropertyChanged(nameof(CurrentMotorLevel));
                RaisePropertyChanged(nameof(BackgroundOffset));
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }

        bool _motorEnabled;
        public bool MotorEnabled
        {
            get => _motorEnabled;
            set
            {
                SetProperty(ref _motorEnabled, value);
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }

        public int CurrentMotorLevel => (MotorSpeed / NumMotorLevelGradations) <= 10
                                      ? (MotorSpeed / NumMotorLevelGradations)
                                      : NumMotorLevelGradations;

        public double BackgroundOffset => -50.0 + CurrentMotorLevel * 5.0;

        public bool SlowerEnabled => MotorEnabled && CurrentMotorLevel > MinMotorLevel;
        public bool FasterEnabled => MotorEnabled && CurrentMotorLevel < MaxMotorLevel;

        public int BabyMonitorColumn => IsNightLightEnabled ? 2 : 1;
        public int SupportColumn => IsNightLightEnabled ? 3 : 2;
        public int TimerColumn => IsNightLightEnabled ? 1 : 0;
        public bool IsNightLightEnabled => ActiveControl != ControlType.Timer &&
                                           (BluetoothModule.ConnectedSnoozHasNightLight || FirmwareVersion == FirmwareVersions.V3);
        public FirmwareVersions FirmwareVersion => BluetoothModule.FirmwareVersion;

        LightState _nightLightState;
        public LightState NightLightState
        {
            get => _nightLightState;
            set
            {
                _nightLightState = value;
                RaisePropertyChanged(nameof(NightLightState));
                RaisePropertyChanged(nameof(DarkModeOn));
            }
        }

        public bool DarkModeOn
        {
            get => NightLightState == LightState.DarkMode;
            set
            {
                if (value && NightLightState != LightState.DarkMode)
                {
                    SetNightLightLevel(LightState.DarkMode);
                }
                else if (!value && NightLightState != LightState.Off)
                {
                    SetNightLightLevel(LightState.Off);
                }
            }
        }

        public int NumberOfActiveTimers => (IsStartTimerActive ? 1 : 0) +
                                           (IsStopTimerActive ? 1 : 0);

        public Night CurrentActiveNight
        {
            get
            {
                // the current timer is either one occuring right now
                // or the next scheduled one, within a day.
                var currentNight = new Night { Start = new EndPoint { Enabled = false }, Stop = new EndPoint { Enabled = false } };
                var today = NightNames.IndexOf(DateTime.Today.DayOfWeek.ToString());
                var nextDay = today == 6 ? 0 : today + 1;
                var timer = Timers.FirstOrDefault(t => t.Enabled &&
                                                  t.WeekDays[today].IsOn &&
                                                  ((t.StartEnabled &&
                                                    DateTime.Today + t.StartTime > DateTime.Now &&
                                                    DateTime.Today + t.StartTime <= DateTime.Now.AddDays(1)) ||
                                                   (t.StopEnabled &&
                                                    DateTime.Today + t.StopTime > DateTime.Now &&
                                                    DateTime.Today + t.StopTime <= DateTime.Now.AddDays(1))));
                if (timer == null)
                {
                    //   Debug.WriteLine($"today = {today.ToString()}");
                    timer = Timers.FirstOrDefault(t => t.Enabled &&
                                                  t.WeekDays[nextDay].IsOn &&
                                                  ((t.StartEnabled &&
                                                    DateTime.Today + t.StartTime > DateTime.Now &&
                                                    DateTime.Today + t.StartTime <= DateTime.Now.AddDays(1)) ||
                                                   (t.StopEnabled &&
                                                    DateTime.Today + t.StopTime > DateTime.Now &&
                                                    DateTime.Today + t.StopTime <= DateTime.Now.AddDays(1))));
                }

                if (timer != null)
                {
                    currentNight.Start = new EndPoint { Enabled = timer.StartEnabled, Time = timer.StartTime };
                    currentNight.Stop = new EndPoint { Enabled = timer.StopEnabled, Time = timer.StopTime };
                }

                return currentNight;
            }
        }

        public string CurrentNightDisplay => CurrentActiveNight.Display;

        public static readonly List<string> NightNames = new List<string>
        {
            "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
        };

        public bool IsStartTimerActive => CurrentActiveNight.Start.Enabled;

        public bool IsStopTimerActive => CurrentActiveNight.Stop.Enabled;

        float _decibelLevel;
        public float DecibelLevel
        {
            get => _decibelLevel;
            set => SetProperty(ref _decibelLevel, value);
        }

        bool UserEnabledTimer { get; set; }
        public bool TimerUIDisabled => !UserEnabledTimer &&
                                       !(Timers?.Any(t => t.StartEnabled || t.StopEnabled) ?? false) &&
                                       ActiveControl == ControlType.Timer;

        public bool ShowMainControl => ActiveControl != ControlType.Timer;

        ControlType _activeControl;
        public ControlType ActiveControl
        {
            get => _activeControl;
            set
            {
                SetProperty(ref _activeControl, value);
                RaisePropertyChanged(nameof(IsNightLightEnabled));
                RaisePropertyChanged(nameof(ControlSelectionRowHeight));
            }
        }

        bool _scheduleChanged;
        public bool ScheduleChanged
        {
            get => _scheduleChanged;
            set => SetProperty(ref _scheduleChanged, value);
        }

        public GridLength ControlSelectionRowHeight => ActiveControl == ControlType.Timer
                                                       ? new GridLength(0) : new GridLength(100, GridUnitType.Star);

        public GridLength FadeRowHeight => FirmwareVersion == FirmwareVersions.V3
                                           ? new GridLength(15, GridUnitType.Star) : new GridLength(0);

        #region Commands

        public ICommand ShowHelpCommand => new Command(ShowHelp);
        public ICommand AddNewTimerCommand => new Command(AddNewTimer);
        public ICommand RemoveTimerCommand => new Command<TimerModel>(RemoveTimer);
        public ICommand ToggleStartTimerCommand => new Command<TimerModel>(item => ToggleTimer(TimerType.Start, item));
        public ICommand ToggleStopTimerCommand => new Command<TimerModel>(item => ToggleTimer(TimerType.Stop, item));
        public ICommand ToggleWeekDayCommand => new Command<WeekDayModel>(ToggleWeekDay);
        public ICommand PowerButtonPress => new Command(MotorPower);
        public ICommand FasterButtonPress => new Command(MotorFaster);
        public ICommand SlowerButtonPress => new Command(MotorSlower);
        public ICommand ControlSelectorButtonPress => new Command(SetActiveControl);
        public ICommand SetNightLightLevelCommand => new Command(SetNightLightLevel);
        public ICommand CalibrateButtonPress => CalibrateCommand;
        public ICommand DevicesPageButtonPress => DevicesCommand;
        public ICommand ToggleDarkModeCommand => new Command(() => DarkModeOn = !DarkModeOn);

        #endregion

        public MainViewModelV2(INavigationService navigationService) : base(navigationService)
        {
            Debug.WriteLine("MainViewModelV2 Ctor");
            AudioLevelModule = App.AudioLevelModule;
        }

        #region Overrides

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            ActiveControl = ControlType.None;

            Debug.WriteLine("MainViewModelV2 OnNavigatedTo");
            BluetoothModule.SnoozStatusUpdated += BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected += BluetoothModule_SnoozDisconnected;
            BluetoothModule.SnoozTimerOffScheduleUpdate += BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.SnoozTimerOnScheduleUpdate += BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.UpdateStatus);

            if (FirmwareVersion >= FirmwareVersions.V2)
            {
                BluetoothModule.WriteSyncTimeCommand();
                BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.ScheduleRequest);
            }

            RaisePropertyChanged(nameof(IsNightLightEnabled));
            RaisePropertyChanged(nameof(TimerColumn));
            RaisePropertyChanged(nameof(BabyMonitorColumn));
            RaisePropertyChanged(nameof(SupportColumn));
            RaisePropertyChanged(nameof(FirmwareVersion));
            RaisePropertyChanged(nameof(ActiveControl));
            RaisePropertyChanged(nameof(ShowMainControl));
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("MainViewModelV2 OnNavigatedFrom");
            BluetoothModule.SnoozStatusUpdated -= BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected -= BluetoothModule_SnoozDisconnected;
            BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;

            ActiveControl = ControlType.None;
            CurrentStateData();
        }

        #endregion

        void MotorPower()
            => BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled,
                                                          MotorEnabled ? (byte) 0 : (byte) 1);

        void MotorFaster()
        {
            if (MotorEnabled)
            {
                ushort motorSpeed;
                if (MotorSpeed <= 90)
                {
                    motorSpeed = (ushort) (MotorSpeed + 10);
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }
                else
                {
                    motorSpeed = 100;
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }

                _motorSpeed = motorSpeed;
            }
            else
                BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
        }

        void MotorSlower()
        {
            if (MotorEnabled)
            {
                ushort motorSpeed;
                if (MotorSpeed >= 20)
                {
                    motorSpeed = (ushort) (MotorSpeed - 10);
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }
                else
                {
                    motorSpeed = 10;
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte) motorSpeed);
                }

                _motorSpeed = motorSpeed;
            }
            else
                BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
        }

        void SetNightLightLevel(object obj)
        {
            if (obj is LightState state)
            {
                var lightLevel1 = OffLightLevel;
                var enabled = true;

                switch (state)
                {
                    case LightState.Low:
                        lightLevel1 = LowLightLevel;
                        break;
                    case LightState.Medium:
                        lightLevel1 = MediumLightLevel;
                        break;
                    case LightState.High:
                        lightLevel1 = HighLightLevel;
                        break;
                    case LightState.DarkMode:
                        break;
                    default:
                        enabled = false;
                        break;
                }

                if (FirmwareVersion >= FirmwareVersions.V3)
                    BluetoothModule.WriteNightlightCommand(enabled, (byte) lightLevel1,
                        (byte) (lightLevel1 == OffLightLevel
                            ? lightLevel1
                            : lightLevel1 + LightLevelIncrement));
                else
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.BottomLed, (byte) lightLevel1);
            }
        }

        void SetActiveControl(object obj)
        {
            if (obj is ControlType newControlType)
            {
                if (ActiveControl == newControlType)
                    ActiveControl = ControlType.None;
                else
                {
                    switch (newControlType)
                    {
                        case ControlType.BabyMonitor when !AudioLevelModule.Calibrated:
                            CalibrateCommand.Execute(null);
                            break;
                        case ControlType.Timer:
                            UserEnabledTimer = false;
                            break;
                        case ControlType.Support:
                            HelpIndexCommand.Execute(null);
                            break;
                    }

                    ActiveControl = newControlType;
                }

                RaisePropertyChanged(nameof(ActiveControl));
                RaisePropertyChanged(nameof(ShowMainControl));
                RaisePropertyChanged(nameof(TimerUIDisabled));
            }
        }

        void SetNightLightUi(int lightLevel1, bool enabled = false, bool enabledApp = false)
        {
            switch (FirmwareVersion)
            {
                case FirmwareVersions.V3:
                    NightLightState = enabled
                        ? lightLevel1 == 0
                            ? LightState.DarkMode
                            : lightLevel1 <= LowLightLevel
                                ? LightState.Low
                                : lightLevel1 <= MediumLightLevel
                                    ? LightState.Medium
                                    : LightState.High
                        : enabledApp && lightLevel1 == 0
                            ? LightState.DarkMode
                            : LightState.Off;
                    break;
                case FirmwareVersions.V2:
                    NightLightState = lightLevel1 == OffLightLevel || lightLevel1 > HighLightLevel
                        ? LightState.Off
                        : lightLevel1 <= LowLightLevel
                            ? LightState.Low
                            : lightLevel1 <= MediumLightLevel
                                ? LightState.Medium
                                : LightState.High;
                    break;
                default:
                    // V1 firmware does not have a night light
                    NightLightState = LightState.Off;
                    break;
            }
        }

        void BluetoothModule_SnoozStatusUpdated(object sender, SnoozStatusUpdatedEventArgs e)
        {
            Debug.WriteLine("MainViewModelV2 SnoozStatusUpdated");
            MotorSpeed = e.StatusUpdate.MotorSpeed;
            MotorEnabled = e.StatusUpdate.MotorEnabled;
            SetNightLightUi(e.StatusUpdate.NightlightLevel1, 
                            e.StatusUpdate.NightlightEnabledSnooz, 
                            e.StatusUpdate.NightlightEnabledApp);
        }

        void BluetoothModule_SnoozDisconnected(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("MainViewModelV2 SnoozDisconnected");

            // If the app is closing don't go to the connection page yet.
            // Wait for the OnResume to open the connection page
            if (!App.ClosingTime)
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await NavigationService.GoBackAsync();
                    await NavigationService.NavigateAsync(nameof(ConnectionPage));
                });
        }

        void GetTimes(BluetoothModule.ScheduleResponseFrameObject scheduleResponseFrame)
        {
            try
            {
                var timersEnabled = Enumerable.Range(0, 7)
                                              .Select(i => (scheduleResponseFrame.TimerEnableMask
                                                            >> i & 0x01) == 0x01)
                                              .ToList();
                var timers = scheduleResponseFrame.Timers
                                                  .Select(t => TimeSpan.FromMinutes(t))
                                                  .ToList();
                var newTimes = timers.Zip(timersEnabled,
                                         (timer, enabled) => new EndPoint
                                         {
                                             Time = timer,
                                             Enabled = enabled
                                         }).ToList();

                switch (scheduleResponseFrame.Command)
                {
                    case BluegigaResponseCommand.SendOffSchedule:
                        _stopTimes = newTimes;
                        break;
                    case BluegigaResponseCommand.SendOnSchedule:
                        _startTimes = newTimes;
                        break;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"MainViewModelV2.GetTimes exception: {ex}");
            }
        }
        
        void MakeTimers(bool wasToggled = false)
        {
            Debug.WriteLine("MakeTimers()");
            try
            {
                var nights = MakeNights();
                var timers = nights.ToTimers().ToList();
                var allLocalTimers = LoadScheduleFromLocalStorage();

                Debug.WriteLine($"Snooz Timers = Local Timers ? {allLocalTimers?.Timers?.ToNights().SequenceEqual(nights)}");
                var localSameAsSnooz = allLocalTimers?.Timers != null && allLocalTimers.Timers.ToNights().SequenceEqual(nights);

                Timers = localSameAsSnooz ? 
                         new ObservableCollection<TimerModel>(allLocalTimers.Timers) : 
                         new ObservableCollection<TimerModel>(timers);

                UpdateTimersNumbers();
                SaveScheduleToLocalStorage();

                if (!Timers.Any())
                {
                    _backupTimers = null;
                    AddNewTimer();
                }

                ScheduleChanged = false;

                RaisePropertyChanged(nameof(Timers));
                RaisePropertyChanged(nameof(TimerUIDisabled));
                RaisePropertyChanged(nameof(CurrentActiveNight));
                RaisePropertyChanged(nameof(CurrentNightDisplay));
                RaisePropertyChanged(nameof(IsStartTimerActive));
                RaisePropertyChanged(nameof(IsStopTimerActive));
                RaisePropertyChanged(nameof(NumberOfActiveTimers));
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"MainViewModelV2.MakeTimers exception: {ex}");
            }
        }

        // Combines separate timers with the same start/stop times and start/stop enabled
        // for comparison with the Snooz device timers
        static IReadOnlyList<TimerModel> CombineActiveTimers(IReadOnlyList<TimerModel> timers, 
                                                             IReadOnlyList<TimerModel> snoozTimers)
        {
            if (timers == null || !timers.Any() || timers.Count < 2)
                return timers;

            var combinedTimers = new List<TimerModel>();
            foreach (var timer in timers)
            {
                if (combinedTimers.FirstOrDefault(t => t.StartEnabled == timer.StartEnabled
                                                       && t.StopEnabled == timer.StopEnabled
                                                       && t.StartTime == timer.StartTime
                                                       && t.StopTime == timer.StopTime) is TimerModel timerToCombineWith)
                {
                    if (snoozTimers.FirstOrDefault(t => t.Id.Equals(timerToCombineWith.Id))
                        is TimerModel snoozTimer)
                        timerToCombineWith.WeekDays.ForEach(day =>
                        {
                            var dayIndex = timerToCombineWith.WeekDays.IndexOf(day);
                            day.SetId(snoozTimer.WeekDays[dayIndex].Id);
                            if (!day.IsOn)
                                day.IsOn = timer.WeekDays[dayIndex].IsOn;
                        });
                }
                else
                {
                    var snoozTimer = snoozTimers.FirstOrDefault(t => t.StartEnabled == timer.StartEnabled
                                                                     && t.StopEnabled == timer.StopEnabled
                                                                     && t.StartTime == timer.StartTime
                                                                     && t.StopTime == timer.StopTime);
                    var newTimer = new TimerModel(timer, true, snoozTimer?.WeekDays.Select(d => d.Id).ToArray());
                    if (snoozTimer != null)
                        newTimer.SetId(snoozTimer.Id);
                    combinedTimers.Add(newTimer);
                }
            }

            return combinedTimers;
        }

        void BluetoothModule_SnoozTimerScheduleUpdate(object sender, BluetoothModule.ScheduleResponseFrameObject scheduleResponseFrame)
        {
            Debug.WriteLine("MainViewModelV2 BluetoothModule_SnoozTimerScheduleUpdate");
            GetTimes(scheduleResponseFrame);

            if (_startTimes == null || _stopTimes == null) return;

            MakeTimers();
            BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
        }

        Night[] MakeNights()
        {
            var nights = new List<Night>();
            for (var i = 0; i < 7; i++)
            {
                nights.Add(new Night
                {
                    Start = new EndPoint { Enabled = _startTimes[i].Enabled, Time = new TimeSpan(_startTimes[i].Time.Hours, _startTimes[i].Time.Minutes, 0) },
                    Stop = new EndPoint { Enabled = _stopTimes[i].Enabled, Time = new TimeSpan(_stopTimes[i].Time.Hours, _stopTimes[i].Time.Minutes, 0) }
                });
            }

            return nights.ToArray();
        }

        int GetFadeValue(string fadeTimeText)
        {
            switch (fadeTimeText)
            {
                case FadeTime.OFF_TEXT:
                    return FadeTime.OFF;
                case FadeTime.BASIC_TEXT:
                    return FadeTime.BASIC;
                case FadeTime.THIRTY_SEC_TEXT:
                    return FadeTime.THIRTY_SEC;
                case FadeTime.ONE_MIN_TEXT:
                    return FadeTime.ONE_MIN;
                case FadeTime.FIVE_MIN_TEXT:
                    return FadeTime.FIVE_MIN;
                default:
                    if (CurrentMotorLevel == 0) //To Do: Check if this is needed
                        return (int)Math.Ceiling(5 / FadeTime.MOTOR_SPEED_RATIO);
                    return (int)Math.Ceiling(CurrentMotorLevel / FadeTime.MOTOR_SPEED_RATIO);
            }
        }

        static string GetFadeText(int fadeTimeValue)
        {
            switch (fadeTimeValue)
            {
                case FadeTime.OFF:
                    return FadeTime.OFF_TEXT;
                case FadeTime.BASIC:
                    return FadeTime.BASIC_TEXT;
                case FadeTime.THIRTY_SEC:
                    return FadeTime.THIRTY_SEC_TEXT;
                case FadeTime.ONE_MIN:
                    return FadeTime.ONE_MIN_TEXT;
                case FadeTime.FIVE_MIN:
                    return FadeTime.FIVE_MIN_TEXT;
                default:
                    return FadeTime.AUTO_TEXT;
            }
        }

        static int GetFadeNumber(uint fadeTimeValue)
        {
            if (fadeTimeValue != FadeTime.OFF &&
                fadeTimeValue != FadeTime.BASIC &&
                fadeTimeValue != FadeTime.THIRTY_SEC &&
                fadeTimeValue != FadeTime.ONE_MIN &&
                fadeTimeValue != FadeTime.FIVE_MIN)
                return FadeTime.AUTO;

            return (int)fadeTimeValue;
        }

        static void ShowHelp()
            => Application.Current.MainPage.DisplayAlert("Volume Fade Control",
                                                         "Fade works with the scheduler to turn SNOOZ on/off slowly. Select a fade time or choose auto, which adjusts the fade between 20s-3min, depending on the set volume level.",
                                                         "OK");

        void WriteSchedule(bool shouldCloseScheduler = true)
        {
            Debug.WriteLine("WriteSchedule()");

            try
            {
                ScheduleChanged = false;
                BluetoothModule.WriteSchedule(Timers.ToNights(toWriteOnDevice: true));

                SaveScheduleToLocalStorage();

                // Goes back to main control if advanced scheduling
                if (shouldCloseScheduler)
                    SetActiveControl(ControlType.Timer);

                RaisePropertyChanged(nameof(Timers));
                RaisePropertyChanged(nameof(TimerUIDisabled));
                RaisePropertyChanged(nameof(CurrentActiveNight));
                RaisePropertyChanged(nameof(CurrentNightDisplay));
                RaisePropertyChanged(nameof(NumberOfActiveTimers));
                RaisePropertyChanged(nameof(IsStartTimerActive));
                RaisePropertyChanged(nameof(IsStopTimerActive));
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"WriteSchedule exception: {ex}");
            }
        }

        ScheduleBackup LoadScheduleFromLocalStorage()
        {
            var json = SettingsModule.SavedAdvancedSchedule;
            if (string.IsNullOrWhiteSpace(json) || json == "null")
                return null;
            
            return JsonConvert.DeserializeObject<ScheduleBackup>(
                json, new TimerJsonConverter());
        }

        void SaveScheduleToLocalStorage()
        {
            var saveJson = JsonConvert.SerializeObject(new ScheduleBackup
            {
                Timers = Timers.ToArray()
            }, new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });
            
            SettingsModule.SavedAdvancedSchedule = saveJson;
        }

        public bool CanAddTimer => Timers.Count < 7;

        ObservableCollection<TimerModel> _timers = new ObservableCollection<TimerModel>();
        public ObservableCollection<TimerModel> Timers
        {
            get => _timers;
            set
            {
                if (_timers != null)
                    _timers.CollectionChanged -= TimersCollectionChanged;

                SetProperty(ref _timers, value);
                _timers.CollectionChanged += TimersCollectionChanged;
            }
        }

        public ICommand WriteScheduleCommand => new Command(() => WriteSchedule());

        bool _isAddingTimer;
        void AddNewTimer()
        {
            if (_isAddingTimer) 
                return;

            _isAddingTimer = true;
            if (Timers.Count < 7)
                Device.BeginInvokeOnMainThread(() =>
                {
                    Timers.Add(new TimerModel());
                    _isAddingTimer = false;
                });
        }

        void RemoveTimer(TimerModel item)
        {
            Timers.Remove(item);
            if (!Timers.Any())
                AddNewTimer();
        }

        void ToggleWeekDay(WeekDayModel item)
        {
            item.IsOn = item.IsChecked = !item.IsOn;

            if (!(Timers.FirstOrDefault(t => t.WeekDays.Any(d => d.Id.Equals(item.Id))) is TimerModel parent)
                || !parent.Enabled)
                return;

            foreach (var timer in Timers.Where(t => !t.Id.Equals(parent.Id) && t.Enabled))
            {
                timer.WeekDays[item.WeekDayIndex].IsOn = false;
                timer.WeekDays[item.WeekDayIndex].IsChecked = false;
            }
        }

        void TimersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
            => UpdateTimersNumbers();

        void UpdateTimersNumbers()
        {
            foreach (var timer in Timers)
                Device.BeginInvokeOnMainThread(() =>timer.Name = $"Timer {Timers.Select(t => t.Id).ToList().IndexOf(timer.Id) + 1}");
            RaisePropertyChanged(nameof(CanAddTimer));
        }

        void ToggleTimer(TimerType type, TimerModel item)
        {
            var hasDaysOn = false;
            switch (type)
            {
                case TimerType.Start:
                    item.StartEnabled = !item.StartEnabled;
                    hasDaysOn = item.StartEnabled && item.WeekDays.Any(d => d.IsOn);
                    break;
                case TimerType.Stop:
                    item.StopEnabled = !item.StopEnabled;
                    hasDaysOn = item.StopEnabled && item.WeekDays.Any(d => d.IsOn);
                    break;
            }

            if (!hasDaysOn)
                return;

            for (var index = 0; index < 7; index++)
            {
                if (!item.WeekDays[index].IsOn)
                    continue;

                foreach (var timer in Timers.Where(t => !t.Id.Equals(item.Id) && t.Enabled))
                    timer.WeekDays[index].IsChecked = timer.WeekDays[index].IsOn = false;
            }
        }
    }
}
