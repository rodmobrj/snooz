﻿using System;
using System.Linq;
using SnoozApp.Modules;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPageV2 : BasePage
    {
        public MainPageV2()
        {
            InitializeComponent();
        }
        
        void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
            => TimersListView.SelectedItem = null;

        void OnAddNewTimerTapped(object sender, EventArgs e)
        {
            var cell = TimersListView.ItemsSource.Cast<TimerModel>().LastOrDefault();
            TimersListView.ScrollTo(cell, ScrollToPosition.MakeVisible, true);
        }

        void OnDarkModeInfoButtonTapped(object sender, EventArgs e)
            => DisplayAlert("Touchpad LED Control", 
                "\r\nOff: (Factory Default) Touchpad lights turn on for 7 seconds after touched." +
                "\r\n\nDark Mode: Keeps the touchpad lights off, always." +
                "\r\n\n1, 2, 3: Brightness settings to use touchpad lights as a nightlight.", 
                "OK");

        protected override bool OnBackButtonPressed() => true;
    }
}