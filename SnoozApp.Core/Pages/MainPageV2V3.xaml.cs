﻿using System;
using System.Linq;
using SnoozApp.Modules;
using SnoozApp.ViewModels;
using Xamarin.Forms;

namespace SnoozApp.Pages
{
    public partial class MainPageV2V3 : BasePage
    {
        public MainPageV2V3()
        {
            InitializeComponent();
            
        }

        protected override bool OnBackButtonPressed() => true;
    }
}
