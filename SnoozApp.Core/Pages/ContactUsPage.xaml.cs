﻿using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactUsPage : BasePage
	{
		public ContactUsPage ()
		{
			InitializeComponent ();
		}
	}
}