﻿using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class About : BasePage
    {
        public About()
        {
            InitializeComponent();
        }
    }
}