﻿using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BLESignalSupportPage : BasePage
    {
        public BLESignalSupportPage()
        {
            InitializeComponent();
        }
    }
}