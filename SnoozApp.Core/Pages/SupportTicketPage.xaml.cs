﻿using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SupportTicketPage : BasePage
    {
		public SupportTicketPage ()
		{
			InitializeComponent ();
		}
	}
}