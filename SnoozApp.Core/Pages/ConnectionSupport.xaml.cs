﻿using System;
using Xamarin.Forms;

namespace SnoozApp.Pages
{
    public partial class ConnectionSupport : BasePage
    {
        public ConnectionSupport()
        {
            InitializeComponent();
        }

        async void ResetButtonClicked(object sender, EventArgs args)
        {
            if (await DisplayAlert("Reset App", "Are you sure you want to forget all app settings?", "Forget", "Cancel"))
                MessagingCenter.Send(args, App.CONFIRM_FORGET_MESSAGE);
        }
    }
}
