﻿using SnoozApp.Modules;
using SnoozApp.ViewModels;
using System;


namespace SnoozApp.Pages
{
    public partial class DevicesPage : BasePage
    {
        public DevicesPage()
        {
            InitializeComponent();
        }

        async void DeleteButtonClicked(object sender, DeleteSnoozEventArgs e)
		{
			if (await DisplayAlert(e.Name, "Are you sure you want to delete this SNOOZ?", "Delete", "Cancel"))
				((DevicesViewModel) BindingContext).HandleDeleteSnooz(e);
		}
    }
}
