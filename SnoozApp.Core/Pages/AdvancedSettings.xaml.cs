﻿using Xamarin.Forms.Xaml;

namespace SnoozApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdvancedSettings : BasePage
    {
        public AdvancedSettings()
        {
            InitializeComponent();
        }
    }
}