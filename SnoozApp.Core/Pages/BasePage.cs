﻿using System;
using Xamarin.Forms;

namespace SnoozApp.Pages
{
    public abstract class BasePage : ContentPage
    {
        protected BasePage()
        {
            Console.WriteLine("BasePage");
            Style = (Style)Application.Current.Resources["DefaultPageStyle"];
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
