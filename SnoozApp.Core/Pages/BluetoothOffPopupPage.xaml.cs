﻿using Prism.Ioc;
using Prism.Navigation;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace SnoozApp.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BluetoothOffPopupPage : PopupPage
    {
        public BluetoothOffPopupPage()
        {
            InitializeComponent();

            if (!App.CanCloseBluetoothPopup)
                BluetoothTextSpan.Text = "Turn Bluetooth on and restart the App. If the phone's Bluetooth is toggled off " +
                                         "with the App open in the background, Android requires " +
                                         "the App to be restarted to access Blueooth. Please close the App and manually " +
                                         "restart it.";

            if (!App.IsBluetoothUnauthorized) 
                return;

            BluetoothTitleSpan.Text = "Bluetooth Permission Denied";
            BluetoothTextSpan.Text = "The app needs Bluetooth permission to connect " +
                                     "to your SNOOZ device. Please grant permission manually.";
            AppPermissionsButton.IsVisible = true;
        }

        protected override bool OnBackgroundClicked() => false;
        protected override bool OnBackButtonPressed() => true;

        void OnNeedSupportClicked(object sender, EventArgs e)
            => Launcher.TryOpenAsync("mailto:App@getsnooz.com");



        //async void OnNeedSupportClicked(object sender, EventArgs e)
       // {

           // await App.IoCContainer.Resolve<INavigationService>()
             //                     .NavigateAsync($"/{nameof(HelpPage)}", 
               //                                  (nameof(BluetoothOffPopupPage), 
                 //                                true));
            //await App.Current.MainPage.Navigation.PopAllPopupAsync();
      //  }

        void AppPermissionsButton_OnClicked(object sender, EventArgs e)
            => Launcher.TryOpenAsync("app-settings:");
    }
}