﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class NightView : AbsoluteLayout
    {
		public static readonly BindableProperty NightNameProperty =
			BindableProperty.Create(nameof(NightName), typeof(string), typeof(NightView), "", propertyChanged: NightNameUpdated);

		public static readonly BindableProperty BodyTextProperty =
			BindableProperty.Create(nameof(BodyText), typeof(string), typeof(NightView), "", propertyChanged: BodyTextUpdated);

		public static readonly BindableProperty CommandProperty =
			BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(NightView), null, propertyChanged: CommandChanged);

		public string NightName
		{
			get => (string)GetValue(NightNameProperty);
			set => SetValue(NightNameProperty, value);
		}

		public string BodyText
		{
			get => (string)GetValue(BodyTextProperty);
			set => SetValue(BodyTextProperty, value);
		}

		public ICommand Command
		{
			get => (ICommand)GetValue(CommandProperty);
			set => SetValue(CommandProperty, value);
		}

		Label headerLabel;
		Label bodyLabel;
		Button deleteButton;

		public NightView()
		{
			this.headerLabel = new Label
			{
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = 18,
				FontAttributes = FontAttributes.Bold
			};
			SetLayoutBounds(this.headerLabel, new Rectangle(0, 0, 1.0, .5));
			SetLayoutFlags(this.headerLabel, AbsoluteLayoutFlags.All);

			this.bodyLabel = new Label
			{
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				FontSize = 14
			};
			SetLayoutBounds(this.bodyLabel, new Rectangle(0, 0.75, 1.0, .25));
			SetLayoutFlags(this.bodyLabel, AbsoluteLayoutFlags.All);

			this.deleteButton = new Button
			{
				Text = "X",
				TextColor = Color.FromHex("#05FFFF"),
				IsVisible = true,
				FontFamily = "{StaticResource Gotham-Medium}",
				BackgroundColor = Color.Transparent,
				Command = this.Command,
				CommandParameter = this.NightName
			};
			SetLayoutBounds(this.deleteButton, new Rectangle(1.0, 0.5, 0.25, .5));
			SetLayoutFlags(this.deleteButton, AbsoluteLayoutFlags.All);

			this.Children.Add(this.headerLabel);
			this.Children.Add(this.bodyLabel);
			this.Children.Add(this.deleteButton);
		}

		private static void NightNameUpdated(BindableObject bindable, object oldValue, object newValue)
		{
			if (bindable is NightView NightView && NightView.headerLabel != null)
			{
				NightView.headerLabel.Text = (string)newValue;
				NightView.deleteButton.CommandParameter = (string)newValue;
			}
		}

		private static void BodyTextUpdated(BindableObject bindable, object oldValue, object newValue)
		{
			if (bindable is NightView NightView && NightView.bodyLabel != null)
			{
				NightView.bodyLabel.Text = (string)newValue;
			}
		}

		private static void CommandChanged(BindableObject bindable, object oldValue, object newValue)
		{
			if (bindable is NightView nightView && nightView.deleteButton != null)
			{
				nightView.deleteButton.Command = (ICommand)newValue;
			}
		}
	}
}
