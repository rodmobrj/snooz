﻿using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System;

namespace SnoozApp.Controls
{
    public class LongPressButton : Button
	{
        public static readonly BindableProperty LongPressCommandProperty =
        BindableProperty.Create(nameof(LongPressCommand), typeof(ICommand), typeof(LongPressButton), null);

		public ICommand LongPressCommand
        {
            get { return (ICommand)GetValue(LongPressCommandProperty); }
            set { SetValue(LongPressCommandProperty, value); }
        }

		public void SendLongPress()
		{
            if (this.LongPressCommand.CanExecute(null))
			{
				this.LongPressCommand.Execute(null);
			}
		}
    }
}