﻿using Xamarin.Forms;
using System.Diagnostics;

namespace SnoozApp.Controls
{
	public class OrbitalView : RelativeLayout
    {

		public static readonly BindableProperty IsAnimatingProperty =
			BindableProperty.Create("IsAnimating", typeof(bool), typeof(OrbitalView), false, propertyChanged: OnIsAnimatingChanged);
		
		public static readonly BindableProperty PlanetImageProperty =
            BindableProperty.Create("PlanetImage", typeof(ImageSource), typeof(OrbitalView), propertyChanged: OnPlanetImageChanged);

        public static readonly BindableProperty MoonImageProperty =
			BindableProperty.Create("OffImage", typeof(ImageSource), typeof(OrbitalView), propertyChanged: OnMoonImageChanged);

		private Image planet;
		private Image moon;

		public bool IsAnimating
		{
			get { return (bool)GetValue(IsAnimatingProperty); }
			set { SetValue(IsAnimatingProperty, value); }
		}

		public ImageSource PlanetImage
		{
			get { return (ImageSource)GetValue(PlanetImageProperty); }
			set { SetValue(PlanetImageProperty, value); }
		}

		public ImageSource MoonImage
		{
			get { return (ImageSource)GetValue(MoonImageProperty); }
			set { SetValue(MoonImageProperty, value); }
		}


		public OrbitalView()
		{
			this.stopAnimations = true;
		}

		//object animationLock = new object();
		bool stopAnimations = false;


		async private static void OnIsAnimatingChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var orbitalView = bindable as OrbitalView;

			if (orbitalView != null && ((bool)oldValue != (bool)newValue))
			{
				if ((bool)newValue == true)
				{
					var orbitalViewState = 0;

					//lock (orbitalView.animationLock)
					//{
						orbitalView.stopAnimations = false;
					//}
					while (!orbitalView.stopAnimations)
					{
						switch (orbitalViewState)
						{
							case 0:
								await orbitalView.moon.TranslateTo(orbitalView.planet.Width * 0.55, -orbitalView.planet.Height * 0.275, 200, Easing.CubicOut);
								orbitalView.LowerChild(orbitalView.moon);
								orbitalViewState = 1;
								Debug.WriteLine("Starting Animation");
								break;
							case 1:
								await orbitalView.moon.TranslateTo(-(orbitalView.planet.Width * 1.45), orbitalView.planet.Height * 0.725 , 1000, Easing.CubicInOut);
								//await orbitalView.moon.TranslateTo(0, -50, 1000);
								orbitalView.RaiseChild(orbitalView.moon);
								orbitalViewState = 2;
								Debug.WriteLine("Bottom Left Animation");
								break;
							case 2:
								await orbitalView.moon.TranslateTo(orbitalView.planet.Width * 0.55, -orbitalView.planet.Height * 0.275, 1000, Easing.CubicInOut);
								//await orbitalView.moon.TranslateTo(0, 50, 1000);
								orbitalView.LowerChild(orbitalView.moon);
								Debug.WriteLine("Top Right Animation");
								orbitalViewState = 1;
								break;
						}
					}
					Debug.WriteLine("Exiting Animation");
				}
				else
				{

					//lock (orbitalView.animationLock)
					//{
						orbitalView.stopAnimations = true;
					//}
					ViewExtensions.CancelAnimations(orbitalView.moon);
				}
			}
		}

		static void OnPlanetImageChanged(BindableObject bindable, object oldValue, object newValue)
		{
		    if (!(bindable is OrbitalView orbitalView)) return;

		    if (orbitalView.planet == null)
		    {
		        orbitalView.planet = new Image
		        {
		            Source = (ImageSource)newValue
		        };
		        orbitalView.Children.Add(orbitalView.planet,
		            Constraint.RelativeToParent((parent) => parent.Width/2 - parent.Height / 2),
		            Constraint.RelativeToParent((parent) => 0.0f),
		            Constraint.RelativeToParent((parent) => parent.Height),
		            Constraint.RelativeToParent((parent) => parent.Height));
		    }
		    else
		    {
		        orbitalView.planet.Source = (ImageSource)newValue;
		    }
		}


		static void OnMoonImageChanged(BindableObject bindable, object oldValue, object newValue)
		{
		    if (!(bindable is OrbitalView orbitalView)) return;

		    if (orbitalView.moon == null)
		    {
		        orbitalView.moon = new Image
		        {
		            Source = (ImageSource)newValue
		        };
		        orbitalView.Children.Add(orbitalView.moon,
		            Constraint.RelativeToView(orbitalView.planet, (parent, planet) => planet.X + (planet.Width * .6)),
		            Constraint.RelativeToView(orbitalView.planet, (parent, planet) => planet.Y - (planet.Width * 0.075)),
		            Constraint.RelativeToView(orbitalView.planet, (parent, planet) => planet.Width * 0.7),
		            Constraint.RelativeToView(orbitalView.planet, (parent, planet) => planet.Height * 0.7));
					
		    }
		    else
		    {
		        orbitalView.moon.Source = (ImageSource)newValue;
		    }
		}
	}
}