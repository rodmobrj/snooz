﻿using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class ImageToggleButton : ImageButton
    {
        public static readonly BindableProperty OnImageProperty =
            BindableProperty.Create("OnImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);

        public static readonly BindableProperty OffImageProperty =
            BindableProperty.Create("OffImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);

        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create("IsChecked", typeof(bool), typeof(ImageToggleButton), false, propertyChanged: OnIsCheckedChanged);

        public ImageSource OnImage
        {
            get { return (ImageSource)GetValue(OnImageProperty); }
            set { SetValue(OnImageProperty, value); }
        }

        public ImageSource OffImage
        {
            get { return (ImageSource)GetValue(OffImageProperty); }
            set { SetValue(OffImageProperty, value); }
        }

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public ImageToggleButton()
        {
            this.Source = this.TapEnabled
				? this.IsChecked 
					? this.OnImage 
					: this.OffImage
				: this.DisabledImage;
        }

        private static void OnIsCheckedChanged(BindableObject bindable, object oldValue, object newValue)
        {
			if (bindable is ImageToggleButton imageToggleButton)
			{
				var isChecked = (bool)newValue;
				imageToggleButton.Source = imageToggleButton.TapEnabled
					? imageToggleButton.IsChecked
						? imageToggleButton.OnImage
						: imageToggleButton.OffImage
					: imageToggleButton.DisabledImage;
			}
		}

        protected static void OnImageChanged(BindableObject bindable, object oldValue, object newValue)
        {
			if (bindable is ImageToggleButton imageToggleButton)
			{
				imageToggleButton.Source = imageToggleButton.TapEnabled
					? imageToggleButton.IsChecked
						? imageToggleButton.OnImage
						: imageToggleButton.OffImage
					: imageToggleButton.DisabledImage;
			}
		}
    }
}