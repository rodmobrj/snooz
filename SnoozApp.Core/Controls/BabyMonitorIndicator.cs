﻿using System;
using System.Collections.Specialized;
using Xamarin.Forms;
using System.Collections.Generic;
/*
 * The baby monitor indicator displays columns of bar of different colors that are bound to the 
 * decibel level output of the audio level module. 
 * To keep alignment correct on different screen sizes the bars are arranged in a grid. 
 */
namespace SnoozApp.Controls
{
	public class BabyMonitorIndicator : Grid
    {
        public static readonly BindableProperty DecibelLevelProperty =
            BindableProperty.Create("DecibelLevel", typeof(float), typeof(BabyMonitorIndicator), 0.0f, propertyChanged: OnDecibelLevelChanged);

        public static readonly BindableProperty CalibratedProperty =
		BindableProperty.Create("Calibrated", typeof(bool), typeof(BabyMonitorIndicator), false, propertyChanged: OnCalibratedPropertyChanged);
		
		public bool Calibrated
		{
			get { return (bool)GetValue(CalibratedProperty); }
			set { SetValue(CalibratedProperty, value); }
		}

		private List<Image> DisplayBars;

        public float DecibelLevel
        {
            get { return (float)GetValue(DecibelLevelProperty); }
            set { SetValue(DecibelLevelProperty, value); }
        }
		private const int middleSpaceColumns = 2;
		private const int leadingSpaceColumns = 1;
		private const int trailingSpaceColumns = 1;
		private const int whiteDotsBar = 6;
		private const int redDotsBar = 11;
		private const int totalBars = 16;

		private const int totalColumns = leadingSpaceColumns + trailingSpaceColumns + totalBars + (totalBars - 1) * middleSpaceColumns;
		private const int whiteDotsColumn = leadingSpaceColumns + whiteDotsBar + (whiteDotsBar - 1) * middleSpaceColumns - 1;
		private const int redDotsColumn = leadingSpaceColumns + redDotsBar + (redDotsBar - 1) * middleSpaceColumns - 1;

		public BabyMonitorIndicator()
		{
			if (this.Calibrated)
			{
				this.Initialize();
			}
		}

		private void Initialize()
		{

			this.RowSpacing = 0;
			this.ColumnSpacing = 0;
			this.DisplayBars = new List<Image>();

			this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(37, GridUnitType.Star) });
			this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(42, GridUnitType.Star) });
			this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(14, GridUnitType.Star) });
			this.RowDefinitions.Add(new RowDefinition { Height = new GridLength(51, GridUnitType.Star) });

			for (var columns = 0; columns < totalColumns; columns++)
			{
				this.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
			}

			this.Children.Add(new Image { Source = "monitor_circle_white.png", VerticalOptions = LayoutOptions.End }, whiteDotsColumn, 0);
			this.Children.Add(new Image { Source = "monitor_circle_white.png", VerticalOptions = LayoutOptions.Start }, whiteDotsColumn, 2);
			this.Children.Add(new Image { Source = "monitor_circle_red.png", VerticalOptions = LayoutOptions.End }, redDotsColumn, 0);
			this.Children.Add(new Image { Source = "monitor_circle_red.png", VerticalOptions = LayoutOptions.Start }, redDotsColumn, 2);
			/*
			// Colored blocks for better understanding the layout.  
			this.Children.Add(new BoxView { BackgroundColor = Color.Blue }, 0, 0);
			this.Children.Add(new BoxView { BackgroundColor = Color.Blue }, 14, 1);
			this.Children.Add(new BoxView { BackgroundColor = Color.Blue }, 29, 2);
			this.Children.Add(new BoxView { BackgroundColor = Color.Blue }, 46, 3);
			*/
			var thirty = new Label { Text = "30", TextColor = Color.FromHex("#555555"), LineBreakMode = LineBreakMode.NoWrap, HorizontalTextAlignment = TextAlignment.Start, VerticalTextAlignment = TextAlignment.Start, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)) };
			var fifty = new Label { Text = "50", TextColor = Color.FromHex("#555555"), LineBreakMode = LineBreakMode.NoWrap, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Start, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)) };
			var seventy = new Label { Text = "70", TextColor = Color.FromHex("#555555"), LineBreakMode = LineBreakMode.NoWrap, HorizontalTextAlignment = TextAlignment.Center, VerticalTextAlignment = TextAlignment.Start, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)) };
			var dBs = new Label { Text = "dB", TextColor = Color.FromHex("#555555"), LineBreakMode = LineBreakMode.NoWrap, HorizontalTextAlignment = TextAlignment.End, VerticalTextAlignment = TextAlignment.Start, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)) };

			this.Children.Add(thirty, 0, 3);
			this.Children.Add(fifty, whiteDotsColumn - middleSpaceColumns - 1, 3);
			this.Children.Add(seventy, redDotsColumn - middleSpaceColumns - 1, 3);
			this.Children.Add(dBs, totalColumns - (2 * (middleSpaceColumns + 1) + 1), 3);

			Grid.SetColumnSpan(thirty, 2 * (middleSpaceColumns + 1) + 1);
			Grid.SetColumnSpan(fifty, 2 * (middleSpaceColumns + 1) + 1);
			Grid.SetColumnSpan(seventy, 2 * (middleSpaceColumns + 1) + 1);
			Grid.SetColumnSpan(dBs, 2 * (middleSpaceColumns + 1) + 1);

			int barcount = 0;
			for (var i = leadingSpaceColumns; i < totalColumns; i = i + middleSpaceColumns + 1)
			{
				this.DisplayBars.Add(new Image { Source = "monitor_bar_grey.png" });
				this.Children.Add(this.DisplayBars[barcount], i, 1);
				barcount++;
			}
		}

		private void Deinitialize()
		{
			foreach (var child in this.Children)
			{
				this.Children.RemoveAt(0);
			}
		}

		/*
		 * When the Decibel level changes this method compares the oldValue and newValue
		 * It converts those values into how many bars would be displayed.
		 * It then replaces the images that need to be changed with the appropriately colored image.
		 */ 
        private static void OnDecibelLevelChanged(BindableObject bindable, object oldValue, object newValue)
        {
			var babyMonitorIndicator = bindable as BabyMonitorIndicator;


			if (babyMonitorIndicator != null && babyMonitorIndicator.Calibrated)
			{
				var newBarCount = (int)(((float)newValue - 26.0f) / 4); //Float to int always rounds down?
				var oldBarCount = (int)(((float)oldValue - 26.0f) / 4); //Subtract 26 so > 30 resolves to 1, etc.

				oldBarCount = (oldBarCount < 0) ? 0 : oldBarCount;  
				oldBarCount = (oldBarCount > totalBars) ? totalBars : oldBarCount;
				newBarCount = (newBarCount < 0) ? 0 : newBarCount;;
				newBarCount = (newBarCount > totalBars) ? totalBars : newBarCount;

				if (newBarCount > oldBarCount)
				{
					for (int i = oldBarCount; i < newBarCount; i++)
					{
						var addColumn = leadingSpaceColumns + (i * (middleSpaceColumns + 1));

						babyMonitorIndicator.Children.Remove(babyMonitorIndicator.DisplayBars[i]);


						if (addColumn <= whiteDotsColumn)
						{
							babyMonitorIndicator.DisplayBars[i] = new Image { Source = "monitor_bar_white.png" };
						}
						else if (addColumn < redDotsColumn)
						{
							babyMonitorIndicator.DisplayBars[i] = new Image { Source = "monitor_bar_yellow.png" };
						}
						else
						{
							babyMonitorIndicator.DisplayBars[i] = new Image { Source = "monitor_bar_red.png" };
						}

						babyMonitorIndicator.Children.Add(babyMonitorIndicator.DisplayBars[i], addColumn, 1);
					}
				}
				else if (newBarCount < oldBarCount)
				{
					for (int j = oldBarCount - 1; j >= newBarCount; j--)
					{
						var deleteColumn = leadingSpaceColumns + (j * (middleSpaceColumns + 1));

						babyMonitorIndicator.Children.Remove(babyMonitorIndicator.DisplayBars[j]);

						babyMonitorIndicator.DisplayBars[j] = new Image { Source = "monitor_bar_grey.png" };

						babyMonitorIndicator.Children.Add(babyMonitorIndicator.DisplayBars[j], deleteColumn, 1);
					}
					
				}
			}
        }

		private static void OnCalibratedPropertyChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var babyMonitorIndicator = bindable as BabyMonitorIndicator;
			if ((bool)newValue)
			{
				babyMonitorIndicator.Initialize();
			}
			else
			{
				babyMonitorIndicator.Deinitialize();
			}
		}
    }
}

// File Changed