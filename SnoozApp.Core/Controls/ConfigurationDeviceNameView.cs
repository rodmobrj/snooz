﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    /*
	 * The Configuration Device Name View creates a popup for naming the SNOOZ when it is first connected
	 */
    public class ConfigurationDeviceNameView : AbsoluteLayout
    {
        public static readonly BindableProperty NameChangedCommandProperty =
            BindableProperty.Create(nameof(NameChangedCommand), typeof(ICommand), typeof(ImageButton), null);

        public static readonly BindableProperty DiscoveredProperty =
            BindableProperty.Create(nameof(Discovered), typeof(object), typeof(bool), null, propertyChanged: DiscoveredChanged);

        public ICommand NameChangedCommand
        {
            get { return (ICommand)GetValue(NameChangedCommandProperty); }
            set { SetValue(NameChangedCommandProperty, value); }
        }

        public bool Discovered
        {
            get { return (bool)GetValue(DiscoveredProperty); }
            set { SetValue(DiscoveredProperty, value); }
        }

        Entry nameEntry;

        public ConfigurationDeviceNameView()
        {

            this.nameEntry = new Entry
            {
                TextColor = Color.White,
                BackgroundColor = Color.Transparent,
                HorizontalTextAlignment = TextAlignment.Center,
                IsVisible = false,
                Placeholder = "Enter a Name"
            };
            this.nameEntry.Unfocused += this.NameEntry_Unfocused;


            //       this.nameEntry.Completed += (sender, e) =>
            //     {
            //       this.nameEntry.Unfocus();
            //     this.nameEntry.IsVisible = false;
            //   this.nameEntry.IsEnabled = false;
            // };

            AbsoluteLayout.SetLayoutBounds(this.nameEntry, new Rectangle(0.5, 0.5, 1.0, 1.0));
            AbsoluteLayout.SetLayoutFlags(this.nameEntry, AbsoluteLayoutFlags.All);

            this.Children.Add(this.nameEntry);
        }

        public void SuppressButtonCommand(object parameter)
        {
        }

        private void NameEntry_Unfocused(object sender, FocusEventArgs e)
        {
            if (this.nameEntry.Text == "")
            {
                Random random = new Random();

                this.nameEntry.Text = "SNOOZ " + random.Next(0, 10000).ToString();
            }

            this.NameChangedCommand?.Execute(this.nameEntry.Text);
            this.nameEntry.IsEnabled = false;
            this.nameEntry.IsVisible = false;
        }

        private static void DiscoveredChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var configurationDeviceNameView = bindable as ConfigurationDeviceNameView;
            if (configurationDeviceNameView != null && configurationDeviceNameView.nameEntry != null)
            {
                if (newValue != null)
                {
                    if ((bool)newValue)
                    {
                        configurationDeviceNameView.nameEntry.Text = "";
                        configurationDeviceNameView.nameEntry.IsVisible = true;
                        configurationDeviceNameView.nameEntry.IsEnabled = true;

                        configurationDeviceNameView.nameEntry.Focus();
                    }
                    else
                    {
                        configurationDeviceNameView.nameEntry.IsVisible = false;
                        configurationDeviceNameView.nameEntry.IsEnabled = false;

                    }
                }
            }
        }
    }
}