﻿using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class TextToggleButton : Button
    {
        public static readonly BindableProperty OnTextProperty =
            BindableProperty.Create("OnText", typeof(string), typeof(TextToggleButton), default(string), propertyChanged: OnTextChanged);

        public static readonly BindableProperty OffTextProperty =
            BindableProperty.Create("OffText", typeof(string), typeof(TextToggleButton), default(string), propertyChanged: OnTextChanged);

        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create("IsChecked", typeof(bool), typeof(TextToggleButton), false, propertyChanged: OnIsCheckedChanged);

        public string OnText
		{
			get => (string)GetValue(OnTextProperty);
			set => SetValue(OnTextProperty, value);
		}

		public string OffText
		{
			get => (string)GetValue(OffTextProperty);
			set => SetValue(OffTextProperty, value);
		}

		public bool IsChecked
		{
			get => (bool)GetValue(IsCheckedProperty);
			set => SetValue(IsCheckedProperty, value);
		}

		private static void OnIsCheckedChanged(BindableObject bindable, object oldValue, object newValue)
        {
			if (bindable is TextToggleButton textToggleButton)
			{
				var isChecked = (bool)newValue;
				textToggleButton.Text = isChecked ? textToggleButton.OnText : textToggleButton.OffText;
			}
		}

        private static void OnTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
			if (bindable is TextToggleButton textToggleButton)
			{
				textToggleButton.Text = textToggleButton.IsChecked ? textToggleButton.OnText : textToggleButton.OffText;
			}
		}
    }
}