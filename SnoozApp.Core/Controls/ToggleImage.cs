﻿using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class ToggleImage : Image
    {
        public static readonly BindableProperty OnImageProperty =
            BindableProperty.Create("OnImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);

        public static readonly BindableProperty OffImageProperty =
            BindableProperty.Create("OffImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);

        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create("IsChecked", typeof(bool), typeof(ImageToggleButton), false, propertyChanged: OnIsCheckedChanged);

        public ImageSource OnImage
        {
            get { return (ImageSource)GetValue(OnImageProperty); }
            set { SetValue(OnImageProperty, value); }
        }

        public ImageSource OffImage
        {
            get { return (ImageSource)GetValue(OffImageProperty); }
            set { SetValue(OffImageProperty, value); }
        }

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

		public ToggleImage()
        {
            this.Source = this.IsChecked ? this.OnImage : this.OffImage;
        }

        private static void OnIsCheckedChanged(BindableObject bindable, object oldValue, object newValue)
        {
			var toggleImage = bindable as ToggleImage;
            if (toggleImage != null)
            {
                var isChecked = (bool) newValue;
                toggleImage.Source = isChecked ? toggleImage.OnImage : toggleImage.OffImage;
            }
        }

        private static void OnImageChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var toggleImage = bindable as ToggleImage;
            if (toggleImage != null)
            {
                toggleImage.Source = toggleImage.IsChecked ? toggleImage.OnImage : toggleImage.OffImage;
            }
        }
    }
}