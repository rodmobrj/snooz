﻿using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
	public class TimerImageButton : ImageButton
	{
		public static readonly BindableProperty SelectedActive2ImageProperty =
			BindableProperty.Create("SelectedActive2Image", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty DeselectedActive2ImageProperty =
			BindableProperty.Create("DeselectedActive2Image", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty SelectedActiveImageProperty =
			BindableProperty.Create("SelectedActiveImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty DeselectedActiveImageProperty =
			BindableProperty.Create("DeselectedActiveImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty SelectedInactiveImageProperty =
			BindableProperty.Create("SelectedInactiveImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty DeselectedInactiveImageProperty =
			BindableProperty.Create("DeselectedInactiveImage", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);

		public static readonly BindableProperty IsSelectedProperty =
			BindableProperty.Create("IsSelected", typeof(bool), typeof(ImageToggleButton), false, propertyChanged: OnIsSelectedChanged);
		public static readonly BindableProperty NumberOfActiveTimersProperty =
			BindableProperty.Create("NumberOfActiveTimers", typeof(int), typeof(ImageToggleButton), 0, propertyChanged: OnNumberOfActiveTimersChanged);

		public ImageSource SelectedActive2Image
		{
			get { return (ImageSource)GetValue(SelectedActive2ImageProperty); }
			set { SetValue(SelectedActive2ImageProperty, value); }
		}

		public ImageSource DeselectedActive2Image
		{
			get { return (ImageSource)GetValue(DeselectedActive2ImageProperty); }
			set { SetValue(DeselectedActive2ImageProperty, value); }
		}

		public ImageSource SelectedActiveImage
		{
			get { return (ImageSource)GetValue(SelectedActiveImageProperty); }
			set { SetValue(SelectedActiveImageProperty, value); }
		}

		public ImageSource DeselectedActiveImage
		{
			get { return (ImageSource)GetValue(DeselectedActiveImageProperty); }
			set { SetValue(DeselectedActiveImageProperty, value); }
		}

		public ImageSource SelectedInactiveImage
		{
			get { return (ImageSource)GetValue(SelectedInactiveImageProperty); }
			set { SetValue(SelectedInactiveImageProperty, value); }
		}

		public ImageSource DeselectedInactiveImage
		{
			get { return (ImageSource)GetValue(DeselectedInactiveImageProperty); }
			set { SetValue(DeselectedInactiveImageProperty, value); }
		}
		public bool IsSelected
		{
			get { return (bool)GetValue(IsSelectedProperty); }
			set { SetValue(IsSelectedProperty, value); }
		}
		public int NumberOfActiveTimers
		{
			get { return (int)GetValue(NumberOfActiveTimersProperty); }
			set { SetValue(NumberOfActiveTimersProperty, value); }
		}

		public TimerImageButton()
		{
			this.Source = this.SelectImage(this, this.IsSelected, this.NumberOfActiveTimers);
		}

		private static void OnIsSelectedChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var timerImageButton = bindable as TimerImageButton;
			if (timerImageButton != null)
			{
				var isSelected = (bool)newValue;
				timerImageButton.Source = timerImageButton.SelectImage(timerImageButton, isSelected, timerImageButton.NumberOfActiveTimers);
			}
		}

		private static void OnNumberOfActiveTimersChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var timerImageButton = bindable as TimerImageButton;
			if (timerImageButton != null)
			{
				var numberOfActiveTimers = (int)newValue;
				timerImageButton.Source = timerImageButton.SelectImage(timerImageButton, timerImageButton.IsSelected, numberOfActiveTimers);
			}
		}

		private static void OnImageChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var timerImageButton = bindable as TimerImageButton;
			if (timerImageButton != null)
			{
				timerImageButton.Source = timerImageButton.SelectImage(timerImageButton, timerImageButton.IsSelected, timerImageButton.NumberOfActiveTimers);
			}
		}

		private ImageSource SelectImage(TimerImageButton timerImageButton, bool isSelected, int numberOfActiveTimers)
		{
			if (numberOfActiveTimers > 0)
			{
				if (isSelected)
					return numberOfActiveTimers == 1 ? timerImageButton.SelectedActiveImage : timerImageButton.SelectedActive2Image;
				else
					return numberOfActiveTimers == 1 ? timerImageButton.DeselectedActiveImage : timerImageButton.DeselectedActive2Image;
			}
			else
			{
				if (isSelected)
					return timerImageButton.SelectedInactiveImage;
				else
					return timerImageButton.DeselectedInactiveImage;
			}
		}
    }
}