﻿using SnoozApp.Core.Extensions;
using SnoozApp.Core.Helpers;
using SnoozApp.Modules;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    /*
	 * The Device Name View handles the interaction with the device select buttons.
	 * The button needs to initiate connecting to a SNOOZ when the button is tapped
	 * and switch to an entry control for changing the SNOOZ name on a long press.
	 * This control handles switching between the different controls and triggering
	 * commands that allow the view model to change the device's name in memory.
	 */
    public class DeviceNameView : AbsoluteLayout
	{
		public static readonly BindableProperty TextProperty =
		BindableProperty.Create(nameof(Text), typeof(string), typeof(CircleIndicatorView), "", propertyChanged: TextUpdated);

		public static readonly BindableProperty CommandProperty =
			BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ImageButton), null, propertyChanged: CommandChanged);

		public static readonly BindableProperty CommandParameterProperty =
			BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ImageButton), null, propertyChanged: CommandParameterChanged);

		public static readonly BindableProperty NameChangedCommandProperty =
			BindableProperty.Create(nameof(NameChangedCommand), typeof(ICommand), typeof(ImageButton), null);

		public static readonly BindableProperty DeviceIdProperty =
			BindableProperty.Create(nameof(DeviceId), typeof(object), typeof(ImageButton), null);

		public static readonly BindableProperty DiscoveredProperty =
			BindableProperty.Create(nameof(Discovered), typeof(object), typeof(bool), null, propertyChanged: DiscoveredChanged);

		public static readonly BindableProperty ConnectingProperty =
			BindableProperty.Create(nameof(Connecting), typeof(object), typeof(bool), null, propertyChanged: ConnectingChanged);

		public static readonly BindableProperty DeviceRssiProperty =
			BindableProperty.Create(nameof(DeviceRssi), typeof(int), typeof(Image), null, propertyChanged: RssiChanged);

        public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		public ICommand Command
		{
			get { return (ICommand)GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}

		public object CommandParameter
		{
			get { return (object)GetValue(CommandParameterProperty); }
			set { SetValue(CommandParameterProperty, value); }
		}

		public ICommand NameChangedCommand
		{
			get { return (ICommand)GetValue(NameChangedCommandProperty); }
			set { SetValue(NameChangedCommandProperty, value); }
		}

		public object DeviceId
		{
			get { return (object)GetValue(DeviceIdProperty); }
			set { SetValue(DeviceIdProperty, value); }
		}

		public bool Discovered
		{
			get { return (bool)GetValue(DiscoveredProperty); }
			set { SetValue(DiscoveredProperty, value); }
		}

		public bool Connecting
		{
			get { return (bool)GetValue(ConnectingProperty); }
			set { SetValue(ConnectingProperty, value); }
		}

		public object DeviceRssi
		{
			get { return (int)GetValue(DeviceRssiProperty); }
			set { SetValue(DeviceRssiProperty, value); }
		}

		public event EventHandler<DeleteSnoozEventArgs> DeleteSnoozPressed;

	    readonly LongPressButton _longPressButton;
	    readonly Label _nameLabel;
	    readonly Entry _nameEntry;
		readonly Image _rssiImage;
	    readonly Button _deleteButton;
	    readonly Button _clearButton;
        public ICommand longPressCommand { get; private set; }
		public ICommand LocalDeleteCommand { get; private set; }

		public DeviceNameView()
		{
			this.longPressCommand = new Command(this.HandleLongPress);
			this.LocalDeleteCommand = new Command(this.HandleDelete);
			this._longPressButton = new LongPressButton
			{
				BackgroundColor = Color.Black,
				TextColor = Color.White,
				LongPressCommand = this.longPressCommand,
				Command = this.Command,
				CommandParameter = this.CommandParameter,
				Opacity = 0.25,
				CornerRadius = 25
			};
			AbsoluteLayout.SetLayoutBounds(this._longPressButton, new Rectangle(0.5, 0.5, 1.0, 1.0));
			AbsoluteLayout.SetLayoutFlags(this._longPressButton, AbsoluteLayoutFlags.All);

			this._rssiImage = new Image
			{
				Source = RssiImageConverter.DefaultImageSource(),
				IsVisible = true
			};

			AbsoluteLayout.SetLayoutBounds(this._rssiImage, new Rectangle(25, 0.5, 50, 25));
			AbsoluteLayout.SetLayoutFlags(this._rssiImage, AbsoluteLayoutFlags.YProportional);

			this._nameLabel = new Label
			{
				InputTransparent = true,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				TextColor = Color.Red
			};
			AbsoluteLayout.SetLayoutBounds(this._nameLabel, new Rectangle(0.5, 0.5, 1.0, 1.0));
			AbsoluteLayout.SetLayoutFlags(this._nameLabel, AbsoluteLayoutFlags.All);

            this._nameEntry = new Entry
            {
                TextColor = Color.Purple,
                BackgroundColor = Color.Transparent,
                HorizontalTextAlignment = TextAlignment.Center,
                IsVisible = false,
                Margin = new Thickness(28, 0, 20, 0)
			};
			this._nameEntry.Unfocused += this.NameEntry_Unfocused;

			AbsoluteLayout.SetLayoutBounds(this._nameEntry, new Rectangle(0.5, 0.5, 1.0, 1.0));
			AbsoluteLayout.SetLayoutFlags(this._nameEntry, AbsoluteLayoutFlags.All);

			_deleteButton = new Button
			{
                ImageSource = "minus_red",
				IsVisible = false,
				FontFamily = "{StaticResource Gotham-Medium}",
                BackgroundColor = Color.Transparent,
				Command = LocalDeleteCommand,
			};

		    SetLayoutBounds(_deleteButton, new Rectangle(0.0, 0.5, 0.25, 1.5));
            SetLayoutFlags(_deleteButton, AbsoluteLayoutFlags.All);

		    _clearButton = new Button
		    {
		        Text = "x",
		        TextColor = Color.FromHex("#05FFFF"),
		        IsVisible = false,
		        FontFamily = "{StaticResource Gotham-Medium}",
                BackgroundColor = Color.Transparent,
                Command = new Command(HandleClear) 
		    };

		    SetLayoutBounds(_clearButton, new Rectangle(1.0, 0.5, 0.25, 1.0));
		    SetLayoutFlags(_clearButton, AbsoluteLayoutFlags.All);

		    if (Device.RuntimePlatform.Equals(Device.Android))
		    {
		        _deleteButton.Scale = 0.35;
                _nameEntry.FontSize = 16;
		        _nameEntry.MaxLength = 14;
            }
		    else
		    {
		        _deleteButton.Scale = 0.4;
		        _nameEntry.FontSize = 12.5;
		        _nameEntry.MaxLength = 14;
            }

			Children.Add(_rssiImage);
            Children.Add(_longPressButton);
			Children.Add(_nameLabel);
			Children.Add(_nameEntry);
			Children.Add(_deleteButton);
            Children.Add(_clearButton);
			
		}

	    void HandleClear()
	    {
	        _nameEntry.Text = string.Empty;
	        _nameLabel.Text = string.Empty;
	    }

		public void HandleLongPress()
		{
            //To Do: Sometimes if you select the delete, it just selects the device. 
		    _nameEntry.Text = _nameLabel.Text.Length > _nameEntry.MaxLength 
		                      ? _nameLabel.Text.Substring(0, _nameEntry.MaxLength) 
		                      : _nameLabel.Text;
            _nameLabel.Text = "";
			_nameEntry.IsVisible = true;
			_nameEntry.Focus();
			_deleteButton.IsVisible = true;
		    _clearButton.IsVisible = true;
			_rssiImage.IsVisible = false;
        }

		public void HandleDelete()
		{
			_nameEntry.Unfocus();
			DeleteSnoozPressed?.Invoke(this, new DeleteSnoozEventArgs((Guid)CommandParameter, this.Text));
		}

		void NameEntry_Unfocused(object sender, FocusEventArgs e)
		{
			if (_nameEntry.Text == "")
                _nameEntry.Text = " ";

            Text = _nameEntry.Text;
            _nameLabel.Text = Text;
            _nameEntry.IsVisible = false;
            _deleteButton.IsVisible = false;
		    _clearButton.IsVisible = false;
			_rssiImage.IsVisible = true;
            NameChangedCommand?.Execute(new DisplaySnooz(Text, (Guid)DeviceId));
        }

		private static void TextUpdated(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			if (deviceNameView != null && deviceNameView._nameLabel != null)
			{
				deviceNameView._nameLabel.Text = (string)newValue;
			}
		}

		private static void CommandChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			if (deviceNameView != null && deviceNameView._longPressButton != null)
			{
				deviceNameView._longPressButton.Command = (ICommand)newValue;
			}
		}

		private static void DeleteCommandChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			if (deviceNameView != null && deviceNameView._deleteButton != null)
			{
				deviceNameView._deleteButton.Command = (ICommand)newValue;
			}
		}

		private static void CommandParameterChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			if (deviceNameView != null)
			{
				if (newValue != null && deviceNameView._longPressButton != null)
				{
					deviceNameView._longPressButton.CommandParameter = (Guid)newValue;
				}
				if (newValue != null && deviceNameView._deleteButton != null)
				{
					deviceNameView._deleteButton.CommandParameter = (Guid)newValue;
				}
			}
		}

		private static void DiscoveredChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			if (deviceNameView != null && deviceNameView._longPressButton != null)
			{
				if (newValue != null)
				{
					if ((bool)newValue)
					{
						deviceNameView._nameEntry.TextColor = Color.White;
						deviceNameView._nameLabel.TextColor = Color.White;
					}
					else
					{
						deviceNameView._nameEntry.TextColor = Color.Gray;
						deviceNameView._nameLabel.TextColor = Color.Gray;
					}
				}
			}
		}

		private static void ConnectingChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			if (deviceNameView != null && deviceNameView._longPressButton != null)
			{
				if (newValue != null)
				{
					if ((bool)newValue)
					{
						deviceNameView._longPressButton.BackgroundColor = Color.White;
					}
					else
					{
						deviceNameView._longPressButton.BackgroundColor = Color.Black;
					}
				}
			}
		}

		private static void RssiChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var deviceNameView = bindable as DeviceNameView;
			int? rssi = (int)newValue;
			
			if (deviceNameView != null && deviceNameView._longPressButton != null)
			{
				if (rssi != null)
				{
					deviceNameView._rssiImage.Source = RssiImageConverter.GetImageSourceFromRssiValue((int)rssi);
				}
			}
		}
	}
}






