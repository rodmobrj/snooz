﻿using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class ColorToggleButtonAndroid : Button
    {
        public static readonly BindableProperty OnColorProperty =
            BindableProperty.Create("OnColor", typeof(Color), typeof(ColorToggleButtonAndroid), default(Color), propertyChanged: OnChange);

        public static readonly BindableProperty OffColorProperty =
            BindableProperty.Create("OffColor", typeof(Color), typeof(ColorToggleButtonAndroid), default(Color), propertyChanged: OnChange);

        public static readonly BindableProperty CheckedColorProperty =
            BindableProperty.Create("CheckedColor", typeof(Color), typeof(ColorToggleButtonAndroid), default(Color), propertyChanged: OnChange);

        // default color setter (does not disable being checked)
        public static readonly BindableProperty IsOnProperty =
            BindableProperty.Create("IsOn", typeof(bool), typeof(ColorToggleButtonAndroid), true, propertyChanged: OnChange);

        // override color setter
        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create("IsChecked", typeof(bool), typeof(ColorToggleButtonAndroid), false, propertyChanged: OnChange);

        public Color OnColor
        {
            get => (Color)GetValue(OnColorProperty);
            set => SetValue(OnColorProperty, value);
        }

        public Color OffColor
        {
            get => (Color)GetValue(OffColorProperty);
            set => SetValue(OffColorProperty, value);
        }

        public Color CheckedColor
        {
            get => (Color)GetValue(CheckedColorProperty);
            set => SetValue(CheckedColorProperty, value);
        }

        public bool IsChecked
        {
            get => (bool)GetValue(IsCheckedProperty);
            set => SetValue(IsCheckedProperty, value);
        }

        public bool IsOn
        {
            get => (bool)GetValue(IsOnProperty);
            set => SetValue(IsOnProperty, value);
        }

        private static void OnChange(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is ColorToggleButtonAndroid colorToggleButton)
            {
               // colorToggleButton.BackgroundColor = colorToggleButton.IsChecked
               //     ? colorToggleButton.CheckedColor
               //     : colorToggleButton.IsOn
               //         ? colorToggleButton.OnColor
               //         : colorToggleButton.OffColor;

				if (colorToggleButton.IsChecked)
                {
                    colorToggleButton.BackgroundColor = colorToggleButton.CheckedColor;
                    colorToggleButton.TextColor = Color.White;
                }
                else if (colorToggleButton.IsOn)
                {
                    colorToggleButton.BackgroundColor = colorToggleButton.OnColor;
                    colorToggleButton.TextColor = Color.White;
                }
                else
                {
                    colorToggleButton.BackgroundColor = colorToggleButton.OffColor;
                    colorToggleButton.TextColor = Color.Gray;
                }            
            }
        }
    }
}