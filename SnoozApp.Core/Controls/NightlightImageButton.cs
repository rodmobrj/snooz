﻿using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using SnoozApp.Enums;

namespace SnoozApp.Controls
{
	public class NightlightImageButton : ImageButton
	{
		public static readonly BindableProperty ActiveImage0Property =
			BindableProperty.Create("ActiveImage0", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty ActiveImage1Property =
			BindableProperty.Create("ActiveImage1", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty ActiveImage2Property =
			BindableProperty.Create("ActiveImage2", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty ActiveImage3Property =
			BindableProperty.Create("ActiveImage3", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty InactiveImage0Property =
			BindableProperty.Create("InactiveImage0", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty InactiveImage1Property =
			BindableProperty.Create("InactiveImage1", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty InactiveImage2Property =
			BindableProperty.Create("InactiveImage2", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);
		public static readonly BindableProperty InactiveImage3Property =
			BindableProperty.Create("InactiveImage3", typeof(ImageSource), typeof(ImageToggleButton), null, propertyChanged: OnImageChanged);

		public static readonly BindableProperty NightlightStateProperty =
			BindableProperty.Create("NightLightState", typeof(LightState), typeof(ImageToggleButton), LightState.Off, propertyChanged: OnNightlightStateChanged);
		public static readonly BindableProperty IsActiveProperty =
			BindableProperty.Create("IsActive", typeof(bool), typeof(ImageToggleButton), false, propertyChanged: OnIsActiveChanged);

		public ImageSource ActiveImage0
		{
			get { return (ImageSource)GetValue(ActiveImage0Property); }
			set { SetValue(ActiveImage0Property, value); }
		}
		public ImageSource ActiveImage1
		{
			get { return (ImageSource)GetValue(ActiveImage1Property); }
			set { SetValue(ActiveImage1Property, value); }
		}
		public ImageSource ActiveImage2
		{
			get { return (ImageSource)GetValue(ActiveImage2Property); }
			set { SetValue(ActiveImage2Property, value); }
		}
		public ImageSource ActiveImage3
		{
			get { return (ImageSource)GetValue(ActiveImage3Property); }
			set { SetValue(ActiveImage3Property, value); }
		}
		public ImageSource InactiveImage0
		{
			get { return (ImageSource)GetValue(InactiveImage0Property); }
			set { SetValue(InactiveImage0Property, value); }
		}
		public ImageSource InactiveImage1
		{
			get { return (ImageSource)GetValue(InactiveImage1Property); }
			set { SetValue(InactiveImage1Property, value); }
		}
		public ImageSource InactiveImage2
		{
			get { return (ImageSource)GetValue(InactiveImage2Property); }
			set { SetValue(InactiveImage2Property, value); }
		}
		public ImageSource InactiveImage3
		{
			get { return (ImageSource)GetValue(InactiveImage3Property); }
			set { SetValue(InactiveImage3Property, value); }
		}

		public bool IsActive
		{
			get { return (bool)GetValue(IsActiveProperty); }
			set { SetValue(IsActiveProperty, value); }
		}
		public LightState NightlightState
		{
			get { return (LightState)GetValue(NightlightStateProperty); }
			set { SetValue(NightlightStateProperty, value); }
		}

		public NightlightImageButton()
		{
			this.Source = this.SelectImage(this, this.IsActive, this.NightlightState);
		}

		private static void OnNightlightStateChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var nightlightImageButton = bindable as NightlightImageButton;
			if (nightlightImageButton != null)
			{
				var lightState = (LightState)newValue;
				nightlightImageButton.Source = nightlightImageButton.SelectImage(nightlightImageButton, nightlightImageButton.IsActive, lightState);
			}
		}

		private static void OnIsActiveChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var nightlightImageButton = bindable as NightlightImageButton;
			if (nightlightImageButton != null)
			{
				var isActive = (bool)newValue;
				nightlightImageButton.Source = nightlightImageButton.SelectImage(nightlightImageButton, isActive, nightlightImageButton.NightlightState);
			}
		}

		private static void OnImageChanged(BindableObject bindable, object oldValue, object newValue)
		{
			var nightlightImageButton = bindable as NightlightImageButton;
			if (nightlightImageButton != null)
			{
				nightlightImageButton.Source = nightlightImageButton.SelectImage(nightlightImageButton, nightlightImageButton.IsActive, nightlightImageButton.NightlightState);
			}
		}

		private ImageSource SelectImage(NightlightImageButton nightlightImageButton, bool isActive, LightState lightState)
		{
			if (isActive)
			{
				switch (lightState)
				{
					case LightState.Off:
						return nightlightImageButton.ActiveImage0;
					case LightState.Low:
						return nightlightImageButton.ActiveImage1;
					case LightState.Medium:
						return nightlightImageButton.ActiveImage2;
					case LightState.High:
						return nightlightImageButton.ActiveImage3;
					default:
						return nightlightImageButton.ActiveImage0;
				}
			}
			else
			{
				switch (lightState)
				{
					case LightState.Off:
						return nightlightImageButton.InactiveImage0;
					case LightState.Low:
						return nightlightImageButton.InactiveImage1;
					case LightState.Medium:
						return nightlightImageButton.InactiveImage2;
					case LightState.High:
						return nightlightImageButton.InactiveImage3;
					default:
						return nightlightImageButton.InactiveImage0;
				}
			}
		}
    }
}