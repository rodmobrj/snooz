﻿using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class ImageButton : Image
    {
        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ImageButton), null);
		
		public static readonly BindableProperty CommandParameterProperty =
			BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ImageButton), null);

        public static readonly BindableProperty EnabledImageProperty =
            BindableProperty.Create(nameof(EnabledImage), typeof(ImageSource), typeof(ImageButton), null, propertyChanged: UpdateImage);

        public static readonly BindableProperty DisabledImageProperty =
            BindableProperty.Create(nameof(DisabledImage), typeof(ImageSource), typeof(ImageButton), null, propertyChanged: UpdateImage);

        // Toggling the IsEnabled property breaks the tap gesture for unknown reasons, and Xamarin considers the issue "unreproducible" according to bugzilla
        // using a custom property does not cause this behavior 
        public static readonly BindableProperty TapEnabledProperty =
            BindableProperty.Create(nameof(TapEnabled), typeof(bool), typeof(ImageButton), true, propertyChanged: UpdateImage);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

		public object CommandParameter
		{
			get { return (object)GetValue(CommandParameterProperty); }
			set { SetValue(CommandParameterProperty, value); }
		}

        public ImageSource EnabledImage
        {
            get { return (ImageSource)GetValue(EnabledImageProperty); }
            set { SetValue(EnabledImageProperty, value); }
        }

        public ImageSource DisabledImage
        {
            get { return (ImageSource)GetValue(DisabledImageProperty); }
            set { SetValue(DisabledImageProperty, value); }
        }

        public bool TapEnabled
        {
            get { return (bool)GetValue(TapEnabledProperty); }
            set { SetValue(TapEnabledProperty, value); }
        }

        public ImageButton()
        {
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += this.TapGestureRecognizer_Tapped;
            this.GestureRecognizers.Add(tapGestureRecognizer);

            this.Source = this.TapEnabled ? this.EnabledImage : this.DisabledImage;
        }

        private void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            if (Command != null && Command.CanExecute(null) && TapEnabled)
				Command.Execute(CommandParameter);
        }

        private static void UpdateImage(BindableObject bindable, object oldValue, object newValue)
        {
			if (bindable is ImageToggleButton imageToggleButton)
			{
				imageToggleButton.Source = imageToggleButton.TapEnabled
					? imageToggleButton.IsChecked
						? imageToggleButton.OnImage
						: imageToggleButton.OffImage
					: imageToggleButton.DisabledImage;
			}
			else if (bindable is ImageButton imageButton)
			{
				imageButton.Source = imageButton.TapEnabled ? imageButton.EnabledImage : imageButton.DisabledImage;
			}
		}
    }
}