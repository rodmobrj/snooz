﻿using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class CircleIndicatorView : BoxView
    {
        public static readonly BindableProperty InnerColorProperty =
            BindableProperty.Create(nameof(InnerColor), typeof(Color), typeof(CircleIndicatorView), default(Color));

        public static readonly BindableProperty OuterColorProperty =
            BindableProperty.Create(nameof(OuterColor), typeof(Color), typeof(CircleIndicatorView), default(Color));

        public static readonly BindableProperty IndicatorColorProperty =
            BindableProperty.Create(nameof(IndicatorColor), typeof(Color), typeof(CircleIndicatorView), default(Color));

        public static readonly BindableProperty NumGradationsProperty =
            BindableProperty.Create(nameof(NumGradations), typeof(int), typeof(CircleIndicatorView), 0);

        public static readonly BindableProperty CurrentLevelProperty =
            BindableProperty.Create(nameof(CurrentLevel), typeof(int), typeof(CircleIndicatorView), 0);

        public static readonly BindableProperty IsCheckedProperty =
            BindableProperty.Create(nameof(IsChecked), typeof(bool), typeof(CircleIndicatorView), false);

        public static readonly BindableProperty OuterDiameterProperty =
            BindableProperty.Create(nameof(OuterDiameter), typeof(double), typeof(CircleIndicatorView), 0.0);

        public Color InnerColor
        {
            get { return (Color)GetValue(InnerColorProperty); }
            set { SetValue(InnerColorProperty, value); }
        }

        public Color OuterColor
        {
            get { return (Color)GetValue(OuterColorProperty); }
            set { SetValue(OuterColorProperty, value); }
        }

        public Color IndicatorColor
        {
            get { return (Color)GetValue(IndicatorColorProperty); }
            set { SetValue(IndicatorColorProperty, value); }
        }

        public int NumGradations
        {
            get { return (int)GetValue(NumGradationsProperty); }
            set { SetValue(NumGradationsProperty, value); }
        }

        public int CurrentLevel
        {
            get { return (int)GetValue(CurrentLevelProperty); }
            set { SetValue(CurrentLevelProperty, value); }
        }

        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public double OuterDiameter
        {
            get { return (double)GetValue(OuterDiameterProperty); }
            set { SetValue(OuterDiameterProperty, value); }
        }


    }
}