﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SnoozApp.Controls
{
    public class LinkButton : Button
    {
		public static readonly BindableProperty IsAllCapsProperty =
			BindableProperty.Create("IsAllCaps", typeof(bool), typeof(LinkButton), false);
		public static readonly BindableProperty TextAlignmentProperty =
			BindableProperty.Create("TextAlignment", typeof(TextAlignment), typeof(LinkButton), TextAlignment.Center);

		public bool IsAllCaps
		{
			get { return (bool)GetValue(IsAllCapsProperty); }
			set { SetValue(IsAllCapsProperty, value); }
		}

		public TextAlignment TextAlignment
		{
			get
			{
				return (TextAlignment)GetValue(TextAlignmentProperty);
			}
			set
			{
				SetValue(TextAlignmentProperty, value);
			}
		}
	}
}
