﻿using Xamarin.Forms;
using System.Windows.Input;
using System;
namespace SnoozApp.Controls
{
	public class CustomTimePicker : TimePicker, ICustomTimePickerController
    {
		public static readonly BindableProperty StartedCommandProperty =
			BindableProperty.Create(nameof(StartedCommand), typeof(ICommand), typeof(CustomTimePicker), null);

		public static readonly BindableProperty ExitedCommandProperty =
			BindableProperty.Create(nameof(ExitedCommand), typeof(ICommand), typeof(CustomTimePicker), null);

		public ICommand StartedCommand
		{
			get { return (ICommand)GetValue(StartedCommandProperty); }
			set { SetValue(StartedCommandProperty, value); }
		}

		public ICommand ExitedCommand
		{
			get { return (ICommand)GetValue(ExitedCommandProperty); }
			set { SetValue(ExitedCommandProperty, value); }
		}

		public void RequestEntryStart()
		{
			this.Unfocus();
			this.Focus();
		}

		void ICustomTimePickerController.TimePickerPressed()
		{
			if (this.StartedCommand.CanExecute(null))
			{
				this.StartedCommand.Execute(null);
			}
		}

		void ICustomTimePickerController.TimePickerExited()
		{
			if (this.ExitedCommand.CanExecute(null))
			{
				this.ExitedCommand.Execute(null);
			}
		}
    }
}