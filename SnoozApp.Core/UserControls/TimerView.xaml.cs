﻿using System;
using System.Collections.Generic;
using System.Linq;
using SnoozApp.Modules;
using SnoozApp.ViewModels;
using Xamarin.Forms;

namespace SnoozApp.Core.UserControls
{
    public partial class TimerView : ContentView
    {
        public TimerView()
        {
            InitializeComponent();
            MessagingCenter.Subscribe<MainViewFadeModel>(this, App.FADE_HELP_MESSAGE, InfoButtonClicked);
        }

        void InfoButtonClicked(object sender)
        {
            App.Current.MainPage.DisplayAlert("Volume Fade Control", "Fade allows the scheduler to turn SNOOZ on/off slowly. " +
                                                "\r\nSelect a fade time or choose Auto. Auto adjusts the fade time between 20 seconds to 3 minutes " +
                                                "depending on your set volume level to try to keep the volume change below the Just-Notiable Difference" +
                                                "threshold, according to Weber's Law.", "Got It");
        }

        void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            TimersListView.SelectedItem = null;
        }

        void OnAddNewTimerTapped(object sender, EventArgs e)
        {
            var cell = TimersListView.ItemsSource.Cast<TimerModel>().LastOrDefault();
            TimersListView.ScrollTo(cell, ScrollToPosition.MakeVisible, true);
        }
    }
}
