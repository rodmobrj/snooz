﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using SnoozApp.Core.Enums;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SnoozApp.Core.UserControls
{
    public partial class SchedulerHeader : ContentView
    {
        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(SchedulerHeader), null);

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(SchedulerHeader), null);

        public object CommandParameter
        {
            get { return (object)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly BindableProperty ActiveIconProperty =
            BindableProperty.Create(nameof(ActiveIcon), typeof(SchedulerHeaderTypes), typeof(SchedulerHeader), SchedulerHeaderTypes.Timer);

        public SchedulerHeaderTypes ActiveIcon
        {
            get { return (SchedulerHeaderTypes)GetValue(ActiveIconProperty); }
            set { SetValue(ActiveIconProperty, value); }
        }

        const double DISABLED_BUTTON_OPACITY = 0.5;
        const double ENABLED_BUTTON_OPACITY = 1;

        public SchedulerHeader()
        {
            InitializeComponent();


            FadeButton.Opacity = DISABLED_BUTTON_OPACITY;
            BedTimeReminderButton.Opacity = DISABLED_BUTTON_OPACITY;
        }

        void IconClicked_Clicked(System.Object sender, System.EventArgs e)
        {
            CommandParameter = null;
            var buttonClicked = (ImageButton)sender;
            buttonClicked.Opacity = ENABLED_BUTTON_OPACITY;

            if(buttonClicked == TimerButton)
            {
                BedTimeReminderButton.Opacity = DISABLED_BUTTON_OPACITY;
                FadeButton.Opacity = DISABLED_BUTTON_OPACITY;

                CommandParameter = ActiveIcon = SchedulerHeaderTypes.Timer;
                Preferences.Set("SchedulerTabSelected", (int)SchedulerHeaderTypes.Timer);
            }
            else if(buttonClicked == FadeButton)
            {
                BedTimeReminderButton.Opacity = DISABLED_BUTTON_OPACITY;
                TimerButton.Opacity = DISABLED_BUTTON_OPACITY;

                CommandParameter = ActiveIcon = SchedulerHeaderTypes.Fade;
                Preferences.Set("SchedulerTabSelected", (int)SchedulerHeaderTypes.Fade);
            }
            else
            {
                TimerButton.Opacity = DISABLED_BUTTON_OPACITY;
                FadeButton.Opacity = DISABLED_BUTTON_OPACITY;

                CommandParameter = ActiveIcon = SchedulerHeaderTypes.BedTimeReminder;
                Preferences.Set("SchedulerTabSelected", (int)SchedulerHeaderTypes.BedTimeReminder);
            }

            if (Command != null && Command.CanExecute(null))
                Command.Execute(CommandParameter);


        }
    }
}
