﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace SnoozApp.Core.UserControls
{
    public partial class NightLightControlView : ContentView
    {
        public NightLightControlView()
        {
            InitializeComponent();
        }

        void OnDarkModeInfoButtonTapped(object sender, EventArgs e)
        {
            App.Current.MainPage.DisplayAlert("Touchpad LED Control", "\r\nOff: (Factory Default) Touchpad lights turn on for 7 seconds after touched." +
                                                 "\r\n\nDark Mode: Keeps the touchpad lights off, always." +
                                                 "\r\n\n1, 2, 3: Brightness settings to use touchpad lights as a nightlight.", "Got It");
        }
    }
}
