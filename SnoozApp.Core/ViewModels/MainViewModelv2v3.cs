﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Plugin.BLE.Abstractions.EventArgs;
using Prism.Navigation;
using Prism.Services;
using SnoozApp.Core.Extensions;
using SnoozApp.Core.Modules;
using SnoozApp.Enums;
using SnoozApp.Modules;
using Xamarin.Forms;

using static SnoozApp.Modules.BluetoothModule;
using Device = Xamarin.Forms.Device;

namespace SnoozApp.ViewModels
{
    public class MainViewModelV2V3 : BaseViewModel
    {
        #region Atributes
        const int OFF_LIGHT_LEVEL = 0;
        const int LOW_LIGHT_LEVEL = 20;
        const int MEDIUM_LIGHT_LEVEL = 50;
        const int HIGH_LIGHT_LEVEL = 90;
        const int LIGHT_LEVEL_INCREMENT = 10;

        const int NUM_MOTOR_LEVEL_GRADATIONS = 10;
        const int MIN_MOTOR_LEVEL = 1;
        const int MAX_MOTOR_LEVEL = 10;

        const int VERIFY_SIGNAL_SECONDS_INTERVAL = 1;
        const int UPDATE_RSSI_IMAGE_SECONDS_INTERVAL = 3;
        const int DEFAULT_RSSI_VALUE = 0;

        readonly string _id;
        bool _justConnected;
        bool _shouldNotRefreshInterface;
        #endregion

        #region App Usage History Generator

        void CurrentStateData()
        {
            GetConnectionHistory();

            //Get current motor speed
            LastMotorSpeed = CurrentMotorLevel;

            //Determine firmware version
            LastFirmware = FirmwareVersion == FirmwareVersions.V2 ? 2 : FirmwareVersion == FirmwareVersions.V3 ? 3 : 4;

            //Count number of scheduled days
            SchedulerInUse = _backupJsonTimers == null ? 0 : 1;

            //Determine Nightlight state
            if (MotorEnabled && NightLightState == LightState.DarkMode)
                LastLightState = 1;
            else if (MotorEnabled && NightLightState == LightState.Off)
                LastLightState = 2;
            else if (MotorEnabled && NightLightState == LightState.Low)
                LastLightState = 3;
            else if (MotorEnabled && NightLightState == LightState.Medium)
                LastLightState = 4;
            else if (MotorEnabled && NightLightState == LightState.High)
                LastLightState = 5;

            //Determine if nursery monitor calibrated
            EverCalibrated = AudioLevelModule.Calibrated ? 1 : 0;

            //Calculate how many times app has been connected
            if (_justConnected)
            {
                ConnectionCount++;
                _justConnected = false;
            }

            SetConnectionHistory();
        }

        void SetConnectionHistory()
            => SettingsModule.ConnectionHistory = JsonConvert.SerializeObject
                (new ConnectionHistory(LastMotorSpeed,
                                       LastFirmware,
                                       SchedulerInUse,
                                       LastLightState,
                                       EverCalibrated,
                                       ConnectionCount));

        void GetConnectionHistory()
        {
            try
            {
                var convertedConnectionHistory = JsonConvert.DeserializeObject<ConnectionHistory>(SettingsModule.ConnectionHistory);
                ConnectionCount = convertedConnectionHistory?.ConnectionCount ?? 0;
            }
            catch
            {
                ConnectionCount = 0;
                Debug.WriteLine("Connection Count catch - re-setting to 0");
            }
        }

        private int _LastMotorSpeed;
        public int LastMotorSpeed
        {
            get
            {
                return this._LastMotorSpeed;
            }
            set
            {
                this.SetProperty(ref this._LastMotorSpeed, value);
            }
        }

        private int _LastFirmware;
        public int LastFirmware
        {
            get
            {
                return this._LastFirmware;
            }
            set
            {
                this.SetProperty(ref this._LastFirmware, value);
            }
        }

        private int _SchedulerInUse;
        public int SchedulerInUse
        {
            get
            {
                return this._SchedulerInUse;
            }
            set
            {
                this.SetProperty(ref this._SchedulerInUse, value);
            }
        }


        private int _LastLightState;
        public int LastLightState
        {
            get
            {
                return this._LastLightState;
            }
            set
            {
                this.SetProperty(ref this._LastLightState, value);
            }
        }

        private int _EverCalibrated;
        public int EverCalibrated
        {
            get
            {
                return this._EverCalibrated;
            }
            set
            {
                this.SetProperty(ref this._EverCalibrated, value);
            }
        }

        private int _ConnectionCount;
        public int ConnectionCount
        {
            get
            {
                return this._ConnectionCount;
            }
            set
            {
                this.SetProperty(ref this._ConnectionCount, value);
            }
        }

        #endregion

        #region Properties
        private AudioLevelModule _AudioLevelModule;
        public AudioLevelModule AudioLevelModule
        {
            get
            {
                return _AudioLevelModule;
            }
            set
            {
                SetProperty(ref _AudioLevelModule, value);
            }
        }

        ushort _motorSpeed;
        public ushort MotorSpeed
        {
            get => _motorSpeed;
            set
            {
                SetProperty(ref _motorSpeed, value);
                RaisePropertyChanged(nameof(CurrentMotorLevel));
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }


        bool _motorEnabled;
        public bool MotorEnabled
        {
            get => _motorEnabled;
            set
            {
                SetProperty(ref _motorEnabled, value);
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }


        public int CurrentMotorLevel => (MotorSpeed / NUM_MOTOR_LEVEL_GRADATIONS) <= 10
                                      ? (MotorSpeed / NUM_MOTOR_LEVEL_GRADATIONS)
                                      : NUM_MOTOR_LEVEL_GRADATIONS;

        //FirmwareVersions.V2;
        LightState _nightLightState;
        public LightState NightLightState
        {
            get => _nightLightState;
            set
            {
                _nightLightState = value;
                RaisePropertyChanged(nameof(NightLightState));
                RaisePropertyChanged(nameof(DarkModeOn));
            }
        }

        public bool DarkModeOn
        {
            get => NightLightState == LightState.DarkMode;
            set
            {
                if (value && NightLightState != LightState.DarkMode)
                {
                    SetNightLightLevel(LightState.DarkMode);
                }
                else if (!value && NightLightState != LightState.Off)
                {
                    SetNightLightLevel(LightState.Off);
                }
            }
        }

        int _numberOfActiveTimers;
        public int NumberOfActiveTimers
        {
            get => _numberOfActiveTimers;
            set => SetProperty(ref _numberOfActiveTimers, value);
        }

        string _currentNightDisplay;
        public string CurrentNightDisplay
        {
            get => _currentNightDisplay;
            set => SetProperty(ref _currentNightDisplay, value);
        }

        bool _isAppBusy;
        public bool IsAppBusy
        {
            get => _isAppBusy;
            set => SetProperty(ref _isAppBusy, value);
        }

        float _DecibelLevel;
        public float DecibelLevel
        {
            get => _DecibelLevel;
            set => SetProperty(ref _DecibelLevel, value);
        }

        ControlType _ActiveControl;
        public ControlType ActiveControl
        {
            get => _ActiveControl;
            set
            {
                SetProperty(ref _ActiveControl, value);
                RaisePropertyChanged(nameof(IsNightLightEnabled));
                //  RaisePropertyChanged(nameof(ControlSelectionRowHeight));
            }
        }

        ImageSource _rssiSource;
        public ImageSource RssiImage
        {
            get => _rssiSource;
            set => SetProperty(ref _rssiSource, value);
        }

        // public GridLength ControlSelectionRowHeight => ActiveControl == ControlType.Timer
        //                                              ? new GridLength(0) : new GridLength(100, GridUnitType.Star);

        public bool IsNightLightEnabled => ActiveControl != ControlType.Timer &&
                                           (BluetoothModule.ConnectedSnoozHasNightLight || FirmwareVersion >= FirmwareVersions.V3);

        public bool ShowMainControl => ActiveControl != ControlType.Timer;


        public bool SlowerEnabled => MotorEnabled && CurrentMotorLevel > MIN_MOTOR_LEVEL;
        public bool FasterEnabled => MotorEnabled && CurrentMotorLevel < MAX_MOTOR_LEVEL;

        public string DeviceName => BluetoothModule.CurrentlyConnectedSnoozName;

        public int BabyMonitorColumn => IsNightLightEnabled ? 2 : 1;
        public int SupportColumn => IsNightLightEnabled ? 3 : 2;
        public int TimerColumn => IsNightLightEnabled ? 1 : 0;

        public FirmwareVersions FirmwareVersion => BluetoothModule.FirmwareVersion;
        #endregion

        #region Commands
        public ICommand PowerButtonPress => new Command(MotorPower);
        public ICommand FasterButtonPress => new Command(MotorFaster);
        public ICommand SlowerButtonPress => new Command(MotorSlower);
        public ICommand ControlSelectorButtonPress => new Command(SetActiveControl);
        public ICommand SetNightLightLevelCommand => new Command(SetNightLightLevel);
        public ICommand CalibrateButtonPress => CalibrateCommand;
        public ICommand DevicesPageButtonPress => DevicesCommand;
        public ICommand ToggleDarkModeCommand => new Command(() => DarkModeOn = !DarkModeOn);
        #endregion

        string _backupJsonTimers;
        readonly IPageDialogService _pageDialogService;
        CancellationTokenSource _cancellationToken;

        public MainViewModelV2V3(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService)
        {
            _cancellationToken = new CancellationTokenSource();
            AudioLevelModule = AudioLevelModule.Instance;
            _pageDialogService = pageDialogService;
            _id = Guid.NewGuid().ToString();
            _justConnected = true;
        }

        static bool teste = false;
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine($"MainViewModelV2V3 OnNavigatedTo {_id}");

            App.AutoFadeAdjustment = 85;

            BluetoothModule.SnoozStatusUpdated += BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected += BluetoothModule_SnoozDisconnected;
            BluetoothModule.SnoozConnectionLost += BluetoothModule_ConnectionLost;

            BluetoothModule.SnoozTimerOffScheduleUpdate += BluetoothModule_SnoozTimerScheduleOffUpdate;
            BluetoothModule.SnoozTimerOnScheduleUpdate += BluetoothModule_SnoozTimerScheduleOnUpdate;


            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.UpdateStatus);
            BluetoothModule.WriteSyncTimeCommand();
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.ScheduleRequest);

            ConfigureRssiVerificationTimer();

            if (_shouldNotRefreshInterface) return;

            ActiveControl = ControlType.None;
            RaisePropertyChanged(nameof(IsNightLightEnabled));
            RaisePropertyChanged(nameof(TimerColumn));
            RaisePropertyChanged(nameof(BabyMonitorColumn));
            RaisePropertyChanged(nameof(SupportColumn));
            RaisePropertyChanged(nameof(FirmwareVersion));
            RaisePropertyChanged(nameof(ActiveControl));
            RaisePropertyChanged(nameof(ShowMainControl));
        }

        static int _elapsedSeconds = 0;
        void ConfigureRssiVerificationTimer()
        {
            CancellationTokenSource cancellationToken = _cancellationToken;
            Device.StartTimer(new TimeSpan(0, 0, VERIFY_SIGNAL_SECONDS_INTERVAL), () =>
            {
                try
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        _elapsedSeconds = 0;
                        return false;
                    }

                    UpdateConectedSnoozRssi();
                    var rssi = GetRssiSmoothValue();

                    if (_elapsedSeconds == UPDATE_RSSI_IMAGE_SECONDS_INTERVAL)
                    {
                        UpdateRssiImage(rssi);
                        _elapsedSeconds = 0;
                    }

                    _elapsedSeconds++;
                    return true;
                }
                catch (Exception e)
                {
                    UpdateRssiImage(0);

                    Crashes.TrackError(e, new Dictionary<string, string>()
                    {
                        {"Erro", "RSSI Update error"}
                    });
                    Debug.WriteLine(e.ToString());

                    return true;
                }
            });
        }

        void UpdateConectedSnoozRssi() => BluetoothModule.CurrentlyConnectedSnooz?.UpdateRssiAsync();

        int rssiOldValue = DEFAULT_RSSI_VALUE;
        double GetRssiSmoothValue()
        {
            var rssiNewValue = GetRssiFromConectedSnooz();

            if (rssiNewValue is DEFAULT_RSSI_VALUE)
                return rssiNewValue;


            var newTillOld = rssiNewValue - rssiOldValue;
            var burden = 0.75;

            var smoothValue = burden * rssiNewValue + (1 - burden) * newTillOld;

            rssiOldValue = rssiNewValue;
            return smoothValue;
        }

        int GetRssiFromConectedSnooz() => BluetoothModule.CurrentlyConnectedSnooz is null ? DEFAULT_RSSI_VALUE :
                                          BluetoothModule.CurrentlyConnectedSnooz.Rssi;

        void UpdateRssiImage(double rssi) =>
            RssiImage = RssiImageConverter.GetImageSourceFromRssiValue(rssi);

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine($"MainViewModelV2V3 OnNavigatedFrom {_id}");
            UnsubscribeFromEvents();
            if (_shouldNotRefreshInterface)
                return;

            ActiveControl = ControlType.None;
            RaisePropertyChanged(nameof(ActiveControl));
            RaisePropertyChanged(nameof(IsNightLightEnabled));
            RaisePropertyChanged(nameof(ShowMainControl));
            RaisePropertyChanged(nameof(TimerColumn));
            RaisePropertyChanged(nameof(BabyMonitorColumn));
            RaisePropertyChanged(nameof(SupportColumn));
            RaisePropertyChanged(nameof(FirmwareVersion));

            RequestCancellationToken();
        }

        void RequestCancellationToken() => Interlocked.Exchange(ref this._cancellationToken,
                                                                new CancellationTokenSource()).Cancel();


        private void BluetoothModule_SnoozStatusUpdated(object sender, SnoozStatusUpdatedEventArgs e)
        {
            Debug.WriteLine($"MainViewModelV2V3 SnoozStatusUpdated {_id}");
            this.MotorSpeed = e.StatusUpdate.MotorSpeed;
            App.AutoFadeAdjustment = 17 * this.MotorSpeed / 10; //This is the interger equivalent of 1/0.06, according to ~Weber's Law. 
            this.MotorEnabled = e.StatusUpdate.MotorEnabled;
            switch (this.FirmwareVersion)
            {
                case FirmwareVersions.V3:
                    this.NightLightState = e.StatusUpdate.NightlightEnabledSnooz
                        ? e.StatusUpdate.NightlightLevel1 == 0
                            ? LightState.DarkMode
                            : e.StatusUpdate.NightlightLevel1 <= LOW_LIGHT_LEVEL
                                ? LightState.Low
                                : e.StatusUpdate.NightlightLevel1 <= MEDIUM_LIGHT_LEVEL
                                    ? LightState.Medium
                                    : LightState.High
                        : e.StatusUpdate.NightlightEnabledApp && e.StatusUpdate.NightlightLevel1 == 0
                            ? LightState.DarkMode
                            : LightState.Off;
                    break;
                case FirmwareVersions.V4:
                    this.NightLightState = e.StatusUpdate.NightlightEnabledSnooz
                        ? e.StatusUpdate.NightlightLevel1 == 0
                            ? LightState.DarkMode
                            : e.StatusUpdate.NightlightLevel1 <= LOW_LIGHT_LEVEL
                                ? LightState.Low
                                : e.StatusUpdate.NightlightLevel1 <= MEDIUM_LIGHT_LEVEL
                                    ? LightState.Medium
                                    : LightState.High
                        : e.StatusUpdate.NightlightEnabledApp && e.StatusUpdate.NightlightLevel1 == 0
                            ? LightState.DarkMode
                            : LightState.Off;
                    break;
                case FirmwareVersions.V2:
                    this.NightLightState = e.StatusUpdate.BottomLedBrightness == OFF_LIGHT_LEVEL || e.StatusUpdate.BottomLedBrightness > HIGH_LIGHT_LEVEL
                        ? LightState.Off
                        : e.StatusUpdate.BottomLedBrightness <= LOW_LIGHT_LEVEL
                            ? LightState.Low
                            : e.StatusUpdate.BottomLedBrightness <= MEDIUM_LIGHT_LEVEL
                                ? LightState.Medium
                                : LightState.High;
                    break;
                //case FirmwareVersions.V1:
                default:
                    // V1 firmware does not have a night light
                    this.NightLightState = LightState.Off;
                    break;
            }
            RaisePropertyChanged(nameof(NightLightState));
        }

        async void BluetoothModule_SnoozDisconnected(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine($"MainViewModelV2V3 SnoozDisconnected, App.ClosingTime: " + App.ClosingTime);

            if (!App.ClosingTime)
            {
                if (Device.RuntimePlatform.Equals(Device.Android))
                    Device.BeginInvokeOnMainThread(async () =>
                        await NavigationService.NavigateAsync($"/{nameof(Pages.ConnectionPage)}"));
                else
                    await NavigationService.NavigateAsync($"/{nameof(Pages.ConnectionPage)}");
            }
        }


        async void BluetoothModule_ConnectionLost (object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("MainView Connection Lost");

            if (Device.RuntimePlatform.Equals(Device.Android))
                Device.BeginInvokeOnMainThread(async () =>
                    await NavigationService.NavigateAsync($"/{nameof(Pages.ConnectionPage)}"));
            else
                await NavigationService.NavigateAsync($"/{nameof(Pages.ConnectionPage)}");
        }



void BluetoothModule_SnoozTimerScheduleOnUpdate(object sender, ScheduleResponseFrameObject scheduleResponseFrame)
        {
            Debug.WriteLine($"MainViewModelV2V3 SnoozTimerScheduleOnUpdate {_id}");

            var timersManager = new TimersManager(scheduleResponseFrame);
            CurrentNightDisplay = timersManager.CurrentNightDisplay;
            NumberOfActiveTimers = timersManager.NumberOfActiveTimers;

            _backupJsonTimers = timersManager.ReturnBackupJsonTimers();

            CurrentStateData();

            BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleOnUpdate;

        }

        void BluetoothModule_SnoozTimerScheduleOffUpdate(object sender, ScheduleResponseFrameObject scheduleResponseFrame)
        {
            Debug.WriteLine($"MainViewModelV2V3 SnoozTimerScheduleOffUpdate {_id}");

            var timersManager = new TimersManager(scheduleResponseFrame);
            CurrentNightDisplay = timersManager.CurrentNightDisplay;
            NumberOfActiveTimers = timersManager.NumberOfActiveTimers;

            _backupJsonTimers = timersManager.ReturnBackupJsonTimers();

            CurrentStateData();
            BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleOffUpdate;

        }


        private void MotorPower()
        {
            if (this.MotorEnabled)
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 0);
            }
            else
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
            }
        }

        private void MotorFaster()
        {
            if (this.MotorEnabled)
            {
                if (this.MotorSpeed <= 90)
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte)(this.MotorSpeed + 10));
                }
                else
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 100);
                }
            }
            else
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
            }
        }

        private void MotorSlower()
        {
            if (this.MotorEnabled)
            {
                if (this.MotorSpeed >= 20)
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte)(this.MotorSpeed - 10));
                }
                else
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 10);
                }
            }
            else
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
            }
        }

        void SetNightLightLevel(object obj)
        {
            if (obj is LightState state)
            {
                var lightLevel1 = OFF_LIGHT_LEVEL;
                var enabled = true;
                switch (state)
                {
                    case LightState.Low:
                        lightLevel1 = LOW_LIGHT_LEVEL;
                        break;
                    case LightState.Medium:
                        lightLevel1 = MEDIUM_LIGHT_LEVEL;
                        break;
                    case LightState.High:
                        lightLevel1 = HIGH_LIGHT_LEVEL;
                        break;
                    case LightState.DarkMode:
                        break;
                    default:
                        enabled = false;
                        break;
                }
                if (FirmwareVersion >= FirmwareVersions.V3)
                {
                    BluetoothModule.WriteNightlightCommand(enabled, (byte)lightLevel1,
                                                           (byte)(lightLevel1 == OFF_LIGHT_LEVEL
                                                                    ? lightLevel1
                                                                    : lightLevel1 + LIGHT_LEVEL_INCREMENT));
                }
                else
                {
                    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.BottomLed, (byte)lightLevel1);
                }
            }
        }

        void SetActiveControl(object obj)
        {
            if (obj is ControlType newControlType)
            {
                if (ActiveControl == newControlType)
                {
                    ActiveControl = ControlType.None;
                }
                else
                {
                    switch (newControlType)
                    {
                        case ControlType.BabyMonitor when !AudioLevelModule.Calibrated:
                            CalibrateCommand.Execute(null);
                            break;
                        case ControlType.Timer:
                            //UserEnabledTimer = false;
                            SchedulerCommand.Execute(null);
                            break;
                        case ControlType.Support:
                            HelpIndexCommand.Execute(null);
                            break;
                    }

                    ActiveControl = newControlType;
                }

                RaisePropertyChanged(nameof(ActiveControl));
                RaisePropertyChanged(nameof(ShowMainControl));
                //RaisePropertyChanged(nameof(TimerUIDisabled));
            }
        }

        void UnsubscribeFromEvents()
        {
            BluetoothModule.SnoozStatusUpdated -= BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected -= BluetoothModule_SnoozDisconnected;
            BluetoothModule.SnoozConnectionLost -= BluetoothModule_ConnectionLost;
        }

        public override void Destroy()
        {
            Debug.WriteLine($"MainViewModelV2V3 Destroy {_id}");
            UnsubscribeFromEvents();
        }

    }
}
