﻿using System;
using System.Windows.Input;
using Prism.Navigation;
using SnoozApp.Pages;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace SnoozApp.ViewModels
{
    public class ContactUsViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand ContactUsCommand => new Command(ContactUs);

        public ContactUsViewModel(INavigationService navigationService) 
            : base(navigationService) { }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { {nameof(BluetoothOffPopupPage), true} };

        }

        static void ContactUs() => Launcher.TryOpenAsync("mailto:App@getsnooz.com");
 
    }
}
