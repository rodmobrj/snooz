﻿using Prism.Navigation;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace SnoozApp.ViewModels
{
    public class DeviceSupportViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand ContactUsCommand => new Command(ContactUs);
        public ICommand OpenManualCommand => new Command(OpenManualLink);

        public DeviceSupportViewModel(INavigationService navigationService) 
            : base(navigationService) {}

        static void OpenManualLink() =>
           Launcher.TryOpenAsync("https://cdn.shopify.com/s/files/1/1378/8621/files/SNOOZ-US-2-User-Manual.pdf?97");

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { {nameof(BluetoothOffPopupPage), true} };
        }

        static void ContactUs() => Launcher.TryOpenAsync("mailto:App@getsnooz.com");
    }
}