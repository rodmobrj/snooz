﻿using Newtonsoft.Json;
using Prism.Navigation;
using SnoozApp.Modules;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SnoozApp.ViewModels
{
    public class ConnectionSupportViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand ContactUsCommand => new Command(ContactUs);

        public ConnectionSupportViewModel(INavigationService navigationService) : base(navigationService)
            => MessagingCenter.Subscribe<EventArgs>(this, App.CONFIRM_FORGET_MESSAGE, ResetSnooz);

        //Purpose is to show help overlay on first time connect or when user selects help button
        bool _resetActive;
        public bool ResetActive
        {
            get => _resetActive;
            set => SetProperty(ref _resetActive, value);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            ResetActive = true;


            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { { nameof(BluetoothOffPopupPage), true } };
        }

        static void ContactUs() => Launcher.TryOpenAsync("mailto:App@getsnooz.com");

        async void ResetSnooz(object args = null)
        {
            if (ResetActive)
            {
                try
                {
                    await BluetoothModule.DisconnectDevicesAsync();
                    // forget all devices
                    var coupledDevices = JsonConvert.DeserializeObject<List<CoupledSnooz>>
                                                        (SettingsModule.CoupledDevices);
                    if (coupledDevices != null)
                        foreach (var device in coupledDevices)
                        {
                            BluetoothModule.ForgetCoupledSnooz(device.Id);
                            Preferences.Set(device.Id.ToString(), string.Empty);
                            Preferences.Set($"{device.Id}{SettingsModule.BASE_SAVED_ADVANCED_SCHEDULE_KEY}", string.Empty);
                        }

                    // reset all settings
                    SettingsModule.CalibrationData = JsonConvert.SerializeObject(null);
                    SettingsModule.LastConnectedDevice = JsonConvert.SerializeObject(null);
                    SettingsModule.SignalMonitorEnabled = false;

                    ResetActive = false;

                    // Device.BeginInvokeOnMainThread(async () =>
                    //   await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}"));
                }
                catch
                {
                }
            }
        }

    }
}
