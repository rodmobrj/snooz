﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Newtonsoft.Json;
using Plugin.BLE.Abstractions.EventArgs;
using Prism.Navigation;
using SnoozApp.Enums;
using SnoozApp.Modules;
using SnoozApp.Pages;
using Xamarin.Forms;
using static SnoozApp.Modules.BluetoothModule;

namespace SnoozApp.ViewModels
{
    public class MainViewModelV1 : BaseViewModel
    {
        private const int OFF_LIGHT_LEVEL = 0;
        private const int LOW_LIGHT_LEVEL = 20;
        private const int MEDIUM_LIGHT_LEVEL = 50;
        private const int HIGH_LIGHT_LEVEL = 90;
        private const int LIGHT_LEVEL_INCREMENT = 10;

        private const int NUM_MOTOR_LEVEL_GRADATIONS = 10;
        private const int MIN_MOTOR_LEVEL = 1;
        private const int MAX_MOTOR_LEVEL = 10;


// ------------------ Start: APP Usage History Generator ----------

        private void CurrentStateData()
        {
            GetConnectionHistory();
            
            //Get current motor speed
            this.LastMotorSpeed = CurrentMotorLevel;

            //Determine firmware version
            this.LastFirmware = 1;

            //Count number of scheduled days
			this.SchedulerInUse = 0;
                     
            //Determine Nightlight state
            this.LastLightState = 0;


            //Determine if nursery monitor calibrated
            if (AudioLevelModule.Calibrated == true)
            {
                this.EverCalibrated = 1;
            }
            else
            {
                this.EverCalibrated = 0;
            }

            //Calcualte how many times app has been connected
            //this.ConnectionCount++;       

            SetConnectionHistory();
        }

        public class ConnectionHistory
        {
            public int LastMotorSpeed;
            public int LastFirmware;
			public int SchedulerInUse;
            public int LastLightState;
            public int EverCalibrated;
            public int ConnectionCount;


			public ConnectionHistory(int lastmotorspeed, int lastfirmware, int schedulerInUse, int lastlightstate, int evercalibrated, int connectioncount)
            {
                LastMotorSpeed = lastmotorspeed;
                LastFirmware = lastfirmware;
				SchedulerInUse = schedulerInUse;
                LastLightState = lastlightstate;
                EverCalibrated = evercalibrated;
                ConnectionCount = connectioncount;
            }
        }

        void SetConnectionHistory()
            => SettingsModule.ConnectionHistory = JsonConvert.SerializeObject
                (new ConnectionHistory(LastMotorSpeed, 
                                       LastFirmware, 
                                       SchedulerInUse, 
                                       LastLightState, 
                                       EverCalibrated, 
                                       ConnectionCount));

        void GetConnectionHistory()
        {
            try
            {
                var convertedConnectionHistory = JsonConvert.DeserializeObject<ConnectionHistory>(SettingsModule.ConnectionHistory);
                if (convertedConnectionHistory != null)
                    ConnectionCount = convertedConnectionHistory.ConnectionCount;
                else
                    ConnectionCount = 0;
            }
            catch
            {
                ConnectionCount = 0;
                Debug.WriteLine("Connection Count catch");

            }
        }

        private int _LastMotorSpeed;
        public int LastMotorSpeed
        {
            get
            {
                return this._LastMotorSpeed;
            }
            set
            {
                this.SetProperty(ref this._LastMotorSpeed, value);
            }
        }

        private int _LastFirmware;
        public int LastFirmware
        {
            get
            {
                return this._LastFirmware;
            }
            set
            {
                this.SetProperty(ref this._LastFirmware, value);
            }
        }

		private int _SchedulerInUse;
		public int SchedulerInUse
        {
            get
            {
				return this._SchedulerInUse;
            }
            set
            {
				this.SetProperty(ref this._SchedulerInUse, value);
            }
        }


        private int _LastLightState;
        public int LastLightState
        {
            get
            {
                return this._LastLightState;
            }
            set
            {
                this.SetProperty(ref this._LastLightState, value);
            }
        }

        private int _EverCalibrated;
        public int EverCalibrated
        {
            get
            {
                return this._EverCalibrated;
            }
            set
            {
                this.SetProperty(ref this._EverCalibrated, value);
            }
        }

        private int _ConnectionCount;
        public int ConnectionCount
        {
            get
            {
                return this._ConnectionCount;
            }
            set
            {
                this.SetProperty(ref this._ConnectionCount, value);
            }
        }

        // ------------------ End: APP Usage History Generator ----------

        private AudioLevelModule _AudioLevelModule;
        public AudioLevelModule AudioLevelModule
        {
            get
            {
                return this._AudioLevelModule;
            }
            set
            {
                this.SetProperty(ref this._AudioLevelModule, value);
            }
        }

        public MainViewFadeModel FadeModel { get; set; }

        private ushort _MotorSpeed;
        public ushort MotorSpeed
        {
            get
            {
                return this._MotorSpeed;
            }
            set
            {
                this.SetProperty(ref this._MotorSpeed, value);
                RaisePropertyChanged(nameof(CurrentMotorLevel));
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }

        private bool _MotorEnabled;
        public bool MotorEnabled
        {
            get
            {
                return this._MotorEnabled;
            }
            set
            {
                this.SetProperty(ref this._MotorEnabled, value);
                RaisePropertyChanged(nameof(SlowerEnabled));
                RaisePropertyChanged(nameof(FasterEnabled));
            }
        }

        public int CurrentMotorLevel => (this.MotorSpeed / NUM_MOTOR_LEVEL_GRADATIONS) <= 10
                                      ? (this.MotorSpeed / NUM_MOTOR_LEVEL_GRADATIONS)
                                      : NUM_MOTOR_LEVEL_GRADATIONS;
        
        public bool SlowerEnabled => this.MotorEnabled && this.CurrentMotorLevel > MIN_MOTOR_LEVEL;
        public bool FasterEnabled => this.MotorEnabled && this.CurrentMotorLevel < MAX_MOTOR_LEVEL;

        public string DeviceName => BluetoothModule.CurrentlyConnectedSnoozName;

        public int BabyMonitorColumn => this.IsNightLightEnabled ? 2 : 1;
        public int TimerColumn => this.IsNightLightEnabled ? 1 : 0;
        public bool IsNightLightEnabled => this.BluetoothModule.ConnectedSnoozHasNightLight
                                        || this.FirmwareVersion >= FirmwareVersions.V3;
        public FirmwareVersions FirmwareVersion => this.BluetoothModule.FirmwareVersion;


        private LightState _nightLightState;
        public LightState NightLightState
        {
            get => this._nightLightState;
            set
            {
                this._nightLightState = value;
                RaisePropertyChanged(nameof(this.NightLightState));
                RaisePropertyChanged(nameof(this.DarkModeOn));
            }
        }

        public bool DarkModeOn
        {
            get => this.NightLightState == LightState.DarkMode;
            set
            {
                if (value && this.NightLightState != LightState.DarkMode)
                {
                    this.SetNightLightLevel(LightState.DarkMode);
                }
                else if (!value && this.NightLightState != LightState.Off)
                {
                    this.SetNightLightLevel(LightState.Off);
                }
            }
        }

        public int NumberOfActiveTimers => (this.IsStartTimerActive ? 1 : 0) +
                                           (this.IsStopTimerActive ? 1 : 0);

        public NightEnabled InputNightEnabled { get; set; }

        private List<EndPoint> StartTimes { get; set; }
        private List<EndPoint> StopTimes { get; set; }

        public Night CurrentActiveNight
        {
            get
            {
                // the current timer is either one occuring right now
                // or the next scheduled one, within a day.
                var sundayStart = DateTime.Today.AddDays(-(int)DateTime.Today.DayOfWeek + (int)DayOfWeek.Sunday);
                var upcomingTimers = Enumerable
                        .Range(0, 7)
                        .Where(index =>
                        {
                            var night = this.WeekSchedule?[index];
                            return night != null && night.Enabled
                                && ((night.Stop.Enabled
                                        && sundayStart + night.Stop.Time > DateTime.Now
                                        && sundayStart + night.Stop.Time <= DateTime.Now.AddHours(24))
                                    || (night.Start.Enabled
                                        && sundayStart + night.Start.Time > DateTime.Now
                                        && sundayStart + night.Start.Time <= DateTime.Now.AddHours(24)));
                        })
                        .ToList();
                return upcomingTimers.Any()
                    ? this.WeekSchedule?[upcomingTimers.FirstOrDefault()]
                    : new Night
                    {
                        Start = new EndPoint { Enabled = false },
                        Stop = new EndPoint { Enabled = false }
                    };
            }
        }
        public string CurrentNightDisplay => this.CurrentActiveNight.Display;

        public List<Night> WeekSchedule
        {
            get
            {
                return this.StartTimes == null || this.StopTimes == null
                    ? null
                    : this.StartTimes.Zip(this.StopTimes, (start, stop) => new Night
                    {
                        Start = start,
                        Stop = stop
                    }).ToList();
            }
        }

        public static List<string> NightNames = new List<string>
        {
            "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
        };

        public ObservableCollection<NightDisplay> NightDisplays
        {
            get
            {
                return new ObservableCollection<NightDisplay>(this.WeekSchedule?
                    .Zip(NightNames, (night, name) => new NightDisplay
                    {
                        NightName = name,
                        NightTimesDisplay = night.Display
                    })
                    .Where(display => !string.IsNullOrEmpty(display.NightTimesDisplay)) ?? new List<NightDisplay>());
            }
        }

        public List<bool> NightsSelected { get; set; }
        public bool AnySelected => this.NightsSelected.Any(b => b);

        private TimeSpan _StartTimer;
        public TimeSpan StartTimer
        {
            get
            {
                return this._StartTimer;
            }
            set
            {
                if (this._StartTimer != value)
                {
                    this.SetProperty(ref this._StartTimer, value);
                    Debug.WriteLine("Start timer set " + value.Hours + ":" + value.Minutes);
                }
            }
        }

        private uint _StartTimeRemaining;
        public uint StartTimeRemaining
        {
            get => this._StartTimeRemaining;
            set
            {
                this.SetProperty(ref this._StartTimeRemaining, value);
                RaisePropertyChanged(nameof(IsStartTimerActive));
                RaisePropertyChanged(nameof(NumberOfActiveTimers));
            }
        }

        public bool IsStartTimerActive
        {
            get
            {
                return this.FirmwareVersion <= FirmwareVersions.V1
                    ? this.StartTimeRemaining > 0
                    : this.CurrentActiveNight.Start.Enabled;
            }
        }

        private TimeSpan _StopTimer;
        public TimeSpan StopTimer
        {
            get
            {
                return this._StopTimer;
            }
            set
            {
                if (this._StopTimer != value)
                {
                    if (this.FirmwareVersion <= FirmwareVersions.V1)
                    {
                        if (this.StopTimeRemaining != 0)
                        {
                            TimeSpan time = value - DateTime.Now.TimeOfDay;
                            if (time < TimeSpan.Zero)
                            {
                                time = time + TimeSpan.FromHours(24);
                            }
                            UInt32 timeRemaining = (UInt32)time.TotalSeconds;
                            this.BluetoothModule.WriteTimerCommand(timeRemaining, value.Hours, value.Minutes);
                        }
                    }

                    this.SetProperty(ref this._StopTimer, value);
                    Debug.WriteLine("Stop timer set " + value.Hours + ":" + value.Minutes);
                }
            }
        }

        private uint _StopTimeRemaining;
        public uint StopTimeRemaining
        {
            get
            {
                return this._StopTimeRemaining;
            }
            set
            {
                this.SetProperty(ref this._StopTimeRemaining, value);
                RaisePropertyChanged(nameof(IsStopTimerActive));
                RaisePropertyChanged(nameof(NumberOfActiveTimers));
            }
        }

        public bool IsStopTimerActive
        {
            get
            {
                return this.FirmwareVersion <= FirmwareVersions.V1
                    ? this.StopTimeRemaining > 0
                    : this.CurrentActiveNight.Stop.Enabled;
            }
        }

        private float _DecibelLevel;
        public float DecibelLevel
        {
            get
            {
                return this._DecibelLevel;
            }
            set
            {
                this.SetProperty(ref this._DecibelLevel, value);
            }
        }

        private bool UserEnabledTimer { get; set; }
        public bool TimerUIDisabled => !(this.FirmwareVersion <= FirmwareVersions.V1)
                                    && !this.UserEnabledTimer
                                    && !(this.WeekSchedule?.Any(night => night.Enabled) ?? false)
                                    && this.ActiveControl == ControlType.Timer;

        public bool ShowMainControl
        {
            get
            {
                return this.FirmwareVersion == FirmwareVersions.V1
                    || this.ActiveControl != ControlType.Timer;
            }
        }

        public string FadeIn
        {
            get => this.FadeModel.FadeIn;
            set => this.FadeModel.FadeIn = value;
        }

        public string FadeOut
        {
            get => this.FadeModel.FadeOut;
            set => this.FadeModel.FadeOut = value;
        }

        private ControlType _ActiveControl;
        public ControlType ActiveControl
        {
            get => this._ActiveControl;
            set => this.SetProperty(ref this._ActiveControl, value);
        }

        public ICommand PowerButtonPress { get; private set; }
        public ICommand FasterButtonPress { get; private set; }
        public ICommand SlowerButtonPress { get; private set; }
        public ICommand ToggleStopTimerCommand { get; private set; }
        public ICommand ToggleStartTimerCommand { get; private set; }
        public ICommand ControlSelectorButtonPress { get; private set; }
        public ICommand SetNightLightLevelCommand { get; private set; }
        public ICommand CalibrateButtonPress { get; private set; }
        public ICommand DevicesPageButtonPress { get; private set; }
        public ICommand StopTimePickerPressed { get; private set; }
        public ICommand StopTimePickerExited { get; private set; }
        public ICommand StartTimePickerPressed { get; private set; }
        public ICommand StartTimePickerExited { get; private set; }
        public ICommand WriteScheduleCommand { get; private set; }
        public ICommand ToggleNightCommand { get; private set; }
        public ICommand ClearNightScheduleCommand { get; private set; }
        public ICommand ToggleScheduleEnabledCommand { get; private set; }
        public ICommand ToggleDarkModeCommand { get; private set; }

        public MainViewModelV1(INavigationService navigationService) : base(navigationService)
        {
            FadeModel = new MainViewFadeModel(this.BluetoothModule, this.FadeTimersUpdated);

            PowerButtonPress = new Command(this.MotorPower);
            FasterButtonPress = new Command(this.MotorFaster);
            SlowerButtonPress = new Command(this.MotorSlower);
            ToggleStopTimerCommand = new Command(this.ToggleStopTimer);
            ToggleStartTimerCommand = new Command(this.ToggleStartTimer);
            ControlSelectorButtonPress = new Command(this.SetActiveControl);
            SetNightLightLevelCommand = new Command(this.SetNightLightLevel);
            CalibrateButtonPress = CalibrateCommand;
            DevicesPageButtonPress = DevicesCommand;
            StopTimePickerPressed = new Command(this.HandleStopTimePickerPressed);
            StopTimePickerExited = new Command(this.HandleStopTimePickerExited);
            StartTimePickerPressed = new Command(this.HandleStartTimePickerPressed);
            StartTimePickerExited = new Command(this.HandleStartTimePickerExited);
            WriteScheduleCommand = new Command(this.WriteSchedule);
            ToggleNightCommand = new Command(this.ToggleNightSelected);
            ClearNightScheduleCommand = new Command(this.ClearNightSchedule);
            ToggleScheduleEnabledCommand = new Command(this.ToggleScheduleEnabled);
            ToggleDarkModeCommand = new Command(() => this.DarkModeOn = !this.DarkModeOn);

            AudioLevelModule = AudioLevelModule.Instance;
            InputNightEnabled = new NightEnabled
            {
                StartEnabled = true,
                StopEnabled = true
            };
            NightsSelected = Enumerable.Repeat(false, 7).ToList();

            _StartTimer = new TimeSpan(22, 0, 0);
            _StopTimer = new TimeSpan(6, 0, 0);
        }

        public void FadeTimersUpdated()
        {
            RaisePropertyChanged(nameof(this.FadeIn));
            RaisePropertyChanged(nameof(this.FadeOut));
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            CurrentStateData();
            ActiveControl = ControlType.None;

            Debug.WriteLine("MainViewModel OnNavigatedTo");
            BluetoothModule.SnoozStatusUpdated += BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected += BluetoothModule_SnoozDisconnected;
            BluetoothModule.SnoozTimerOffScheduleUpdate += BluetoothModule_SnoozTimerScheduleUpdate;
            //BluetoothModule.SnoozTimerOnScheduleUpdate += BluetoothModule_SnoozTimerScheduleUpdate;
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.UpdateStatus);
           // FadeModel.Subscribe();

            //if (FirmwareVersion >= FirmwareVersions.V2)
            //{
            //    BluetoothModule.WriteSyncTimeCommand();
            //    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.ScheduleRequest);
            //}

            //if (FirmwareVersion >= FirmwareVersions.V3)
            //    BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.FadeRequest);

            RaisePropertyChanged(nameof(IsNightLightEnabled));
            RaisePropertyChanged(nameof(TimerColumn));
            RaisePropertyChanged(nameof(BabyMonitorColumn));
            RaisePropertyChanged(nameof(FirmwareVersion));
            RaisePropertyChanged(nameof(ActiveControl));
            RaisePropertyChanged(nameof(ShowMainControl));
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("MainViewModel OnNavigatedFrom");
            UnsubscribeFromEvents();

            //BluetoothModule.SnoozStatusUpdated -= BluetoothModule_SnoozStatusUpdated;
            //BluetoothModule.SnoozDisconnected -= BluetoothModule_SnoozDisconnected;
            //BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
            //BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleUpdate;
            //FadeModel.Unsubscribe();
            ActiveControl = ControlType.None;
        }

        private void MotorPower()
        {
            if (this.MotorEnabled)
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 0);
            }
            else
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
            }
        }

        private void MotorFaster()
        {
            if (this.MotorEnabled)
            {
                if (this.MotorSpeed <= 90)
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte)(this.MotorSpeed + 10));
                }
                else
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 100);
                }
            }
            else
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
            }
        }

        private void MotorSlower()
        {
            if (this.MotorEnabled)
            {
                if (this.MotorSpeed >= 20)
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, (byte)(this.MotorSpeed - 10));
                }
                else
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorSpeed, 10);
                }
            }
            else
            {
                this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.MotorEnabled, 1);
            }
        }

        private void ToggleStopTimer()
        {
            if (this.FirmwareVersion <= FirmwareVersions.V1)
            {
                if (this.IsStopTimerActive)
                {
                    this.BluetoothModule.WriteTimerCommand(0, this.StopTimer.Hours, this.StopTimer.Minutes);
                }
                else
                {
                    var time = this.StopTimer - DateTime.Now.TimeOfDay;
                    if (time < TimeSpan.Zero)
                    {
                        time = time + TimeSpan.FromHours(24);
                    }
                    var timeRemaining = (uint)time.TotalSeconds;
                    this.BluetoothModule.WriteTimerCommand(timeRemaining, this.StopTimer.Hours, this.StopTimer.Minutes);
                }
            }
            else
            {
                this.InputNightEnabled.StopEnabled = !this.InputNightEnabled.StopEnabled;
                RaisePropertyChanged(nameof(this.InputNightEnabled));
            }

        }

        private void ToggleStartTimer()
        {
            if (this.FirmwareVersion >= FirmwareVersions.V2)
            {
                this.InputNightEnabled.StartEnabled = !this.InputNightEnabled.StartEnabled;
                RaisePropertyChanged(nameof(this.InputNightEnabled));
            }
        }

        private void SetNightLightLevel(object obj)
        {
            if (this.FirmwareVersion <= FirmwareVersions.V1)
            {
                return;
            }
            if (obj is LightState state)
            {
                var lightLevel1 = OFF_LIGHT_LEVEL;
                var enabled = true;
                switch (state)
                {
                    case LightState.Low:
                        lightLevel1 = LOW_LIGHT_LEVEL;
                        break;
                    case LightState.Medium:
                        lightLevel1 = MEDIUM_LIGHT_LEVEL;
                        break;
                    case LightState.High:
                        lightLevel1 = HIGH_LIGHT_LEVEL;
                        break;
                    case LightState.DarkMode:
                        break;
                    case LightState.Off:
                    default:
                        enabled = false;
                        break;
                }
                if (this.FirmwareVersion >= FirmwareVersions.V3)
                {
                    this.BluetoothModule.WriteNightlightCommand(enabled, (byte)lightLevel1,
                                                                (byte)(lightLevel1 == OFF_LIGHT_LEVEL
                                                                    ? lightLevel1
                                                                    : lightLevel1 + LIGHT_LEVEL_INCREMENT));
                }
                else
                {
                    this.BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.BottomLed, (byte)lightLevel1);
                }
            }
        }

        void SetActiveControl(object obj)
        {
            if (obj is ControlType newControlType)
            {
                if (ActiveControl == newControlType)
                {
                    ActiveControl = ControlType.None;
                }
                else
                {
                    if (newControlType == ControlType.BabyMonitor && !AudioLevelModule.Calibrated)
                        CalibrateButtonPress.Execute(null);
                    if (newControlType == ControlType.Timer)
                        UserEnabledTimer = false;
                    if (newControlType == ControlType.Support)
                        HelpIndexCommand.Execute(null);
                    ActiveControl = newControlType;
                }
                RaisePropertyChanged(nameof(ActiveControl));
                RaisePropertyChanged(nameof(ShowMainControl));
                RaisePropertyChanged(nameof(TimerUIDisabled));
            }
        }

        private void HandleStartTimePickerExited()
        {
            if (!this.IsStartTimerActive)
            {
                this.ToggleStartTimer();
            }
        }

        private void HandleStartTimePickerPressed()
        {
            if (!this.IsStartTimerActive)
            {
                this.ToggleStartTimer();
            }
            if (this.ActiveControl != ControlType.Timer)
            {
                this.SetActiveControl(ControlType.Timer);
            }
        }

        private void HandleStopTimePickerPressed()
        {
            if (!this.IsStopTimerActive)
            {
                this.ToggleStopTimer();
            }
            if (this.ActiveControl != ControlType.Timer)
            {
                this.SetActiveControl(ControlType.Timer);
            }
        }

        private void HandleStopTimePickerExited()
        {
            if (!this.IsStopTimerActive)
            {
                this.ToggleStopTimer();
            }
        }

        private void WriteSchedule()
        {
            if (!this.AnySelected)
            {
                // effectively a soft disable on the button
                return;
            }

            // Write current status of control selection to current schedule
            // i.e. if T & W are checked, write the selected start/stop to their schedule
            var newWeekSchedule = this.WeekSchedule.Select((existingNight, index) =>
                this.NightsSelected[index]
                    ? new Night(this.InputNightEnabled.StartEnabled, this.StartTimer, this.InputNightEnabled.StopEnabled, this.StopTimer, (index + 1) % 7)
                    : existingNight
            ).ToList();

            // update control selection UI (checked, potentially time selection)
            // don't update schedule UI - this should be handled when the bluetooth responds
            this.NightsSelected = Enumerable.Repeat(false, 7).ToList();
            RaisePropertyChanged(nameof(this.NightsSelected));
            RaisePropertyChanged(nameof(this.AnySelected));

            // send schedule update to bluetooth
            this.BluetoothModule.WriteSchedule(newWeekSchedule);
        }

        private void ToggleNightSelected(object obj)
        {
            if (obj is string nightIndexString
                && int.TryParse(nightIndexString, out var nightIndex)
                && nightIndex >= 0
                && nightIndex < 7)
            {
                this.NightsSelected[nightIndex] = !this.NightsSelected[nightIndex];
                RaisePropertyChanged(nameof(this.NightsSelected));
                RaisePropertyChanged(nameof(this.AnySelected));
            }
        }

        static Dictionary<Guid, List<NightEnabled>> StoredNightsEnabled
        {
            get
            {
                var daysEnabledJson = SettingsModule.DeviceScheduleMask;
                if (string.IsNullOrEmpty(daysEnabledJson))
                {
                    return new Dictionary<Guid, List<NightEnabled>>();
                }
                try
                {
                    var daysEnabledDict = JsonConvert.DeserializeObject<Dictionary<Guid, List<NightEnabled>>>(daysEnabledJson);
                    return daysEnabledDict ?? new Dictionary<Guid, List<NightEnabled>>();
                }
                catch
                {
                    return new Dictionary<Guid, List<NightEnabled>>();
                }
            }
            set => SettingsModule.DeviceScheduleMask = JsonConvert.SerializeObject(value);
        }

        private void ToggleScheduleEnabled()
        {
            if (this.WeekSchedule.Any(night => night.Enabled) || this.UserEnabledTimer)
            {
                // disable the scheduler
                var nightsEnabled = this.WeekSchedule.Select(night => new NightEnabled
                {
                    StartEnabled = night.Start.Enabled,
                    StopEnabled = night.Stop.Enabled
                })
                .ToList();
                var connectedSnoozId = this.BluetoothModule.CurrentlyConnectedSnooz.Id;

                var nightsEnabledDict = StoredNightsEnabled;
                nightsEnabledDict[connectedSnoozId] = nightsEnabled;
                StoredNightsEnabled = nightsEnabledDict;

                var disabledWeekSchedule = this.WeekSchedule.Select((existingNight, index) =>
                    new Night(false, existingNight.Start.Time, false, existingNight.Stop.Time)
                ).ToList();

                // write all disabled nights down to the snooz
                this.BluetoothModule.WriteSchedule(disabledWeekSchedule);
                this.UserEnabledTimer = false;
            }
            else
            {
                // no nights were enabled, so we're re-enabling the scheduler
                // this will allow editing of a timer, though the 'enabled' state will not persist
                // it doesn't mean anything to enable the schedule if you don't schedule anything
                this.UserEnabledTimer = true;

                if (!this.WeekSchedule.All(night => night.IsDefault))
                {
                    // if all nights have the default value, assume a reset has occurred
                    // in that case, do not want to restore any settings
                    var nightsEnabledDict = StoredNightsEnabled;
                    if (nightsEnabledDict.TryGetValue(this.BluetoothModule.CurrentlyConnectedSnooz.Id,
                                                    out var daysEnabled)
                        && daysEnabled != null
                        && daysEnabled.Count == 7)
                    {
                        // restore previous setting
                        this.BluetoothModule
                            .WriteSchedule(this.WeekSchedule.Select((night, index) =>
                                new Night(daysEnabled[index].StartEnabled, night.Start.Time, daysEnabled[index].StopEnabled, night.Stop.Time)
                            ));
                    }
                }
            }
            RaisePropertyChanged(nameof(this.TimerUIDisabled));
        }

        private void ClearNightSchedule(object obj)
        {
            if (obj is string nightName
                && NightNames.Contains(nightName))
            {
                if (this.WeekSchedule.Where(night => night.Enabled).Count() == 1)
                {
                    // if we're deleting the last one, we'll go into the disabled state.
                    // we want to save that no nights are enabled
                    var storedNightsEnabled = StoredNightsEnabled;
                    storedNightsEnabled[this.BluetoothModule.CurrentlyConnectedSnooz.Id] = null;
                    StoredNightsEnabled = storedNightsEnabled;
                }
                var nightIndex = NightNames.IndexOf(nightName);
                // disable the indicated night
                var newWeekSchedule = this.WeekSchedule.Select((existingNight, index) =>
                                                            nightIndex == index
                                                                ? new Night(false, existingNight.Start.Time, false, existingNight.Stop.Time)
                                                                : existingNight)
                                                    .ToList();

                this.BluetoothModule.WriteSchedule(newWeekSchedule);
            }
        }

        private void BluetoothModule_SnoozStatusUpdated(object sender, SnoozStatusUpdatedEventArgs e)
        {
            //Debug.WriteLine("MainViewModel SnoozStatusUpdated");
            this.MotorSpeed = e.StatusUpdate.MotorSpeed;
            this.MotorEnabled = e.StatusUpdate.MotorEnabled;
            switch (this.FirmwareVersion)
            {
                case FirmwareVersions.V3:
                    this.NightLightState = e.StatusUpdate.NightlightEnabledSnooz
                        ? e.StatusUpdate.NightlightLevel1 == 0
                            ? LightState.DarkMode
                            : e.StatusUpdate.NightlightLevel1 <= LOW_LIGHT_LEVEL
                                ? LightState.Low
                                : e.StatusUpdate.NightlightLevel1 <= MEDIUM_LIGHT_LEVEL
                                    ? LightState.Medium
                                    : LightState.High
                        : e.StatusUpdate.NightlightEnabledApp && e.StatusUpdate.NightlightLevel1 == 0
                            ? LightState.DarkMode
                            : LightState.Off;
                    break;
                case FirmwareVersions.V2:
                    this.NightLightState = e.StatusUpdate.BottomLedBrightness == OFF_LIGHT_LEVEL || e.StatusUpdate.BottomLedBrightness > HIGH_LIGHT_LEVEL
                        ? LightState.Off
                        : e.StatusUpdate.BottomLedBrightness <= LOW_LIGHT_LEVEL
                            ? LightState.Low
                            : e.StatusUpdate.BottomLedBrightness <= MEDIUM_LIGHT_LEVEL
                                ? LightState.Medium
                                : LightState.High;
                    break;
                case FirmwareVersions.V1:
                default:
                    // V1 firmware does not have a night light
                    this.NightLightState = LightState.Off;
                    break;
            }
            RaisePropertyChanged(nameof(this.NightLightState));
            // Timers sent in the status frame are only for V1. Otherwise they are sent through
            // the response frame
            if (this.FirmwareVersion <= FirmwareVersions.V1)
            {
                this.StartTimeRemaining = 0;
                this.StopTimeRemaining = e.StatusUpdate.TimeRemaining;

                if (TimeSpan.FromMinutes(e.StatusUpdate.TimerHour * 60 + e.StatusUpdate.TimerMinute) != this._StopTimer)
                {
                    this._StopTimer = TimeSpan.FromMinutes(e.StatusUpdate.TimerHour * 60 + e.StatusUpdate.TimerMinute);
                    RaisePropertyChanged(nameof(StopTimer));
                }
            }
            RaisePropertyChanged(nameof(this.CurrentActiveNight));
            RaisePropertyChanged(nameof(this.CurrentNightDisplay));
            RaisePropertyChanged(nameof(this.NumberOfActiveTimers));
        }

        async void BluetoothModule_SnoozDisconnected(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine($"MainViewModelV1 SnoozDisconnected, App.ClosingTime: " + App.ClosingTime);
            UnsubscribeFromEvents();

            // If the app is closing don't go to the connection page yet.
            // Wait for the OnResume to open the connection page
            if (!App.ClosingTime)
            {
                if (Device.RuntimePlatform.Equals(Device.Android))
                    Device.BeginInvokeOnMainThread(async () =>
                        await NavigationService.NavigateAsync($"/{nameof(Pages.ConnectionPage)}"));
                else
                    await NavigationService.NavigateAsync($"/{nameof(Pages.ConnectionPage)}");
            }



            //if (!App.ClosingTime)
            //    Device.BeginInvokeOnMainThread(async () =>
            //    {
            //        await NavigationService.GoBackAsync();
            //        await NavigationService.NavigateAsync(nameof(ConnectionPage));
            //    });
        }

        private void BluetoothModule_SnoozTimerScheduleUpdate(object sender, ScheduleResponseFrameObject scheduleResponseFrame)
        {
            // Time Remaining is only used for determining if the timer is enabled or not so we can just set it to 1 or 0
            var timeRemaining = (scheduleResponseFrame.TimerEnableMask & 0x1) == 0x1 ? 1 : 0;

            var timersEnabled = Enumerable.Range(0, 7)
                                          .Select(i => (scheduleResponseFrame.TimerEnableMask >> i & 0x01) == 0x01)
                                          .ToList();
            var timers = scheduleResponseFrame.Timers
                                              .Select(t => TimeSpan.FromMinutes(t))
                                              .ToList();

            if (this.FirmwareVersion <= FirmwareVersions.V1)
            {
                if (scheduleResponseFrame.Command == BluegigaResponseCommand.SendOffSchedule)
                {
                    this.StopTimeRemaining = (uint)timeRemaining;
                    this._StopTimer = timers[0];
                    RaisePropertyChanged(nameof(this.StopTimer));
                }
                else
                {
                    this.StartTimeRemaining = (uint)timeRemaining;
                    this._StartTimer = timers[0];
                    RaisePropertyChanged(nameof(this.StartTimer));
                }
            }
            else
            {
                var newTimes = timers.Zip(timersEnabled, (timer, enabled) => new EndPoint
                {
                    Time = timer,
                    Enabled = enabled
                }).ToList();
                if (scheduleResponseFrame.Command == BluegigaResponseCommand.SendOffSchedule)
                {
                    this.StopTimes = newTimes;
                }
                else if (scheduleResponseFrame.Command == BluegigaResponseCommand.SendOnSchedule)
                {
                    this.StartTimes = newTimes;
                }
                RaisePropertyChanged(nameof(this.WeekSchedule));
                RaisePropertyChanged(nameof(this.NightDisplays));
                RaisePropertyChanged(nameof(this.TimerUIDisabled));
                RaisePropertyChanged(nameof(this.CurrentActiveNight));
                RaisePropertyChanged(nameof(this.CurrentNightDisplay));
                RaisePropertyChanged(nameof(this.NumberOfActiveTimers));
            }
        }

        void UnsubscribeFromEvents()
        {
            BluetoothModule.SnoozStatusUpdated -= BluetoothModule_SnoozStatusUpdated;
            BluetoothModule.SnoozDisconnected -= BluetoothModule_SnoozDisconnected;
        }

        public override void Destroy()
        {
            Debug.WriteLine($"MainViewModelV1 Destroy");
        }
    }
}