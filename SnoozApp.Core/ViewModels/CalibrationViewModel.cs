﻿using Plugin.BLE.Abstractions.EventArgs;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Prism.Navigation;
using SnoozApp.Core.Interfaces;
using SnoozApp.Core.Services.Permission;
using SnoozApp.Enums;
using SnoozApp.Modules;
using SnoozApp.Pages;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.ViewModels
{
    public class CalibrationViewModel : BaseViewModel
	{
		public ICommand StartCalibrationButtonPress { get; private set; }
		public ICommand BackButtonPress { get; private set; }
		public ICommand NextButtonPress { get; private set; }
		public ICommand SetupPageButtonPress { get; private set; }
		public AudioLevelModule AudioLevelModule { get; set; }

		public bool PageIndicatorVisible => (CalibrationWorkflowState == CalibrationWorkflowState.Start)
			|| (CalibrationWorkflowState == CalibrationWorkflowState.Step1)
			|| (CalibrationWorkflowState == CalibrationWorkflowState.Step2);

		string _setupText;
		public string SetupText
		{
			get => _setupText;
			set
			{
				SetProperty(ref _setupText, value);
				RaisePropertyChanged(nameof(PageIndicatorVisible));
			}
		}

		string _setupBib;
		public string SetupBib
		{
			get => _setupBib;
			set
			{
				SetProperty(ref _setupBib, value);
				RaisePropertyChanged(nameof(PageIndicatorVisible));
			}
		}

		CalibrationWorkflowState _calibrationWorkflowState;
		public CalibrationWorkflowState CalibrationWorkflowState
		{
			get => _calibrationWorkflowState;
			set
			{
				SetProperty(ref _calibrationWorkflowState, value);
				RaisePropertyChanged(nameof(PageIndicatorVisible));
			}
		}

		IPermissionService _permissionService;

		public CalibrationViewModel(INavigationService navigationService,
			IPermissionService permissionService) : base(navigationService)
		{
			_permissionService = permissionService;

			AudioLevelModule = AudioLevelModule.Instance;
			BackButtonPress = new Command(HandleBackButtonPressAsync);
			NextButtonPress = new Command(HandleNextButtonPressAsync);
			SetupPageButtonPress = new Command(HandleSetupPageButtonPress);
			CalibrationWorkflowState = CalibrationWorkflowState.Setup;

			// The calibration text is added here because the trailing newline is necessary to keep the subscript 4
			// from being cutoff. The trailing newline was impossible to implement in XAML.
			SetupText = "Place SNOOZ in the preferred room location.\n\t\nPlace the phone in the location where the person will be sleeping—the bed or crib.\n\t\nAdjust the volume to a safe, desired sound level.\n\nFor reference on what may be considered a safe level of sound, two recommendations on exposure to noise over time include the following:\n\nNoise recommendations for hospital nurseries suggest a limit of 50 dBA. ₁ ₂\n\nThe U.S. EPA recommends an exposure limit of 70 dBA over a 24 hour period to prevent hearing loss. ₃\n\nWhen determining if any sound level is safe, both intensity and exposure time must be considered.\n\nSleeping environments are no exception, especially since such a large part of the day is spent in them.\n\nSNOOZ incorporates a feature to help calibrate its volume for use in a nursery as many sound machines on the market are capable of generating sound levels for a sleeping environment that is unsafe for hearing.₄ \n";
			SetupBib = "1 Philbin, M. Kathleen, Alex Robertson, and James W. Hall. 'Recommended Permissible Noise Criteria for Occupied, Newly Constructed or Renovated Hospital Nurseries.' Journal of Perinatology 19.8 (1999): 559-63. Print. \n2 Graven, Stanley N. 'Sound and the Developing Infant in the NICU: Conclusions and Recommendations for Care.' Journal of Perinatology 20 (2000): S88-93. Print. \n3 Hammer, Monica S., Tracy K. Swinburn, and Richard L. Neitzel. 'Environmental Noise Pollution in the United States: Developing an Effective Public Health Response.' Environmental Health Perspectives 122.2 (2014): 115-19. Print. \n4 Hugh, S. C., N. E. Wolter, E. J. Propst, K. A. Gordon, S. L. Cushing, and B. C. Papsin. 'Infant Sleep Machines and Hazardous Sound Pressure Levels.' Pediatrics 133.4 (2014): 677-81. Web.\n";
		}

		public override async void OnNavigatedTo(INavigationParameters parameters)
		{
			CalibrationWorkflowState = CalibrationWorkflowState.Setup;
			AudioLevelModule.CalibrationFinished += AudioLevelModule_CalibrationFinished;

			if (Device.RuntimePlatform.Equals(Device.Android))
				BluetoothModule.SnoozDisconnected += MainThread_BluetoothModule_SnoozDisconnected;
			else
				BluetoothModule.SnoozDisconnected += BluetoothModule_SnoozDisconnected;

			await AskForMicrophonePermissions();
			Debug.WriteLine("CalibrationViewModel OnNavigatedTo");
		}

		private async Task AskForMicrophonePermissions()
		{

			var status = await _permissionService.CheckAndRequestLocationPermission(new Xamarin.Essentials.Permissions.Microphone());
            //var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Microphone);

            if (status != Xamarin.Essentials.PermissionStatus.Granted)
            {
                // When we request permissions the popup sends us through OnSleep and OnResume
                // Set a boolean so we know to ignore actions in those methods in App.xaml.cs
                App.CalibrationPrompted = true;
                
            }
        }

		public override void OnNavigatedFrom(INavigationParameters parameters)
		{
			AudioLevelModule.CalibrationFinished -= AudioLevelModule_CalibrationFinished;

			if (Device.RuntimePlatform.Equals(Device.Android))
				BluetoothModule.SnoozDisconnected -= MainThread_BluetoothModule_SnoozDisconnected;
			else
				BluetoothModule.SnoozDisconnected -= BluetoothModule_SnoozDisconnected;

			Debug.WriteLine("CalibrationViewModel OnNavigatedFrom");
		}

		void AudioLevelModule_CalibrationFinished(object sender, EventArgs e)
			=> CalibrationWorkflowState = CalibrationWorkflowState.Finished;

		void HandleSetupPageButtonPress()
			=> CalibrationWorkflowState = CalibrationWorkflowState.Setup;

		async void HandleBackButtonPressAsync()
		{
			switch (CalibrationWorkflowState)
			{
				case CalibrationWorkflowState.Disabled:
				case CalibrationWorkflowState.Setup:
					await NavigationService.GoBackAsync();
					break;
				case CalibrationWorkflowState.Start:
					CalibrationWorkflowState = CalibrationWorkflowState.Setup;
					break;
				case CalibrationWorkflowState.Step1:
					CalibrationWorkflowState = CalibrationWorkflowState.Start;
					break;
				case CalibrationWorkflowState.Step2:
					CalibrationWorkflowState = CalibrationWorkflowState.Step1;
					break;
				case CalibrationWorkflowState.Calibrating:
					break;
				case CalibrationWorkflowState.Finished:
					break;
			}
		}

		async void HandleNextButtonPressAsync()
		{
			try
			{
				if (AudioLevelModule.CalibrationState == CalibrationState.Disabled)
				{
					CalibrationWorkflowState = CalibrationWorkflowState.Disabled;
					return;
				}
				switch (CalibrationWorkflowState)
				{
					case CalibrationWorkflowState.Setup:
						AudioLevelModule.AudioDetector.Initialize();
						CalibrationWorkflowState = CalibrationWorkflowState.Start;
						break;
					case CalibrationWorkflowState.Start:
						CalibrationWorkflowState = CalibrationWorkflowState.Step1;
						break;
					case CalibrationWorkflowState.Step1:
						CalibrationWorkflowState = CalibrationWorkflowState.Step2;
						break;
					case CalibrationWorkflowState.Step2:
						CalibrationWorkflowState = CalibrationWorkflowState.Calibrating;
						AudioLevelModule.StartCalibration();
						break;
					case CalibrationWorkflowState.Calibrating:
						break;
					case CalibrationWorkflowState.Finished:
						await NavigationService.GoBackAsync();
						break;
					case CalibrationWorkflowState.Disabled:
						break;
				}
			}
			catch
			{
				CalibrationWorkflowState = CalibrationWorkflowState.Disabled;
			}
		}

		async void BluetoothModule_SnoozDisconnected(object sender, DeviceEventArgs e)
		{
			AudioLevelModule.AbortCalibration();
			await NavigationService.NavigateAsync($"{nameof(ConnectionPage)}");
		}

		void MainThread_BluetoothModule_SnoozDisconnected(object sender, DeviceEventArgs e)
			=> Device.BeginInvokeOnMainThread(async () =>
			{
				AudioLevelModule.AbortCalibration();
				await NavigationService.NavigateAsync($"{nameof(ConnectionPage)}");
			});
	}
}