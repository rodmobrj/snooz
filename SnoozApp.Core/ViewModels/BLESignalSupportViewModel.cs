﻿using Prism.Navigation;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using Xamarin.Essentials;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;
using Plugin.BLE.Abstractions.EventArgs;
using SnoozApp.Modules;
using System.Linq;


using System.Collections.ObjectModel;




namespace SnoozApp.ViewModels
{
    public class BLESignalSupportViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand BLESignalScanCommand => new Command(StartBLEScanCommand);
        public ICommand ContactUsCommand => new Command(ContactUs);


        public BLESignalSupportViewModel(INavigationService navigationService)
            : base(navigationService) { }

        bool _isScanRunning;
        public bool IsScanRunning
        {
            get => _isScanRunning;
            set => SetProperty(ref _isScanRunning, value);
        }

        bool _noSNOOZsFound;
        public bool NoSNOOZsFound
        {
            get => _noSNOOZsFound;
            set => SetProperty(ref _noSNOOZsFound, value);
        }


        public class DeviceScanList
        {
            public string Name { get; set; }
            public bool Signal1Bar { get; set; }
            public bool Signal2Bar { get; set; }
            public bool Signal3Bar { get; set; }
            public bool Signal4Bar { get; set; }
        }

        ObservableCollection<DeviceScanList> devices = new ObservableCollection<DeviceScanList>();
        public ObservableCollection<DeviceScanList> Devices { get { return devices; } }


    public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine("BLE Signal Check OnNavigatedTo");
            IsScanRunning = false;
            NoSNOOZsFound = false;

            base.OnNavigatedTo(parameters);

            BluetoothModule.SnoozDiscovered += BluetoothModule_SnoozDiscovered;
            BluetoothModule.ScanTimeoutElapsed += BluetoothModule_ScanTimeoutElapsed;

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { { nameof(BluetoothOffPopupPage), true } };
        }

        public async override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("BLE Signal Check OnNavigatedFrom");
            BluetoothModule.SnoozDiscovered -= BluetoothModule_SnoozDiscovered;
            BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
            await BluetoothModule.StopScanningAsync();

        }

        async void StartBLEScanCommand()
        {
            Debug.WriteLine("Starting BLE Scan");
            IsScanRunning = true;
            NoSNOOZsFound = false;
            devices.Clear();
            await BluetoothModule.StopScanningAsync();
            await BluetoothModule.DisconnectDevicesAsync();
            await BluetoothModule.StartScanningAsync();

        }

        void BluetoothModule_SnoozDiscovered(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("Snooz Discovered");
            Debug.WriteLine("Name: " + e.Device.Name);
            Debug.WriteLine("RSSI: " + e.Device.Rssi);
            if (e.Device.Rssi > -70) //-60s
            {
                devices.Add(new DeviceScanList { Name = e.Device.Name, Signal1Bar = false, Signal2Bar = false, Signal3Bar = false, Signal4Bar = true });
            }
            else if (e.Device.Rssi > -80) //-70s
            {
                devices.Add(new DeviceScanList { Name = e.Device.Name, Signal1Bar = false, Signal2Bar = false, Signal3Bar = true, Signal4Bar = false });
            }
            else if (e.Device.Rssi > -90) //-80s
            {
                devices.Add(new DeviceScanList { Name = e.Device.Name, Signal1Bar = false, Signal2Bar = true, Signal3Bar = false, Signal4Bar = false });
            }
            else //-90s
            {
                devices.Add(new DeviceScanList { Name = e.Device.Name, Signal1Bar = true, Signal2Bar = false, Signal3Bar = false, Signal4Bar = false });
            }

        }

        static void ContactUs() => Launcher.TryOpenAsync("mailto:App@getsnooz.com");

        void BluetoothModule_ScanTimeoutElapsed(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Connection ScanTimeoutElapsed");
            IsScanRunning = false;
            if (devices.Count == 0)
                NoSNOOZsFound = true;
        }
    }
}
