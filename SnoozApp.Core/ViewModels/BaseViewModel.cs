﻿using Prism.Mvvm;
using Prism.Navigation;
using SnoozApp.Pages;
using SnoozApp.Modules;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace SnoozApp.ViewModels
{
    public abstract class BaseViewModel : BindableBase, INavigationAware, IDestructible, IInitializeAsync
    {
        protected INavigationParameters NavigationParameters;
        protected BluetoothModule BluetoothModule { get; }
        protected INavigationService NavigationService { get; }

        public ICommand CalibrateCommand => new Command(NavigateToCalibrationAsync);
        public ICommand DevicesCommand => new Command(NavigateToDevicesAsync);
		public ICommand HelpIndexCommand => new Command(NavigateToHelpAsync);
        public ICommand GoBackCommand => new Command(NavigateBackAsync);
        public ICommand SchedulerCommand => new Command(NavigateToSchedulerAsync);

		protected BaseViewModel(INavigationService navigationService)
        {
            BluetoothModule = BluetoothModule.Instance;
            NavigationService = navigationService;
		}

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
            => NavigationParameters = parameters;

        public virtual void OnNavigatedTo(INavigationParameters parameters)
            => NavigationParameters = parameters;

        protected virtual async void NavigateToSchedulerAsync()
            => await NavigationService.NavigateAsync(nameof(SchedulerPage), useModalNavigation: true);

        protected virtual async void NavigateToHelpAsync() 
            => await NavigationService.NavigateAsync(nameof(HelpPage), useModalNavigation: true);

        protected virtual async void NavigateToCalibrationAsync() 
            => await NavigationService.NavigateAsync(nameof(CalibrationPage), useModalNavigation: true);

        protected virtual async void NavigateToDevicesAsync() 
            => await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}");

        protected virtual async void NavigateBackAsync()
            => await NavigationService.GoBackAsync(NavigationParameters);

        public virtual void Destroy() { }

        public virtual Task InitializeAsync(INavigationParameters parameters)
        {
            return Task.CompletedTask;
        }
    }
}