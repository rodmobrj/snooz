﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.EventArgs;
using Prism.Navigation;
using SnoozApp.Modules;
using SnoozApp.Pages;
using Xamarin.Essentials;
using Xamarin.Forms;
using SnoozApp.Enums;
using SnoozApp.Pages.DeviceConfiguration;

namespace SnoozApp.ViewModels
{
    public class DevicesViewModel : BaseViewModel
    {
        public ICommand ConfigureCommand => new Command(StopScanningAndNavigateToConfigurationAsync);
        public ICommand SupportCommand => new Command(StopScanningAndNavigateToHelpPageAsync);

        // public ICommand SearchAgainCommand => new Command(async () => await BluetoothModule.StartScanningAsync());
        public ICommand DeviceButtonPushed => new Command(async args => await HandleDeviceButtonPushedAsync(args));
        public ICommand DeviceNameChanged => new Command(HandleDeviceNameChanged);
        public ICommand HideOverlayCommand => new Command(HideOverlay);

        //Purpose is to lock out screen when App is busy.
        bool _disableButtons;
        public bool DisableButtons
        { 
            get => _disableButtons;
            set => SetProperty(ref _disableButtons, value);
        }

        //Purpose is to display activity indicator and message when App is searching or connecting
        bool _appBusy;
        public bool AppBusy
        {
            get => _appBusy;
            set => SetProperty(ref _appBusy, value);
        }

        //Purpose is to display message of what the App is busy doing (searching/connecting/error)
        String _appBusyMessage;
        public String AppBusyMessage
        {
            get => _appBusyMessage;
            set => SetProperty(ref _appBusyMessage, value);
        }

        //Purpose is to show help overlay on first time connect or when user selects help button
        bool _showOverlay;
        public bool ShowOverlay
        {
            get => _showOverlay;
            set => SetProperty(ref _showOverlay, value);
        }

        ObservableCollection<DisplaySnooz> _displaySnoozes;
        public ObservableCollection<DisplaySnooz> DisplaySnoozes
        {
            get => _displaySnoozes;
            set => SetProperty(ref _displaySnoozes, value);
        }

        public bool ConnectingToSelectedSnooz;

        public DevicesViewModel(INavigationService navigationService)
            : base(navigationService)
        {
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine("DevicesViewModel OnNavigatedTo");
            AppBusyMessage = "Searching for Available Devices";

            DisableButtons = true;
            ConnectingToSelectedSnooz = false;

            PopulateDisplaySnoozes();

            if (DisplaySnoozes != null && DisplaySnoozes.Any())
                AppBusy = true;
            else
                AppBusy = false;


            await Task.Delay(100);
            await BluetoothModule.DisconnectDevicesAsync();

            BluetoothModule.SnoozDiscovered += BluetoothModule_SnoozDiscovered;
            BluetoothModule.SnoozConnected += BluetoothModule_SnoozConnected;
            BluetoothModule.ScanTimeoutElapsed += BluetoothModule_ScanTimeoutElapsed;

            DisableButtons = false;

            //show overlay if connecting first time
            if (DisplaySnoozes != null
                && DisplaySnoozes.Any()
                && SettingsModule.IsFirstTimeHasDevice)
            {
                SettingsModule.IsFirstTimeHasDevice = false;
                ShowOverlay = true;
            }

            await Task.Run(BluetoothModule.StartScanningAsync);
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("DevicesViewModel OnNavigatedFrom");
            DisplaySnoozes = null;
            UnsubscribeFromEvents();
            AppBusy = false;
            DisableButtons = false; //Needed or can crash if button pressed too rapidly
            ConnectingToSelectedSnooz = false;
            App.TryConnecting = false;
            AppBusyMessage = "Searching for Available Devices";
        }

        public override void Destroy()
        {
            Debug.WriteLine("DevicesViewModel Destroy");
            UnsubscribeFromEvents();
        }

        async void StopScanningAndNavigateToConfigurationAsync()
        {
            // Call to StopScanningAsync was moved from OnewDisappearing to here to ensure that
            // StopScanningAsync completes before StartScanningAsync in the OnViewAppearing of the
            // Configuration screen gets called.
            DisableButtons = true; //Needed or can crash if button pressed too rapidly
            App.TryConnecting = false;
            UnsubscribeFromEvents();
            Console.WriteLine("UnsubscribeFromEvents");
            if (BluetoothModule.ConnectionState == ConnectionState.Scanning
                || BluetoothModule.ConnectionState == ConnectionState.Disconnected)
            {
                await BluetoothModule.StopScanningAsync();
                await Task.Delay(200);
                //Device.BeginInvokeOnMainThread(async () =>
                //await NavigationService.NavigateAsync($"/{nameof(About)}");
                await NavigationService.NavigateAsync($"/{nameof(DeviceConfigurationPage)}");//);
            }
            else //is connected or is connecting
            {
                AppBusyMessage = "Canceling...";
                AppBusy = true;
                await this.BluetoothModule.DisconnectDevicesAsync();
                await Task.Delay(5000);
                Device.BeginInvokeOnMainThread(async () =>
                    await NavigationService.NavigateAsync($"/{nameof(DeviceConfigurationPage)}"));
            }
        }

        async void StopScanningAndNavigateToHelpPageAsync()
        {
            // Call to StopScanningAsync was moved from OnewDisappearing to here to ensure that
            // StopScanningAsync completes before StartScanningAsync in the OnViewAppearing of the
            // Configuration screen gets called.
            DisableButtons = true; //Needed or can crash if button pressed too rapidly
            App.TryConnecting = false;
            UnsubscribeFromEvents();

            if (BluetoothModule.ConnectionState == ConnectionState.Scanning
                || BluetoothModule.ConnectionState == ConnectionState.Disconnected)
            {
                await BluetoothModule.StopScanningAsync();
                await Task.Delay(200);
                Device.BeginInvokeOnMainThread(async () =>
                    await NavigationService.NavigateAsync($"/{nameof(HelpPage)}"));

            }
            else //is connected or is connecting
            {
                AppBusyMessage = "Canceling...";
                AppBusy = true;
                await this.BluetoothModule.DisconnectDevicesAsync();
                await Task.Delay(5000);
                Device.BeginInvokeOnMainThread(async () =>
                    await NavigationService.NavigateAsync($"/{nameof(HelpPage)}"));
            }
        }

        void PopulateDisplaySnoozes()
        {
            DisplaySnoozes = new ObservableCollection<DisplaySnooz>();
            foreach (var coupledSnooz in BluetoothModule.CoupledSnoozes)
                DisplaySnoozes.Add(new DisplaySnooz(coupledSnooz.Name, coupledSnooz.Id));
            
        }

        async Task HandleDeviceButtonPushedAsync(object obj)
        {
            CoupledSnooz coupledSnooz;
            IDevice discoveredSnooz;
            DisplaySnooz displaySnooz;

            // Don't attempt to connect if we are already attempting to connect
            if (!ConnectingToSelectedSnooz)
            {
                if (obj is Guid id)
                {
                    discoveredSnooz = BluetoothModule.DiscoveredSnoozes.FirstOrDefault(s => s.Id == id);
                    coupledSnooz = BluetoothModule.CoupledSnoozes.FirstOrDefault(s => s.Id == id);
                    displaySnooz = DisplaySnoozes.FirstOrDefault(s => s.Id == id);

                    if (discoveredSnooz != null && coupledSnooz != null)
                    {
                        AppBusyMessage = "Connecting...";
                        AppBusy = true;
                        ConnectingToSelectedSnooz = true;
                        displaySnooz.IsConnecting = true;
                        BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
                        App.TryConnecting = true;
                        var ConnectionResult = await BluetoothModule.ConnectToDeviceAsync(discoveredSnooz, coupledSnooz.Password);

                        if (!ConnectionResult && !App.ClosingTime && App.TryConnecting)
                        {
                            Debug.WriteLine("Connection Fail 1");

                            ConnectionResult = await BluetoothModule.RetryConnectToDevice(discoveredSnooz, coupledSnooz.Password);
                        }

                        if (!ConnectionResult && !App.ClosingTime && App.TryConnecting)
                        {
                            Debug.WriteLine("Connection Fail 2");
                            AppBusy = false;
                            ConnectingToSelectedSnooz = false;
                            displaySnooz.IsConnecting = false;
                            PopulateDisplaySnoozes();
                            BluetoothModule.ScanTimeoutElapsed += BluetoothModule_ScanTimeoutElapsed;
                            await this.BluetoothModule.StartScanningAsync();

                        }

                    }
                }
            }
        }

        public void HandleDeleteSnooz(DeleteSnoozEventArgs e)
        {
            Preferences.Set(e.Id.ToString(), JsonConvert.SerializeObject(null));
            Preferences.Set($"{e.Id}{SettingsModule.BASE_SAVED_ADVANCED_SCHEDULE_KEY}", string.Empty);
            BluetoothModule.ForgetCoupledSnooz(e.Id);
            PopulateDisplaySnoozes();
        }

        void HandleDeviceNameChanged(object obj)
        {
            if (!(obj is DisplaySnooz displaySnooz))
                return;

            var modifiedSnooz = DisplaySnoozes.FirstOrDefault(s => s.Id == displaySnooz.Id);
            if (modifiedSnooz != null)
                BluetoothModule.RenameCoupledSnooz(displaySnooz.Id, displaySnooz.Name);
        }

        void BluetoothModule_SnoozDiscovered(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("DevicesViewModel SnoozDiscovered");
            var snoozInDisplayList = DisplaySnoozes?.FirstOrDefault(s => s.Id == e.Device.Id);
            if (snoozInDisplayList != null)
            {
                snoozInDisplayList.DiscoveredThisScan = true;
                UpdateSnoozRssi(snoozInDisplayList,e.Device.Rssi);
            }

            Debug.WriteLine($"Device: {e.Device.Name} Sinal: {e.Device.Rssi} dBm");
            //Debug.WriteLine(snoozInDisplayList.Rssi);
        }

        void UpdateSnoozRssi(DisplaySnooz snoozInDisplayList, int newRssi)
        {
            var index = DisplaySnoozes.IndexOf(snoozInDisplayList);
            DisplaySnoozes[index].Rssi = newRssi;
        }

        async void BluetoothModule_SnoozConnected(object sender, EventArgs e)
        {
            Debug.WriteLine("DevicesViewModel SnoozConnected");
            UnsubscribeFromEvents();

            if (Device.RuntimePlatform.Equals(Device.Android))
                Device.BeginInvokeOnMainThread(async () =>
                    await NavigationService.NavigateAsync($"/{App.GetMainPageName()}"));
            else
                await NavigationService.NavigateAsync($"/{App.GetMainPageName()}");

            ConnectingToSelectedSnooz = false;
        }

        async void BluetoothModule_ScanTimeoutElapsed(object sender, EventArgs e)
        {
            Debug.WriteLine("DevicesViewModel ScanTimeoutElapsed");
            AppBusy = false;
            await BluetoothModule.StartScanningAsync();
            // DisplaySnoozes can be null if the StartScanningAsync is still running when the
            // user changes screens and DisplaySnoozes gets deinitialized to null in OnViewDisappearing
            if (DisplaySnoozes == null)
            {
                return;
            }
            foreach (var snooz in DisplaySnoozes)
            {
                snooz.DiscoveredLastScan = snooz.DiscoveredThisScan;
                snooz.DiscoveredThisScan = false;
            }
        }

        void HideOverlay() 
            => ShowOverlay = false;

        void UnsubscribeFromEvents()
        {
            Console.WriteLine("DevicesViewModel Unsubscribe");
            BluetoothModule.SnoozDiscovered -= BluetoothModule_SnoozDiscovered;
            BluetoothModule.SnoozConnected -= BluetoothModule_SnoozConnected;
            BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
        }
    }
}