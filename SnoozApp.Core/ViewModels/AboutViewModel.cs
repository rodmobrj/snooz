﻿using Prism.Navigation;
using SnoozApp.Interfaces;
using SnoozApp.Pages;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SnoozApp.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public string Version => $"Version {DependencyService.Get<IAppInfo>().Version}";
        public ICommand OpenPrivacyPolicyCommand => new Command(OpenPrivacyPolicyLink);
        public ICommand OpenShopSnoozCommand => new Command(OpenShopSnoozLink);

        async void NavigateToContactUsAsync()
            => await NavigationService.NavigateAsync(nameof(ContactUsPage), NavigationParameters);
        async void NavigateToAboutAsync()
            => await NavigationService.NavigateAsync(nameof(About), NavigationParameters);

        public AboutViewModel(INavigationService navigationService) 
            : base(navigationService) { }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { {nameof(BluetoothOffPopupPage), true} };
        }

        static void OpenPrivacyPolicyLink() =>
            Launcher.TryOpenAsync("https://getsnooz.com/pages/privacy-policy");

        static void OpenShopSnoozLink() =>
            Launcher.TryOpenAsync("https://getsnooz.com/");
    }
}
