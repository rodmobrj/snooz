﻿using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using SnoozApp.Core.Enums;
using SnoozApp.Core.Interfaces;
using SnoozApp.Core.Modules;
using SnoozApp.Enums;
using SnoozApp.Extensions;
using SnoozApp.Modules;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using static SnoozApp.Modules.BluetoothModule;
using DependencyService = Xamarin.Forms.DependencyService;

namespace SnoozApp.Core.ViewModels
{
    public class SchedulerPageViewModel : BaseViewModel
    {
        #region Atributes
        int SchedulerTabSelected;

        string _backupJsonTimers;
        string _backupFadeIn;
        string _backupFadeOut;
        bool _justConnected;
        bool _shouldNotRefreshInterface;
        bool _timersIsBusy;
        bool _canGetTimes;

        readonly string _id;
        bool _isInit = true;

        readonly IAlertPermissionRequest _alertPermissionRequest;
        //readonly INavigationService _navigationService;
        //bool _isInit = true;
        List<TimerModel> _timersBedTime;
        TimersManager _timersManager;
        readonly IPageDialogService _pageDialogService;
        #endregion

        #region Properties

        public ObservableCollection<ScheduleNotification> Minutes { get; set; }
        public ObservableCollection<ScheduleAlarm> Alarms { get; set; }

        bool _isEnabledAlarm;
        public bool IsEnabledAlarm
        {
            get => _isEnabledAlarm;
            set => SetProperty(ref _isEnabledAlarm, value);
        }

        bool _isToggledAlarm;
        public bool IsToggledAlarm
        {
            get => _isToggledAlarm;
            set
            {
                IsEnabledAlarm = value;
                SetProperty(ref _isToggledAlarm, value);
            }
        }

        int _selectedIndex;
        public int SelectedIndex
        {
            get => _selectedIndex;
            set => SetProperty(ref _selectedIndex, value);
        }

        int _alarmSelectedIndex = -1;
        public int AlarmSelectedIndex
        {
            get => _alarmSelectedIndex;
            set
            {
                _alarmSelectedIndex = value;
                SetProperty(ref _alarmSelectedIndex, value);

                if (_isInit)
                {
                    _isInit = false;
                    return;
                }

                if (_alarmSelectedIndex > -1)
                    PlayAlarm(Alarms[_alarmSelectedIndex].AlarmName);
            }
        }

        #region Test Random Messages
        MessageTypeWithDescription _selectedMessageType;
        public MessageTypeWithDescription SelectedMessageType
        {
            get => _selectedMessageType;
            set
            {
                SetProperty(ref _selectedMessageType, value);
            }
        }

        List<MessageTypeWithDescription> _messages;
        public List<MessageTypeWithDescription> Messages { get => _messages; set => SetProperty(ref _messages, value); }
        #endregion


        bool _isDaylightSavingsToggled;
        public bool IsDaylightSavingsToggled
        {
            get => _isDaylightSavingsToggled;
            set
            {
                SetProperty(ref _isDaylightSavingsToggled, value);
            }
        }

        bool _isEnabledNotification;
        public bool IsEnabledNotification
        {
            get => _isEnabledNotification;
            set => SetProperty(ref _isEnabledNotification, value);
        }

        bool _isToggledNotification;
        public bool IsToggledNotification
        {
            get => _isToggledNotification;
            set
            {
                IsEnabledNotification = value;
                SetProperty(ref _isToggledNotification, value);
            }
        }


        int newFadeInValue;
        int _FadeIn;
        public string FadeIn
        {
            get => GetFadeText(_FadeIn);
            set => newFadeInValue = GetFadeValue(value);
        }

        string _selectedFadeIN = string.Empty;
        public string SelectedFadeIn
        {
            get => _selectedFadeIN;
            set => SetProperty(ref _selectedFadeIN, value);
        }

        int newFadeOutValue;
        int _FadeOut;
        public string FadeOut
        {
            get => GetFadeText(_FadeOut);
            set => newFadeOutValue = GetFadeValue(value);
        }

        string _selectedFadeOut = string.Empty;
        public string SelectedFadeOut
        {
            get => _selectedFadeOut;
            set => SetProperty(ref _selectedFadeOut, value);
        }

        ObservableCollection<TimerModel> _timers = new ObservableCollection<TimerModel>();
        public ObservableCollection<TimerModel> Timers
        {
            get => _timers;
            set => SetProperty(ref _timers, value);
        }

        bool _fadeInNotification;
        public bool FadeInNotification
        {
            get => _fadeInNotification;
            set => SetProperty(ref _fadeInNotification, value);
        }

        bool _showScheduler;
        public bool showScheduler
        {
            get => _showScheduler;
            set => SetProperty(ref _showScheduler, value);
        }


        bool _maskScheduler;
        public bool maskScheduler
        {
            get => _maskScheduler;
            set => SetProperty(ref _maskScheduler, value);
        }

        bool _isAppBusy;
        public bool IsAppBusy
        {
            get => _isAppBusy;
            set => SetProperty(ref _isAppBusy, value);
        }

        public bool CanAddTimer => Timers.Count < 7;

        public FirmwareVersions FirmwareVersion => BluetoothModule.FirmwareVersion;

        //int _alarmSelectedIndex = -1;
        //public int AlarmSelectedIndex
        //{
        //    get => _alarmSelectedIndex;
        //    set
        //    {
        //        _alarmSelectedIndex = value;
        //        SetProperty(ref _alarmSelectedIndex, value);

        //        if (_isInit)
        //        {
        //            _isInit = false;
        //            return;
        //        }

        //        if (_alarmSelectedIndex > -1)
        //            PlayAlarm(Alarms[_alarmSelectedIndex].AlarmName);
        //    }
        //}

        //int _selectedIndex;
        //public int SelectedIndex
        //{
        //    get => _selectedIndex;
        //    set => SetProperty(ref _selectedIndex, value);
        //}

        //bool _isEnabledAlarm;
        //public bool IsEnabledAlarm
        //{
        //    get => _isEnabledAlarm;
        //    set => SetProperty(ref _isEnabledAlarm, value);
        //}

        //bool _isToggledAlarm;
        //public bool IsToggledAlarm
        //{
        //    get => _isToggledAlarm;
        //    set
        //    {
        //        IsEnabledAlarm = value;
        //        SetProperty(ref _isToggledAlarm, value);
        //    }
        //}

        #endregion

        #region Commands

        public ICommand FadeOnSelected => new Command<object>(ExecuteFadeOnSelected);
        public ICommand FadeOutSelected => new Command<object>(ExecuteFadeOutSelected);
        public ICommand SelectedItem => new Command<object>(ExecuteSelectItem);
        public ICommand CancelScheduleCommand => new Command(CancelSchedule);
        public ICommand BedTimerReminderCommand => new Command(OnBedTimerReminder);
        public ICommand ShowHelpCommand => new Command(ShowHelpAsync);
        public ICommand ToggleWeekDayCommand => new Command<WeekDayModel>(ToggleWeekDay);
        public ICommand RemoveTimerCommand => new Command<TimerModel>(RemoveTimer);
        public ICommand ToggleStartTimerCommand => new Command<TimerModel>(item => ToggleTimer(TimerType.Start, item));
        public ICommand ToggleStopTimerCommand => new Command<TimerModel>(item => ToggleTimer(TimerType.Stop, item));
        public ICommand AddNewTimerCommand => new Command(AddNewTimer);
        public ICommand WriteScheduleCommand => new Command(() => WriteSchedule());

        public DelegateCommand AddNotificationCommand => new DelegateCommand(async () => await OnAddNotification());

        public ICommand ToggleDarkModeCommand => new Command(async () =>
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                App.CalibrationPrompted = true;
                if (await RequestAlertPermission())
                    IsToggledNotification = !IsToggledNotification;
            }
            else
                IsToggledNotification = !IsToggledNotification;

        });

        ICommand _daylightSavingsToggleCommand;
        public ICommand DaylightSavingsToggleCommand => _daylightSavingsToggleCommand ??
            (_daylightSavingsToggleCommand = new Command(async () =>
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
                    App.CalibrationPrompted = true;
                    if (await RequestAlertPermission())
                        IsDaylightSavingsToggled = !IsDaylightSavingsToggled;
                }
                else
                    IsDaylightSavingsToggled = !IsDaylightSavingsToggled;

            }));

        #endregion

        #region Constructor
        public SchedulerPageViewModel(INavigationService navigationService, IAlertPermissionRequest alertPermissionRequest, IPageDialogService pageDialogService) : base(navigationService)
        {
           // _navigationService = navigationService;
            _pageDialogService = pageDialogService;
            _alertPermissionRequest = alertPermissionRequest;

            _id = Guid.NewGuid().ToString();
            _justConnected = true;
            _canGetTimes = true;

            InitializeProperties();
            
            //var messageManager = new MessageManager();
            //Messages = messageManager.GetMessageCategories();
        }
        #endregion

        #region Methods

        static void PlayAlarm(string alarmName)
        {
            DependencyService.Get<IAlarmSetter>()
                             .PlayAlarmSoundAsync(alarmName);
        }

        private void InitializeProperties()
        {

            Minutes = new ObservableCollection<ScheduleNotification>();
            Alarms = new ObservableCollection<ScheduleAlarm>();
            Messages = new List<MessageTypeWithDescription>();

            var messageManager = new MessageManager();
            Messages = messageManager.GetMessageCategories();

            var selectedMessageType = (MessageType)Preferences.Get("MessageTypeNameIndexKey", 1);
            SelectedMessageType = Messages.FirstOrDefault(x => x.Enum == selectedMessageType);

            OnGetMinutePicker();
        }

        void OnGetMinutePicker()
        {
            Minutes.Clear();
            foreach (var minute in ScheduleNotification.Options)
                Minutes.Add(minute);

            Alarms.Clear();
            foreach (var alarm in ScheduleAlarm.Options)
                Alarms.Add(alarm);

            SelectedIndex = SettingsModule.SelectedMinutesBeforeIndex;
            _alarmSelectedIndex = SettingsModule.SelectedAlarmNameIndex;
            IsToggledAlarm = SettingsModule.IsAlarmToggled;
            IsToggledNotification = SettingsModule.IsNotificationToggled;
            IsDaylightSavingsToggled = SettingsModule.IsDaylightSavingsToggled;
        }

        private async Task OnAddNotification()
        {
            SettingsModule.IsAlarmToggled = IsToggledAlarm;
            SettingsModule.IsNotificationToggled = IsToggledNotification;
            SettingsModule.IsDaylightSavingsToggled = IsDaylightSavingsToggled;
            SettingsModule.SelectedMinutesBeforeIndex = SelectedIndex;
            SettingsModule.SelectedAlarmNameIndex = AlarmSelectedIndex;
            SettingsModule.MessageTypeValue = (int)SelectedMessageType.Enum;

            if (_timersBedTime != null && _timersBedTime.Any())
                DependencyService.Get<IAlarmSetter>().SetAlarmsFromTimers(_timersBedTime);

            if (!IsToggledNotification)
                DependencyService.Get<IAlarmSetter>().CancelAllAlarms();

            if (IsDaylightSavingsToggled)
                DependencyService.Get<IAlarmSetter>().SetDaylightSavingAlarm();
            else
                DependencyService.Get<IAlarmSetter>().CancelDaylightSaverAlarms();

            await NavigationService.GoBackAsync();
        }

        private void ExecuteFadeOnSelected(object fadeIn)
        {
            var fadeValue = fadeIn.ToString();

            _FadeIn = newFadeInValue = GetFadeValue(fadeValue.ToString());
            SelectedFadeIn = fadeValue;

            FadeTimersUpdated();
        }

        private void ExecuteFadeOutSelected(object fadeOut)
        {
            var fadeValue = fadeOut.ToString();

            _FadeOut = newFadeOutValue = GetFadeValue(fadeValue.ToString());
            SelectedFadeOut = fadeValue;

            FadeTimersUpdated();
        }

        private void ExecuteSelectItem(object headerType)
        {
            //if (headerType is SchedulerHeaderTypes type)
            //    ActiveIcon = type;


        }

        private async Task<bool> RequestAlertPermission()
        {

            var result = await _alertPermissionRequest.RequestPermission();
            if (result)
            {
                return true;
            }
            else
            {
                var resultDialog = await _pageDialogService.DisplayAlertAsync("Attention", "The app needs notification permission to send bedtime reminder. Please grant permission manually.", "Settings", "Cancel");
                if (resultDialog)
                    await Launcher.OpenAsync(new Uri("app-settings:"));

                return false;
            }

        }

        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            //const string timersKey = nameof(SchedulerPageViewModel.Timers);

            //if (parameters.ContainsKey(timersKey)
            //    && parameters[timersKey] is List<TimerModel> timers
            //    && timers.Any(t => t.Enabled))
            //    _timersBedTime = timers;


            Debug.WriteLine($"Scheduler OnNavigatedTo {_id}");

            showScheduler = false;
            maskScheduler = true;
            IsAppBusy = true;
            FadeInNotification = false;

            var navigationMode = parameters.GetNavigationMode();
            if (navigationMode == Prism.Navigation.NavigationMode.Back)
                _canGetTimes = false;

            BluetoothModule.SnoozTimerOnScheduleUpdate += BluetoothModule_SnoozTimerScheduleOnUpdate;
            BluetoothModule.SnoozTimerOffScheduleUpdate += BluetoothModule_SnoozTimerScheduleOffUpdate;
            BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.ScheduleRequest);

            if (FirmwareVersion >= FirmwareVersions.V3)
            {
                FadeInNotification = true;
                BluetoothModule.SnoozFadeUpdate += BluetoothModule_SnoozFadeTimersUpdate;
                BluetoothModule.WriteCommandCharacteristic(BluegigaCommand.FadeRequest);
            }

            await Task.Delay(600);
            showScheduler = true;
            maskScheduler = false;
            IsAppBusy = false;
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            //UnsubscribeFromEvents();
            Debug.WriteLine($"Scheduler OnNavigatedFrom {_id}");

            if (_shouldNotRefreshInterface)
                return;

            if (FirmwareVersion >= FirmwareVersions.V3)
                FadeInNotification = true;

            RaisePropertyChanged(nameof(FadeInNotification));
            RaisePropertyChanged(nameof(FirmwareVersion));
        }


        void BluetoothModule_SnoozTimerScheduleOnUpdate(object sender, ScheduleResponseFrameObject scheduleResponseFrame)
        {
            Debug.WriteLine($"Scheduler SnoozTimerScheduleOnUpdate {_id}");

            if (!_canGetTimes)
                return;

            if (Timers != null)
                Timers.CollectionChanged -= TimersCollectionChanged;

            _timersManager = new TimersManager(scheduleResponseFrame);
            Timers = _timersManager.Timers;
            Timers.CollectionChanged += TimersCollectionChanged;
            RaisePropertyChanged(nameof(CanAddTimer));

            BluetoothModule.SnoozTimerOnScheduleUpdate -= BluetoothModule_SnoozTimerScheduleOnUpdate;
        }

        void BluetoothModule_SnoozTimerScheduleOffUpdate(object sender, ScheduleResponseFrameObject scheduleResponseFrame)
        {
            Debug.WriteLine($"Scheduler SnoozTimerScheduleOffUpdate {_id}");

            if (!_canGetTimes)
                return;

            if (Timers != null)
                Timers.CollectionChanged -= TimersCollectionChanged;

            _timersManager = new TimersManager(scheduleResponseFrame);
            Timers = _timersManager.Timers;
            Timers.CollectionChanged += TimersCollectionChanged;
            RaisePropertyChanged(nameof(CanAddTimer));

            BluetoothModule.SnoozTimerOffScheduleUpdate -= BluetoothModule_SnoozTimerScheduleOffUpdate;

        }

        void BluetoothModule_SnoozFadeTimersUpdate(object sender, FadeResponseFrameObject responseFrame)
        {
            Debug.WriteLine($"Scheduler SnoozFadeTimersUpdate {_id}");
           
            if (_shouldNotRefreshInterface)
            {
                _shouldNotRefreshInterface = false;
                return;
            }

            _FadeIn = newFadeInValue = GetFadeValue(GetFadeText(GetFadeNumber(responseFrame.FadeOnTime)));
            _FadeOut = newFadeOutValue = GetFadeValue(GetFadeText(GetFadeNumber(responseFrame.FadeOffTime)));

            SelectedFadeIn = _backupFadeIn = GetFadeText(_FadeIn);
            SelectedFadeOut = _backupFadeOut = GetFadeText(_FadeOut);

            FadeTimersUpdated();

            BluetoothModule.SnoozFadeUpdate -= BluetoothModule_SnoozFadeTimersUpdate;
        }

        int GetFadeValue(string fadeTimeText)
        {
            switch (fadeTimeText)
            {
                case FadeTime2.OffText:
                    return FadeTime2.Off;
                case FadeTime2.ThirtySecText:
                    return FadeTime2.ThirtySec;
                case FadeTime2.OneMinText:
                    return FadeTime2.OneMin;
                case FadeTime2.FiveMinText:
                    return FadeTime2.FiveMin;
                default:
                    return FadeTime2.Auto*App.AutoFadeAdjustment;
            }
        }

        static string GetFadeText(int fadeTimeValue)
        {
            switch (fadeTimeValue)
            {
                case FadeTime2.Off:
                    return FadeTime2.OffText;
                case FadeTime2.ThirtySec:
                    return FadeTime2.ThirtySecText;
                case FadeTime2.OneMin:
                    return FadeTime2.OneMinText;
                case FadeTime2.FiveMin:
                    return FadeTime2.FiveMinText;
                default:
                    return FadeTime2.AutoText;
            }
        }

        static int GetFadeNumber(uint fadeTimeValue)
        {
            if (fadeTimeValue != FadeTime2.Off &&
                fadeTimeValue != FadeTime2.ThirtySec &&
                fadeTimeValue != FadeTime2.OneMin &&
                fadeTimeValue != FadeTime2.FiveMin)
                return FadeTime2.Auto;

            return (int)fadeTimeValue;
        }

        public void FadeTimersUpdated()
        {
            RaisePropertyChanged(nameof(FadeIn));
            RaisePropertyChanged(nameof(FadeOut));
        }

        async void OnBedTimerReminder()
        {
            _shouldNotRefreshInterface = true;
            await NavigationService.NavigateAsync(nameof(BedTimerReminderPage),
                                                  new NavigationParameters { { nameof(Timers), Timers.ToList() } },
                                                  true);
        }

        void ToggleWeekDay(WeekDayModel item)
        {
            item.IsOn = item.IsChecked = !item.IsOn;

            if (!(Timers.FirstOrDefault(t => t.WeekDays.Any(d => d.Id.Equals(item.Id))) is TimerModel parent)
                || !parent.Enabled)
                return;

            foreach (var timer in Timers.Where(t => !t.Id.Equals(parent.Id) && t.Enabled))
            {
                timer.WeekDays[item.WeekDayIndex].IsOn = false;
                timer.WeekDays[item.WeekDayIndex].IsChecked = false;
            }
        }

        void RemoveTimer(TimerModel item)
        {
            if (Device.RuntimePlatform.Equals(Device.Android))
                Timers.Remove(item);
            else
            {
                if (_timersIsBusy) return;

                _timersIsBusy = true;
                if (Timers.Count == 1)
                {
                    Timers.CollectionChanged -= TimersCollectionChanged;

                    var timers = new ObservableCollection<TimerModel>();
                    timers.CollectionChanged += TimersCollectionChanged;
                    Timers = timers;
                }
                else
                    Timers.Remove(item);
                _timersIsBusy = false;
            }
        }

        void ToggleTimer(TimerType type, TimerModel item)
        {
            var hasDaysOn = false;
            switch (type)
            {
                case TimerType.Start:
                    item.StartEnabled = !item.StartEnabled;
                    hasDaysOn = item.StartEnabled && item.WeekDays.Any(d => d.IsOn);
                    break;
                case TimerType.Stop:
                    item.StopEnabled = !item.StopEnabled;
                    hasDaysOn = item.StopEnabled && item.WeekDays.Any(d => d.IsOn);
                    break;
            }

            if (!hasDaysOn)
                return;

            for (var index = 0; index < 7; index++)
            {
                if (!item.WeekDays[index].IsOn)
                    continue;

                foreach (var timer in Timers.Except(new[] { item }))
                    timer.WeekDays[index].IsChecked = timer.WeekDays[index].IsOn = false;
            }
        }

        void AddNewTimer()
        {
            if (Timers.Count >= 7 || _timersIsBusy) return;

            _timersIsBusy = true;
            Timers.Add(new TimerModel());
            _timersIsBusy = false;
        }

        async void WriteSchedule()
        {
            IsAppBusy = true;
            Debug.WriteLine($"Scheduler WriteSchedule() {_id}");
            // Save Time 
            await OnAddNotification();
            try
            {
                BluetoothModule.WriteSchedule(Timers.ToNights(toWriteOnDevice: true));

                if (FirmwareVersion >= FirmwareVersions.V3)
                {
                    var shouldDisableFade = false;
                    foreach (var timerModel in Timers)
                        if (timerModel.StartEnabled
                            && timerModel.StopEnabled
                            && ShouldDisableFade(timerModel.StartTime, timerModel.StopTime))
                        {
                            shouldDisableFade = true;
                            break;
                        }

                    if (shouldDisableFade)
                        newFadeOutValue = newFadeInValue = FadeTime2.Off;

                    if (GetFadeText(newFadeInValue).Equals(FadeTime2.AutoText))
                    {
                        newFadeInValue = GetFadeValue(FadeTime2.AutoText);
                    }

                    if (GetFadeText(newFadeOutValue).Equals(FadeTime2.AutoText))
                    {
                        newFadeOutValue = GetFadeValue(FadeTime2.AutoText);
                    }

                    // Set FadeIn
                    BluetoothModule.WriteFadeOnCommand(true, (uint)newFadeInValue);
                    // Set FadeOut
                    BluetoothModule.WriteFadeOffCommand(true, (uint)newFadeOutValue);
                }

                ScheduleBackup.SaveToLocalStorage(Timers.ToArray(), newFadeInValue, newFadeOutValue);

                if (SettingsModule.IsNotificationToggled || SettingsModule.IsAlarmToggled)
                #pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                Task.Run(() => Xamarin.Forms.DependencyService.Get<IAlarmSetter>().SetAlarmsFromTimers(Timers.ToList()));
                #pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                maskScheduler = true;
                showScheduler = false;
                await Task.Delay(500);
                GoBackCommand.Execute(null);// Go back to main screen
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"WriteSchedule exception: {ex}");
            }

            IsAppBusy = false;
        }

        void CancelSchedule()
        {
            if (string.IsNullOrWhiteSpace(_backupJsonTimers))
            {
                Timers.Add(new TimerModel());
            }
            else
            {
                if (Timers != null)
                    Timers.CollectionChanged -= TimersCollectionChanged;

                var timers = new ObservableCollection<TimerModel>();
                timers.CollectionChanged += TimersCollectionChanged;

                var timersArray = JsonConvert.DeserializeObject<TimerModel[]>(_backupJsonTimers,
                    ScheduleBackup.TimerJsonConverter);
                timersArray.ForEach(timers.Add);
                
                Timers = timers;
                RaisePropertyChanged(nameof(Timers));
            }

            _FadeIn = GetFadeValue(_backupFadeIn);
            _FadeOut = GetFadeValue(_backupFadeOut);
            FadeTimersUpdated();

            //// Hides schedule
            //SetActiveControl(ControlType.Timer);

            GoBackCommand.Execute(null);
        }

        async void ShowHelpAsync()
        {
            await _pageDialogService.DisplayAlertAsync("Volume Fade Control", "Fade allows the scheduler to turn SNOOZ on/off slowly. " +
                                                      "\r\n\nSelect a fade time or choose Auto. Auto adjusts the fade time between 20 seconds to 3 minutes " +
                                                      "to try to keep the volume change below the just-noticeable difference " +
                                                      "threshold, according to Weber's Law.", "Got It");
        }

        static bool ShouldDisableFade(TimeSpan start, TimeSpan stop)
        {
            if (start == stop)
                return false;

            var difference = new TimeSpan(0, 30, 0);
            var max = start > stop ? start : stop;
            var min = max == start ? stop : start;

            if (max - min >= difference)
            {
                return false;
            }
            return true;
        }

        public override void Destroy()
        {
            Debug.WriteLine($"Scheduler Destroy {_id}");
        }

        void TimersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Timers.ForEach(timer => timer.Name = $"Timer {Timers.Select(t => t.Id).ToList().IndexOf(timer.Id) + 1}");
            RaisePropertyChanged(nameof(CanAddTimer));
        }
        #endregion

    }
    
}
