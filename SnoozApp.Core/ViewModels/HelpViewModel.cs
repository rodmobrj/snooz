﻿using System;
using System.Linq;
using System.Windows.Input;
using Plugin.BLE;
using Prism.Navigation;
using SnoozApp.Pages;
using Xamarin.Forms;
using SnoozApp.Enums;
using Xamarin.Essentials;
using SnoozApp.Pages.DeviceConfiguration;

namespace SnoozApp.ViewModels
{
    public class HelpViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand AppSupportCommand => new Command(NavigateToAppSupportAsync);
        public ICommand ConnectionSupportCommand => new Command(NavigateToConnectionSupportAsync);
        public ICommand ContactUsCommand => new Command(NavigateToContactUsAsync);
        public ICommand AboutCommand => new Command(NavigateToAboutAsync);
        public ICommand HardwareSupportCommand => new Command(NavigateToHardwareSupportAsync);
        public ICommand OpenManualCommand => new Command(OpenManualLink);
        public ICommand OpenPrivacyPolicyCommand => new Command(OpenPrivacyPolicyLink);
        public ICommand OpenShopSnoozCommand => new Command(OpenShopSnoozLink);
        public ICommand SupportTicketCommand => new Command(NavigateToSupportTicketAsync);
        public ICommand SupportSignalCommand => new Command(NavigateToSupportSignalAsync);
        public ICommand AdvancedSettingsCommand => new Command(NavigateToAdvancedSettingsAsync);



        public HelpViewModel(INavigationService navigationService) 
            : base(navigationService) {}

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { {nameof(BluetoothOffPopupPage), true} };
        }

        async void NavigateToAppSupportAsync() 
            => await NavigationService.NavigateAsync(nameof(AppSupport), NavigationParameters);

        async void NavigateToConnectionSupportAsync()
            => await NavigationService.NavigateAsync(nameof(ConnectionSupport), NavigationParameters);

        async void NavigateToContactUsAsync() 
            => await NavigationService.NavigateAsync(nameof(ContactUsPage), NavigationParameters);

        async void NavigateToAboutAsync()
            => await NavigationService.NavigateAsync(nameof(About), NavigationParameters);

        async void NavigateToHardwareSupportAsync() 
            => await NavigationService.NavigateAsync(nameof(DeviceSupportPage), NavigationParameters);

        static void OpenManualLink() =>
            Launcher.TryOpenAsync(
                "https://cdn.shopify.com/s/files/1/1378/8621/files/User_Manual_SNOOZ-US-2.pdf?1439286537390845005");

        static void OpenPrivacyPolicyLink() =>
            Launcher.TryOpenAsync("https://getsnooz.com/pages/privacy-policy");

        static void OpenShopSnoozLink() =>
            Launcher.TryOpenAsync("https://getsnooz.com/");

        async void NavigateToSupportTicketAsync() 
            => await NavigationService.NavigateAsync(nameof(SupportTicketPage), NavigationParameters);

        async void NavigateToSupportSignalAsync()
            => await NavigationService.NavigateAsync(nameof(BLESignalSupportPage), NavigationParameters);

        async void NavigateToAdvancedSettingsAsync()
            => await NavigationService.NavigateAsync(nameof(AdvancedSettings), NavigationParameters);



        protected override async void NavigateBackAsync()
        {
            if (_isFromBluetoothOffPopUp || BluetoothModule.ConnectionState != ConnectionState.Connected)
            {
                if (BluetoothModule.CoupledSnoozes.Any())
                    await NavigationService.NavigateAsync($"/{nameof(ConnectionPage)}");
                else
                    await NavigationService.NavigateAsync($"/{nameof(DeviceConfigurationPage)}");

                if (!App.CanCloseBluetoothPopup 
                    || !CrossBluetoothLE.Current.IsOn)
                    await App.ShowBluetoothOffPopUpAsync();
                    
                return;
            }

            base.NavigateBackAsync();
        }
    }
}
