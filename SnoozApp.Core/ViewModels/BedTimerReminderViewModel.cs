﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using SnoozApp.Core.Enums;
using SnoozApp.Core.Interfaces;
using SnoozApp.Core.Modules;
using SnoozApp.Core.ViewModels;
using SnoozApp.Extensions;
using SnoozApp.Modules;
using SnoozApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using DependencyService = Xamarin.Forms.DependencyService;

namespace SnoozApp.ViewModels
{
    public class BedTimerReminderViewModel : BaseViewModel
    {
        readonly INavigationService _navigationService;
        readonly IAlertPermissionRequest _alertPermissionRequest;
        readonly IPageDialogService _pageDialogService;

        bool _isInit = true;
        List<TimerModel> _timers;

        public ObservableCollection<ScheduleNotification> Minutes { get; set; }
        public ObservableCollection<ScheduleAlarm> Alarms { get; set; }
        public DelegateCommand AddNotificationCommand => new DelegateCommand (async ()=> await OnAddNotification());
        public DelegateCommand CancelNotificationCommand => new DelegateCommand(async () => await OnCancelNotification());
        public ICommand ToggleDarkModeCommand => new Command(async () =>
        {
            if(Device.RuntimePlatform == Device.iOS)
            {
                App.CalibrationPrompted = true;
                if(await RequestAlertPermission())
                    IsToggledNotification = !IsToggledNotification;
            }
            else
                IsToggledNotification = !IsToggledNotification;

        });

        ICommand _daylightSavingsToggleCommand;
        public ICommand DaylightSavingsToggleCommand => _daylightSavingsToggleCommand ??
            (_daylightSavingsToggleCommand = new Command(async () =>
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
                    App.CalibrationPrompted = true;
                    if(await RequestAlertPermission())
                        IsDaylightSavingsToggled = !IsDaylightSavingsToggled;
                }
                else
                    IsDaylightSavingsToggled = !IsDaylightSavingsToggled;

            }));

        ICommand _showHelpCommand;
        public ICommand ShowHelpCommand => _showHelpCommand ??
            (_showHelpCommand = new Command(async ()=> await _pageDialogService.DisplayAlertAsync("Attention", "Choose a theme for your bedtime reminder messages.", "Ok")));


        #region Test Random Messages
        MessageTypeWithDescription _selectedMessageType;
        public MessageTypeWithDescription SelectedMessageType
        {
            get => _selectedMessageType;
            set
            {
                SetProperty(ref _selectedMessageType, value);
            }
        }

        List<MessageTypeWithDescription> _messages;
        public List<MessageTypeWithDescription> Messages { get => _messages; set => SetProperty(ref _messages, value); }
        #endregion

        private async Task<bool> RequestAlertPermission()
        {
            
            var result = await _alertPermissionRequest.RequestPermission();
            if (result)
            {
                return true;
            }
            else
            {
                var resultDialog = await _pageDialogService.DisplayAlertAsync("Attention", "The app needs notification permission to send bedtime reminder. Please grant permission manually.", "Settings", "Cancel");
                if(resultDialog)
                    await Launcher.OpenAsync(new Uri("app-settings:"));

                return false;
            }

        }

        public ICommand ToggleAlarmDarkModeComman => new Command(() => IsToggledAlarm = !IsToggledAlarm);
        
        #region Properties
        int _selectedIndex;
        public int SelectedIndex
        {
            get => _selectedIndex;
            set => SetProperty(ref _selectedIndex, value);
        }

        int _alarmSelectedIndex = -1;
        public int AlarmSelectedIndex
        {
            get => _alarmSelectedIndex;
            set
            {
                _alarmSelectedIndex = value;
                SetProperty(ref _alarmSelectedIndex, value);

                if (_isInit)
                {
                    _isInit = false;
                    return;
                }

                if (_alarmSelectedIndex > -1)
                    PlayAlarm(Alarms[_alarmSelectedIndex].AlarmName);
            }
        }

        bool _isEnabledNotification;
        public bool IsEnabledNotification
        {
            get => _isEnabledNotification;
            set => SetProperty(ref _isEnabledNotification, value);
        }

        bool _isEnabledAlarm;
        public bool IsEnabledAlarm
        {
            get => _isEnabledAlarm;
            set => SetProperty(ref _isEnabledAlarm, value);
        }

        bool _isToggledNotification;
        public bool IsToggledNotification
        {
            get => _isToggledNotification;
            set
            {
                IsEnabledNotification = value;
                SetProperty(ref _isToggledNotification, value);
            }
        }

        bool _isDaylightSavingsToggled;
        public bool IsDaylightSavingsToggled
        {
            get => _isDaylightSavingsToggled;
            set
            {
                SetProperty(ref _isDaylightSavingsToggled, value);
            }
        }

        bool _isToggledAlarm;
        public bool IsToggledAlarm
        {
            get => _isToggledAlarm;
            set
            {
                IsEnabledAlarm = value;
                SetProperty(ref _isToggledAlarm, value);
            }
        }
        #endregion

        public BedTimerReminderViewModel(INavigationService navigationService,
                                         IPageDialogService pageDialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _pageDialogService = pageDialogService;


            InitializeProperties();
        }

        public BedTimerReminderViewModel(INavigationService navigationService,
                                         IAlertPermissionRequest alertPermissionRequest,
                                         IPageDialogService pageDialogService) : base(navigationService)
        {
            _navigationService = navigationService;
            _alertPermissionRequest = alertPermissionRequest;
            _pageDialogService = pageDialogService;

            InitializeProperties();
        }

        private void InitializeProperties()
        {
            Minutes = new ObservableCollection<ScheduleNotification>();
            Alarms = new ObservableCollection<ScheduleAlarm>();
            Messages = new List<MessageTypeWithDescription>();

            var messageManager = new MessageManager();
            Messages = messageManager.GetMessageCategories();

            var selectedMessageType = (MessageType)Preferences.Get("MessageTypeNameIndexKey", 1);
            SelectedMessageType = Messages.FirstOrDefault(x => x.Enum == selectedMessageType);

            OnGetMinutePicker();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            const string timersKey = nameof(SchedulerPageViewModel.Timers);

            if (parameters.ContainsKey(timersKey)
                && parameters[timersKey] is List<TimerModel> timers
                && timers.Any(t => t.Enabled))
                _timers = timers;
        }

        async Task OnAddNotification()
        {
            SettingsModule.IsAlarmToggled = IsToggledAlarm;
            SettingsModule.IsNotificationToggled = IsToggledNotification;
            SettingsModule.IsDaylightSavingsToggled = IsDaylightSavingsToggled;
            SettingsModule.SelectedMinutesBeforeIndex = SelectedIndex;
            SettingsModule.SelectedAlarmNameIndex = AlarmSelectedIndex;
            SettingsModule.MessageTypeValue = (int)SelectedMessageType.Enum;

            if (_timers != null && _timers.Any())
                DependencyService.Get<IAlarmSetter>().SetAlarmsFromTimers(_timers);

            if (!IsToggledNotification)
                DependencyService.Get<IAlarmSetter>().CancelAllAlarms();

            if(IsDaylightSavingsToggled)
                DependencyService.Get<IAlarmSetter>().SetDaylightSavingAlarm();
            else
                DependencyService.Get<IAlarmSetter>().CancelDaylightSaverAlarms();

            await _navigationService.GoBackAsync();
        }

        static void PlayAlarm(string alarmName)
        {
            DependencyService.Get<IAlarmSetter>()
                             .PlayAlarmSoundAsync(alarmName);
        }

        async Task OnCancelNotification()
        {
           await _navigationService.GoBackAsync();
        }

        void OnGetMinutePicker()
        {
            Minutes.Clear();
            foreach (var minute in ScheduleNotification.Options)
                Minutes.Add(minute);

            Alarms.Clear();
            foreach (var alarm in ScheduleAlarm.Options)
                Alarms.Add(alarm);

            SelectedIndex = SettingsModule.SelectedMinutesBeforeIndex;
            _alarmSelectedIndex = SettingsModule.SelectedAlarmNameIndex;
            IsToggledAlarm = SettingsModule.IsAlarmToggled;
            IsToggledNotification = SettingsModule.IsNotificationToggled;
            IsDaylightSavingsToggled = SettingsModule.IsDaylightSavingsToggled;
        }
    }
}
