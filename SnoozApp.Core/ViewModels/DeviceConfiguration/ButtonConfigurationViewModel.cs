﻿using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Navigation;
using SnoozApp.Modules;
using SnoozApp.Pages;
using SnoozApp.Pages.DeviceConfiguration;
using SnoozApp.ViewModels;
using Xamarin.Forms;

namespace SnoozApp.Core.ViewModels.DeviceConfiguration
{
    public class ButtonConfigurationViewModel : BaseViewModel
    {
        public ICommand DevicesPageButtonPress => new Command(StopScanningAndNavigateToDevicesAsync);


        public ButtonConfigurationViewModel(INavigationService navigationService)
            : base(navigationService)
        {
        }

        async void StopScanningAndNavigateToDevicesAsync()
        {
            // Call to StopScanningAsync was moved from OnViewDisappearing to here to ensure that
            // StopScanningAsync completes before StartScanningAsync in the OnViewAppearing of the
            // Devices screen gets called.

            await BluetoothModule.StopScanningAsync();
            await Task.Delay(100);
            await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}");
        }
    }
}


