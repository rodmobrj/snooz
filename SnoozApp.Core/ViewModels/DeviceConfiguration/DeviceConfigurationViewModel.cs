﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using SnoozApp.Core.Modules;
using SnoozApp.Pages;
using SnoozApp.Pages.DeviceConfiguration;
using SnoozApp.ViewModels;

namespace SnoozApp.Core.ViewModels.DeviceConfiguration
{
    public class DeviceConfigurationViewModel : BaseViewModel
    {

        public DelegateCommand<object> ItemTappedCommand { get; set; }
        public DelegateCommand GoBackNavigationCommand => new DelegateCommand(() => GoBackNavigation());

        private async void GoBackNavigation()
        {
            // await _navigationService.GoBackAsync();
           // NavigateBackAsync();

           await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}");

            //Device.BeginInvokeOnMainThread(async () =>
            //    await NavigationService.NavigateAsync($"/{nameof(DeviceConfigurationPage)}"));
        }

        ObservableCollection<DevicesModel> listDevices = new ObservableCollection<DevicesModel>();
        public ObservableCollection<DevicesModel> ListDevices { get { return listDevices; } }


        public override Task InitializeAsync(INavigationParameters parameters)
        {   
            return base.InitializeAsync(parameters);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            Console.WriteLine("DeviceConfigurationViewModel - OnNavigatedTo");

        }
        public DeviceConfigurationViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Console.WriteLine("DeviceConfigurationViewModel - Constructor");
            ItemTappedCommand = new DelegateCommand<object>(ItemTapped);

            var original = new DevicesModel();
            original.DeviceImage = "Original_Setup";
            original.DeviceName = "Original";
            ListDevices.Add(original);

            var button = new DevicesModel();
            button.DeviceImage = "Button_Setup";
            button.DeviceName = "Button";
            ListDevices.Add(button);

            var go = new DevicesModel();
            go.DeviceImage = "Go_Setup";
            go.DeviceName = "Go";
            ListDevices.Add(go);
        }

        public async void ItemTapped(object args)
        {
            var p = new NavigationParameters();
            var itemArg = (DevicesModel) args;

            if(itemArg.DeviceName == "Original")
                await NavigationService.NavigateAsync(nameof(OriginalConfigurationPage));
            else if (itemArg.DeviceName == "Button")
                await NavigationService.NavigateAsync(nameof(ButtonConfigurationPage));
            else if (itemArg.DeviceName == "Go")
                await NavigationService.NavigateAsync(nameof(GoConfigurationPage));
        }

    }
}
