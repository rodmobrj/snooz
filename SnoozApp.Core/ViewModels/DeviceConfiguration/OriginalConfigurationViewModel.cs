﻿using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.EventArgs;
using Prism.Navigation;
using SnoozApp.Modules;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.Core.ViewModels.DeviceConfiguration
{
    public class OriginalConfigurationViewModel : BaseViewModel
    {
        public ICommand DevicesPageButtonPress => new Command(StopScanningAndNavigateToDevicesAsync);
        public ICommand DeviceNameChanged => new Command(HandleDeviceNameChanged);

        public OriginalConfigurationViewModel(INavigationService navigationService)
            : base(navigationService) { }

        bool _snoozConnected;
        string _snoozName;
        bool _snoozNameSet;
        Guid _snoozToName;

        string _connectionStatusLabel;
        public string ConnectionStatusLabel
        {
            get => _connectionStatusLabel;
            set => SetProperty(ref _connectionStatusLabel, value);
        }

        bool _searchingForSNOOZ;
        public bool SearchingForSNOOZ
        {
            get => _searchingForSNOOZ;
            set => SetProperty(ref _searchingForSNOOZ, value);
        }

        bool _disableButton;
        public bool disableButton
        {
            get => _disableButton;
            set => SetProperty(ref _disableButton, value);
        }

        bool _deviceDiscovered;
        public bool DeviceDiscovered
        {
            get => _deviceDiscovered;
            set => SetProperty(ref _deviceDiscovered, value);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine("ConfigurationViewModel OnNavigatedTo");
            ConnectionStatusLabel = "Searching...";

            SearchingForSNOOZ = true;

            BluetoothModule.ScanTimeoutElapsed += BluetoothModule_ScanTimeoutElapsed;
            BluetoothModule.SnoozDiscovered += BluetoothModule_SnoozDiscovered;
            BluetoothModule.SnoozConnected += BluetoothModule_SnoozConnected;
            Debug.WriteLine("ConfigurationViewModel Subscribed");

            disableButton = false;
            DeviceDiscovered = false;
            _snoozConnected = false;
            _snoozNameSet = false;
            _snoozName = "";

            Task.Run(BluetoothModule.StartScanningAsync);
        }

        public override async void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("ConfigurationViewModel OnNavigatedFrom");
            await this.BluetoothModule.StopScanningAsync(); //In case help screen command called. 

            UnsubscribeFromEvents();
        }

        public override void Destroy()
        {
            Debug.WriteLine("ConfigurationViewModel Destroy");
            UnsubscribeFromEvents();
        }

        async void StopScanningAndNavigateToDevicesAsync()
        {
            // Call to StopScanningAsync was moved from OnViewDisappearing to here to ensure that
            // StopScanningAsync completes before StartScanningAsync in the OnViewAppearing of the
            // Devices screen gets called.

            disableButton = true;
            await BluetoothModule.StopScanningAsync();
            if (Device.RuntimePlatform.Equals(Device.Android))
                Device.BeginInvokeOnMainThread(async () =>
                    await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}"));
            else
                await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}");
        }

        async void BluetoothModule_SnoozDiscovered(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("ConfigurationViewModel SnoozDiscovered");
            var adData =
                e.Device?.AdvertisementRecords?.FirstOrDefault(s => s.Type == AdvertisementRecordType.ManufacturerSpecificData);
            if (adData?.Data != null)
            {
                if ((adData.Data[BluetoothModule.SnoozAdvertisingDataFlagsByte] & BluetoothModule.SnoozAdvertisingDataCouplingBit) == BluetoothModule.SnoozAdvertisingDataCouplingBit)
                {
                    Debug.WriteLine("Coupling Snooz Discovered");
                    ulong password;

                    byte[] passwordBytes = new byte[8];
                    Array.Copy(adData.Data, 3, passwordBytes, 0, 8);
                    if (BitConverter.ToUInt64(passwordBytes, 0) != 0)
                    {
                        password = BitConverter.ToUInt64(passwordBytes, 0);
                        Debug.WriteLine("Coupling Snooz Password: " + password);

                        BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
                        var ConnectionResult = await this.BluetoothModule.ConnectToDeviceAsync(e.Device, password);

                        if (!ConnectionResult)
                        {
                            Debug.WriteLine("Connecton Failed");
                            ConnectionResult = await this.BluetoothModule.RetryConnectToDevice(e.Device, password);
                        }
                        //TO DO: Need better logic here to add in logic if this fails to try again

                        // If the Snooz is already in our list don't prompt to rename it
                        if (this.BluetoothModule.CoupledSnoozes.Any(s => s.Id == e.Device.Id))
                        {
                            this._snoozNameSet = true;
                        }
                        else
                        {
                            this.SearchingForSNOOZ = false;
                            this._snoozToName = e.Device.Id;
                            this.DeviceDiscovered = true;
                        }
                    }
                }
            }
        }

        void BluetoothModule_SnoozConnected(object sender, EventArgs e)
        {
           // ConnectionStatusLabel = "Connecting...";
            Debug.WriteLine("ConfigurationViewModel SnoozConnected");
            _snoozConnected = true;

            // If the user has already entered the name change the name in the CoupledSnooz and switch to the main page
            if (!_snoozNameSet) return;

            BluetoothModule.RenameCoupledSnooz(_snoozToName, _snoozName);

            UnsubscribeFromEvents();

            Device.BeginInvokeOnMainThread(async () =>
                await NavigationService.NavigateAsync($"/{App.GetMainPageName()}"));

        }

        void HandleDeviceNameChanged(object name)
        {
            this.SearchingForSNOOZ = true;

            ConnectionStatusLabel = "Connecting...";
            _snoozName = name as string;
            _snoozNameSet = true;

            /* 
             * The CoupledSnooz object won't be created until the connected event so don't attempt to rename yet
             */
            if (!_snoozConnected || _snoozName == "")
                return;

            BluetoothModule.RenameCoupledSnooz(_snoozToName, _snoozName);
            /* This task wrapper resolves an issue on Android.
                 * Without the task if the app switches the main page immediately after the entry is unfocused Xamarin 
                 * will throw an exception, presumably because it is trying to finish updating the entry on the configuration page
                 * that has been disposed.
                 */
            UnsubscribeFromEvents();

            Device.BeginInvokeOnMainThread(async () =>
                await NavigationService.NavigateAsync($"/{App.GetMainPageName()}"));
        }

        async void BluetoothModule_ScanTimeoutElapsed(object sender, EventArgs e)
        {
            Debug.WriteLine("ConfigurationViewModel ScanTimeoutElapsed");
            try
            {
                  await BluetoothModule.StartScanningAsync();
            }
            catch (Exception)
            {
                if (!Plugin.BLE.CrossBluetoothLE.Current.IsOn)
                    await App.ShowBluetoothOffPopUpAsync();
            }
        }

        void UnsubscribeFromEvents()
        {
            BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
            BluetoothModule.SnoozDiscovered -= BluetoothModule_SnoozDiscovered;
            BluetoothModule.SnoozConnected -= BluetoothModule_SnoozConnected;
        }
    }
}