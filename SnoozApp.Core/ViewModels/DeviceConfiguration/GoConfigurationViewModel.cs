﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Navigation;
using SnoozApp.Core.Interfaces;
using SnoozApp.Modules;
using SnoozApp.Pages;
using SnoozApp.Pages.DeviceConfiguration;
using SnoozApp.ViewModels;
using Xamarin.Forms;

namespace SnoozApp.Core.ViewModels.DeviceConfiguration
{
    public class GoConfigurationViewModel : BaseViewModel
    {
        public ICommand DevicesPageButtonPress => new Command(StopScanningAndNavigateToDevicesAsync);

        public ICommand BLESettingsCommand => new Command(OpenBLESettings);

        private void OpenBLESettings()
        {
            DependencyService.Get<ISettingsAppLauncher>().LaunchSettingsApp("com.snooz.snoozapp");
        }

        public GoConfigurationViewModel(INavigationService navigationService)
            : base(navigationService)
        {
          
        }

        async void StopScanningAndNavigateToDevicesAsync()
        {
            // Call to StopScanningAsync was moved from OnViewDisappearing to here to ensure that
            // StopScanningAsync completes before StartScanningAsync in the OnViewAppearing of the
            // Devices screen gets called.

            await BluetoothModule.StopScanningAsync();
            await Task.Delay(100);
            await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}");
        }

        //public override void OnNavigatedTo(INavigationParameters parameters)
        //{


        //}

    }
}