﻿using Prism.Navigation;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using Xamarin.Essentials;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.ViewModels
{
    public class AppSupportViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand DevicesPageButtonPress => DevicesCommand;
        public ICommand ContactUsCommand => new Command(ContactUs);

        public AppSupportViewModel(INavigationService navigationService)
            : base(navigationService) { }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { { nameof(BluetoothOffPopupPage), true } };
        }

        static void ContactUs() => Launcher.TryOpenAsync("mailto:App@getsnooz.com");
    }
}