﻿using System;
using System.Windows.Input;
using SnoozApp.Modules;
using Xamarin.Forms;
using static SnoozApp.Modules.BluetoothModule;

namespace SnoozApp.ViewModels
{
    public class MainViewFadeModel
    {
		private int _FadeIn;
		public string FadeIn
		{
			get => this._FadeIn == 0 
				? "Off"
				: this._FadeIn.ToString();
			set
			{
				var newFadeInValue = int.TryParse(value, out var intValue)
					? intValue
					: 0;
				if (newFadeInValue != this._FadeIn)
				{
					this.BluetoothModule.WriteFadeOnCommand(newFadeInValue != 0, (uint)newFadeInValue);
				}
			}
		}

		private int _FadeOut;
		public string FadeOut
		{
			get => this._FadeOut == 0
				? "Off"
				: this._FadeOut.ToString();
			set
			{
				var newFadeOutValue = int.TryParse(value, out var intValue)
					? intValue
					: 0;
				if (newFadeOutValue != this._FadeOut)
				{
					this.BluetoothModule.WriteFadeOffCommand(newFadeOutValue != 0, (uint)newFadeOutValue);
				}
			}
		}
		private BluetoothModule BluetoothModule { get; set; }

		public ICommand ShowHelpCommand { get; private set; }

		public Action NotifyFadeTimersUpdated { get; set; }

		public MainViewFadeModel(BluetoothModule bluetoothModule, Action notifyFadeUpdate)
		{
			this.BluetoothModule = bluetoothModule;
			this.ShowHelpCommand = new Command(this.ShowHelp);
			this.NotifyFadeTimersUpdated = notifyFadeUpdate;
		}

		public void Subscribe()
		{
			this.BluetoothModule.SnoozFadeUpdate += this.BluetoothModule_SnoozFadeTimersUpdate;
		}

		public void Unsubscribe()
		{
			this.BluetoothModule.SnoozFadeUpdate -= this.BluetoothModule_SnoozFadeTimersUpdate;
		}

		private void BluetoothModule_SnoozFadeTimersUpdate(object sender, FadeResponseFrameObject responseFrame)
		{
			this._FadeIn = responseFrame.FadeOnEnabled 
						&& responseFrame.FadeOnTime > 0
				? responseFrame.FadeOnTime < 60
					? (int)responseFrame.FadeOnTime
					: 60
				: 0;

			this._FadeOut = responseFrame.FadeOffEnabled
						&& responseFrame.FadeOffTime > 0
				? responseFrame.FadeOffTime < 60
					? (int)responseFrame.FadeOffTime
					: 60
				: 0;
			this.NotifyFadeTimersUpdated();
		}

		private void ShowHelp(object obj)
		{
			MessagingCenter.Send(this, App.FADE_HELP_MESSAGE);
		}
	}
}
