﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;
using Newtonsoft.Json;
using Prism.Navigation;
using SnoozApp.Modules;
using SnoozApp.Pages;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace SnoozApp.ViewModels
{
    public class ConnectionHistory
    {
        public int LastMotorSpeed;
        public int LastFirmware;
        public int SchedulerInUse;
        public int LastLightState;
        public int EverCalibrated;
        public int ConnectionCount;


        public ConnectionHistory(int lastmotorspeed, int lastfirmware, int schedulerInUse, 
            int lastlightstate, int evercalibrated, int connectioncount)
        {
            LastMotorSpeed = lastmotorspeed;
            LastFirmware = lastfirmware;
            SchedulerInUse = schedulerInUse;
            LastLightState = lastlightstate;
            EverCalibrated = evercalibrated;
            ConnectionCount = connectioncount;
        }
    }

    public class SupportTicketViewModel : BaseViewModel
    {
        int _connectionCount;
        bool _isFromBluetoothOffPopUp;

        public string SupportTicket { get; }

        string _appStatusCode;
        public string AppStatusCode
        {
            get => _appStatusCode;
            set => SetProperty(ref _appStatusCode, value);
        }

        int _lastMotorSpeed;
        public int LastMotorSpeed
        {
            get => _lastMotorSpeed;
            set => SetProperty(ref _lastMotorSpeed, value);
        }

        int _lastFirmware;
        public int LastFirmware
        {
            get => _lastFirmware;
            set => SetProperty(ref _lastFirmware, value);
        }

        int _schedulerInUse;
        public int SchedulerInUse
        {
            get => _schedulerInUse;
            set => SetProperty(ref _schedulerInUse, value);
        }

        int _lastLightState;
        public int LastLightState
        {
            get => _lastLightState;
            set => SetProperty(ref _lastLightState, value);
        }

        int _everCalibrated;
        public int EverCalibrated
        {
            get => _everCalibrated;
            set => SetProperty(ref _everCalibrated, value);
        }

        public int ConnectionCount
        {
            get => _connectionCount;
            set => SetProperty(ref _connectionCount, value);
        }

        public ICommand ContactUsCommand => new Command(ContactUs);

        public SupportTicketViewModel(INavigationService navigationService) : base(navigationService)
        {
            SupportTicket = $"{new Random().Next(1000)}{DateTime.Today:yyMMdd}";
            SupportCodeGenerator();
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { {nameof(BluetoothOffPopupPage), true} };
        }

        void ContactUs() => Launcher.TryOpenAsync("mailto:App@getsnooz.com" +
                                                   $"?subject=Support Ticket %23{SupportTicket}" +
                                                   $"&body=OS Platform: {Device.RuntimePlatform}%0D%0A" +
                                                   $"App Status Code: {AppStatusCode}%0D%0A");

        void SupportCodeGenerator()
        {
            GetConnectionHistory();

            // Retrieve last connected snooz from settings data
            var availableSnoozes = BluetoothModule.DiscoveredSnoozes;
            var coupledSnoozescount = 0;

            try
            {
                var coupledSnoozes2 = JsonConvert.DeserializeObject<List<CoupledSnooz>>
                    (SettingsModule.CoupledDevices);
                if (coupledSnoozes2 != null)
                    coupledSnoozescount = coupledSnoozes2.Count;
            }
            catch
            {
            }

            var serviceCode = $"22.{availableSnoozes.Count}" +
                                $".{coupledSnoozescount}" +
                                $".{LastMotorSpeed}" +
                                $".{LastFirmware}" +
                                $".{SchedulerInUse + LastLightState + EverCalibrated + ConnectionCount}";
            AppStatusCode = serviceCode;
        }

        void GetConnectionHistory()
        {
            try
            {
                var convertedConnectionHistory = JsonConvert.DeserializeObject<ConnectionHistory>(SettingsModule.ConnectionHistory);
                if (convertedConnectionHistory == null) return;

                LastMotorSpeed = convertedConnectionHistory.LastMotorSpeed;
                LastFirmware = convertedConnectionHistory.LastFirmware;
                SchedulerInUse = convertedConnectionHistory.SchedulerInUse;
                LastLightState = convertedConnectionHistory.LastLightState;
                EverCalibrated = convertedConnectionHistory.EverCalibrated;
                ConnectionCount = convertedConnectionHistory.ConnectionCount;
            }
            catch (Exception ex)
            {
               Debug.WriteLine($"SupportTicketViewModel.GetConnectionHistory error: {ex}");
            }
        }
    }
}
