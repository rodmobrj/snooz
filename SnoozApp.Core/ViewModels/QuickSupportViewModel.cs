﻿using Prism.Navigation;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace SnoozApp.ViewModels
{
    public class QuickSupportViewModel : BaseViewModel
    {
        bool _isFromBluetoothOffPopUp;

        public ICommand MoreSupportCommand => new Command(NavigateToConnectionSupportAsync);
        public ICommand TryAgainCommand => new Command(NavigateToConnectionPageAsync);
        public ICommand DevicePageCommand => new Command(NavigateToDevicePageAsync);



        public QuickSupportViewModel(INavigationService navigationService)
            : base(navigationService) { }


        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            await BluetoothModule.DisconnectDevicesAsync();

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { { nameof(BluetoothOffPopupPage), true } };
        }

        async void NavigateToConnectionSupportAsync()
            => await NavigationService.NavigateAsync(nameof(HelpPage), NavigationParameters);
        async void NavigateToConnectionPageAsync()
            => await NavigationService.NavigateAsync(nameof(ConnectionPage), NavigationParameters);
        async void NavigateToDevicePageAsync()
            => await NavigationService.NavigateAsync(nameof(DevicesPage), NavigationParameters);

    }
}