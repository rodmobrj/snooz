﻿using Plugin.BLE.Abstractions.EventArgs;
using Plugin.StoreReview;
using Prism.Navigation;
using SnoozApp.Enums;
using SnoozApp.Interfaces;
using SnoozApp.Modules;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Services;
using Xamarin.Forms;
using DependencyService = Xamarin.Forms.DependencyService;
using SnoozApp.Pages;
using SnoozApp.Core.Extensions;

namespace SnoozApp.ViewModels
{
    public class ConnectionViewModel : BaseViewModel
    {
        readonly IPageDialogService PageDialogService;

        public ICommand DeviceCommand => new Command(DevicesPageCommand);
        public ICommand QuickSupportCommand => new Command(QuickSupportPageCommand);

        public ConnectionViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService)
        {
            PageDialogService = pageDialogService;
        }

        string _connectionStatusLabel;
        public string ConnectionStatusLabel
        {
            get => _connectionStatusLabel;
            set => SetProperty(ref _connectionStatusLabel, value);
        }

        bool _showBottomLabel;
        public bool ShowBottomLabel
        {
            get => _showBottomLabel;
            set => SetProperty(ref _showBottomLabel, value);
        }

        bool _disableButton;
        public bool disableButton
        {
            get => _disableButton;
            set => SetProperty(ref _disableButton, value);
        }

        ImageSource _rssiImage;
        public ImageSource RssiImage
        {
            get => _rssiImage;
            set => SetProperty(ref _rssiImage, value);
        }

        public string DeviceName => BluetoothModule.LastConnectedSnooz.Name != null
            ? BluetoothModule.LastConnectedSnooz.Name
            : "Last SNOOZ";

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            Debug.WriteLine("ConnectionViewModel OnNavigatedTo: " + BluetoothModule.ConnectionState);

            ConnectionStatusLabel = $"Searching for " + '"' + DeviceName + '"';
            RssiImage = RssiImageConverter.DefaultImageSource();

            ShowBottomLabel = false;
            disableButton = false;

            if (!App.ClosingTime)
                ConnectionManager();
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("ConnectionViewModel OnNavigatedFrom");
            ShowBottomLabel = false;
            disableButton = false;
            App.TryConnecting = false;
            UnsubscribeFromEvents();
        }

        public override void Destroy()
        {
            Debug.WriteLine("ConnectionViewModel Destroy");
            UnsubscribeFromEvents();
        }

      async void ConnectionManager()
        {
            BluetoothModule.SnoozDiscovered += BluetoothModule_SnoozDiscovered;
            BluetoothModule.SnoozConnected += BluetoothModule_SnoozConnected;
            BluetoothModule.ScanTimeoutElapsed += BluetoothModule_ScanTimeoutElapsed;
            BluetoothModule.SnoozConnectionLost += BluetoothModule_ConnectionLost;

            var quickConnect = await BluetoothModule.ConnectToKnownDeviceAsync(); //Connecting to last connected device

            if (quickConnect)
                 RssiImage = RssiImageConverter.GetImageSourceFromRssiValue(70);
            else
                 await Task.Run(BluetoothModule.StartScanningAsync);
        }

        async void NavigateToCantConnectAsync()
            => await NavigationService.NavigateAsync(nameof(QuickSupportPage), NavigationParameters);

        async void BluetoothModule_SnoozDiscovered(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("ConnectionViewModel: SnoozDiscovered");

            var snoozInList = BluetoothModule.CoupledSnoozes.FirstOrDefault(s => s.Id == e.Device.Id);

            if (e.Device.Id != BluetoothModule?.LastConnectedSnooz?.Id 
                || snoozInList == null) 
                return;

            RssiImage = RssiImageConverter.GetImageSourceFromRssiValue(e.Device.Rssi);

            Debug.WriteLine("RSSI: " + e.Device.Rssi);
            Debug.WriteLine("Signal Monitor: " + SettingsModule.SignalMonitorEnabled);

            //Will only allow a connection if Bluetooth signal is strong
            if (SettingsModule.SignalMonitorEnabled && e.Device.Rssi < -90)
                return;

            ConnectionStatusLabel = "Connecting to " + '"' + DeviceName + '"';

            Debug.WriteLine("==================================SNOOZ Found, Attempting to Connect");
            BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
            App.TryConnecting = true;
            var ConnectionResult = false;

            if(!App.ClosingTime)
                ConnectionResult = await this.BluetoothModule.ConnectToDeviceAsync(e.Device, snoozInList.Password);

            //App.ClosingTime = if someone minimizes the App, TryConnecting = if someone cancels
            if (!ConnectionResult && !App.ClosingTime && App.TryConnecting)
            {
                Debug.WriteLine("Connection Fail 1");
                ConnectionResult = await this.BluetoothModule.RetryConnectToDevice(e.Device, snoozInList.Password);
            }

            if (!ConnectionResult && !App.ClosingTime && App.TryConnecting)
            {
                ConnectionStatusLabel = "Searching for " + '"' + DeviceName + '"';

                Debug.WriteLine("Connection Fail 2");
                BluetoothModule.ScanTimeoutElapsed += BluetoothModule_ScanTimeoutElapsed;
                await this.BluetoothModule.StartScanningAsync();
            }
        }

        async void BluetoothModule_SnoozConnected(object sender, EventArgs e)
        {

            if(App.ClosingTime)
            {
                await this.BluetoothModule.DisconnectDevicesAsync();
                return;
            }

            Debug.WriteLine("ConnectionViewModel SnoozConnected");
            Debug.WriteLine("App Ever Reviewed: " + SettingsModule.EverReviewedApp);
            Debug.WriteLine("Connection Count: " + SettingsModule.ConnectionCount);
            SettingsModule.ConnectionCount++;
            //SettingsModule.ConnectionCount = 28; //TBD
            //SettingsModule.EverReviewedApp = false; //TBD


            UnsubscribeFromEvents(); //don't need scan time out
            if (Device.RuntimePlatform.Equals(Device.Android))
                Device.BeginInvokeOnMainThread(async () => 
                    await NavigationService.NavigateAsync($"/{App.GetMainPageName()}"));
            else
                await NavigationService.NavigateAsync($"/{App.GetMainPageName()}");

            if (SettingsModule.ConnectionCount <= 30
                || SettingsModule.EverReviewedApp || BluetoothModule.FirmwareVersion != FirmwareVersions.V3)
                return;

            SettingsModule.EverReviewedApp = true;
            Device.BeginInvokeOnMainThread(async () =>
            {
                var shouldRateApp = await PageDialogService.DisplayAlertAsync("Enjoying SNOOZ?",
                                                                              "If so, can we trouble you for a review? We'd be grateful for your help in spreading the good SNOOZ!",
                                                                              "Yes",
                                                                              "No");
                if (!shouldRateApp)
                    return;

                switch (Device.RuntimePlatform)
                {
                    case Device.Android:
                        await CrossStoreReview.Current.RequestReview(false);
                        break;
                    case Device.iOS:
                        await CrossStoreReview.Current.RequestReview(false);
                        break;
                }
            });
        }


        void BluetoothModule_ConnectionLost(object sender, DeviceEventArgs e)
        {
            Debug.WriteLine("Connection View Model: Connction Lost");
            ConnectionManager();
        }


        async void BluetoothModule_ScanTimeoutElapsed(object sender, System.EventArgs e)
        {
            Debug.WriteLine("ConnectionView: ScanTimeoutElapsed");
            await Task.Run(BluetoothModule.StartScanningAsync);
        }

        void QuickSupportPageCommand()
        {
            Debug.WriteLine("ConnectionView: Quick Support Page Request");
            CancelConnectionCommandAsync(true);
        }

        void DevicesPageCommand()
        {
            Debug.WriteLine("ConnectionView: Device Page Request");
            CancelConnectionCommandAsync(false);
        }

        async void CancelConnectionCommandAsync(bool QuickSupportPageRequest)
        {
            Debug.WriteLine("Cancelling Connection Request");

            // Don't attempt to cancel connection if we are already attempting to cancel
            // You could use a return here, but locking out the screen is more crash-proof
            disableButton = true;
            App.TryConnecting = false;
            UnsubscribeFromEvents();

            if (BluetoothModule.ConnectionState == ConnectionState.Scanning 
                || BluetoothModule.ConnectionState == ConnectionState.Disconnected)
            {
                await BluetoothModule.StopScanningAsync();
                if(QuickSupportPageRequest)
                    Device.BeginInvokeOnMainThread(async () =>
                        await NavigationService.NavigateAsync($"/{nameof(QuickSupportPage)}"));
                else
                    Device.BeginInvokeOnMainThread(async () =>
                        await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}"));
            }
            else //is connected or is connecting
            {
                ShowBottomLabel = true;
                ConnectionStatusLabel = "Cancelling Connection";
                await Task.Delay(5000);
                if (QuickSupportPageRequest)
                    Device.BeginInvokeOnMainThread(async () =>
                        await NavigationService.NavigateAsync($"/{nameof(QuickSupportPage)}"));
                else
                    Device.BeginInvokeOnMainThread(async () =>
                        await NavigationService.NavigateAsync($"/{nameof(DevicesPage)}"));
            }
        }

        void UnsubscribeFromEvents()
        {
            Debug.WriteLine("ConnectionView Unsubscribe Called");
            BluetoothModule.SnoozDiscovered -= BluetoothModule_SnoozDiscovered;
            BluetoothModule.SnoozConnected -= BluetoothModule_SnoozConnected;
            BluetoothModule.ScanTimeoutElapsed -= BluetoothModule_ScanTimeoutElapsed;
            BluetoothModule.SnoozConnectionLost -= BluetoothModule_ConnectionLost;
        }
    }   
}