﻿using Prism.Navigation;
using SnoozApp.Pages;
using SnoozApp.ViewModels;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;
using Plugin.BLE.Abstractions.EventArgs;
using SnoozApp.Modules;
using System.Linq;


using System.Collections.ObjectModel;


namespace SnoozApp.ViewModels
{
    public class AdvancedSettingsViewModel : BaseViewModel
    {

        bool _isFromBluetoothOffPopUp;

        bool _isSignalMonitorEnabled;
        public bool IsSignalMonitorEnabled
        {
            get => _isSignalMonitorEnabled;
            set
            {
                SetProperty(ref _isSignalMonitorEnabled, value);
            }
        }


        public ICommand ToggleSignalMonitorCommand => new Command(_toggleSignalMonitorCommand);


        void _toggleSignalMonitorCommand()
        {
            IsSignalMonitorEnabled = !IsSignalMonitorEnabled;
            SettingsModule.SignalMonitorEnabled = IsSignalMonitorEnabled;
        }


        public AdvancedSettingsViewModel(INavigationService navigationService)
            : base(navigationService) { }


    
        public override void OnNavigatedTo(INavigationParameters parameters)
        {

            base.OnNavigatedTo(parameters);
            Debug.WriteLine("Navigated to  Advanced Settings Module)");

            IsSignalMonitorEnabled = SettingsModule.SignalMonitorEnabled;

            _isFromBluetoothOffPopUp = parameters.ContainsKey(nameof(BluetoothOffPopupPage));
            if (_isFromBluetoothOffPopUp)
                NavigationParameters = new NavigationParameters { { nameof(BluetoothOffPopupPage), true } };
        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            Debug.WriteLine("Advanced Settings OnNavigatedFrom");
        }






    }
}
