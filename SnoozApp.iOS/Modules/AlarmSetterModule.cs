﻿using Foundation;
using SnoozApp.Core.Enums;
using SnoozApp.Core.Modules;
using SnoozApp.iOS.Modules;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using UserNotifications;
using Xamarin.Forms;

[assembly: Dependency(typeof(AlarmSetterModule))]
namespace SnoozApp.iOS.Modules
{
    public class AlarmSetterModule : BaseAlarmSetterModule
    {
        static UNUserNotificationCenter NotificationCenter => UNUserNotificationCenter.Current;
        static string NOTIFICATION_PREFIX_ID = "Daylight_";

        public override void CancelAllAlarms()
            => NotificationCenter.RemoveAllPendingNotificationRequests();

        //TODO: Implement. Method only used by WakeUp Alarm feature.
        public override Task PlayAlarmSoundAsync(string alarmName)
            => Task.CompletedTask;

        public override void SetBedTimeReminderAlarm(WeeklySchedule schedule, int minutesBefore, bool setNextWeekAlarms = false,
            bool isNextWeek = false)
            => NotificationCenter.AddNotificationRequest(CreateNotificationRequest(schedule, minutesBefore), error => { });

        public override void SetWakeUpAlarm(WeeklySchedule schedule, string alarmName, bool setNextWeekAlarms = false, bool isNextWeek = false)
        {
            //TODO: Implement. Method only used by WakeUp Alarm feature.
        }

        static UNNotificationRequest CreateNotificationRequest(WeeklySchedule schedule, int minutesBefore, bool isBedtime = true)
        {
            var isOneHourLessStart = schedule.MinuteStart < minutesBefore;
            var isOneHourLessStop = schedule.MinuteStop < minutesBefore;
            var hourStart = isOneHourLessStart
                ? (schedule.HourStart == 0 ? 24 : schedule.HourStart) - 1
                : schedule.HourStart;
            var hourStop = isOneHourLessStop
                ? (schedule.HourStop == 0 ? 24 : schedule.HourStop) - 1
                : schedule.HourStop;
            var hour = isBedtime ? hourStart : hourStop;

            var minuteStart = isOneHourLessStart
                ? 60 + (schedule.MinuteStart - minutesBefore)
                : schedule.MinuteStart - minutesBefore;
            var minuteStop = isOneHourLessStop
                ? 60 + (schedule.MinuteStop - minutesBefore)
                : schedule.MinuteStop - minutesBefore;
            var minute = isBedtime ? minuteStart : minuteStop;

            var isPreviousWeekDay = isBedtime
                ? schedule.HourStart == 0 && hourStart > 0
                : schedule.HourStop == 0 && hourStop > 0;
            var weekDay = isPreviousWeekDay
                ? schedule.Week == (int)DayOfWeekValue.Sunday ? (int)DayOfWeekValue.Saturday : schedule.Week - 1
                : schedule.Week;

            return UNNotificationRequest.FromIdentifier(schedule.Id.ToString(),
                                                        new UNMutableNotificationContent
                                                        {
                                                            Title = schedule.Title,
                                                            Body = schedule.Message,
                                                            Sound = UNNotificationSound.DefaultCriticalSound
                                                        },
                                                        UNCalendarNotificationTrigger.CreateTrigger(new NSDateComponents
                                                        {
                                                            Calendar = NSCalendar.CurrentCalendar,
                                                            Weekday = weekDay,
                                                            Hour = hour,
                                                            Minute = minute
                                                        }, true));
        }

        public override void CancelDaylightSaverAlarms()
        {
            System.Diagnostics.Debug.WriteLine($"Daylight Saving notification deleted: {DateTime.Now.ToString("dd/MM/yyyy hh:mm") }");

            var notificationIDs = new List<string>();

            for (var i = 1; i <= 9; i++)
            {
                notificationIDs.Add($"{NOTIFICATION_PREFIX_ID}{i * 100}");
                notificationIDs.Add($"{NOTIFICATION_PREFIX_ID}{i * 1000}");
            }

            NotificationCenter.RemovePendingNotificationRequests(notificationIDs.ToArray());
        }



        public override void CreateDaylightSavingAlarm(DateTime dateAlarm, int id, string alarmName, bool isStartDate)
            => NotificationCenter.AddNotificationRequest(CreateNotificationRequest(dateAlarm, id.ToString(), isStartDate), (error) => { });

        static UNNotificationRequest CreateNotificationRequest(DateTime dateAlarm, string id, bool isStartDate)
        {
            System.Diagnostics.Debug.WriteLine($"Daylight Saving notification created: {dateAlarm.ToString("dd/MM/yyyy hh:mm") } // ID: {id}");

            var trigger = UNCalendarNotificationTrigger.CreateTrigger(GetNSDateComponentsFromDateTime(dateAlarm), false);
            var content = new UNMutableNotificationContent
            {
                Title = "Daylight Savings",
                Body = isStartDate ? "Daylight saving time begins tomorrow." : "Daylight saving time ends tomorrow.",
                Sound = UNNotificationSound.DefaultCriticalSound
            };
            return UNNotificationRequest.FromIdentifier($"{NOTIFICATION_PREFIX_ID}{id}", content, trigger);

        }

        static NSDateComponents GetNSDateComponentsFromDateTime(DateTime dateTime)
        {
            return new NSDateComponents
            {
                Month = dateTime.Month,
                Day = dateTime.Day,
                Year = dateTime.Year,
                Hour = 19,
                Minute = 0
            };
        }

    }
}