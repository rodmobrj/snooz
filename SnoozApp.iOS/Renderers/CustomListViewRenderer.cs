﻿using SnoozApp.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ListView), typeof(CustomListViewRenderer))]
namespace SnoozApp.iOS.Renderers
{
    public class CustomListViewRenderer : ListViewRenderer
    {
		protected override void OnElementChanged (ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;
			Control.ShowsVerticalScrollIndicator = false;
        }
    }
}

