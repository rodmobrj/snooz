﻿using SnoozApp.iOS.Renderers;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Picker), typeof(TransparentPickerRenderer))]
namespace SnoozApp.iOS.Renderers
{
    public class TransparentPickerRenderer : PickerRenderer
	{
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

            BackgroundColor = null;

            if (Control == null) return;
			Control.BorderStyle = UITextBorderStyle.None;
            Control.TextColor = UIColor.White;

            if (Control.Layer == null) return;
            Control.Layer.BorderWidth = 0;
		}
	}
}
