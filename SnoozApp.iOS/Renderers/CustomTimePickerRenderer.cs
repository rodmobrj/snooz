﻿using CoreGraphics;
using Foundation;
using SnoozApp.Controls;
using SnoozApp.iOS.Renderers;
using System;
using System.ComponentModel;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]

namespace SnoozApp.iOS.Renderers
{
    public class CustomTimePickerRenderer : ViewRenderer<TimePicker, UITextField>
    {
        UIDatePicker _picker;
        UIPopoverController _popOver;

        protected override void OnElementChanged (ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged (e);

            if (e.NewElement != null) 
            {
                if (Control == null) 
                {
					var entry = new UITextField
					{
						BorderStyle = UITextBorderStyle.None,
						TextAlignment = UITextAlignment.Center,
						TextColor = this.Element?.TextColor.ToUIColor() ?? UIColor.White,
						BackgroundColor = UIColor.Clear,
						Font = UIFont.FromName("DINPro-Medium", 20f)                     
                    };

                    entry.Started += this.OnStarted;
                    entry.Ended += this.OnEnded;

                    this._picker = new UIDatePicker 
                    {
                        Mode = UIDatePickerMode.Time,
                        TimeZone = new NSTimeZone ("UTC")
                    };

                    nfloat width = UIScreen.MainScreen.Bounds.Width;
                    var uIToolbar = new UIToolbar (new CGRect (0, 0, width, 44)) 
                    {
                        BarStyle = UIBarStyle.Default,
                        Translucent = true
                    };

                    var uIBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);
                    var doneButton = new UIBarButtonItem (UIBarButtonSystemItem.Done,
                        delegate {
                            this.UpdateBoundTime();
                            entry.ResignFirstResponder ();
                        });
                    //var cancelButton = new UIBarButtonItem (UIBarButtonSystemItem.Cancel, 
                    //    delegate { 
                    //        entry.ResignFirstResponder (); 
                    //    });

                    //uIToolbar.SetItems (new [] { uIBarButtonItem, cancelButton, doneButton }, false);
                    uIToolbar.SetItems (new [] { uIBarButtonItem, doneButton }, false);

                    if (Device.Idiom == TargetIdiom.Phone) 
                    {
                        entry.InputView = this._picker;
                        entry.InputAccessoryView = uIToolbar;
                    } 
                    else 
                    {
                        entry.InputView = new UIView (CGRect.Empty);
                        entry.InputAccessoryView = new UIView (CGRect.Empty);
                    }

                    this._picker.ValueChanged += this.HandleValueChanged;
                    SetNativeControl (entry);
                }
            }

            UpdateTime ();
        }

        protected override void OnElementPropertyChanged (object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged (sender, e);
            if (e.PropertyName == TimePicker.TimeProperty.PropertyName || e.PropertyName == TimePicker.FormatProperty.PropertyName) 
            {
                UpdateTime ();
            }
            if (e.PropertyName == TimePicker.TextColorProperty.PropertyName)
            {
                this.Control.TextColor = this.Element?.TextColor.ToUIColor() ?? UIColor.White;
            }
        }

        private void OnStarted (object sender, EventArgs eventArgs)
        {
            this.UpdateTime ();
			((ICustomTimePickerController)Element)?.TimePickerPressed();

            if (Device.Idiom == TargetIdiom.Phone) 
                return;

            var vc = new UIViewController { this._picker };
            vc.View.Frame = new CGRect (0, 0, 320, 200);
            vc.PreferredContentSize = new CGSize (320, 200);
            this._popOver = new UIPopoverController (vc);
            this._popOver.PresentFromRect (
                new CGRect (this.Control.Frame.Width / 2, this.Control.Frame.Height - 3, 0, 0),
                this.Control,
                UIPopoverArrowDirection.Any,
                true);

            this._popOver.DidDismiss += (s, e) => 
            {
                this._popOver = null;
                this.Control.ResignFirstResponder ();
            };
        }

        private void OnEnded (object sender, EventArgs eventArgs)
        {
			((ICustomTimePickerController)Element)?.TimePickerExited();
        }

        private void UpdateTime ()
        {
            if (this.Element == null || Control == null) 
                return;

            this._picker.Date = new DateTime (1, 1, 1).Add (this.Element.Time).ToNSDate ();
            this.Control.Text = DateTime.Today.Add (this.Element.Time).ToString(this.Element.Format);
        }

        private void UpdateBoundTime ()
        {
            this.Element.Time = this._picker.Date.ToDateTime () - new DateTime (1, 1, 1);
        }

        private void HandleValueChanged (object sender, EventArgs e)
        {
            if (this.Element == null) 
                return;
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose (bool disposing)
        {
            base.Dispose (disposing);
            if (disposing && this._picker != null) {
                this._picker.ValueChanged -= this.HandleValueChanged;
                this._picker.Dispose ();
                this._picker = null;
                if (this._popOver != null) {
                    this._popOver.Dispose ();
                    this._popOver = null;
                }
                if (Control != null) {
                    Control.Started -= this.OnStarted;
                    Control.Ended -= this.OnEnded;
                }

            }
        }
    }
}

