﻿using SnoozApp.Controls;
using SnoozApp.iOS.Renderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LinkButton), typeof(LinkButtonRenderer))]
namespace SnoozApp.iOS.Renderers
{
    public class LinkButtonRenderer : ButtonRenderer
    {
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

            if (Control == null) return;

		    Control.TitleEdgeInsets = new UIKit.UIEdgeInsets(0, 0, 0, 0);
		    Control.HorizontalAlignment = UIKit.UIControlContentHorizontalAlignment.Left;
		    Control.VerticalAlignment = UIKit.UIControlContentVerticalAlignment.Top;
		}
	}
}
