﻿using System;
using SnoozApp.iOS.Renderers;
using SnoozApp.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Windows.Input;

[assembly: ExportRenderer(typeof(LongPressButton), typeof(LongPressButtonRenderer))]

namespace SnoozApp.iOS.Renderers
{
	public class LongPressButtonRenderer : ButtonRenderer
    {
		LongPressButton view;

		public LongPressButtonRenderer()
		{
			this.AddGestureRecognizer(new UILongPressGestureRecognizer((longpress) =>
			{
				if (longpress.State == UIGestureRecognizerState.Began)
				{
					view.SendLongPress();
				}
			}));
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			if (e.NewElement != null)
				view = e.NewElement as LongPressButton;
		}
    }
}

