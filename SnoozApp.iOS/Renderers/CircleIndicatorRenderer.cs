﻿using CoreGraphics;
using SnoozApp.Controls;
using SnoozApp.iOS.Renderers;
using System;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Color = Xamarin.Forms.Color;

[assembly: ExportRenderer(typeof(CircleIndicatorView), typeof(CircleIndicatorRenderer))]

namespace SnoozApp.iOS.Renderers
{
    public class CircleIndicatorRenderer : VisualElementRenderer<CircleIndicatorView>
    {
        public override void Draw(CoreGraphics.CGRect rect)
        {
            base.Draw(rect);

            var currentContext = UIGraphics.GetCurrentContext();
            DrawCircleRenderer(currentContext, rect);
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            this.SetNeedsDisplay();
        }

        protected void DrawCircleRenderer(CGContext currentContext, CGRect rect)
        {
            var centerX = rect.X + (rect.Width / 2);
            var centerY = rect.Y + (rect.Height / 2);
            var startAngle = 0;
            var endAngle = (float)(Math.PI * 2);

			var innerRadius = (float)this.Element.OuterDiameter / 5.28f;
            var innerColor = this.Element.InnerColor.ToCGColor();

            var outerRadius = (float)this.Element.OuterDiameter / 2.4f;
            var outerColor = this.Element.OuterColor.ToCGColor();

            var indicatorRadius = innerRadius + ((outerRadius - innerRadius) / this.Element.NumGradations) * this.Element.CurrentLevel;
            var indicatorColor = this.Element.IndicatorColor.ToCGColor();

            currentContext.SetFillColor(outerColor);
			currentContext.SetStrokeColor(Color.Transparent.ToCGColor());
			currentContext.SetAlpha(0.2f);
            currentContext.AddArc(centerX, centerY, outerRadius, startAngle, endAngle, true);
            currentContext.DrawPath(CGPathDrawingMode.FillStroke);

            if (this.Element.IsChecked)
            {
                currentContext.SetFillColor(indicatorColor);
                currentContext.SetStrokeColor(indicatorColor);
				currentContext.SetAlpha(1.0f);
                currentContext.SetShadow(new CGSize(0, 0), 30,indicatorColor); 
                currentContext.AddArc(centerX, centerY, indicatorRadius, startAngle, endAngle, true);
                currentContext.DrawPath(CGPathDrawingMode.FillStroke);
            }

            currentContext.SetFillColor(innerColor);
            currentContext.SetStrokeColor(innerColor);
			currentContext.SetAlpha(1.0f);
            currentContext.AddArc(centerX, centerY, innerRadius, startAngle, endAngle, true);
            currentContext.DrawPath(CGPathDrawingMode.FillStroke);
        }
    }
}

