﻿using SnoozApp.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(NoHighlightViewCellRenderer))]
namespace SnoozApp.iOS.Renderers
{
    public class NoHighlightViewCellRenderer : ViewCellRenderer
    {
        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            item.Tapped += (sender, e) =>
            {
                UpdateBackground(cell, item);
            };

            return cell;
        }
    }
}