﻿using System;
using System.Threading.Tasks;
using SnoozApp.Core.Interfaces;
using SnoozApp.iOS.Dependencies;
using UserNotifications;

[assembly: Xamarin.Forms.Dependency(typeof(SnoozApp.iOS.AlertPermission.AlertPermissionRequest))]
namespace SnoozApp.iOS.AlertPermission
{
    public class AlertPermissionRequest : IAlertPermissionRequest
    {
        public async Task<bool> RequestPermission()
        {
            UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

            var result = await UNUserNotificationCenter.Current.RequestAuthorizationAsync(UNAuthorizationOptions.Alert | UNAuthorizationOptions.Sound);

            return result.Item1;
        }
    }

}
