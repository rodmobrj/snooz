﻿using Foundation;
using SnoozApp.Interfaces;
using SnoozApp.iOS.Dependencies;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppInfo))]
namespace SnoozApp.iOS.Dependencies
{
    public class AppInfo : IAppInfo
    {
        public string PackageNameOrBundleId => throw new System.NotImplementedException();
        public string Version => NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString();
    }
}