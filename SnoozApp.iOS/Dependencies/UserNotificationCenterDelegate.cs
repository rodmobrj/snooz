﻿using System;
using UserNotifications;

namespace SnoozApp.iOS.Dependencies
{
    public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        public override void WillPresentNotification (UNUserNotificationCenter center, UNNotification notification, 
            Action<UNNotificationPresentationOptions> completionHandler)
            => completionHandler (UNNotificationPresentationOptions.Alert | UNNotificationPresentationOptions.Sound);
    }
}