﻿using Foundation;
using SnoozApp.Core.Interfaces;
using SnoozApp.iOS.Dependencies;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(SettingsAppLauncher))]
namespace SnoozApp.iOS.Dependencies
{
    public class SettingsAppLauncher : ISettingsAppLauncher
    {
        public void LaunchSettingsApp(string appBundleId)
        {
            var url = new NSUrl($"app-settings:{appBundleId}");
            UIApplication.SharedApplication.OpenUrl(url);
        }
    }

}