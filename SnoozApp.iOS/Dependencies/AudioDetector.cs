﻿using System;
using System.Timers;
using Xamarin.Forms;
using AVFoundation;
using Foundation;
using SnoozApp.iOS.Dependencies;

[assembly: Xamarin.Forms.Dependency(typeof(AudioDetector))]
namespace SnoozApp.iOS.Dependencies
{
    /* Resources
     * https://developer.xamarin.com/recipes/ios/media/sound/record_sound/
     * https://github.com/picciano/iOS-Audio-Recoginzer/blob/master/ARAudioRecognizer.m
     */

    public class AudioDetector : IAudioDetector
    {
        public event EventHandler<EventArgs> AudioLevelsUpdated;
		public event EventHandler<EventArgs> InitializationFailed;

        public float AudioLevel { get; private set; }

        AVAudioRecorder recorder;
        NSError error;
        string audioFilePath;
        NSUrl url;
        NSDictionary settings;

        public float runningAverage;

        private bool initialized = false;
		private bool recording = false;

        public AudioDetector()
        {
            
        }

		/* 
		 * This method starts the Initialization procedure. 
		 * Because the RequestRecordPermission uses an asynchronous callback
		 * the finishInitialization method is response for finishing the calibration process
		 */
        public void Initialize()
        {
            if (!this.initialized)  // Only initialize once
            {
                // When we request permissions the popup sends us through OnSleep and OnResume
                // Set a boolean so we know to ignore actions in those methods in App.xaml.cs
				//App.CalibrationPrompted = true; //**This has been moved to CalibrationViewModel
                var audioSession = AVAudioSession.SharedInstance();

				//Check Audio Session permissions
				audioSession.RequestRecordPermission( (granted) => finishInitialization(granted) );

			}
		}

		/*
		 * Finish the calibration process after the user has responded to the permissions prompt.
		 */ 
		void finishInitialization(bool granted)
		{
			var audioSession = AVAudioSession.SharedInstance();
			if (!granted)
			{ 
				audioSession = null;
				this.InitializationFailed?.Invoke(this, new EventArgs());
				return;
			}

			var err = audioSession.SetCategory(AVAudioSessionCategory.PlayAndRecord);

			if (err != null)
			{
				audioSession = null;
				Console.WriteLine("audioSession: {0}", err);
				this.InitializationFailed?.Invoke(this, new EventArgs());
				return;
			}
			err = audioSession.SetActive(true);
			if (err != null)
			{
				audioSession = null;
				Console.WriteLine("audioSession: {0}", err);
				this.InitializationFailed?.Invoke(this, new EventArgs());
				return;
			}

			//Declare string for application temp path and tack on the file extension
			string fileName = string.Format("Myfile{0}.aac", DateTime.Now.ToString("yyyyMMddHHmmss"));
			this.audioFilePath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), fileName);
			Console.WriteLine("Audio File Path: " + audioFilePath);

			url = NSUrl.FromFilename(audioFilePath);
			//set up the NSObject Array of values that will be combined with the keys to make the NSDictionary
			NSObject[] values = new NSObject[]
			{
					NSNumber.FromFloat(44100.0f), //Sample Rate
                    NSNumber.FromInt32((int) AudioToolbox.AudioFormatType.MPEG4AAC), //AVFormat
                    NSNumber.FromInt32(1), //Channels
                    NSNumber.FromInt32(16), //PCMBitDepth
                    NSNumber.FromBoolean(false), //IsBigEndianKey
                    NSNumber.FromBoolean(false) //IsFloatKey
			};

			//Set up the NSObject Array of keys that will be combined with the values to make the NSDictionary
			NSObject[] keys = new NSObject[]
			{
					AVAudioSettings.AVSampleRateKey,
					AVAudioSettings.AVFormatIDKey,
					AVAudioSettings.AVNumberOfChannelsKey,
					AVAudioSettings.AVLinearPCMBitDepthKey,
					AVAudioSettings.AVLinearPCMIsBigEndianKey,
					AVAudioSettings.AVLinearPCMIsFloatKey
			};

			//Set Settings with the Values and Keys to create the NSDictionary
			settings = NSDictionary.FromObjectsAndKeys(values, keys);

			//Set recorder parameters
			recorder = AVAudioRecorder.Create(url, new AudioSettings(settings), out this.error);



			if (this.error != null)
			{
				audioSession = null;
				this.recorder = null;
				this.settings = null;
				Console.WriteLine("audioSession: {0}", this.error);
				this.InitializationFailed?.Invoke(this, new EventArgs());
				return;
			}
			this.initialized = true;
		}

		public void StartRecording()
		{

            if (this.initialized)
			{
                this.recording = true;
				//Set Recorder to Prepare To Record
				recorder.PrepareToRecord();
				recorder.MeteringEnabled = true;

				recorder.Record();
				Device.StartTimer(TimeSpan.FromSeconds(0.25), this.UpdateTask);
			}
		}

        private bool UpdateTask()
        {
            //float alpha = 0.5f;
            this.recorder.UpdateMeters();
			this.runningAverage = this.recorder.AveragePower(0);// (this.recorder.PeakPower(0) * alpha) + (this.runningAverage * (1 - alpha));

			this.AudioLevel = this.runningAverage + 90.0f;

			recorder.Stop();

            System.IO.File.Delete(this.audioFilePath);

            recorder.Record();
            this.AudioLevelsUpdated?.Invoke(this,new EventArgs());

			return this.recording;
        }
    }
}