﻿using Foundation;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Prism;
using Prism.Ioc;
using SnoozApp.Core.Interfaces;
using SnoozApp.iOS.AlertPermission;
using SnoozApp.iOS.Dependencies;
using UIKit;
using UserNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace SnoozApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate, IPlatformInitializer
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {

            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();
            Forms.Init();
            Rg.Plugins.Popup.Popup.Init();
            ButtonCircle.FormsPlugin.iOS.ButtonCircleRenderer.Init();

            LoadApplication(new App(this));
            //RequestPermissionAndConfigureNotificationCenter();
            return base.FinishedLaunching(app, options);
        }

        static void RequestPermissionAndConfigureNotificationCenter()
        {
            //UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

            //TODO: finalize notification permission request
            //UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert | UNAuthorizationOptions.Sound,
            //    (approved, err) =>
            //    {

            //    });
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAlertPermissionRequest, AlertPermissionRequest>();
        }
    }
}
