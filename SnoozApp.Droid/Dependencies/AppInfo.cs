﻿using SnoozApp.Droid.Dependencies;
using SnoozApp.Interfaces;
using Xamarin.Forms;
using Application = Android.App.Application;

[assembly: Dependency(typeof(AppInfo))]
namespace SnoozApp.Droid.Dependencies
{
    public class AppInfo : IAppInfo
    {
        public string PackageNameOrBundleId => Application.Context.PackageName;
        public string Version => Application.Context
                                            .PackageManager
                                            .GetPackageInfo(Application.Context.PackageName, 0)
                                            .VersionName;
    }
}