﻿using System;
using Android.Media;
using SnoozApp.Droid.Dependencies;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(AudioDetector))]
namespace SnoozApp.Droid.Dependencies
{
    public class AudioDetector : IAudioDetector
    {
        private float runningAverage;

        private float _AudioLevel;
        public float AudioLevel { get; private set; }

        public event EventHandler<EventArgs> AudioLevelsUpdated;

        private MediaRecorder recorder = null;

        private bool initialized = false;
        
        public event EventHandler<EventArgs> InitializationFailed;

        public AudioDetector()
        {
        }

        public void Initialize()
        {
            if (!this.initialized)
            {
                var path = PrepareOutputFile();

                this.recorder = new MediaRecorder();
                recorder.SetAudioSource(AudioSource.Mic);
                recorder.SetOutputFormat(OutputFormat.ThreeGpp);
                recorder.SetAudioEncoder(AudioEncoder.AmrNb);
                recorder.SetOutputFile(path);
                recorder.Prepare();
                recorder.Start(); // Recording is now started

                Device.StartTimer(TimeSpan.FromSeconds(0.25), this.UpdateTask);
                this.initialized = true;
            }
            return;
        }

        string PrepareOutputFile()
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return System.IO.Path.Combine(path, "audioSnooz.3gp");
        }

        private bool UpdateTask()
        {
            float alpha = 0.5f;
            this.runningAverage = (this.recorder.MaxAmplitude * alpha) + (this.runningAverage * (1 - alpha));
            this.AudioLevel = (float) (20*Math.Log10(this.runningAverage));
            this.AudioLevelsUpdated?.Invoke(this, new EventArgs());
            return true;
        }

        // The StartRecording method is used on iOS to control when permissions are requested
        // Since Android doesn't need runtime permissions this method isn't used
        public void StartRecording()
        {
            
        }
    }
}