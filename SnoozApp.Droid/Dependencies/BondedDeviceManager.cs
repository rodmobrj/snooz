﻿using System;
using Android.Media;
using SnoozApp.Droid.Dependencies;
using Xamarin.Forms;
using Android.Bluetooth;

[assembly: Xamarin.Forms.Dependency(typeof(BondedDeviceManager))]
namespace SnoozApp.Droid.Dependencies
{
    public class BondedDeviceManager : IBondedDeviceManager
    {
        public BondedDeviceManager()
        {
        }
        public void Initialize()
        {

            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            var pairedDevices = mBluetoothAdapter.BondedDevices;

            Console.WriteLine("---- BLUETOOH BOND SCAN --------------------");

            if (pairedDevices.Count > 0)
            {

                foreach (BluetoothDevice device in pairedDevices)
                {
                    Console.WriteLine("Device Listed Start");
                    Console.WriteLine(device.Name);
                    Console.WriteLine(device.Address);

                    if (device.Name.Contains("Snooz-"))
                    {
                        BluetoothDevice bluetoothDevice = mBluetoothAdapter.GetRemoteDevice(device.Address);
                        var mi = bluetoothDevice.Class.GetMethod("removeBond", null);
                        mi.Invoke(bluetoothDevice, null);
                    }
                    Console.WriteLine("Device Listed End");
                }
            }

        }

    }
}

