﻿using Android.Content;
using Plugin.CurrentActivity; // https://github.com/jamesmontemagno/CurrentActivityPlugin
using SnoozApp.Core.Interfaces;
using SnoozApp.Droid.Dependencies;
using Xamarin.Forms;
using System;
using Application = Android.App.Application;

[assembly: Xamarin.Forms.Dependency(typeof(SettingsAppLauncher))]
namespace SnoozApp.Droid.Dependencies
{
    public class SettingsAppLauncher : ISettingsAppLauncher
    {

        public void LaunchSettingsApp(string appBundleId)
        {
            Intent intent = new Intent();
            intent.SetAction(Android.Provider.Settings.ActionBluetoothSettings);
            intent.SetFlags(ActivityFlags.NewTask);
            intent.SetComponent(new ComponentName("com.android.settings",
               "com.android.settings.bluetooth.BluetoothSettings"));

            Android.App.Application.Context.StartActivity(intent);
        }
    }
}