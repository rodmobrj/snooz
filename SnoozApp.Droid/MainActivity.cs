﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Locations;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using Prism;
using Prism.Ioc;
using SnoozApp.Droid.AlarmManager;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.Android;
using Plugin.Permissions;
using SnoozApp.Core.Interfaces;
using SnoozApp.Droid.Mock;
using Android.Content.Res;
using Android.Util;

namespace SnoozApp.Droid
{
    [Activity(Label = "SNOOZ", 
        Icon = "@mipmap/ic_launcher",
        RoundIcon = "@mipmap/ic_launcher_round",
        Theme = "@style/Theme.Snooz",
        ScreenOrientation = ScreenOrientation.Portrait,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        const int MultipleRequestCode = 0;
        const int AccessFineLocationRequestCode = 1;
        const int RecordAudioRequestCode = 25;

        AlertDialog _alert;
        
        public static bool IsActive { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
           // initFontScale();
           
            base.OnCreate(savedInstanceState);

            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(enableFastRenderer: true);
            Xamarin.Forms.Forms.Init(this, savedInstanceState);
            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);
            ButtonCircle.FormsPlugin.Droid.ButtonCircleRenderer.Init();

            AskForPermissions();
            LoadApplication(new App(new AndroidInitializer()));
        }

        //private void initFontScale()
        //{
        //    Configuration configuration = Resources.Configuration;
        //    configuration.FontScale = (float)1;
        //    DisplayMetrics metrics = new DisplayMetrics();
        //    WindowManager.DefaultDisplay.GetMetrics(metrics);
        //    metrics.ScaledDensity = configuration.FontScale * metrics.Density;

        //  //  Context context = CreateConfigurationContext(configuration);

        //    BaseContext.ApplicationContext.CreateConfigurationContext(configuration);
        //    BaseContext.Resources.DisplayMetrics.SetTo(metrics);

        //   // base.AttachBaseContext(context);

        //   // Context.GetResources().UpdateConfiguration(configuration, metrics);
        //   // BaseContext.Resources.UpdateConfiguration(configuration, metrics);
        //}

        public override Resources Resources
        {
            get
            {
                var config = new Configuration();
                config.SetToDefaults();

                return CreateConfigurationContext(config).Resources;
            }
        }




        protected override void OnStart()
        {
            IsActive = true;
            base.OnStart();
        }

        protected override void OnResume()
        {
            //if (!AlarmActivity.IsActive)
            base.OnResume();
        }

        protected override void OnStop()
        {
            IsActive = false;
            base.OnStop();
        }

        public override void OnBackPressed() 
            => Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed);

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            switch (requestCode)
            {
                case AccessFineLocationRequestCode:
                    HandlePermissionResult(Manifest.Permission.AccessFineLocation, permissions, grantResults);
                    return;
                case RecordAudioRequestCode:
                    HandlePermissionResult(Manifest.Permission.RecordAudio, permissions, grantResults);
                    return;
                case MultipleRequestCode:
                    HandleMultiplePermissionResult(permissions, grantResults);
                    return;
                default:
                    return;
            }
        }

        void AskForPermissions()
        {
            
            var locationPermission = ContextCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation);

            if (locationPermission == Permission.Denied)
                ActivityCompat.RequestPermissions(this, 
                    new [] { Manifest.Permission.AccessFineLocation }, AccessFineLocationRequestCode);
            else
                Console.WriteLine("Course Location & Record Audio Permission Already Granted");

            if (Build.VERSION.SdkInt < BuildVersionCodes.M) 
                return;

            // Create location request
            var locationManager = (LocationManager) GetSystemService(LocationService);
            if (locationManager.IsProviderEnabled(LocationManager.GpsProvider))
            {
                Console.WriteLine("GPS Location is Enabled");
                return;
            }

            Console.WriteLine("GPS Location is Disabled");
            var builder = new AlertDialog.Builder(this);
            builder.SetMessage("In Android 6.0+ the Location Service must be enabled to use Bluetooth LE.");
            builder.SetTitle("Location Not Enabled");
            builder.SetCancelable(false);
            builder.SetPositiveButton("Settings",
                delegate
                {
                    Console.WriteLine("Settings button pushed");
                    var intent =
                        new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                    StartActivity(intent);
                    _alert.Dismiss();
                });
            _alert = builder.Create();
            _alert.Show();
        }

        void HandlePermissionResult(string manifestPermission, string[] permissions, Permission[] grantResults)
        {
            var index = permissions.IndexOf(manifestPermission);
            if (index <= -1 || grantResults[index] != Permission.Denied) 
                return;

            Console.WriteLine($"{manifestPermission} Denied");

            var message = string.Empty;
            var title = string.Empty;
            var requestCode = 0;
            EventHandler<DialogClickEventArgs> alertCancelEvent = 
                (sender, args) => { _alert.Dismiss(); FinishAndRemoveTask(); };

            switch (manifestPermission)
            {
                case Manifest.Permission.AccessFineLocation:
                    message = "Location permission is needed to use Bluetooth LE.";
                    title = "Location Permission Denied";
                    requestCode = AccessFineLocationRequestCode;
                    break;
                case Manifest.Permission.RecordAudio:
                    message = "Record audio permission is needed to calibrate volume.";
                    title = "Record Audio Permission Denied";
                    requestCode = RecordAudioRequestCode;
                    alertCancelEvent = 
                        (sender, args) => { _alert.Dismiss(); };
                    break;
                default:
                    return;
            }

            // Verifies if "Don't Ask Again" was checked, if checked returns false
            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, manifestPermission))
            {
                var builder = new AlertDialog.Builder(this);
                builder.SetMessage(message);
                builder.SetTitle(title);
                builder.SetCancelable(true);
                builder.SetNegativeButton("Cancel", alertCancelEvent);
                builder.SetPositiveButton("OK", delegate
                {
                    ActivityCompat.RequestPermissions(this, 
                                                      new [] { manifestPermission }, 
                                                      requestCode);
                    _alert.Dismiss();
                });
                _alert = builder.Create();
                _alert.Show();
            }
            else
            {
                var builder = new AlertDialog.Builder(this);
                builder.SetMessage(message + " Manually grant it through Settings.");
                builder.SetTitle(title);
                builder.SetCancelable(true);
                builder.SetNegativeButton("Cancel", alertCancelEvent);
                builder.SetPositiveButton("Settings", delegate
                {
                    Console.WriteLine("Settings button pushed");
                    var intent = new Intent(Android.Provider.Settings.ActionApplicationDetailsSettings,
                                            Android.Net.Uri.Parse("package:" + PackageName));
                    intent.AddFlags(ActivityFlags.NewTask);
                    StartActivity(intent);
                    _alert.Dismiss();
                });
                _alert = builder.Create();
                _alert.Show();
            }
        }

        void HandleMultiplePermissionResult(string[] permissions, Permission[] grantResults)
        {
            var accessFineLocationIndex = permissions.IndexOf(Manifest.Permission.AccessFineLocation);
            var recordAudioIndex = permissions.IndexOf(Manifest.Permission.RecordAudio);

            if (accessFineLocationIndex > -1 
                && grantResults[accessFineLocationIndex] == Permission.Denied) 
                HandlePermissionResult(Manifest.Permission.AccessFineLocation, permissions, grantResults);

            if (recordAudioIndex > -1 
                && grantResults[recordAudioIndex] == Permission.Denied)
                HandlePermissionResult(Manifest.Permission.RecordAudio, permissions, grantResults);
        }


    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAlertPermissionRequest, AlertPermissonRequestMock>();
        }
    }
}