﻿using System;
using System.Threading.Tasks;
using SnoozApp.Core.Interfaces;

namespace SnoozApp.Droid.Mock
{
    public class AlertPermissonRequestMock : IAlertPermissionRequest
    {
        public AlertPermissonRequestMock()
        {
        }

        public Task<bool> RequestPermission() => null;
    }
}
