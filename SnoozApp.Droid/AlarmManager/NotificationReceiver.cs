﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Support.V4.App;
using SnoozApp.Modules;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SnoozApp.Droid.AlarmManager
{
    [BroadcastReceiver(Exported = false)]
    public class NotificationReceiver : BroadcastReceiver
    {
        public const int NOTIFICATION_ID = 0;

        //Intent Extras
        public const string ALARM_NAME = "alarm_name";
        public const string MESSAGE = "message";
        public const string SET_NEXT_WEEK_ALARMS = "SET_NEXT_WEEK_ALARMS";
        public const string TITLE = "title";

        public override void OnReceive(Context context, Intent intent)
        {
            Console.WriteLine("NotificationReceiver.OnReceive()");

            var resultIntent = new Intent(context, typeof(MainActivity));
            resultIntent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);

            SendNotification(context, intent.GetStringExtra(TITLE), intent.GetStringExtra(MESSAGE));

            if (intent.GetBooleanExtra(SET_NEXT_WEEK_ALARMS, false))
                Task.Run(() => new AlarmSetterModule().SetAlarmsFromTimers(ScheduleBackup.LoadFromLocalStorage(true).Timers.ToList(), true));
        }

        public static NotificationManager GetNotificationManager(Context context)
        {
            Console.WriteLine("NotificationReceiver.GetNotificationManager()");
            NotificationManager notificationManager = null;

            try
            {
                notificationManager = (NotificationManager)context.GetSystemService(Context.NotificationService);

                if (Build.VERSION.SdkInt < BuildVersionCodes.O)
                    return notificationManager;

                var channelId = context.ApplicationInfo.PackageName;
                var notificationChannel = notificationManager.GetNotificationChannel(channelId);
                if (notificationChannel != null)
                    return notificationManager;

                notificationChannel = new NotificationChannel(channelId, "SNOOZ Alarms",
                    NotificationImportance.High) {Description = "SNOOZ Bedtime Reminder and Wake Up Alarm", LightColor = Color.Green};
                notificationChannel.EnableVibration(true);
                notificationManager.CreateNotificationChannel(notificationChannel);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return notificationManager;
        }

        static void SendNotification(Context context, string title, string message, Android.Net.Uri soundUri = null)
        {
            Console.WriteLine("NotificationReceiver.SendNotification()");
            Notification notification;
            var pendingIntent = PendingIntent.GetService(context, 0,
                new Intent(context, typeof(CancelNotificationService)), PendingIntentFlags.OneShot);
            if (soundUri == null)
                soundUri = RingtoneManager.GetDefaultUri(RingtoneType.Notification);

            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                notification =
                    #pragma warning disable CS0618 // Type or member is obsolete
                    new Notification.Builder(context, context.ApplicationInfo.PackageName)
                        .SetContentTitle(title)
                        .SetContentText(message)
                        .SetSmallIcon(Resource.Drawable.icon_notification)
                        .SetContentIntent(pendingIntent)
                        .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
                        .SetAutoCancel(true)
                        .SetSound(soundUri)
                        #pragma warning restore CS0618 // Type or member is obsolete
                        .Build();
            } 
            else    
            {
                notification =
                    new NotificationCompat.Builder(context) // new NotificationCompat.Builder(context, context.ApplicationInfo.PackageName)
                        .SetContentTitle(title)
                        .SetContentText(message)
                        .SetSmallIcon(Resource.Drawable.icon_notification)
                        .SetContentIntent(pendingIntent)
                        .SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis())
                        .SetAutoCancel(true)
                        .SetSound(soundUri)
                        .SetStyle(new NotificationCompat.BigTextStyle().BigText(message))
                        .Build();
            }

            using (notification)
                using (var manager = GetNotificationManager(context))
                    manager.Notify(NOTIFICATION_ID, notification);
        }
    }
}