﻿using Android.Content;
using Android.Icu.Text;
using Android.Media;
using Android.OS;
using Android.Views;
using Android.Widget;
using Java.Util;
using SnoozApp.Extensions;
using SnoozApp.Modules;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SnoozApp.Droid.AlarmManager
{
    [Android.App.Activity(Label = "SNOOZ",
     MainLauncher = false,
     NoHistory = true,
     Theme = "@style/Theme.Snooz",
     ConfigurationChanges = Android.Content.PM.ConfigChanges.ScreenSize | Android.Content.PM.ConfigChanges.Orientation)]
    public class AlarmActivity : Android.App.Activity, View.IOnClickListener
    {
        MediaPlayer _mediaPlayer;
        TextView _timeTextView;
        TextView _dateTextView;
        TextView _morningTextView;
        PowerManager.WakeLock _wakelock;

        public static bool IsActive { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            IsActive = true;
            _wakelock = ((PowerManager)GetSystemService(PowerService)).NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.AcquireCausesWakeup, nameof(AlarmActivity));
            _wakelock.Acquire();

            if (Intent.GetBooleanExtra(NotificationReceiver.SET_NEXT_WEEK_ALARMS, false))
                Task.Run(() => new AlarmSetterModule().SetAlarmsFromTimers(ScheduleBackup.LoadFromLocalStorage(true).Timers.ToList(), true));

            Window.RequestFeature(WindowFeatures.NoTitle);
            Window.DecorView.SystemUiVisibility = StatusBarVisibility.Hidden;

            _mediaPlayer = new MediaPlayer();
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AlarmActivity);
            FindViewById<Button>(Resource.Id.closeButton).SetOnClickListener(this); 
            var date = DateTimeOffset.Now;

            _timeTextView = FindViewById<TextView>(Resource.Id.timeTextView);
            _dateTextView = FindViewById<TextView>(Resource.Id.dateTextView);
            _morningTextView = FindViewById<TextView>(Resource.Id.morningTextView);

            var dt = new Date(date.ToUnixTimeMilliseconds());
            var sdf = new SimpleDateFormat("hh:mm aa");

            _timeTextView.Text = sdf.Format(dt);
            var text = date.ToString("dddd, MMMM dd");
            _dateTextView.Text = char.ToUpper(text[0]) + text.Substring(1);

            if (date.Hour > 00 && date.Hour < 12)
                _morningTextView.Text = "Good Morning";
            else if (date.Hour >= 12 && date.Hour < 18)
                _morningTextView.Text = "Good Afternoon";
            else
                _morningTextView.Text = "Good Night";

            var alarmResId = GetSoundResourceId(Intent.GetStringExtra(NotificationReceiver.ALARM_NAME));
            if (alarmResId == 0) // No extra
                return;

            OnAlarm(alarmResId);
        }

        int GetSoundResourceId(string alarmName)
        {
            switch (alarmName)
            {
                case ScheduleAlarm.ACOUSTIC_LOOP:
                    return Resource.Raw.Acoustic_Loop;
                case ScheduleAlarm.PHONE_RING:
                    return Resource.Raw.Phone_Ring;
            }

            return 0;
        }

        void OnAlarm(int alarmResId)
        {
            _mediaPlayer = MediaPlayer.Create(ApplicationContext, alarmResId);
            _mediaPlayer.SetVolume(0.7f, 0.7f);
            _mediaPlayer.Start();
            _mediaPlayer.Looping = true;
        }

        public override void OnAttachedToWindow()
        {
            Window.AddFlags(WindowManagerFlags.ShowWhenLocked |
                            WindowManagerFlags.KeepScreenOn |
                            WindowManagerFlags.DismissKeyguard |
                            WindowManagerFlags.TurnScreenOn);
        }
        //TODO Add ProgressIndicator
        public void OnClick(View v)
        {
            IsActive = false;

            if (!MainActivity.IsActive)
                StartActivity(new Intent(ApplicationContext, typeof(MainActivity)));

            _mediaPlayer.Stop();
            _mediaPlayer.Release();
            _mediaPlayer.Dispose();
            _wakelock.Release();
            Finish();
        }
    }
}