﻿using Android.App;
using Android.Content;
using Android.Media;
using Java.Util;
using SnoozApp.Core.Enums;
using SnoozApp.Core.Modules;
using SnoozApp.Droid.AlarmManager;
using SnoozApp.Extensions;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AlarmSetterModule))]
namespace SnoozApp.Droid.AlarmManager
{
    public class AlarmSetterModule : BaseAlarmSetterModule
    {
        readonly Intent _alarmStartIntent = new Intent(Context, typeof(NotificationReceiver));
        readonly Intent _alarmStopIntent = new Intent(Context, typeof(AlarmActivity));
        readonly Android.App.AlarmManager _alarmManager = (Android.App.AlarmManager)Context.GetSystemService(Context.AlarmService);
        static int i = 0;
        static Context Context => Android.App.Application.Context;

        public override void CancelAllAlarms()
        {
            _alarmStartIntent.SetFlags(ActivityFlags.IncludeStoppedPackages);
            for (var i = 1; i <= 7; i++)
            {
                var pendingIntent = PendingIntent.GetBroadcast(Context, i, _alarmStartIntent, PendingIntentFlags.UpdateCurrent);
                _alarmManager.Cancel(pendingIntent);
            }

            _alarmStopIntent.SetFlags(ActivityFlags.IncludeStoppedPackages);
            for (var i = 1; i <= 7; i++)
            {
                var pendingIntent2 = PendingIntent.GetActivity(Context, i * 10, _alarmStopIntent, PendingIntentFlags.UpdateCurrent);
                _alarmManager.Cancel(pendingIntent2);
                
            }
        }

        public override void CancelDaylightSaverAlarms()
        {
            _alarmStartIntent.SetFlags(ActivityFlags.IncludeStoppedPackages);
            for (var i = 1; i <= 9; i++)
            {
                var pendingIntent = PendingIntent.GetBroadcast(Context, i * 100, _alarmStartIntent, PendingIntentFlags.UpdateCurrent);
                _alarmManager.Cancel(pendingIntent);
            }

            for (var i = 1; i <= 9; i++)
            {
                var pendingIntent = PendingIntent.GetBroadcast(Context, i * 1000, _alarmStartIntent, PendingIntentFlags.UpdateCurrent);
                _alarmManager.Cancel(pendingIntent);
            }
        }

        public override async Task PlayAlarmSoundAsync(string alarmName)
        {
            var alarm = 0;
            switch (alarmName)
            {
                case ScheduleAlarm.ACOUSTIC_LOOP:
                    alarm = Resource.Raw.Acoustic_Loop;
                    break;
                case ScheduleAlarm.PHONE_RING:
                    alarm = Resource.Raw.Phone_Ring;
                    break;
            }

            using (var mediaPlayer = MediaPlayer.Create(Android.App.Application.Context, alarm))
            {
                mediaPlayer.SetVolume(0.7f, 0.7f);
                mediaPlayer.Start();
                await Task.Delay(5000);
                mediaPlayer.Stop();
                mediaPlayer.Release();
            }
        }
        
        public override void SetBedTimeReminderAlarm(WeeklySchedule schedule, int minutesBefore, bool setNextWeekAlarms = false, bool isNextWeek = false)
        {
            _alarmStartIntent.PutExtra(NotificationReceiver.TITLE, schedule.Title);
            _alarmStartIntent.PutExtra(NotificationReceiver.MESSAGE, schedule.Message);
            _alarmStartIntent.PutExtra(NotificationReceiver.SET_NEXT_WEEK_ALARMS, setNextWeekAlarms);
            _alarmStartIntent.SetFlags(ActivityFlags.IncludeStoppedPackages);

            var startPendingIntent = PendingIntent.GetBroadcast(Context, schedule.Id, _alarmStartIntent, PendingIntentFlags.UpdateCurrent);
            var startTriggerTime = GetBedTimeReminderTimeInMillis(schedule, minutesBefore, isNextWeek);

            _alarmManager.Cancel(startPendingIntent);
            _alarmManager.SetAlarmClock(new Android.App.AlarmManager.AlarmClockInfo(startTriggerTime, null), startPendingIntent);
        }

        public override void SetWakeUpAlarm(WeeklySchedule schedule, string alarmName, bool setNextWeekAlarms = false, bool isNextWeek = false)
        {
            _alarmStopIntent.AddFlags(ActivityFlags.NewTask);
            _alarmStopIntent.AddFlags(ActivityFlags.MultipleTask);
            _alarmStopIntent.PutExtra(NotificationReceiver.ALARM_NAME, alarmName);
            _alarmStopIntent.PutExtra(NotificationReceiver.SET_NEXT_WEEK_ALARMS, setNextWeekAlarms);
            _alarmStopIntent.SetFlags(ActivityFlags.IncludeStoppedPackages);

            var stopPendingIntent = PendingIntent.GetActivity(Context, schedule.Id * 10, _alarmStopIntent, PendingIntentFlags.UpdateCurrent);
            var stopTriggerTime = GetWakeUpAlarmTimeInMillis(schedule, isNextWeek);

            _alarmManager.Cancel(stopPendingIntent);
            _alarmManager.SetAlarmClock(new Android.App.AlarmManager.AlarmClockInfo(stopTriggerTime, null), stopPendingIntent);
        }

        static long GetBedTimeReminderTimeInMillis(WeeklySchedule schedule, int minutesBefore = 0, bool isNextWeek = false)
        {
            var calendar = Calendar.Instance;
            var todayDayOfWeek = calendar.Get(CalendarField.DayOfWeek);
            var todayHourOfDay = calendar.Get(CalendarField.HourOfDay);
            var todayMinute = calendar.Get(CalendarField.Minute);
            var year = calendar.Get(CalendarField.Year);
            var baseWeekOfYear = calendar.Get(CalendarField.WeekOfYear);
            var dayOfWeek = schedule.Week;
            var hourOfDay = schedule.HourStart;
            var minute = schedule.MinuteStart;

            var isEarlierWeekDay = schedule.Week < todayDayOfWeek;
            var isSameWeekDay = schedule.Week == todayDayOfWeek;
            var isEarlierHour = hourOfDay < todayHourOfDay;
            var isSameHour = hourOfDay == todayHourOfDay;
            var isSameOrEarlierMinute = minute <= todayMinute;
            // If it's yesterday or today but hours or minutes past, it's an alarm for next week
            if (isEarlierWeekDay
                || isSameWeekDay && isEarlierHour
                || isSameWeekDay && isSameHour && isSameOrEarlierMinute)
                isNextWeek = true;
            
            var weekOfYear = isNextWeek ? ++baseWeekOfYear : baseWeekOfYear;

            calendar.SetWeekDate(year, weekOfYear, dayOfWeek);
            calendar.Set(CalendarField.HourOfDay, hourOfDay);
            calendar.Set(CalendarField.Minute, minute);
            calendar.Set(CalendarField.Second, 0);
            calendar.Add(CalendarField.Minute, -minutesBefore);

            Debug.WriteLine($"GetBedTimeReminderTimeInMillis(): {calendar.Time}");
            return calendar.TimeInMillis;
        }

        static long GetWakeUpAlarmTimeInMillis(WeeklySchedule schedule, bool isNextWeek = false)
        {
            var calendar = Calendar.Instance;
            var todayDayOfWeek = calendar.Get(CalendarField.DayOfWeek);
            var todayHourOfDay = calendar.Get(CalendarField.HourOfDay);
            var year = calendar.Get(CalendarField.Year);
            var baseWeekOfYear = calendar.Get(CalendarField.WeekOfYear);
            var todayMinute = calendar.Get(CalendarField.Minute);
            var hourOfDay = schedule.HourStop;
            var minute = schedule.MinuteStop;

            var isEarlierWeekDay = schedule.Week < todayDayOfWeek;
            var isSameWeekDay = schedule.Week == todayDayOfWeek;
            var isEarlierHour = hourOfDay < todayHourOfDay;
            var isSameHour = hourOfDay == todayHourOfDay;
            var isSameOrEarlierMinute = minute <= todayMinute;
            // If it's yesterday or today but hours or minutes past, it's an alarm for next week
            if (isEarlierWeekDay
                || isSameWeekDay && isEarlierHour
                || isSameWeekDay && isSameHour && isSameOrEarlierMinute)
                isNextWeek = true;

            var weekOfYear = isNextWeek ? ++baseWeekOfYear : baseWeekOfYear;
            var dayOfWeekHasStart = schedule.HourStart > schedule.HourStop 
                                       || (schedule.HourStart == schedule.HourStop && schedule.MinuteStart > schedule.MinuteStop) 
                                       ? schedule.Week + 1 : schedule.Week;
            var dayOfWeek = schedule.StartEnabled ? dayOfWeekHasStart : schedule.Week;

            calendar.SetWeekDate(year, weekOfYear, dayOfWeek);
            calendar.Set(CalendarField.HourOfDay, hourOfDay);
            calendar.Set(CalendarField.Minute, minute);
            calendar.Set(CalendarField.Second, 0);

            Debug.WriteLine($"GetWakeUpAlarmTimeInMillis(): {calendar.Time}");
            return calendar.TimeInMillis;
        }

        public override void CreateDaylightSavingAlarm(DateTime dateAlarm,int id, string alarmName, bool isStartDate)
        {
            _alarmStartIntent.PutExtra(NotificationReceiver.TITLE, "Daylight Savings");
            _alarmStartIntent.PutExtra(NotificationReceiver.MESSAGE, isStartDate ? "Daylight saving time begins tomorrow." : "Daylight saving time ends tomorrow.");
            _alarmStartIntent.AddFlags(ActivityFlags.NewTask);
            _alarmStartIntent.AddFlags(ActivityFlags.MultipleTask);
            _alarmStartIntent.PutExtra(NotificationReceiver.ALARM_NAME, alarmName);
            _alarmStartIntent.SetFlags(ActivityFlags.IncludeStoppedPackages);

            var startPendingIntent = PendingIntent.GetBroadcast(Context, id, _alarmStartIntent, PendingIntentFlags.UpdateCurrent);

            var calendar = Calendar.Instance;
            calendar.Set(CalendarField.Year, dateAlarm.Year);
            calendar.Set(CalendarField.Month, dateAlarm.Month);
            calendar.Set(CalendarField.DayOfYear, dateAlarm.DayOfYear);
            calendar.Set(CalendarField.HourOfDay, 19);
            calendar.Set(CalendarField.Minute, 0);
            calendar.Set(CalendarField.Second, 0);
            var triggertimer = calendar.TimeInMillis;

            _alarmManager.Cancel(startPendingIntent);

            Debug.WriteLine($"{dateAlarm.Year} - {dateAlarm.Month} - {dateAlarm.Day} - {dateAlarm.DayOfYear}");
            _alarmManager.SetAlarmClock(new Android.App.AlarmManager.AlarmClockInfo(triggertimer, null), startPendingIntent);
        }
    }
}