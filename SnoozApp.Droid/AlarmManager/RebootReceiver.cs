﻿using Android.App;
using Android.Content;
using SnoozApp.Modules;
using System.Linq;
using Android;

namespace SnoozApp.Droid.AlarmManager
{
    [BroadcastReceiver(Enabled = true, DirectBootAware = true, Exported = true, Permission = Manifest.Permission.ReceiveBootCompleted)]
    [IntentFilter(new [] { Intent.ActionLockedBootCompleted }, Priority = (int)IntentFilterPriority.HighPriority)]
    public class RebootReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (!(ScheduleBackup.LoadFromLocalStorage(getLastConnectedSnoozSchedule: true) is ScheduleBackup schedule) 
                || schedule.Timers == null 
                || !schedule.Timers.Any(t => t.Enabled))
            {
                Android.Util.Log.Debug("RebootReceiver", "OnReceive(): No timers.");
                return;
            }

            Android.Util.Log.Debug("RebootReceiver", "OnReceive(): Setting alarms...");
            new AlarmSetterModule().SetAlarmsFromTimers(schedule.Timers.ToList());
        }
    }
}