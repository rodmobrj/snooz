﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;

namespace SnoozApp.Droid.AlarmManager
{
    [Service(Exported = false)]
    public class CancelNotificationService : Service
    {
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            NotificationReceiver.GetNotificationManager(this)
                                .Cancel(NotificationReceiver.NOTIFICATION_ID);
            StopSelf();

            return StartCommandResult.NotSticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnDestroy()
        {
            System.Diagnostics.Debug.WriteLine("CancelNotificationService.OnDestroy called");
            base.OnDestroy();
        }
    }
}