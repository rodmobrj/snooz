﻿using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using SnoozApp.Controls;
using System;
using Xamarin.Forms.Platform.Android;

namespace SnoozApp.Droid
{
    public class CircleIndicator : View
    {
        readonly float _density = 1.0f;
        bool _isDisposing;

        public CircleIndicatorView CircleIndicatorView { get; set; }

        public CircleIndicator(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}

        public CircleIndicator(Context context, float density) : base(context)
        {
            this._density = density;
        }
        public CircleIndicator(Context context, IAttributeSet attrs) : base(context, attrs) {}
        public CircleIndicator(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {}
        public CircleIndicator(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes) {}

        public void Update()
        {
            try
            {
                if (!_isDisposing)
                    PostInvalidate();
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine(e);
            }
        }

        protected override void Dispose(bool disposing)
        {
            _isDisposing = disposing;
            base.Dispose(disposing);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            this.DrawCircleIndicator(canvas);
        }

        protected void DrawCircleIndicator(Canvas canvas)
        {
            var cx = this.GetX() + (this.Width / 2f);
            var cy = this.GetY() + (this.Height / 2f);

            var innerRadius = (float)this.CircleIndicatorView.OuterDiameter/5.28f * this._density;
            var innerPaint = new Paint();
            innerPaint.SetStyle(Paint.Style.Fill);
            innerPaint.AntiAlias = true;
            innerPaint.Color = this.CircleIndicatorView.InnerColor.ToAndroid();

            var outerRadius = (float)this.CircleIndicatorView.OuterDiameter/2.4f * this._density;
            var outerPaint = new Paint();
            outerPaint.SetStyle(Paint.Style.Fill);
            outerPaint.AntiAlias = true;
            outerPaint.Color = this.CircleIndicatorView.OuterColor.ToAndroid();
            outerPaint.Alpha = 51; // Value is between 0 and 255. 51 is equivalent to 0.2
            var indicatorRadius = innerRadius + ((outerRadius - innerRadius)/this.CircleIndicatorView.NumGradations)*this.CircleIndicatorView.CurrentLevel;
            var indicatorPaint = new Paint();
            indicatorPaint.SetStyle(Paint.Style.Fill);
            indicatorPaint.AntiAlias = true;
            indicatorPaint.Color = this.CircleIndicatorView.IndicatorColor.ToAndroid();

            canvas.DrawCircle(cx, cy, outerRadius, outerPaint);

            if (this.CircleIndicatorView.IsChecked && this.CircleIndicatorView.CurrentLevel != 0)
                canvas.DrawCircle(cx, cy, indicatorRadius, indicatorPaint);

            canvas.DrawCircle(cx, cy, innerRadius, innerPaint);
        }
    }
}