﻿using SnoozApp.Droid.Renderers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;

[assembly: ExportRenderer(typeof(Picker), typeof(TransparentPickerRenderer))]
namespace SnoozApp.Droid.Renderers
{
    public class TransparentPickerRenderer : PickerRenderer
	{
		public TransparentPickerRenderer(Context context) : base(context)
		{
		}
		protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement == null)
			{
				this.Control.Background = null;

				var layoutParams = new MarginLayoutParams(this.Control.LayoutParameters);
				layoutParams.SetMargins(0, 0, 0, 0);
				this.LayoutParameters = layoutParams;
				this.Control.LayoutParameters = layoutParams;
				this.Control.SetPadding(0, 0, 0, 0);
				SetPadding(0, 0, 0, 0);
			}
		}
	}
}
