﻿using System;
using System.ComponentModel;
using Android.Graphics;
using Android.Views;
using SnoozApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Color = Android.Graphics.Color;
using TimePicker = Xamarin.Forms.TimePicker;
using Android.Content;

[assembly: ExportRenderer(typeof(TimePicker), typeof(CustomTimePickerRenderer))]
namespace SnoozApp.Droid.Renderers
{
    public class CustomTimePickerRenderer : TimePickerRenderer
    {

        public CustomTimePickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Focusable = false;
                Control.Background.Mutate().SetColorFilter(this.Element?.BackgroundColor.ToAndroid() ?? Color.White, PorterDuff.Mode.SrcAtop);
                Control.Gravity = GravityFlags.Center;
                Control.Text = DateTime.Today.Add(this.Element.Time).ToString(this.Element.Format);
				Control.TextSize = (float) Element.FontSize;
				Control.SetBackgroundColor(Color.Transparent);
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == TimePicker.TimeProperty.PropertyName ||
                e.PropertyName == TimePicker.FormatProperty.PropertyName)
                Control.Text = DateTime.Today.Add(this.Element.Time).ToString(Element.Format);
            if (e.PropertyName.Equals(TimePicker.FontSizeProperty))
                Control.TextSize = (float) Element.FontSize;
        }
    }
}