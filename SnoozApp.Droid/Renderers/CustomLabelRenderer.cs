﻿using Android.Graphics;
using SnoozApp.Droid.Renderers;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Label), typeof(CustomLabelRenderer))]
namespace SnoozApp.Droid.Renderers
{
    public class CustomLabelRenderer : LabelRenderer
    {
		public CustomLabelRenderer(Context context) : base(context)
		{
		}

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
			this.Control.Typeface = Typeface.CreateFromAsset(this.Context.Assets, e.NewElement.FontFamily.Replace('-', '_') + ".otf");
		}
    }
}