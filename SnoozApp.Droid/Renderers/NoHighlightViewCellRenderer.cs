﻿using Android.Content;
using Android.Views;
using SnoozApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ListView = Android.Widget.ListView;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(ViewCell), typeof(NoHighlightViewCellRenderer))]
namespace SnoozApp.Droid.Renderers
{
    public class NoHighlightViewCellRenderer : ViewCellRenderer
    {
        protected override View GetCellCore(Cell item, View convertView, ViewGroup parent, Context context)
        {
            if (parent is ListView listView)
            {
                listView.SetSelector(Android.Resource.Color.Transparent);
                listView.CacheColorHint = Color.Transparent.ToAndroid();
            }

            return base.GetCellCore(item, convertView, parent, context);
        }
    }
}