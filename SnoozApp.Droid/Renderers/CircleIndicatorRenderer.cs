﻿using SnoozApp.Controls;
using SnoozApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Content;

[assembly: ExportRenderer(typeof(CircleIndicatorView), typeof(CircleIndicatorRenderer))]
namespace SnoozApp.Droid.Renderers
{

    public class CircleIndicatorRenderer : ViewRenderer<CircleIndicatorView, CircleIndicator>
    {
        public CircleIndicatorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<CircleIndicatorView> e)
        {

            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
                return;

            this.SetNativeControl(new CircleIndicator(this.Context, this.Resources.DisplayMetrics.Density)
                                  {
                                      CircleIndicatorView = this.Element
                                  });

            this.Element.PropertyChanged += this.Element_PropertyChanged;
        }

        private void Element_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            if (this.Control != null)
            {
                this.Control.Update();
            }
        }
    }
}