﻿using Android.Content;
using SnoozApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

// https://forums.xamarin.com/discussion/147701/picker-shows-multiple-times
// https://github.com/xamarin/Xamarin.Forms/issues/4187

[assembly: ExportRenderer(typeof(Picker), typeof(HackFix20181030PickerRenderer))]
namespace SnoozApp.Droid.Renderers
{
    public class HackFix20181030PickerRenderer : Xamarin.Forms.Platform.Android.AppCompat.PickerRenderer
    {
        public HackFix20181030PickerRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Focusable = false;
            }
        }
    }
}