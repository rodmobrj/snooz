﻿using Android.Content;
using Android.Views;
using SnoozApp.Controls;
using SnoozApp.Droid.Renderers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LinkButton), typeof(LinkButtonRenderer))]
namespace SnoozApp.Droid.Renderers
{
    public class LinkButtonRenderer : ButtonRenderer
    {
		public LinkButtonRenderer(Context context) : base(context)
		{
		}

		public new LinkButton Element
		{
			get
			{
				return (LinkButton)base.Element;
			}
		}
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
		}
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			this.Control.SetPadding(0, 0, 0, 0);
			this.Control.SetAllCaps(this.Element.IsAllCaps);
			this.Control.Gravity = GravityFlags.CenterVertical 
								| (this.Element.TextAlignment == Xamarin.Forms.TextAlignment.Center
									? GravityFlags.AxisSpecified
									: this.Element.TextAlignment == Xamarin.Forms.TextAlignment.End
										? GravityFlags.Right
										: GravityFlags.Left);
		}
	}
}
