﻿using System;
using SnoozApp.Droid.Renderers;
using SnoozApp.Controls;
using Xamarin.Forms;
using System.Windows.Input;
using AndroidX;
//using Android.Support.V7.App;
using Xamarin.Forms.Platform.Android;
using Android.Content;

[assembly: ExportRenderer(typeof(LongPressButton), typeof(LongPressButtonRenderer))]

namespace SnoozApp.Droid.Renderers
{
	public class LongPressButtonRenderer : ButtonRenderer
    {
        public LongPressButtonRenderer(Context context) : base(context)
        {
        }

        LongPressButton view;
        
		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);
			if (e.NewElement != null)
            {
                var nativeButton = Control;
                nativeButton.LongClick += delegate {
                    this.view = e.NewElement as LongPressButton;
                    this.view.SendLongPress();
                };
            }
        }
    }
}

