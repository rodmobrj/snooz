﻿using Android.Content;
using SnoozApp.Controls;
using SnoozApp.Droid.Renderers;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Button), typeof(LowercaseButtonRenderer))]
namespace SnoozApp.Droid.Renderers
{
	public class LowercaseButtonRenderer : ButtonRenderer
	{

		public LowercaseButtonRenderer(Context context) : base(context)
		{
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			this.Control.SetAllCaps(false);
		}
	}
}

