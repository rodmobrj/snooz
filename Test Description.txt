Baby Monitor calibration page. To access this page navigate to the baby monitor in the bottom tray. If you have not calibrated the baby monitor before the baby monitor tray will display a big button with the text “Calibrate” that will take you to the calibration page. If you have already calibrated the baby monitor tray will display the current calibrated decibel level. Clicking the baby face icon will take you to the calibration page. 
o	The page is currently in a diagnostic configuration. Click the “Start Calibration” button to start the calibration sequence. The text below the SNOOZ logo will display a status as the app steps through calibrations. The values display at the top will be modified as part of the calibration process.
	Ambient: The decibel level sampled when the device is off.
	40%: The decibel level sampled when the fan is on at 40% (fan level 4).
	70%: The decibel level sampled when the fan is on at 70% (fan level 7).
	Calibrated: The difference between the decibel level sampled at 40% and the expected decibel level 3 inches away from a SNOOZ at 40% (68.5 dBA).
	Adjusted: The current measured decibel level adjusted by the calibrated offset. This is also the value that is displayed in the Baby Monitor tray on the main screen.
